#!/bin/sh
set -e

# docker build -t fun .
# docker run --rm -it -p 8000:8000 fun


docker-compose up -d --build      # to test only web use "docker-compose up web"

docker-compose logs -f
