

function validateInputMinLen(n, inputElem, errorElem){
  var isValid = inputElem.value.length >= n
  if (isValid) {
    hideInputError(errorElem);
  }
  else {
    showInputError(errorElem);
  }
  return isValid;
}


function validateInputMaxLen(n, inputElem, errorElem){
  let isValid = inputElem.value.length <= n
  if (isValid) {
    hideInputError(errorElem);
  }
  else {
    showInputError(errorElem);
  }
  return isValid;
}


function checkInputExists(url, inputElem, errorElem, submitElem){
  let destinationURL = url + '?p=' + inputElem.value;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 303) {
      showInputError(errorElem);
      markInputAsWrongAndDisableSubmit(inputElem,submitElem);
    }
    else {
      hideInputError(errorElem);
    }
  };
  xhttp.open("GET", destinationURL, true);
  xhttp.send();
}



function showInputError(errorE){
  errorE.classList.remove('display-none');
  errorE.classList.add('display-block');
}


function hideInputError(errorE){
  errorE.classList.remove('display-block');
  errorE.classList.add('display-none');
}


function markInputAsWrongAndDisableSubmit(inputElem, submitElem){
  inputElem.classList.add('input-error');
  inputElem.classList.remove('input-ok');
  submitElem.classList.add('disabled-btn');
  submitElem.disabled = true;
}


function markInputAsValidAndEnableSubmit(inputElem, submitElem){
  inputElem.classList.remove('input-error');
  inputElem.classList.add('input-ok');
  submitElem.classList.remove('disabled-btn');
  submitElem.disabled = false;
}


// -----------------------------------------------------------------------------
