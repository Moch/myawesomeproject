{-# LANGUAGE RecordWildCards #-}


module Base.Error where

import Data.Either.Combinators (mapLeft)
import Data.Text as T (Text, unpack, length, filter)

import Base.Lang

--------------------------------------------------------------------------------

data Error a = Error
  { expected :: a
  , actual   :: a
  }



data BaseError =
    ExstErr (Error Existence)
  | FileErr FileError
  | TextErr TextError
  | UnknErr (Lang -> String)

instance Translate BaseError where
  translate = translateBaseError

translateBaseError :: Lang -> BaseError -> String
translateBaseError lang err =
  case err of
    ExstErr x -> translate lang $ actual x
    FileErr _ -> "AAAAA"
    TextErr x -> translate lang x
    UnknErr f -> f lang

--------------------------------------------------------------------------------

shouldExist :: BaseError
shouldExist = ExstErr shouldExist_

shouldExist_ :: Error Existence
shouldExist_ = Error { expected = AlreadyExist , actual = DoesNotExist}

shouldNotExist :: BaseError
shouldNotExist = ExstErr shouldNotExist_

shouldNotExist_ :: Error Existence
shouldNotExist_ = Error { expected = DoesNotExist , actual = AlreadyExist}


data Existence =
    AlreadyExist
  | DoesNotExist

instance Translate Existence where
  translate = translateExistence

translateExistence :: Lang -> Existence -> String
translateExistence lang e =
  case e of
    AlreadyExist  -> case lang of
      ES -> "ya existe"
      _  -> "already exists"
    DoesNotExist  -> case lang of
      ES -> "no existe"
      _  -> "does not exist"


--------------------------------------------------------------------------------

data FileRestrictions = FileRestrictions
  { minSize :: Integer
  , maxSize :: Integer
  }


data FileError
  = EmptyFile
  | TooLight (Error Integer)    -- bytes
  | TooHeavy (Error Integer)    -- bytes
  | TooSmall (Error (Int,Int))  -- pixels
  | TooLarge (Error (Int,Int))  -- pixels
  | BadFile  (Lang -> String)

--------------------------------------------------------------------------------


validateText :: TextRestrictions -> Text -> Either BaseError Text
validateText r = mapLeft TextErr . validateText_ r


validateText_ :: TextRestrictions -> Text -> Either TextError Text
validateText_ TextRestrictions{..} t
    | minLen > l = Left $ TooShort $ Error minLen l
    | maxLen < l = Left $ TooLong  $ Error maxLen l
    | not $ null $ badList = Left $ HasInvalidChars badList
    | otherwise  = Right t
  where
    l = T.length t
    badList = unpack $ T.filter (not . validChars) t




data TextRestrictions = TextRestrictions
  { minLen     :: Int
  , maxLen     :: Int
  , validChars :: (Char -> Bool)
  }


data TextError =
    EmptyText
  | TooShort (Error Int)
  | TooLong  (Error Int)
  | HasInvalidChars [Char]
  | MalformedSyntax (Lang -> String)

instance Translate TextError where
  translate = translateTextError


translateTextError :: Lang -> TextError -> String
translateTextError lang err =
  case err of
    MalformedSyntax f -> f lang
    EmptyText -> case lang of
      ES -> " No puede ser vacío"
      EN -> " It cannot be empty"
      _  -> " Ĝi ne povas esti malplena"
    TooShort e -> case lang of
      ES -> concat
        [ "Longitud insuficiente - "
        , show $ actual e
        , " símbolos - "
        , "el mínimo requerido es "
        , show $ expected e
        ]
      EN -> concat
        [ "It is too short - "
        , show $ actual e
        , " symbols - "
        , "the minimum required is "
        , show $ expected e
        ]
      _  -> concat
        [ "Ne sufiĉa longo - "
        , show $ actual e
        , " simboloj - "
        , "la minimuma postulita estas "
        , show $ expected e
        ]
    TooLong e -> case lang of
      ES -> concat
        [ "Longitud excesiva - "
        , show $ actual e
        , " símbolos - "
        , "el máximo permitido es "
        , show $ expected e
        ]
      EN -> concat
        [ "It is too long - "
        , show $ actual e
        , " symbols - "
        , "the maximum allowed is "
        , show $ expected e
        ]
      _  -> concat
        [ "Troa longa - "
        , show $ actual e
        , " gravuloj - "
        , "la maksimuma permesita estas "
        , show $ expected e
        ]
    HasInvalidChars cList -> case lang of
      ES -> concat
        [ "Contiene símbolos inválidos:  "
        , cList
        ]
      EN -> concat
        [ "It has invalid symbols:  "
        , show cList
        ]
      _  -> concat
        [ "Havas nevalidajn simbolojn:  "
        , cList
        ]


printErrorTooShort :: (Lang -> String) -> Int -> Lang -> String
printErrorTooShort f n lang =
  case lang of
    ES -> (f lang) ++ " no alcanza la longitud requerida (el mínimo es " ++ show n ++ " caracteres)"
    _  -> (f lang) ++ " is too short (minimum is " ++ show n ++ " characters)"


printErrorTooLong :: (Lang -> String) -> Int -> Lang -> String
printErrorTooLong f n lang =
  case lang of
    ES -> (f lang) ++ " rebasa la longitud permitida (el máximo es " ++ show n ++ " caracteres)"
    _  -> (f lang) ++ " is too long (maximum is " ++ show n ++ " characters)"


--------------------------------------------------------------------------------
