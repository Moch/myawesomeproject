{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Views.ChoosePackage.Html where

import           Data.Text
import           Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Head
import Base.Html.Page
import Base.Lang
import Base.UI.Layout.Template.Html
import Job.API.Routes
import Job.Logic.All
import Job.UI.Elems.JobType.Css
import Job.UI.Views.ChoosePackage.Svg

--------------------------------------------------------------------------------

choosePackagePageView :: PageParams -> Html
choosePackagePageView params =
    H.toHtml choosePackagePage
  where
    choosePackagePage = Page
      { pageParams = params
      , htmlHead   = choosePackageHead
      , htmlBody   = mainTemplate choosePackageBody
      }


choosePackageHead :: Head
choosePackageHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""


choosePackageBody :: PageParams -> Html
choosePackageBody p =
  H.div ! A.class_ "choose-package-body" $ do
    pageHeading (ppLang p)
    techWrapper (ppLang p)
    ourPackages (ppLang p)
    ourPerks    (ppLang p)
    buttons     (ppLang p)


--------------------------------------------------------------------------------


pageHeading :: Lang -> Html
pageHeading lang =
    H.div (H.h1 heading) ! A.class_ "flex-center"
  where
    heading = case lang of
      ES -> "¿Buscas Ingenieros de Software?"
      _  -> "Need Software Engineers?"


--------------------------------------------------------------------------------





techWrapper :: Lang -> Html
techWrapper lang =
    H.div ! A.class_ "resp-w choose-package-tech-wrapper" $ do
      H.div (H.h2 heading) ! A.class_ "flex-center"
      H.div ! A.class_ "choose-package-techs" $ do
        functionalLanguages lang
        webTechnologies     lang
        fintechAndBigData   lang
        bestMethodologies   lang
        latestTrends        lang
  where
    heading = case lang of
      ES -> "Tenemos lo que quieres"
      _  -> "We've got you covered"



techTable :: Html -> [ (Html,Html) ] -> Html
techTable heading contents =
    H.div ! A.class_ "choose-package-tech-table-wrapper" $ do
      H.h3 heading
      H.table ! A.class_ "choose-package-tech-table" $
        mapM_ f contents
  where
    f (x,y) =
      H.tr $ do
        H.td x ! A.class_ "choose-package-tech-left-cell"
        H.td y


image :: Text -> Html
image fileName =
  H.img
    ! A.class_ "choose-package-tech-table-logo"
    ! A.src    (H.toValue $ "/static/tech-logos/" <> fileName)



functionalLanguages :: Lang -> Html
functionalLanguages lang =
    techTable
      heading
      [ (,) (image "lang_haskell.svg") "Haskell"
      , (,) (image "lang_scala.png"  ) "Scala"
      , (,) (image "lang_erlang.png" ) "Erlang"
      , (,) (image "lang_elixir.jpeg") "Elixir"
      , (,) (image "lang_clojure.svg") "Clojure"
      , (,) (image "lang_ocaml.png"  ) "OCaml"
      , (,) (image "lang_lisp.png"   ) "Lisp"
      , (,) (image "lang_fsharp.png" ) "F#"
      ]
  where
    heading = case lang of
      ES -> "Lenguajes Funcionales"
      _  -> "Functional Languages"


webTechnologies :: Lang -> Html
webTechnologies lang =
    techTable
      heading
      [ (,) (image "web_html5.png"  ) "Html 5"
      , (,) (image "web_css3.png"   ) "Css 3"
      , (,) (image "web_js.jpg"     ) "JavaScript"
      , (,) (image "web_ps.svg"     ) "PureScript"
      , (,) (image "web_elm.svg"    ) "Elm"
      , (,) (image "web_react.svg"  ) "React/Redux"
      , (,) (image "web_angular.svg") "Angular"
      , (,) (image "web_vue.svg"    ) "Vue.js"
      ]
  where
    heading = case lang of
      ES -> "Tecnologías Web"
      _  -> "Web Technologies"


fintechAndBigData :: Lang -> Html
fintechAndBigData lang =
    techTable
      heading
      [ (,) (image "fintech_python.png"    ) "Python"
      , (,) (image "fintech_R.png"         ) "R language"
      , (,) (image "fintech_matlab.png"    ) "Matlab"
      , (,) (image "fintech_blockchain.jpg") "Blockchain"
      , (,) (image "fintech_spark.png"     ) "Apache Spark"
      , (,) "DLT" "Distributed Ledger Technologies"
      , (,) "SMO" "Smart Contracts and Oracles"
      , (,) "DBM" ("Database Management " >> H.br >> "(Sql / NoSql)")
      ]
  where
    heading = case lang of
      ES -> "FinTech & BigData"
      _  -> "FinTech & BigData"


bestMethodologies :: Lang -> Html
bestMethodologies lang =
    techTable
      heading
      [ (,) "TDD"   "Test Driven Development"
      , (,) "DDD"   "Domain Driven Design"
      , (,) "ADE"   "Agile Development Environments"
      , (,) "DSL"   "Domain Specific Languages"
      , (,) "ADP"   "Asynchronous, Distributed and Parallel systems"
      , (,) "SPA"   "Single Page Applications"
      , (,) "REST"  "RESTful APIs and Architectures"
      , (,) "CI/CD" "Continuous Integration and Continuous Delivery"
      ]
  where
    heading = case lang of
      ES -> "Las Mejores Metodologías"
      _  -> "Best Methodologies"


latestTrends :: Lang -> Html
latestTrends lang =
    techTable
      heading
      [ (,) "AI"  "Artificial Intelligence"
      , (,) "ML"  "Machine Learning"
      , (,) "DL"  "Deep Learning"
      , (,) "CV"  "Computer Vision"
      , (,) "NLP" "Natural Language Processing"
      , (,) "IoT" "Internet of Things"
      , (,) "VR"  "Virtual Reality"
      , (,) "UX"  "User Experience"
      ]
  where
    heading = case lang of
      ES -> "Últimas Tendencias"
      _  -> "Latest Trends"





--------------------------------------------------------------------------------





ourPackages :: Lang -> Html
ourPackages lang =
  H.div ! A.class_ "choose-package-offers-section" $
    H.div ! A.class_ "resp-w" $ do
      H.div (H.h1 heading) ! A.class_ "flex-center"
      H.div ! A.class_ "flex-center" $
        H.p note ! A.class_ "choose-package-offers-note"
      H.div ! A.class_ "choose-package-offers" $ do
        package1 lang
        package2 lang
        package3 lang
  where
    heading = case lang of
      ES -> "Nuestras ofertas"
      _  -> "Meet our plans"
    note = case lang of
      ES -> "En función del nivel que busques"
      _  -> "Depending on the level you seek"



packageTemplate :: Lang -> JobType -> [Html] -> Html -> Html
packageTemplate lang p points price =
    H.div ! A.class_ (H.toValue $ "choose-package-offer-wrapper " <> x) $ do
      H.div ! A.class_ "flex-center" $
        H.h4 $ H.toHtml (translate lang p)
      H.ul
        $ mapM_ enlistPoint points
      H.div ! A.class_ "flex-center" $ do
        H.span price ! A.class_ "choose-package-offer-price-number"
        H.span month ! A.class_ "choose-package-offer-price-frequency"
      H.div ! A.class_ "flex-center" $
        H.a ! A.href (H.toValue $ createJobPostRouteByType p)
            ! A.class_ (H.toValue $ "choose-package-offer-button " <> x <> y)
            $ case lang of
              ES -> "Elegir"
              _  -> "Choose"
  where
    x = jobTypeClass p
    y = " " <> jobTypeButtonClass p
    month = case lang of
      ES -> "/mes"
      _  -> "/month"
    enlistPoint point =
      H.li $ do
        lambdaIcon ! A.class_ (H.toValue x)
        point



package1 :: Lang -> Html
package1 lang =
    packageTemplate lang Novice [p1,p2,p3]
      (H.toHtml . (\x -> show x ++ "€") $ packagePrice Novice)
  where
    p1 = case lang of
      ES -> "0-5 años de experiencia"
      _  -> "0-5 years of experience"
    p2 = case lang of
      ES -> "Facilidad para aprender"
      _  -> "Learns quickly"
    p3 = case lang of
      ES -> "Alto potencial"
      _  -> "High potential"


package2 :: Lang -> Html
package2 lang =
    packageTemplate lang Expert [p1,p2,p3]
      (H.toHtml . (\x -> show x ++ "€") $ packagePrice Expert)
  where
    p1 = case lang of
      ES -> "5-10 años de experiencia"
      _  -> "5-10 years of experience"
    p2 = case lang of
      ES -> "Conoce algunas tecnologías y explora otras nuevas"
      _  -> "Knows some techs and explores new ones"
    p3 = case lang of
      ES -> "Alta productividad"
      _  -> "High productivity"


package3 :: Lang -> Html
package3 lang =
    packageTemplate lang Master [p1,p2,p3]
      (H.toHtml . (\x -> show x ++ "€") $ packagePrice Master)
  where
    p1 = case lang of
      ES -> ">10 años de experiencia"
      _  -> ">10 years of experience"
    p2 = case lang of
      ES -> "Puede instruir a los talentos más jóvenes"
      _  -> "Can train the younger talents"
    p3 = case lang of
      ES -> "Se sabe todos los trucos del oficio"
      _  -> "Knows the tricks of the trade"





--------------------------------------------------------------------------------




ourPerks :: Lang -> Html
ourPerks lang = do
    H.div (H.h1 heading) ! A.class_ "flex-center"
    H.div ! A.class_ "flex-center" $
      H.div ! A.class_ "choose-package-perks-wrapper" $
        mapM_ wrap
          [ unlimitedHiring
          , analytics
          , emailAlerts
          , editAnytime
          , qualifiedCandidates
          , socialMedia
          ]
  where
    heading = case lang of
      ES -> "Ventajas"
      _  -> "Perks"
    wrap f = H.div (f lang) ! A.class_ "choose-package-perk"



makePerk :: Html -> Html -> Html -> Html
makePerk icon heading content = do
  icon
  H.h5 heading
  H.span content



unlimitedHiring :: Lang -> Html
unlimitedHiring lang =
    makePerk icon heading content
  where
    icon = unlimitedHiringSvg
    heading = case lang of
      ES -> "Contratos Ilimitados"
      _  -> "Unlimited Hiring"
    content = case lang of
      ES -> "lo que tú necesites"
      _  -> "whatever you need"


analytics :: Lang -> Html
analytics lang =
    makePerk icon heading content
  where
    icon = analyticsSvg
    heading = case lang of
      ES -> "Estadísticas"
      _  -> "Analytics"
    content = case lang of
      ES -> "para el seguimiento de tu anuncio"
      _  -> "to track your job post"


emailAlerts :: Lang -> Html
emailAlerts lang =
    makePerk icon heading content
  where
    icon = emailAlertsSvg
    heading = case lang of
      ES -> "Alertas por Email"
      _  -> "Email Alerts"
    content = case lang of
      ES -> "enviadas a miles de candidatos"
      _  -> "sent to thousands of candidates"


editAnytime :: Lang -> Html
editAnytime lang =
    makePerk icon heading content
  where
    icon = editAnytimeSvg
    heading = case lang of
      ES -> "Edita o Cancela"
      _  -> "Edit or Cancel"
    content = case lang of
      ES -> "con herramientas sencillas"
      _  -> "with simple tools"


qualifiedCandidates :: Lang -> Html
qualifiedCandidates lang =
    makePerk icon heading content
  where
    icon = qualifiedCandidatesSvg
    heading = case lang of
      ES -> "Candidatos Cualificados"
      _  -> "Qualified Candidates"
    content = case lang of
      ES -> "lo mejor de cada sector"
      _  -> "best from each sector"


socialMedia :: Lang -> Html
socialMedia lang =
    makePerk icon heading content
  where
    icon = socialMediaSvg
    heading = case lang of
      ES -> "Redes Sociales"
      _  -> "Social Media"
    content = case lang of
      ES -> "máximo alcance"
      _  -> "highest reach"



--------------------------------------------------------------------------------





buttons :: Lang -> Html
buttons lang =
    H.div ! A.class_ "choose-package-buttons-section" $ do
      H.div (H.h1 heading) ! A.class_ "flex-center"
      H.div ! A.class_ "resp-w flex-center choose-package-buttons-wrapper" $ do
        choosePackageButton Novice lang
        choosePackageButton Expert lang
        choosePackageButton Master lang
  where
    heading = case lang of
      ES -> "Elige tu Paquete"
      _  -> "Choose your package"


choosePackageButton :: JobType -> Lang -> Html
choosePackageButton p lang =
    H.a ! A.class_ (H.toValue $ "choose-package-button " <> x <> " " <> y)
        ! A.href   (H.toValue $ createJobPostRouteByType p)
        $ H.toHtml $ translate lang p
  where
    y = jobTypeClass p
    x = jobTypeButtonClass p



--------------------------------------------------------------------------------
