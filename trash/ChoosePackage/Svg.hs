{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Views.ChoosePackage.Svg where

import Text.Blaze.Svg11 as S

import Base.Svg.Business
import Base.Svg.Geometry
import Base.Svg.Lambdas
import Base.Svg.Utils

--------------------------------------------------------------------------------

lambdaIcon :: Svg
lambdaIcon = stdSvg curvyLambda

--------------------------------------------------------------------------------

unlimitedHiringSvg :: Svg
unlimitedHiringSvg = stdSvg infinity

analyticsSvg :: Svg
analyticsSvg = stdSvg analytics

emailAlertsSvg :: Svg
emailAlertsSvg = stdSvg envelope

editAnytimeSvg :: Svg
editAnytimeSvg = stdSvg documentWithPencil

qualifiedCandidatesSvg :: Svg
qualifiedCandidatesSvg =
  stdSvg $ horizontalMirrorSymmetry $ g bullseye

socialMediaSvg :: Svg
socialMediaSvg =
  stdSvg circlesWithConnections

--------------------------------------------------------------------------------
