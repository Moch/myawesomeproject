{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Views.ChoosePackage.Css where

import Clay
import qualified Clay.Flexbox as Flex

import Base.Css.Utils

--------------------------------------------------------------------------------

choosePackageCss :: Css
choosePackageCss =
  associateCss
    [ (,) "choose-package-body"                  bodyCss
    , (,) "choose-package-tech-wrapper"          techWrapperCss
    , (,) "choose-package-techs"                 techsCss
    , (,) "choose-package-tech-table-wrapper"    techTableWrapperCss
    , (,) "choose-package-tech-table"            techTableCss
    , (,) "choose-package-tech-table-logo"       techTableLogoCss
    , (,) "choose-package-tech-left-cell"        techTableLeftCell
    , (,) "choose-package-offers-section"        offersSectionCss
    , (,) "choose-package-offers-note"           offersNoteCss
    , (,) "choose-package-offers"                offersCss
    , (,) "choose-package-offer-wrapper"         offerWrapperCss
    , (,) "choose-package-offer-price-number"    offerPriceCss
    , (,) "choose-package-offer-price-frequency" offerFrequencyCss
    , (,) "choose-package-offer-button"          offerButtonCss
    , (,) "choose-package-perks-wrapper"         perksWrapperCss
    , (,) "choose-package-perk"                  perkCss
    , (,) "choose-package-buttons-section"       buttonsSectionCss
    , (,) "choose-package-buttons-wrapper"       buttonsWrapperCss
    , (,) "choose-package-button"                buttonCss
    ]


--------------------------------------------------------------------------------

bodyCss :: Css
bodyCss = do
  -- color
  background mistyrose --(rgb 245 235 255)
  -- flex child
  flexGrow 1
  -- flex parent
  display flex
  flexDirection column
  justifyContent center
  alignItems center
  -- geometry
  paddingTop (Clay.rem 6)
  -- HEADINGS
  h1 ? h1Css
  h2 ? h2Css
  h3 ? h3Css


h1Css :: Css
h1Css = do
  -- font
  fontColor white -- thistle -- mistyrose --sienna
  setFont (weight 650) (Clay.rem 2) ["Times"]
  -- geometry
  marginBottom (Clay.rem 3)
  -- text
  textAlign center
  (-:) "text-shadow" "2px 0 0 indigo, -2px 0 0 indigo, 0 2px 0 indigo, 0 -2px 0 indigo"


h2Css :: Css
h2Css = do
  -- font
  fontColor sienna
  setFont bold (Clay.rem 1.5) [montserrat]
  fontVariant smallCaps
  -- geometry
  let x = Clay.rem 0.8
  let y = Clay.rem 1.5
  margin y x y x
  -- text
  textAlign center
  -- textShadow (px 1) (px 0) (px 0) deeppink


h3Css :: Css
h3Css = do
  -- font
  fontColor sienna
  setFont normal (Clay.rem 1.1) [smooth]


--------------------------------------------------------------------------------


techWrapperCss :: Css
techWrapperCss = do
  -- border
  borderStyle none
  (-:) "box-shadow" "0px 3px 5px 0px rgba(0,0,0,0.2)"
  -- color
  backgroundColor white
  -- geometry
  let x = px 15
  padding x x x x
  marginBottom (Clay.rem 6)


techsCss :: Css
techsCss = do
  -- flex parent
  display flex
  justifyContent center
  alignItems center
  flexWrap Flex.wrap


techTableWrapperCss :: Css
techTableWrapperCss = do
  -- flex parent
  display flex
  flexDirection column
  justifyContent spaceBetween
  -- geometry
  let w = 250
  width  (px w)
  height (px $ 1.618 * w)
  let x = px 12
  let y = px 18
  margin y x y x


techTableCss :: Css
techTableCss = do
  -- font
  setFont normal (Clay.rem 0.9) [ubuntu]
  fontColor (rgb 55 55 55)
  -- geometry
  width (pct 100)
  height (pct 90)


techTableLogoCss :: Css
techTableLogoCss = do
  -- geometry
  let x = px 25
  width  x
  height x


techTableLeftCell :: Css
techTableLeftCell = do
  -- font
  -- fontSize (Clay.rem 1)
  setFont normal (Clay.rem 1) [montserrat]
  -- geometry
  width (px 50)
  paddingRight (px 20)




--------------------------------------------------------------------------------

offersSectionCss :: Css
offersSectionCss = do
  -- color
  backgroundColor (rgb 220 220 255)
  -- flex parent
  display flex
  justifyContent center
  -- geometry
  width (pct 100)
  paddingTop    (Clay.rem 3)
  paddingBottom (Clay.rem 1)
  marginBottom  (Clay.rem 5)


offersNoteCss :: Css
offersNoteCss = do
  -- font
  setFont normal (Clay.rem 1.2) [smooth]
  fontColor (rgb 20 20 20)
  -- geometry
  width (px 280)
  portrait $ width (px 450)
  -- text
  textAlign center


offersCss :: Css
offersCss = do
  -- flex parent
  display flex
  justifyContent center
  alignItems center
  -- flexWrap Flex.wrap
  flexDirection column
  laptop $ flexDirection row
  -- geometry
  marginTop (Clay.rem 3)



offerWrapperCss :: Css
offerWrapperCss = do
  ul ? offerListCss
  h4 ? do
    setFont (bold) (Clay.rem 1.5) [montserratAlt]
    fontVariant smallCaps
  -- border
  borderStyle solid
  borderWidth (px 10)
  -- color
  backgroundColor white
  -- flex parent
  display flex
  flexDirection column
  justifyContent spaceBetween
  alignItems center
  -- geometry
  paddingTop    (Clay.rem 3)
  paddingBottom (Clay.rem 3)
  let w = 250
  width  (px w)
  height (px (1.618 * w))
  marginBottom (px 50)
  let m1 = px 30
  marginRight m1
  marginLeft  m1
  portrait $ do
    let w1 = 300
    width (px w1)
    height (px (1.618 * w1))
  laptop $ do
    let m2 = px 15
    marginRight m2
    marginLeft  m2
  desktop $ do
    let m3 = px 80
    marginRight m3
    marginLeft  m3


offerListCss :: Css
offerListCss = do
  -- flex parent
  display flex
  flexDirection column
  justifyContent spaceBetween
  -- font
  fontColor (rgb 95 95 95)
  setFont normal (Clay.rem 0.9) [smooth]
  -- geometry
  height (pct 30)
  width (pct 70)
  -- list
  listStyleType none
  -- SVG
  svg ? do
    let x = px 13
    width x
    height x
    marginRight (px 5)


offerPriceCss :: Css
offerPriceCss = do
  -- font
  fontColor (rgb 80 80 80)
  setFont bolder (Clay.rem 2.1) [montserrat]


offerFrequencyCss :: Css
offerFrequencyCss = do
  -- flex parent
  display inlineFlex
  alignItems flexEnd
  -- font
  fontColor (rgb 120 120 120)
  setFont normal (Clay.rem 0.9) [montserratAlt]
  -- geometry
  height (pct 100)
  paddingBottom (px 5)


offerButtonCss :: Css
offerButtonCss = do
  -- border
  borderStyle solid
  borderWidth (px 2)
  -- flex parent
  display flex
  justifyContent center
  alignItems center
  -- font
  setFont normal (Clay.rem 0.9) [montserrat]
  -- geometry
  sym2 padding (Clay.rem 0.8) (Clay.rem 1.5)
  -- text
  textDecoration none
  -- transition
  transition "all" (ms 300) linear (ms 0)


--------------------------------------------------------------------------------




perkSize :: Size LengthUnit
perkSize = px 240


perkMargin :: Size LengthUnit
perkMargin = px 30


perksWrapperCss :: Css
perksWrapperCss = do
  -- flex parent
  display flex
  flexWrap Flex.wrap
  justifyContent center
  alignItems center
  -- geometry
  maxWidth $ (3 *@ perkSize) @+@ (6 *@ perkMargin)
  marginBottom (Clay.rem 6)


perkCss :: Css
perkCss = do
  -- background
  backgroundImage $ url "/static/svg/octagon.svg"
  -- flex parent
  display flex
  flexDirection column
  justifyContent spaceBetween
  alignItems center
  -- font
  setFont normal (Clay.rem 0.8) [montserrat]
  fontColor (rgb 80 80 80)
  -- geometry
  width  perkSize
  height perkSize
  margin  `sym` perkMargin
  padding `sym` (px 40)
  -- text
  textAlign center
  -- SVG
  svg ? do
    -- color
    (-:) "stroke" "#6A5ACD"
    -- geometry
    width  (px 70)
    height (px 70)
  -- HEADING
  h5 ? do
    -- flex parent
    display flex
    justifyContent center
    alignItems center
    -- font
    fontSize (Clay.rem 0.9)
    fontColor indigo
    -- geometry
    height (px 45)
    paddingTop (px 10)
  -- SPAN
  Clay.span ? do
    -- flex parent
    display flex
    justifyContent center
    alignItems center
    -- geometry
    height (px 35)






--------------------------------------------------------------------------------





buttonsSectionCss :: Css
buttonsSectionCss = do
  -- color
  backgroundImage $ vGradient (rgb 150 205 225) (rgb 180 180 255)
  -- flex parent
  display flex
  flexDirection column
  alignItems center
  justifyContent center
  -- geometry
  width (pct 100)
  paddingTop (Clay.rem 3)
  paddingBottom (Clay.rem 3)



buttonsWrapperCss :: Css
buttonsWrapperCss = do
  -- flex parent
  display flex
  justifyContent center
  alignItems center
  flexWrap Flex.wrap
  -- geometry
  paddingBottom (Clay.rem 2)


buttonCss :: Css
buttonCss = do
  -- border
  borderStyle solid
  borderWidth (px 5)
  let r = px 10
  borderRadius r r r r
  -- flex parent
  display flex
  flexDirection column
  justifyContent center
  alignItems center
  -- font
  setFont bold (Clay.rem 2) [smooth]
  fontVariant smallCaps
  -- geometry
  let h = 135
  height (px h)
  width  (px $ h*1.618)
  let m1 = px 52
  let m2 = px 30
  margin m2 m1 m2 m1
  hover & do
    marginTop (px 20)
    marginBottom (px 40)
    (-:) "box-shadow" "0px 5px 3px 4px darkgrey"
  -- text
  textDecoration none
  textAlign center
  -- transition
  transition "all" (ms 400) linear (ms 0)




--------------------------------------------------------------------------------
