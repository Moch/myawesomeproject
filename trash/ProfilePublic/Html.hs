{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.ProfilePublic.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Head
import Base.Html.Page
import Base.Lang
import Base.UI.Elems.GoldenLayout.Html
import Base.UI.Layout.Template.Html
import User.API.Routes
import User.Logic.All
import User.UI.Views.ProfilePublic.Svg


--------------------------------------------------------------------------------

profilePublicHead :: Head
profilePublicHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

profilePublicPageView :: PageParams -> User a -> Html
profilePublicPageView params u =
    H.toHtml page
  where
    page = Page
      { pageParams = params
      , htmlHead   = profilePublicHead
      , htmlBody   = mainTemplate (profilePublicBody u)
      }

profilePublicBody :: User a -> PageParams -> Html
profilePublicBody u p =
  let
    lang = ppLang p
    uType = userType $ accessInfo u
    fibLayout = fibLayoutFullPage7
  in do
    H.div
      ! A.class_ "profile-public-body"
      $ fibLayout $ map whiteBox
        [ ""
        , ""
        , profilePublicCore  lang u
        , profilePublicImage uType
        , jobsButton   lang uType
        , editButton   lang
        , logoutButton lang
        ]



--------------------------------------------------------------------------------


profilePublicCore :: Lang -> User a -> Html
profilePublicCore lang u =
  profilePublicCoreInfo
    lang
    (show $ userName $ accessInfo u)
    (userEmail $ accessInfo u)




--------------------------------------------------------------------------------

whiteBox :: Html -> Html
whiteBox =
  H.div ! A.class_ "flex-center profile-public-white-box"

--------------------------------------------------------------------------------

logoutButton:: Lang -> Html
logoutButton lang =
  let
    textToDisplay = tooltip :: String
    tooltip = case lang of
      ES -> "Cerrar Sesión"
      _  -> "Logout"
  in
    H.a
      ! A.class_ "profile-public-small-btn profile-public-logout-btn"
      ! A.title (H.toValue tooltip)
      ! A.href (H.toValue logoutRoute)
      $ do
        logoutIcon
        H.span (H.toHtml textToDisplay)



editButton :: Lang -> Html
editButton lang =
  let
    textToDisplay = tooltip :: String
    tooltip = case lang of
      ES -> "Editar Perfil"
      _  -> "Edit Profile"
  in
  H.a
    ! A.class_ "profile-public-small-btn profile-public-edit-btn"
    ! A.title (H.toValue tooltip)
    ! A.href "/"
    $ do
      editIcon
      H.span (H.toHtml textToDisplay)


jobsButton :: Lang -> UserType -> Html
jobsButton lang uType =
  let
    tooltip = textToDisplay :: String
    textToDisplay =
      case uType of
        UserCompany -> case lang of
          ES -> "Publicar Anuncio"
          _  -> "Post Job"
        UserPerson -> case lang of
          ES -> "Ver Empleos"
          _  -> "View Jobs"
    hypertextReference =
      case uType of
        UserCompany -> "/"
        UserPerson  -> "/"
    iconToDisplay =
      case uType of
        UserCompany -> postJobIcon
        UserPerson  -> viewJobIcon
  in
    H.a
      ! A.href hypertextReference
      ! A.class_ "profile-public-small-btn profile-public-jobs-btn"
      ! A.title (H.toValue tooltip)
      $ do
        iconToDisplay
        H.p $ H.toHtml textToDisplay


--------------------------------------------------------------------------------

profilePublicImage :: UserType -> Html
profilePublicImage uType =
  let
    iconToDisplay =
      case uType of
        UserCompany -> companyIcon
        UserPerson  -> personIcon
  in
    H.div
      ! A.class_ "profile-public-image"
      $ iconToDisplay


--------------------------------------------------------------------------------

profilePublicCoreInfo :: Lang -> String -> Text -> Html
profilePublicCoreInfo lang uName uEmail =
  let
    changePw =
      H.button
        $ case lang of
            ES -> "Cambiar contraseña"
            _  -> "Change password"
    deleteAccount =
      H.button
        $ case lang of
            ES -> "Eliminar cuenta"
            _  -> "Delete account"
  in
    H.div
      ! A.class_ "profile-public-core"
      $ do
        H.h3 (H.toHtml uName)
        H.div $ do
          H.span "Email: "
          H.span (H.toHtml $ show uEmail)
        H.div changePw
        H.div deleteAccount

--------------------------------------------------------------------------------
