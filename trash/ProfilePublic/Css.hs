{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.ProfilePublic.Css where

import Clay
-- import qualified Clay.Flexbox as Flex

import Base.Css.Utils

--------------------------------------------------------------------------------

profilePublicCss :: Css
profilePublicCss =
  associateCss
    [ (,) "profile-public-body"        bodyCss
    , (,) "profile-public-white-box"   whiteBoxCss
    , (,) "profile-public-small-btn"   smallBtnCss
    , (,) "profile-public-logout-btn"  logoutBtnCss
    , (,) "profile-public-edit-btn"    editBtnCss
    , (,) "profile-public-jobs-btn"    jobsBtnCss
    , (,) "profile-public-image"       imageCss
    ]

--------------------------------------------------------------------------------

bodyCss :: Css
bodyCss = do
  -- background
  backgroundColor $ rgba 216 191 216 0.4
  -- flex parent
  display flex
  flexDirection  column
  justifyContent center
  alignItems center
  -- flex child
  flexGrow 1
  -- geometry
  let x = Clay.rem 2.5
  paddingTop    x
  paddingBottom x


whiteBoxCss :: Css
whiteBoxCss = do
  -- border
  borderWithRadius solid (px 1) silver (px 8)
  (-:) "box-shadow" "0px 0px 5px 2px rgba(0,0,0,0.1)"
  -- color
  backgroundColor white
  -- geometry
  height (pct 100)
  width  (pct 100)
  maxWidth (px 500)
  landscape $ maxWidth (pct 100)


smallBtnCss :: Css
smallBtnCss = do
  -- border
  borderStyle  solid
  borderWidth  (px 2)
  borderRadius `sym` (px 5)
  -- flex parent
  display flex
  flexDirection column
  justifyContent spaceBetween
  alignItems center
  -- font
  setFont normal (px 10) [montserratAlt]
  -- geometry
  width (pct 100)
  height (pct 100)
  padding `sym` (px 15)
  landscape $ padding `sym` (px 3)
  -- text
  textAlign center
  textDecoration none
  -- transition
  transition "all" (ms 200) linear (ms 0)
  -- PARAGRAPH
  p ? do
    paddingTop (px 8)
  -- SPAN
  Clay.span ? do
    landscape $ display none
  -- SVG
  svg ? do
    maxWidth (px 50)



--------------------------------------------------------------------------------

logoutBtnCss :: Css
logoutBtnCss = do
  -- color
  borderColor firebrick
  fontColor firebrick
  (-:) "stroke" "firebrick"
  hover & do
    backgroundColor $ rgba 255 0 0 0.05
    borderColor red
    fontColor red
    "stroke" -: "red"



editBtnCss :: Css
editBtnCss = do
  -- color
  borderColor forestgreen
  fontColor forestgreen
  (-:) "stroke" "forestgreen"
  hover & do
    backgroundColor $ rgba 0 255 0 0.05
    borderColor limegreen
    fontColor limegreen
    "stroke" -: "limegreen"



jobsBtnCss :: Css
jobsBtnCss = do
  -- color
  borderColor navy
  fontColor navy
  (-:) "stroke" "navy"
  hover & do
    backgroundColor $ rgba 0 0 255 0.05
    borderColor blue
    fontColor blue
    "stroke" -: "blue"
  -- geometry
  padding `sym` (px 15)


--------------------------------------------------------------------------------


imageCss :: Css
imageCss = do
  -- color
  (-:) "stroke" "silver"
  (-:) "fill"   "silver"
  -- SVG
  Clay.svg ? margin `sym` (px 15)



--------------------------------------------------------------------------------
