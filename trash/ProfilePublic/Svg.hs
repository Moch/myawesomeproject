{-# LANGUAGE    OverloadedStrings       #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}


module User.UI.Views.ProfilePublic.Svg where

-- import Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
-- import qualified Text.Blaze.Svg11.Attributes as A

import Base.Svg.Business
import Base.Svg.User
import Base.Svg.Utils

--------------------------------------------------------------------------------

logoutIcon :: S.Svg
logoutIcon = stdSvg onOff

editIcon :: S.Svg
editIcon = stdSvg bigLeanedPencil

postJobIcon :: S.Svg
postJobIcon = stdSvg documentWithPencil

viewJobIcon :: S.Svg
viewJobIcon = stdSvg magnifyingGlass

personIcon :: S.Svg
personIcon = stdSvg personSimple 

companyIcon :: S.Svg
companyIcon = stdSvg company

--------------------------------------------------------------------------------
