{-# LANGUAGE OverloadedStrings #-}


module User.UI.Elems.AccessInputs.Html where
{-
import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Lang
import Base.Validate
import User.API.Routes
import User.Logic.All
import User.UI.Elems.AccessInputs.Svg

--------------------------------------------------------------------------------

foo :: H.Attribute
foo = A.class_ "error-simple display-none"


--------------------------------------------------------------------------------



nameInputJS :: String -> String -> String -> Lang -> Html
nameInputJS val submitId cssClass lang =
  do
    H.input
      ! A.type_       "text"
      ! A.required    "required"
      ! A.name        "name-input"
      ! A.id          "name-input"
      ! A.onchange    (H.toValue $ "validateUserName(" ++ args ++ ");")
      ! A.value       (H.toValue val)
      ! A.class_      (H.toValue cssClass)
      ! A.placeholder ph
    H.span
      ! A.id "name-error-minL" ! foo
      $ "Too Short"
    H.span
      ! A.id "name-error-maxL" ! foo
      $ "Too Long"
    H.span
      ! A.id "name-error-malS" ! foo
      $ H.toHtml $ printErrorMalformedSyntax
    H.span
      ! A.id "name-error-exst" ! foo
      $ H.toHtml $ printErrorAlreadyTaken
  where
    minL = textMinLength userNameRestrictions
    maxL = textMaxLength userNameRestrictions
    args = show minL ++ "," ++ show maxL ++ "," ++ url ++ ", '" ++ submitId ++ "'"
    url  = "'" ++ userNameExistsRoute ++ "'"
    ph = case lang of
      ES -> "Nombre de usuario (alias)"
      _  -> "Username (alias)"
    f l = case l of
      ES -> "El nombre de usuario "
      _  -> "Username "
    printErrorAlreadyTaken =
      f lang ++ "already exist"
    printErrorMalformedSyntax = case lang of
      ES -> (f lang) ++ " solo puede contener caracteres alfanuméricos o guiones simples, y no puede empezar o terminar en guión"
      _  -> (f lang) ++ " may only contain alphanumeric characters or single hyphens, and cannot begin or end with a hyphen"


--------------------------------------------------------------------------------




emailInputJS :: String -> String -> Lang -> Html
emailInputJS submitId cssClass lang =
  do
    H.input
      ! A.type_       "text"
      ! A.required    "required"
      ! A.name        "email-input"
      ! A.id          "email-input"
      ! A.onchange    (H.toValue $ "validateUserEmail(" ++ args ++ ");")
      ! A.placeholder "Email"
      ! A.class_      (H.toValue cssClass)
    H.span
      ! A.id "email-error-minL" ! foo
      $ "too short"
    H.span
      ! A.id "email-error-maxL" ! foo
      $ "too long"
    H.span
      ! A.id "email-error-malS" ! foo
      $ H.toHtml $ printErrorMalformedSyntax
    H.span
      ! A.id "email-error-exst" ! foo
      $ H.toHtml $ printErrorAlreadyTaken
  where
    minL = textMinLength userNameRestrictions
    maxL = textMaxLength userNameRestrictions
    args = show minL ++ "," ++ show maxL ++ "," ++ url ++ ", '" ++ submitId ++ "'"
    url  = "'" ++ userEmailExistsRoute ++ "'"
    f l = case l of
      ES -> "La dirección de email "
      _  -> "Email address "
    printErrorAlreadyTaken =
      f lang ++ "ya existe"
    printErrorMalformedSyntax = case lang of
      ES -> (f lang) ++ " no es válida"
      _  -> (f lang) ++ " is invalid"


--------------------------------------------------------------------------------




pwInputJS :: String -> String -> String -> Lang -> Html
pwInputJS ph submitId cssClass _ =
  do
    H.div
      ! A.class_ "access-inputs-pw-wrap"
      $ do
        H.input
          ! A.type_       "password"
          ! A.required    "required"
          ! A.name        "pw-input"
          ! A.id          "pw-input"
          ! A.onchange    (H.toValue $ "validateUserPw(" ++ args ++ ");")
          ! A.placeholder (H.toValue ph)
          ! A.class_      (H.toValue cssClass)
        H.button
          ! A.type_   "button"
          ! A.class_  "access-inputs-pw-eye"
          ! A.onclick "togglePwVisibility();"
          $ do
            closedEyeIcon
              ! A.id     "pw-hide-icon"
              ! A.class_ "display-none"
            openEyeIcon
              ! A.id     "pw-show-icon"
    H.span
      ! A.id "pw-error-minL" ! foo
      $ "too short"
    H.span
      ! A.id "pw-error-maxL" ! foo
      $ "too long"
  where
    minL = textMinLength userNameRestrictions
    maxL = textMaxLength userNameRestrictions
    args = show minL ++ "," ++ show maxL ++ ", '" ++ submitId ++ "'"



--------------------------------------------------------------------------------
-}
