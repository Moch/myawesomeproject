

function validateUserNameSyntax(inputElem,errorElem){
  let str = inputElem.value;
  let len = str.length;
  var isValid = true;
  var code, i;
  for (i = 0; i < len; i++) {
    code = str.charCodeAt(i);
    if (!(code > 47 && code < 58)   &&    // numeric (0-9)
        !(code > 64 && code < 91)   &&    // upper alpha (A-Z)
        !(code > 96 && code < 123)  &&    // lower alpha (a-z)
        !(code === 45)              ||    // hyphen
        (code === 45 && str.charCodeAt(i-1) === 45)) {
      isValid = false;
    }
  }
  if (str.charCodeAt(0) === 45 || str.charCodeAt(len-1) === 45) {
    isValid = false;
  }
  if (isValid) {
    hideInputError(errorElem); }
  else {
    showInputError(errorElem); }
  return isValid
}


function validateUserName(minL,maxL,url,submitId){
  let inputElem  = document.getElementById('name-input');
  let minLenErr  = document.getElementById('name-error-minL');
  let maxLenErr  = document.getElementById('name-error-maxL');
  let malformedE = document.getElementById('name-error-malS');
  let exstErr    = document.getElementById('name-error-exst');
  let submitElem = document.getElementById(submitId);
  var lBoundOk   = validateInputMinLen(minL,inputElem,minLenErr);
  var uBoundOk   = validateInputMaxLen(maxL,inputElem,maxLenErr);
  var wellformed = validateUserNameSyntax(inputElem, malformedE);
  checkInputExists(url, inputElem, exstErr, submitElem);
  let isValid = lBoundOk && uBoundOk && wellformed
  if (isValid) {
    markInputAsValidAndEnableSubmit(inputElem,submitElem);
  }
  else {
    markInputAsWrongAndDisableSubmit(inputElem,submitElem);
  }
}


function validateUserEmailSyntax(inputElem, errorElem){
  let str = inputElem.value;
  isValid = /^.+@.+\..+$/.test(str);
  if (isValid) {
    hideInputError(errorElem); }
  else {
    showInputError(errorElem); }
  return isValid
}


function validateUserEmail(minL,maxL,url,submitId){
  let inputElem  = document.getElementById('email-input');
  let minLenErr  = document.getElementById('email-error-minL');
  let maxLenErr  = document.getElementById('email-error-maxL');
  let malformedE = document.getElementById('email-error-malS');
  let exstErr    = document.getElementById('email-error-exst');
  let submitElem = document.getElementById(submitId);
  var lBoundOk   = validateInputMinLen(minL,inputElem,minLenErr);
  var uBoundOk   = validateInputMaxLen(maxL,inputElem,maxLenErr);
  var wellformed = validateUserEmailSyntax(inputElem,malformedE);
  checkInputExists(url,inputElem,exstErr,submitElem);
  let isValid = lBoundOk && uBoundOk && wellformed
  if (isValid) {
    markInputAsValidAndEnableSubmit(inputElem,submitElem);
  }
  else {
    markInputAsWrongAndDisableSubmit(inputElem,submitElem);
  }
}



function validateUserPw(minL,maxL,submitId){
  let inputElem  = document.getElementById('pw-input');
  let minLenErr  = document.getElementById('pw-error-minL');
  let maxLenErr  = document.getElementById('pw-error-maxL');
  let submitElem = document.getElementById(submitId);
  var lBoundOk  = validateInputMinLen(minL,inputElem,minLenErr);
  var uBoundOk  = validateInputMaxLen(maxL,inputElem,maxLenErr);
  let isValid   = lBoundOk && uBoundOk
  if (isValid) {
    markInputAsValidAndEnableSubmit(inputElem,submitElem);
  }
  else {
    markInputAsWrongAndDisableSubmit(inputElem,submitElem);
  }
}



function togglePwVisibility(){
  let inputElem  = document.getElementById('pw-input');
  let hideIcon   = document.getElementById('pw-hide-icon');
  let showIcon   = document.getElementById('pw-show-icon');
  if (inputElem.type == "password") {
    inputElem.type = "text";
    hideIcon.classList.remove('display-none');
    showIcon.classList.add('display-none');
  }
  else {
    inputElem.type = "password";
    hideIcon.classList.add('display-none');
    showIcon.classList.remove('display-none');
  }
}



// -----------------------------------------------------------------------------
