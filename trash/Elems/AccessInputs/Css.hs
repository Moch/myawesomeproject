{-# LANGUAGE OverloadedStrings #-}


module User.UI.Elems.AccessInputs.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

accessInputsCss :: Css
accessInputsCss =
  associateCss
    [ (,) "access-inputs-pw-wrap" pwWrapCss
    , (,) "access-inputs-pw-eye"  eyeButtonCss
    ]

--------------------------------------------------------------------------------


pwWrapCss :: Css
pwWrapCss = do
  -- flex parent
  display flex
  justifyContent center
  alignItems center


eyeButtonCss :: Css
eyeButtonCss = do
  -- border
  borderStyle  none
  outlineStyle none
  -- color
  backgroundColor transparent
  -- display
  cursor pointer
  -- flex parent
  display flex
  justifyContent center
  alignItems     center
  -- geometry
  marginLeft    (px 10)
  padding `sym` (px 5)
  -- SVG
  svg ? do
    "stroke" -: "dimgray"
    "fill"   -: "dimgray"
    height (px 20)
    width  (px 20)


--------------------------------------------------------------------------------
