{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.RememberMeInput.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Lang


--------------------------------------------------------------------------------

rememberMeInput :: Lang -> Html
rememberMeInput lang =
    H.label ! A.class_ "remember-me" $ do
      checkboxInput
      textSpan
  where
    checkboxInput =
      H.input
        ! A.type_   "checkbox"
        ! A.name    "remember-me-input"
        ! A.checked "checked"
        ! A.value   "Remember"
    textSpan =
      H.span $ case lang of
        ES -> "Recordarme"
        _  -> "Remember me"

--------------------------------------------------------------------------------
