{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.RememberMeInput.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

rememberMeInputCss :: Css
rememberMeInputCss =
  associateCss
    [ (,) "remember-me"   rememberCss
    ]

--------------------------------------------------------------------------------



rememberCss :: Css
rememberCss = do
  -- display
  cursor pointer
  -- font
  setFont normal (Clay.rem 0.9) [smooth]
  fontColor gray
  -- flex child
  alignSelf flexStart
  -- flex parent
  display flex
  alignItems center
  -- geometry
  width (pct 100)
  height (Clay.rem 2)
  -- INPUT
  input ? do
    -- border
    outlineStyle none
    -- display
    cursor pointer
    -- geometry
    marginRight (px 10)
  -- SPAN
  Clay.span ? do
    marginBottom (px (-2))


--------------------------------------------------------------------------------
