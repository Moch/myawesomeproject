


module Static.Compiler.Js where


--------------------------------------------------------------------------------

compileJs :: FilePath -> IO ()
compileJs p =
  do
    allJs <- getAllJs
    mdEditorJs <- readFile "src/Base/UI/Elems/MarkdownEditor/mde.js"
    mdViewerJs <- readFile "src/Base/UI/Elems/MarkdownViewer/mdv.js"
    showdownJs <- readFile "src/Base/UI/Elems/MarkdownViewer/showdown.min.js"
    showdownYt <- readFile "src/Base/UI/Elems/MarkdownViewer/showdown-youtube.js"
    writeFile (p ++ "all.js") allJs
    writeFile (p ++ "mde.js") mdEditorJs
    writeFile (p ++ "mdv.js") mdViewerJs
    writeFile (p ++ "showdown.min.js")  showdownJs
    writeFile (p ++ "showdown-youtube.js") showdownYt


--------------------------------------------------------------------------------


getAllJs :: IO String
getAllJs =
  fmap concat $ mapM readFile
    [ "src/Base/UI/Elems/InputValidate/Js.js"
    , "src/Base/UI/Elems/ModalWindow/Js.js"
    , "src/Base/UI/Elems/PhotoCropper/Js.js"
    , "src/Base/UI/Elems/SelectEnriched/Js.js"
    , "src/Base/UI/Elems/SelectMulti/Js.js"
    , "src/Base/UI/Layout/Nav/Js.js"
    , "src/Base/UI/Layout/Settings/Js.js"
    ]


--------------------------------------------------------------------------------
