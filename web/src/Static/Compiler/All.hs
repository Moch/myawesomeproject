

module Static.Compiler.All where

import System.Directory

import Static.Compiler.Css
import Static.Compiler.Js
import Static.Compiler.Svg

--------------------------------------------------------------------------------

makeStaticFiles :: IO ()
makeStaticFiles =
  let p = "app/static/volatile/"
  in do
    resetStaticFolder p
    compileStaticFiles p

--------------------------------------------------------------------------------

resetStaticFolder :: FilePath -> IO ()
resetStaticFolder p =
  do
    createDirectoryIfMissing True p
    removeDirectoryRecursive p
    createDirectory p
    createDirectory (p ++ "css/")
    createDirectory (p ++ "js/")
    createDirectory (p ++ "svg/")


compileStaticFiles :: FilePath -> IO ()
compileStaticFiles p = do
  compileCss (p ++ "css/")
  compileJs  (p ++ "js/")
  compileSvg (p ++ "svg/")

--------------------------------------------------------------------------------
