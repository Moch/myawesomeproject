


module Static.Compiler.Css where

import Clay (Css, renderWith, pretty)
import qualified Data.Text.Lazy.IO as T (writeFile)

import Base.Css.General                       (fontCss, resetCss, generalCss)
import Base.UI.Elems.ClosableMsg.Css          (closableMsgCss)
import Base.UI.Elems.GoldenLayout.Css         (goldenLayoutCss)
import Base.UI.Elems.InputValidate.Css        (inputValidateCss)
import Base.UI.Elems.MarkdownEditor.Css       (markdownEditorCss)
import Base.UI.Elems.MarkdownViewer.Css       (markdownViewerCss)
import Base.UI.Elems.ModalWindow.Css          (modalWindowCss)
import Base.UI.Elems.PhotoCropper.Css         (photoCropperCss)
import Base.UI.Elems.SelectEnriched.Css       (richSelectCss)
import Base.UI.Elems.SelectMulti.Css          (selectMultiCss)
import Base.UI.Layout.Template.Css            (uiLayoutCss)
import Job.UI.Elems.Inputs.Css                (jobInputsCss)
import Job.UI.Elems.JobType.Css               (jobTypeCss)
import Job.UI.Views.CreateJobPost.Css         (createJobPostCss)
import Job.UI.Views.PostJob.Css               (postJobCss)
import Root.UI.Views.Found.Css                (foundCss)
import Root.UI.Views.Home.Css                 (homeCss)
import Root.UI.Views.NotFound.Css             (notFoundCss)
import User.UI.Views.Profile.Css              (profileCss)
import User.UI.Views.SettingsAccount.Css      (settingsAccountCss)
import User.UI.Views.SettingsProfile.Css      (settingsProfileCss)
import User.UI.Views.SignIn.Css               (signInCss)
import User.UI.Views.SignUp.Css               (signUpCss)
import User.UI.Views.SignUpEval.Css           (signUpEvalCss)


--------------------------------------------------------------------------------

compileCss :: FilePath -> IO ()
compileCss p =
  T.writeFile (p ++ "all.css") $ renderWith pretty [] allCss

--------------------------------------------------------------------------------

allCss :: Css
allCss = do
  fontCss
  resetCss
  generalCss
  closableMsgCss
  goldenLayoutCss
  inputValidateCss
  markdownEditorCss
  markdownViewerCss
  modalWindowCss
  photoCropperCss
  richSelectCss
  selectMultiCss
  uiLayoutCss
  -- JOB
  jobInputsCss
  jobTypeCss
  createJobPostCss
  postJobCss
  -- ROOT
  foundCss
  homeCss
  notFoundCss
  -- USER
  profileCss
  settingsAccountCss
  settingsProfileCss
  signInCss
  signUpCss
  signUpEvalCss

--------------------------------------------------------------------------------
