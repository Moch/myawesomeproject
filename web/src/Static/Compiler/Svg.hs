{-# LANGUAGE OverloadedStrings #-}


module Static.Compiler.Svg where

import Text.Blaze.Svg11
import Text.Blaze.Svg11.Attributes as A
import Text.Blaze.Svg.Renderer.String

-- import Base.Svg.Arrows
-- import Base.Svg.Business
-- import Base.Svg.Faces
import Base.Svg.Geometry
import Base.Svg.Religion
import Base.Svg.Text
-- import Base.Svg.User
import Base.Svg.Utils


--------------------------------------------------------------------------------

compileSvg :: FilePath -> IO ()
compileSvg folder =
    mapM_ f allSvg
  where
    f (fileName , svgCode) =
      writeFile (folder ++ fileName) (renderSvg $ docTypeSvg svgCode)

--------------------------------------------------------------------------------

allSvg :: [ (FilePath , Svg) ]
allSvg =
  [ (,) "wip.svg"            workInProgressSvg
  , (,) "octagon.svg"        indigoOctagon
  , (,) "dodecagon.svg"      indigoDodecagon
  , (,) "redX.svg"           redX
  , (,) "whiteX.svg"         whiteX
  , (,) "greenTick.svg"      greenTick
  , (,) "mosaicGeo.svg"      mosaicGeo
  , (,) "mosaicBee.svg"      mosaicBee
  , (,) "mosaicPajarita.svg" pajarita
  , (,) "volutas.svg"        volutas
  ]

--------------------------------------------------------------------------------

workInProgressSvg :: Svg
workInProgressSvg =
  devModeSvg $ do
    squareFrame
    centeredAxes
    abcBubble

--------------------------------------------------------------------------------

indigoOctagon :: Svg
indigoOctagon =
  stdSvg $ octagon ! A.stroke "#4B0082"

indigoDodecagon :: Svg
indigoDodecagon =
  stdSvg $ regularPolygon 12 0.025 ! A.stroke "#4B0082"


redX :: Svg
redX =
  stdSvg $ bigX ! A.style "stroke: red;"

whiteX :: Svg
whiteX =
  stdSvg $ bigX ! A.style "stroke: white;"

greenTick :: Svg
greenTick =
  stdSvg $ tick ! A.style "stroke: limegreen;"

mosaicGeo :: Svg
mosaicGeo =
  stdSvg $ geometricalMosaic ! A.style "stroke: silver; fill: silver;"

mosaicBee :: Svg
mosaicBee =
  beeHiveMosaic

pajarita :: Svg
pajarita =
  pajaritaNazaríMosaic

volutas :: Svg
volutas =
  stdSvg decoradoVolutas

--------------------------------------------------------------------------------
{-
textareaEditorIcons :: [ (FilePath, Svg) ]
textareaEditorIcons =
   [ (,) "iconBold.svg"    boldIcon
   , (,) "iconItalic.svg"  italicIcon
   , (,) "iconLink.svg"    linkIcon
   , (,) "iconImage.svg"   imageIcon
   , (,) "iconVideo.svg"   videoIcon
   , (,) "iconBulletL.svg" bulletListIcon
   , (,) "iconNumberL.svg" numberListIcon
   , (,) "iconHeader.svg"  headerIcon
   , (,) "iconHR.svg"      horizontalRuleIcon
   , (,) "iconUndo.svg"    undoIcon
   , (,) "iconRedo.svg"    redoIcon
   , (,) "iconHelp.svg"    helpIcon
   ]
-}
--------------------------------------------------------------------------------
