{-# LANGUAGE OverloadedStrings #-}


module Static.API.Handler where

import           Control.Monad (msum)
import           Happstack.Server
import           Text.Blaze.Html (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

--------------------------------------------------------------------------------

staticHandler :: ServerPart Response
staticHandler =
    dir "static" $ msum $
      staticAssets
      ++
      [ok $ toResponse someLinks]
  where
    ----
    serveDir :: (String, String) -> ServerPart Response
    serveDir (x,y) = dir x $ serveDirectory EnableBrowsing [] y
    ----
    staticAssets :: [ServerPart Response]
    staticAssets =
      map serveDir
        [ (,) "fonts"    "app/static/permanent/fonts/"
        , (,) "pictures" "app/static/permanent/pictures/"
        , (,) "sounds"   "app/static/permanent/sounds/"
        , (,) "css"      "app/static/volatile/css/"
        , (,) "js"       "app/static/volatile/js/"
        , (,) "svg"      "app/static/volatile/svg/"
        ]


--------------------------------------------------------------------------------

someLinks :: Html
someLinks =
  H.docTypeHtml $ do
    H.head ""
    H.body $ do
      H.a "such css"         ! A.href "/static/css"
      H.br
      H.a "very javascript"  ! A.href "/static/js"
      H.br
      H.a "much svg"         ! A.href "/static/svg"
      H.br
      H.a "amazing pictures" ! A.href "/static/pictures"
      H.br
      H.a "amusing sounds"   ! A.href "/static/sounds"
      H.br
      H.a "stylized fonts"   ! A.href "/static/fonts"
      H.br
      H.a "wow!"             ! A.href "/"


--------------------------------------------------------------------------------
