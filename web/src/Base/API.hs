


module Base.API where

import           Control.Applicative  (optional)
import           Data.List      (uncons)
import           Data.Text      (Text, pack, unpack)
import           Data.Text.Lazy (toStrict)
import           Happstack.Server as S


--------------------------------------------------------------------------------

safeLookText :: Text -> ServerPart (Maybe Text)
safeLookText =
  fmap ( ((toStrict . fst) <$>) . uncons ) . lookTexts . unpack


nullResponse :: Int -> ServerPart Response
nullResponse n = setResponseCode n >> (return $ toResponse "")


redirectToRoot :: ServerPart Response
redirectToRoot = seeOther "/" (toResponse ())


(^-^) :: Text -> ServerPart a -> ServerPart a
(^-^) route part =
  dir (tail $ unpack route) part

--------------------------------------------------------------------------------

serve :: ServerPart Response -> IO ()
serve part =
  let
    ramQuota  = 100000
    diskQuota = 2000000
    tmpDir    = "/tmp/"
    policy    = defaultBodyPolicy tmpDir diskQuota ramQuota (ramQuota `div` 10)
  in
    simpleHTTP (nullConf {port = 8000}) $ do
      decodeBody policy
      part

--------------------------------------------------------------------------------

saveErrorMsg :: Text -> Text -> ServerPart Response
saveErrorMsg url msg = do
  putCookieErrorMsg msg
  seeOther url (toResponse ())


saveOkMsg :: Text -> Text -> ServerPart Response
saveOkMsg url msg = do
  putCookieOkMsg msg
  seeOther url (toResponse ())




putCookieErrorMsg :: Text -> ServerPart ()
putCookieErrorMsg err =
  addCookies
    [ (,) (MaxAge 300) (mkCookie "error-msg" $ unpack err)
    ]

getCookieErrorMsg :: ServerPart (Maybe Text)
getCookieErrorMsg = do
  m <- optional $ lookCookieValue "error-msg"
  _ <- expireCookie "error-msg"
  return (pack <$> m)


putCookieOkMsg :: Text -> ServerPart ()
putCookieOkMsg msg =
  addCookies
    [ (,) (MaxAge 300) (mkCookie "ok-msg" $ unpack msg)
    ]

getCookieOkMsg :: ServerPart (Maybe Text)
getCookieOkMsg = do
  m <- optional $ lookCookieValue "ok-msg"
  _ <- expireCookie "ok-msg"
  return (pack <$> m)


--------------------------------------------------------------------------------
