{-# LANGUAGE     OverloadedStrings       #-}
{-# OPTIONS_GHC -fno-warn-type-defaults  #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}


module Base.Svg.Textbox where

import Data.List (intercalate)
import Text.Blaze.Svg11 ((!))
import Text.Blaze.Svg11 as S
import Text.Blaze.Svg11.Attributes as A

import Base.Svg.Arrows
import Base.Svg.Utils

--------------------------------------------------------------------------------

bold :: S.Svg
bold =
    g ! A.transform (translate 0.03 0) $ do
      S.path ! d leftPath
      S.path ! d topPath
      S.path ! d botPath
  where
    k1 = 0.40
    k2 = 0.12
    k3 = 2 * k2
    leftPath = mkPath $ do
      m  0   0
      l  k1  0
      l  k1  1
      l  0   1
      l  0   (1 - k2)
      q  k2  (1 - k2) k2  (1 - k3)
      l  k2  k3
      q  k2  k2  0   k2
      S.z
    ---------------------------------------
    k4 = k1 - 0.01
    k5 = 0.60
    topPath = mkPath $ do
      m   k4   0
      l   k5   0
      aa  0.25 0.25 0 True True k5 0.5
      l   k4   0.51
      l   k4   0.37
      aa  0.14 0.14 0 True False k4 0.17
      S.z
    ---------------------------------------
    k6 = 0.65
    botPath = mkPath $ do
      m   k4    0.45
      l   k6    0.5
      aa  0.30  0.25 0 True True k6 1
      l   k4    1
      l   k4    0.85
      l   0.50  0.85
      aa  0.18  0.12 0 True False 0.50 0.60
      l   k4    0.60
      S.z

--------------------------------------------------------------------------------

italic :: S.Svg
italic =
    do
      topLine
      midLine
      botLine
  where
    b = 0.8
    h = 0.15
    topLine =
      rect
        ! (width  .: b)
        ! (height .: h)
        ! (x .: (1 - b))
        ! (y .: 0)
    botLine =
      rect
        ! (width  .: b)
        ! (height .: h)
        ! (x .: 0)
        ! (y .: (1 - h))
    w = 0.32
    topMid = (1 - b) + b / 2
    botMid = b / 2
    topL = (,) (topMid - w/2) (    h - 0.01)
    topR = (,) (topMid + w/2) (    h - 0.01)
    botR = (,) (botMid + w/2) (1 - h + 0.01)
    botL = (,) (botMid - w/2) (1 - h + 0.01)
    ps =
      toValue
      $ intercalate " "
      $ map (\ (p1,p2) -> show p1 ++ "," ++ show p2)
        [topL, topR, botR, botL]
    midLine =
      polygon ! points ps


--------------------------------------------------------------------------------

link :: S.Svg
link =
    g ! A.transform (S.rotateAround 45 0.5 0.5)
      $ do
        topPart
        rotate180 topPart
  where
    topPart = S.path ! d topPath
    -----------------
    w1 = 0.2
    w2 = 0.09
    -----------------
    h1 = 0.5 - w2
    h2 = 0.2
    h3 = 0.15
    h4 = h3 - (w1 - w2)
    topPath = mkPath $ do
      m   (0.5 - w1)  (0.5 - h3)
      l   (0.5 - w1)  (0.5 - h1)
      aa  w1 w1 0 True True (0.5 + w1) (0.5 - h1)
      l   (0.5 + w1)  (0.5 - h4)
      aa  w1 w1 0 False True 0.5 (0.5 + h3)
      aa  ((h3 - h4)/2)   ((h3 - h4)/2) 0 False True 0.5 (0.5 + h4)
      aa  w2 w2 0 False False (0.5 + w2) (0.5 - h4)
      l   (0.5 + w2)  (0.5 - h1)
      aa  w2 w2 0 True False (0.5 - w2) (0.5 - h1)
      l   (0.5 - w2) (0.5 - h2)
      S.z

--------------------------------------------------------------------------------

image :: S.Svg
image =
    do
      imageFrame
      mountain
      sun
  where
    sun =
      circle
        ! (cx .: 0.75)
        ! (cy .: 0.25)
        ! (r  .: 0.12)
    -------------------------
    x = 0.06
    imageFrame =
      S.path
        ! d framePath
    framePath = mkPath $ do
      m 0 0
      l 1 0
      l 1 1
      l 0 1
      l 0 0
      m x     x
      l x     (1-x)
      l (1-x) (1-x)
      l (1-x) x
      l x     x
      S.z
    -------------------------
    mountain =
      S.path
        ! A.d mountainPath
    mountainPath = mkPath $ do
      m   0      1
      l   0.35   0.35
      l   0.5    0.65
      l   0.7    0.5
      l   1      1
      S.z

--------------------------------------------------------------------------------

video :: S.Svg
video =
    do
      topLeftCorner
      botRightCorner
      otherTwoCorners
      triangle
  where
    otherTwoCorners =
      horizontalMirrorSymmetry $
        g $ topLeftCorner >> botRightCorner
    botRightCorner = rotate180 topLeftCorner
    ---------------------------------
    topLeftCorner =
      S.path
        ! d boxPath
    y1 = 0.2
    y2 = 1 - y1
    w  = 1.618 * (y2 - y1)
    x1 = 0.5 - w/2
    boxPath = mkPath $ do
      m x1 0.5
      c (x1)
        (y1 + 0.15)
        (x1 )
        (y1 + 0.02)
        (x1 + 0.11)
        (y1)
      s (x1 + 0.20)
        (y1 - 0.01)
        (0.5)
        (y1 - 0.02)
      l 0.51 0.51
      S.z
    ---------------------------------
    triangle =
      polygon
        ! fill "#ffffff"
        ! points ps
    t = 0.41
    h = 0.26
    t1 = (,)  t      (0.5 - h/2)
    t2 = (,)  t      (0.5 + h/2)
    t3 = (,) (t + h) (0.5)
    ps =
      toValue
      $ intercalate " "
      $ map (\ (p1,p2) -> show p1 ++ "," ++ show p2)
        [t1, t2, t3]

--------------------------------------------------------------------------------

horizontalBars :: S.Svg
horizontalBars =
    do
      S.path ! d topLine
      S.path ! d midLine
      S.path ! d botLine
  where
    w  = 0.12
    x1 = 0.35
    x2 = 1
    y1 = 0.2
    y2 = 0.5
    y3 = 0.8
    topLine = mkPath $ do
      m x1 (y1 - w/2)
      l x2 (y1 - w/2)
      l x2 (y1 + w/2)
      l x1 (y1 + w/2)
      S.z
    midLine = mkPath $ do
      m x1 (y2 - w/2)
      l x2 (y2 - w/2)
      l x2 (y2 + w/2)
      l x1 (y2 + w/2)
      S.z
    botLine = mkPath $ do
      m x1 (y3 - w/2)
      l x2 (y3 - w/2)
      l x2 (y3 + w/2)
      l x1 (y3 + w/2)
      S.z


bulletList :: S.Svg
bulletList =
    do
      horizontalBars
      bullets
  where
    radius = 0.09
    x1 = 0.1
    y1 = 0.2
    y2 = 0.5
    y3 = 0.8
    bullets = do
      circle ! (cx .: x1) ! (cy .: y1) ! (r .: radius)
      circle ! (cx .: x1) ! (cy .: y2) ! (r .: radius)
      circle ! (cx .: x1) ! (cy .: y3) ! (r .: radius)


numberList :: Svg
numberList =
    do
      horizontalBars
      numbers
  where
    f x = x ! dominantBaseline "alphabetical"
            ! textAnchor "middle"
            ! fontFamily "Dancing Script"
            ! fontWeight "bold"
            ! fontSize   "0.3"
    x1 = 0.05
    y1 = 0.28
    y2 = 0.58
    y3 = 0.88
    numbers = do
      f $ S.text_ "1" ! (A.x .: x1) ! (A.y .: y1)
      f $ S.text_ "2" ! (A.x .: x1) ! (A.y .: y2)
      f $ S.text_ "3" ! (A.x .: x1) ! (A.y .: y3)

--------------------------------------------------------------------------------

header :: S.Svg
header =
    do
      S.path ! d line1
      S.path ! d line2
      S.path ! d line3 ! opacity "0.4"
      S.path ! d line4 ! opacity "0.4"
      S.path ! d line5 ! opacity "0.4"
  where
    l1 = 0.05
    l2 = 0.25
    r2 = 0.75
    r1 = 0.95
    h = 1/6
    w = 0.1
    line1 = mkPath $ do
       m l1 (1*h - w/2)
       l r1 (1*h - w/2)
       l r1 (1*h + w/2)
       l l1 (1*h + w/2)
       S.z
    line2 = mkPath $ do
       m l2 (2*h - w/2)
       l r2 (2*h - w/2)
       l r2 (2*h + w/2)
       l l2 (2*h + w/2)
       S.z
    line3 = mkPath $ do
       m l1 (3*h - w/2)
       l r1 (3*h - w/2)
       l r1 (3*h + w/2)
       l l1 (3*h + w/2)
       S.z
    line4 = mkPath $ do
       m l2 (4*h - w/2)
       l r2 (4*h - w/2)
       l r2 (4*h + w/2)
       l l2 (4*h + w/2)
       S.z
    line5 = mkPath $ do
       m l1 (5*h - w/2)
       l r1 (5*h - w/2)
       l r1 (5*h + w/2)
       l l1 (5*h + w/2)
       S.z


horizontalRule :: S.Svg
horizontalRule =
    do
      S.path ! d line1 ! opacity "0.4"
      S.path ! d line2 ! opacity "0.4"
      line3
      S.path ! d line4 ! opacity "0.4"
      S.path ! d line5 ! opacity "0.4"
  where
    l1 = 0.05
    l2 = 0.25
    r2 = 0.75
    r1 = 0.95
    h = 1/6
    w = 0.1
    line1 = mkPath $ do
       m l1 (1*h - w/2)
       l r1 (1*h - w/2)
       l r1 (1*h + w/2)
       l l1 (1*h + w/2)
       S.z
    line2 = mkPath $ do
       m l2 (2*h - w/2)
       l r2 (2*h - w/2)
       l r2 (2*h + w/2)
       l l2 (2*h + w/2)
       S.z
    square leftX =
      S.rect
        ! (A.x    .: leftX)
        ! (A.y    .: 3*h - w/2)
        ! (width  .: w)
        ! (height .: w)
    line3 = do
      square l1
      square l2
      square (0.5 - w/2)
      square (r2 - w)
      square (r1 - w)
    line4 = mkPath $ do
       m l2 (4*h - w/2)
       l r2 (4*h - w/2)
       l r2 (4*h + w/2)
       l l2 (4*h + w/2)
       S.z
    line5 = mkPath $ do
       m l1 (5*h - w/2)
       l r1 (5*h - w/2)
       l r1 (5*h + w/2)
       l l1 (5*h + w/2)
       S.z


--------------------------------------------------------------------------------

undo :: S.Svg
undo =
  curvyArrowLeft


redo :: S.Svg
redo =
  curvyArrowRight


--------------------------------------------------------------------------------

questionMark :: S.Svg
questionMark =
    do
      topArm
      dot
  where
    (a1,a2) = ( 0.40 ,  0.40 )
    (b1,b2) = ( 0.30 ,  0.45 )
    (c1,c2) = ( 0.25 ,  0.40 )
    (d1,d2) = ( 0.25 ,  0.25 )
    (e1,e2) = ( 0.25 ,  0.15 )
    (f1,f2) = ( 0.25 ,  0.10 )
    (g1,g2) = ( 0.50 ,  0.10 )
    (h1,h2) = ( 0.60 ,  0.10 )
    (i1,i2) = ( 0.80 ,  0.10 )
    (j1,j2) = ( 0.80 ,  0.25 )
    (k1,k2) = ( 0.80 ,  0.45 )
    (l1,l2) = ( 0.40 ,  0.50 )
    (m1,m2) = ( 0.50 ,  0.68 )
    topArm =
      S.path
        ! d topArmPath
        ! stroke "#696969"
        ! (strokeWidth .: 0.13)
        ! fill "none"
        ! strokeLinecap "round"
    topArmPath = S.mkPath $ do
      m a1 a2
      c b1 b2 c1 c2 d1 d2
      c e1 e2 f1 f2 g1 g2
      c h1 h2 i1 i2 j1 j2
      c k1 k2 l1 l2 m1 m2
    dot = S.circle
      ! (cx .: 0.5)
      ! (cy .: 0.9)
      ! (r  .: 0.08)
      ! fill "#696969"

--------------------------------------------------------------------------------
