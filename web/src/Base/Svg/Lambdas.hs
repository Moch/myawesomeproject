{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}


module Base.Svg.Lambdas where

import           Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
import qualified Text.Blaze.Svg11.Attributes as A


--------------------------------------------------------------------------------

curvyLambda :: S.Svg
curvyLambda = do
    arm
    leftLeg
    rightLeg
  where
    --------------------------------
    (a1,a2) = (,)  0.312  0.981
    (b1,b2) = (,)  0.186  a2
    (c1,c2) = (,)  0.506  0.294
    (d1,d2) = (,)  0.544  0.451
    leftLeg = S.path
      ! A.d    leftLegPath
    leftLegPath = S.mkPath $ do
      S.m a1 a2
      S.l b1 b2
      S.l c1 c2
      S.l d1 d2
      S.z
    --------------------------------
    (e1,e2) = (,)  0.618  0.77
    (f1,f2) = (,)  0.663  0.932
    (g1,g2) = (,)  0.805  0.945
    (h1,h2) = (,)  0.814  0.788
    (i1,i2) = (,)  0.838  h2
    (j1,j2) = (,)  0.855  1.06
    (k1,k2) = (,)  0.615  1.06
    (l1,l2) = (,)  0.571  0.80
    (m1,m2) = (,)  0.473  0.363
    rightLeg = S.path
      ! A.d    rightLegPath
    rightLegPath = S.mkPath $ do
      S.m d1 d2
      S.l e1 e2
      S.c f1 f2 g1 g2 h1 h2
      S.l i1 i2
      S.c j1 j2 k1 k2 l1 l2
      S.l m1 m2
      S.z
    --------------------------------
    (n1,n2) = (,)  0.44   0.07
    (o1,o2) = (,)  0.265  0.07
    (p1,p2) = (,)  0.238  0.249
    (q1,q2) = (,)  0.213  p2
    (r1,r2) = (,)  0.215  (-0.03)
    (s1,s2) = (,)  0.429  (-0.03)
    (t1,t2) = (,)  0.48   0.17
    arm = S.path
      ! A.d    armPath
    armPath = S.mkPath $ do
      S.m m1 m2
      S.c n1 n2 o1 o2 p1 p2
      S.l q1 q2
      S.c r1 r2 s1 s2 t1 t2
      S.l d1 d2
      S.z

--------------------------------------------------------------------------------
