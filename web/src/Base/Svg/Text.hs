{-# LANGUAGE     OverloadedStrings       #-}
{-# OPTIONS_GHC -fno-warn-type-defaults  #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}


module Base.Svg.Text where

import Text.Blaze.Svg11 ((!))
import Text.Blaze.Svg11 as S
import Text.Blaze.Svg11.Attributes as A

import Base.Svg.Utils

--------------------------------------------------------------------------------

bigHorizontalBars :: S.Svg
bigHorizontalBars = do
    topLine
    midLine
    botLine
  where
    yTop = 0.25
    yMid = 0.50
    yBot = 0.75
    xMin = 0.00
    xMax = 1 - xMin
    styleLine x = x ! A.stroke "#ffffff" ! (A.strokeWidth .: 0.10)
    -------------------------
    (a1,a2) = (xMin , yTop)
    (b1,b2) = (xMax , yTop)
    lineAB  = S.mkPath $ S.m a1 a2 >> S.l b1 b2
    topLine = styleLine $ S.path
      ! A.d lineAB
    -------------------------
    (c1,c2) = (xMin , yMid)
    (d1,d2) = (xMax , yMid)
    lineCD  = S.mkPath $ S.m c1 c2 >> S.l d1 d2
    midLine = styleLine $ S.path
      ! A.d lineCD
    -------------------------
    (e1,e2) = (xMin , yBot)
    (f1,f2) = (xMax , yBot)
    lineEF  = S.mkPath $ S.m e1 e2 >> S.l f1 f2
    botLine = styleLine $ S.path
      ! A.d lineEF

--------------------------------------------------------------------------------

bigX :: Svg
bigX =
    S.path
      ! d directions
      ! fill "none"
      ! (strokeWidth .: (2*w))
      ! strokeLinecap "round"
  where
    w = 0.1
    k = 0.11
    directions = mkPath $ do
      m   k     k
      l   (1-k) (1-k)
      m   (1-k) k
      l   k     (1-k)


--------------------------------------------------------------------------------

tick :: Svg
tick =
    S.path
      ! d directions
      ! fill "none"
      ! (strokeWidth .: (2*w))
      ! strokeLinecap  "round"
      ! strokeLinejoin "round"
  where
    w = 0.1
    k = 0.11
    directions = mkPath $ do
      m   k     0.5
      l   0.3   0.85
      l   (1-k) k

--------------------------------------------------------------------------------

exclamationSignal :: Svg
exclamationSignal = do
    triangle
    stick
    dot
  where
    k1 = 0.1
    k2 = 0.06
    k3 = 0.035
    h1 = (1 - 2*k1)
    y1 = 0.4
    y2 = 0.65
    y3 = 0.78
    triangle =
      S.path
        ! d trianglePath
        ! (strokeWidth .: 1.5 * k1)
        ! strokeLinejoin "round"
    trianglePath = mkPath $ do
      m   0.5    (1 - k1 - h1)
      l   k1     (1-k1)
      l   (1-k1) (1-k1)
      S.z
    stick =
      S.path
        ! d stickPath
        ! fill "white"
        ! (strokeWidth .: 0)
    stickPath = mkPath $ do
      m   (0.5 - k3)  y2
      l   (0.5 + k3)  y2
      l   (0.5 + k2)  y1
      aa  k2  k2  0  True  False  (0.5 - k2)  y1
      S.z
    dot =
      S.circle
        ! (cx .: 0.5)
        ! (cy .: y3)
        ! (r  .: k2)
        ! fill "white"
        ! (strokeWidth .: 0)


--------------------------------------------------------------------------------


abcBubble :: Svg
abcBubble = do
    bubble
    abc
  where
    bubble =
      S.path
        ! d              bubblePath
        ! fill           "none"
        ! stroke         "indigo"
        ! strokeLinejoin "round"
        ! (strokeWidth .: 0.03)
    bubblePath =
      mkPath $ do
        m   0.22  0.81
        aa  0.47  0.39  0  True  True   0.5   0.89
        l   0.20  0.95
        S.z
    ---------------------------------------------
    abc =
      S.text_ "A B C"
        ! A.x          "0.5"
        ! A.y          "0.6"
        ! A.fill       "indigo"
        ! A.stroke     "none"
        ! A.textAnchor "middle"
        ! A.fontSize   "0.22"
        ! A.fontFamily "Arial"


--------------------------------------------------------------------------------
