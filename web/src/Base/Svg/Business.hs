{-# LANGUAGE     OverloadedStrings       #-}
{-# OPTIONS_GHC -fno-warn-type-defaults  #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}


module Base.Svg.Business where

import Data.Monoid ((<>))
import Text.Blaze.Svg11 ((!))
import Text.Blaze.Svg11 as S
import Text.Blaze.Svg11.Attributes as A

import Base.Svg.Utils

--------------------------------------------------------------------------------

analytics :: Svg
analytics = do
    coordinateAxes
    leftBar
    centerBar
    rightBar
  where
    axesW = 0.025
    coordinateAxes =
      S.path
        ! d axesPath
        ! (strokeWidth .: 2*axesW)
        ! fill "none"
    axesPath = mkPath $ do
      m   axesW   0
      l   axesW   (1 - axesW)
      l   1       (1 - axesW)
    -------------------------------------
    barW = 0.1
    h1 = 0.45
    h2 = 0.65
    h3 = 0.85
    x1 = 0.25
    x2 = 0.5
    x3 = 0.75
    bar p =
      S.path
        ! d p
        ! (strokeWidth .: 2*barW)
        ! fill "none"
    leftBar   = bar $ mkPath $ m x1 1 >> l x1 (1 - h1)
    centerBar = bar $ mkPath $ m x2 1 >> l x2 (1 - h2)
    rightBar  = bar $ mkPath $ m x3 1 >> l x3 (1 - h3)


--------------------------------------------------------------------------------

envelope :: Svg
envelope =
    S.path
      ! d envelopePath
      ! fill "none"
      ! (strokeWidth .: 2*s)
      ! strokeLinejoin "round"
  where
    s = 0.03         -- half-width of the stroke
    h = 0.618 - 2*s  -- height of the whole envelope
    k = 0.58         -- y-coordinate of the middle point
    ohShit = (/)
      (0.309 - 2*s + 2*k*s - 0.618*s + 4*s*s - 4*k*s*s)
      (2*k - 2*s - 0.382 - 4*k*s + 4*s*s + 0.764*s)
    (a1,a2) = (,)  s      ((1-h)/2)      -- top left corner
    (b1,b2) = (,)  s      (1 - (1-h)/2)  -- bottom left corner
    (c1,c2) = (,)  (1-s)  (1 - (1-h)/2)  -- bottom right corner
    (d1,d2) = (,)  (1-s)  ((1-h)/2)      -- top right corner
    (e1,e2) = (,)  0.5    k              -- middle point
    (f1,f2) = (,)  ohShit 0.5
    (g1,g2) = (,)  (1-f1) 0.5
    envelopePath = mkPath $ do
      m   a1 a2
      l   b1 b2
      l   c1 c2
      l   d1 d2
      l   a1 a2
      l   e1 e2
      l   d1 d2
      m   b1 b2
      l   f1 f2
      m   c1 c2
      l   g1 g2


--------------------------------------------------------------------------------

pencil :: Svg
pencil =
    S.path
      ! d pencilPath
      ! (strokeWidth .: 2*s)
      ! strokeLinejoin "round"
      ! fill "none"
  where
    s  = 0.008  -- half-width of the stroke
    w  = 0.1   -- width of the pencil
    x1 = 0.5 - w/2
    x2 = 0.5 + w/2
    y1 = 0.3
    y2 = 0.34
    y3 = 0.36
    y4 = 0.6
    y5 = y6 - 0.04
    y6 = 0.7
    pencilPath = mkPath $ do
      m   0.5  y5
      l   0.5  y6
      l   x1   y4
      l   x2   y4
      l   0.5  y6
      m   x1   y4
      l   x1   y3
      l   x2   y3
      l   x2   y4
      m   0.5  y4
      l   0.5  y3
      m   x1   y3
      l   x1   y2
      l   x2   y2
      l   x2   y3
      m   x1   y2
      l   x1   y1
      m   x2   y1
      l   x2   y2
      m   x1   y1
      aa  (w/2) 0.03 0 True True x2 y1



documentWithPencil :: Svg
documentWithPencil = do
    positionedPencil
    paperBorder
    textLines
    xMark
  where
    positionedPencil =
      pencil
        ! transform ( (translate 0.31 0.19) <> (rotateAround 45 0.5 0.5) )
    --------------------------------------------------
    paperW = 0.72 - paperS
    paperS = 0.025
    paperBorder =
      S.path
        ! (strokeWidth .: 2*paperS)
        ! strokeLinejoin "round"
        ! strokeLinecap  "round"
        ! fill "none"
        ! d paperPath
    paperPath = mkPath $ do
      m   (0.5 + paperW/2)  0.42
      l   (0.5 + paperW/2)  paperS
      l   (0.5 - paperW/2)  paperS
      l   (0.5 - paperW/2)  (1-paperS)
      l   (0.5 + paperW/2)  (1-paperS)
      l   (0.5 + paperW/2)  0.85
    --------------------------------------------------
    textLines =
      S.path
        ! (strokeWidth .: 0.04)
        ! fill "none"
        ! d textPath
    x1 = 0.28
    x2 = 1 - x1
    y1 = 0.2
    y2 = 0.35
    y3 = 0.5
    textPath = mkPath $ do
      m  x1  y1   >>   l  x2  y1
      m  x1  y2   >>   l  x2  y2
      m  x1  y3   >>   l  x2  y3
    --------------------------------------------------
    mX = 0.33
    mY = 0.71
    k  = 0.1
    xMark =
      S.path
        ! (strokeWidth .: 0.04)
        ! strokeLinecap "round"
        ! fill "none"
        ! d xMarkPath
    xMarkPath = mkPath $ do
      m  mX        mY
      l  (mX + k)  (mY + k)
      m  (mX + k)  mY
      l  mX        (mY + k)



--------------------------------------------------------------------------------

bullseye :: Svg
bullseye = do
    board
    arrow
  where
    distanceToCenter x y = distance (x,y) (0.5,0.5)
    (p1,k1) = (,)  0.2   0.1
    (p2,k2) = (,)  0.28  0.07
    (p3,k3) = (,)  0.36  0.07
    (p4,k4) = (,)  0.44  0.05
    d1 = distanceToCenter (p1 + k1) (p1 - k1)
    d2 = distanceToCenter (p2 + k2) (p2 - k2)
    d3 = distanceToCenter (p3 + k3) (p3 - k3)
    d4 = distanceToCenter (p4 + k4) (p4 - k4)
    board =
      S.path
        ! d circles
        ! (strokeWidth .: 0.03)
        ! strokeLinecap "round"
        ! fill "none"
    circles = mkPath $ do
      m                         (p1 + k1) (p1 - k1)
      aa  d1 d1  0  True  True  (p1 - k1) (p1 + k1)
      m                         (p2 + k2) (p2 - k2)
      aa  d2 d2  0  True  True  (p2 - k2) (p2 + k2)
      m                         (p3 + k3) (p3 - k3)
      aa  d3 d3  0  True  True  (p3 - k3) (p3 + k3)
      m                         (p4 + k4) (p4 - k4)
      aa  d4 d4  0  True  True  (p4 - k4) (p4 + k4)
    --------------------------------------------------
    fl = 0.1   -- feather length
    q1 = 0.12
    q2 = 0.16
    q3 = 0.20
    arrow =
      S.path
        ! d (mkPath $ stick >> feathers)
        ! (strokeWidth .: 0.03)
        ! strokeLinecap "round"
        ! fill "none"
    stick = do
      m   q1    q1
      l   0.495 0.495
    feathers = do
      m   q1         q1
      l   q1         (q1 - fl)
      m   q1         q1
      l   (q1 - fl)  q1
      m   q2         q2
      l   q2         (q2 - fl)
      m   q2         q2
      l   (q2 - fl)  q2
      m   q3         q3
      l   q3         (q3 - fl)
      m   q3         q3
      l   (q3 - fl)  q3


--------------------------------------------------------------------------------

circlesWithConnections :: Svg
circlesWithConnections = do
    strokedCircle x0 y0 r0
    strokedCircle x1 y1 r1
    strokedCircle x2 y2 r2
    strokedCircle x3 y3 r3
    strokedCircle x4 y4 r4
    strokedCircle x5 y5 r5
    strokedCircle x6 y6 r6
    strokedCircle x7 y7 r7
    strokedCircle x8 y8 r8
    strokedCircle x9 y9 r9
    S.path
      ! fill "none"
      ! (strokeWidth .: 0.015)
      ! d (mkPath connectingLines)
  where
    rad1 = 0.08
    rad2 = 0.04
    (x0,y0,r0) = (,,)  0.5   0.5   0.15
    (x1,y1,r1) = (,,)  0.18  0.5   rad1
    (x2,y2,r2) = (,,)  0.78  0.3   rad1
    (x3,y3,r3) = (,,)  0.78  0.7   rad1
    (x4,y4,r4) = (,,)  0.18  0.27  rad2
    (x5,y5,r5) = (,,)  0.09  0.70  rad2
    (x6,y6,r6) = (,,)  0.30  0.85  rad2
    (x7,y7,r7) = (,,)  0.55  0.13  rad2
    (x8,y8,r8) = (,,)  0.90  0.10  rad2
    (x9,y9,r9) = (,,)  0.78  0.90  rad2
    strokedCircle c1 c2 radius =
      circle
        ! fill "none"
        ! (strokeWidth .: 0.025)
        ! (cx .: c1) ! (cy .: c2) ! (r .: radius)
    --------------------------------------------------
    connect (p1,p2,radius1) (q1,q2,radius2) =
      let
        d = distance (p1,p2) (q1,q2)
        k1 = radius1 / d
        k2 = radius2 / d
      in do
        m  (k2*p1 + q1 - k2*q1)  (k2*p2 + q2 - k2*q2)
        l  (p1 - k1*p1 + k1*q1)  (p2 - k1*p2 + k1*q2)
    connectingLines = do
      connect (x0,y0,r0) (x1,y1,r1)
      connect (x0,y0,r0) (x2,y2,r2)
      connect (x0,y0,r0) (x3,y3,r3)
      connect (x1,y1,r1) (x4,y4,r4)
      connect (x1,y1,r1) (x5,y5,r5)
      connect (x1,y1,r1) (x6,y6,r6)
      connect (x2,y2,r2) (x7,y7,r7)
      connect (x2,y2,r2) (x8,y8,r8)
      connect (x3,y3,r3) (x9,y9,r9)



--------------------------------------------------------------------------------


pin :: Svg
pin =
    S.path
      ! A.fill "none"
      ! (A.strokeWidth .: 0.05)
      ! A.strokeLinejoin "arcs"
      ! A.strokeMiterlimit "8"
      ! A.d (mkPath $ topPath >> bodyPath >> needlePath)
      ! A.transform (S.rotateAround 45 0.5 0.5)
  where
    w1 = 0.13
    w2 = 0.04
    y1 = 0.05
    y2 = 0.15
    y3 = 0.45
    y4 = 0.58
    y5 = 0.8
    y6 = 1.03
    r1 = (y2 - y1) / 2
    r2 = (y4 - y3)
    topPath = do
      m   (0.5 - w1)  y1
      aa  r1 r1 0 True False (0.5 - w1) y2
      l   (0.5 + w1)  y2
      aa  r1 r1 0 True False (0.5 + w1) y1
      l   (0.5 - w1)  y1
    bodyPath = do
      m   (0.5 - w1)  y2
      l   (0.5 - w1)  y3
      aa  r2 r2 0 False False (0.5 - w1 - r2) y4
      l   (0.5 + w1 + r2) y4
      aa  r2 r2 0 False False (0.5 + w1) y3
      l   (0.5 + w1)  y2
    needlePath = do
      m   (0.5 - w2)  y4
      l   (0.5 - w2)  y5
      l   0.5         y6
      l   (0.5 + w2)  y5
      l   (0.5 + w2)  y4



--------------------------------------------------------------------------------


briefcase :: Svg
briefcase = do
    handle
    topPart
    midPart
    botPart
    button
  where
    x1 = 0.05
    x2 = 0.35
    x3 = 1 - x2
    x4 = 1 - x1
    y1 = 0.15
    y2 = 0.25
    y3 = 0.5
    y4 = y3 + 0.02
    y5 = 0.85
    r0 = 0.08
    r1x = 0.03
    r1y = 0.05
    r2x = r1x + (y4 - y3)
    r2y = r1y + (y4 - y3)
    ----------------------------------------
    handle =
      S.path
        ! A.strokeWidth "0.08"
        ! A.fill "none"
        ! A.strokeLinejoin "round"
        ! A.d handlePath
    handlePath = mkPath $ do
      m   x2   (y2 + 0.1)
      l   x2   y1
      l   x3   y1
      l   x3   (y2 + 0.1)
    ----------------------------------------
    topPart =
      S.path
        ! A.stroke "none"
        ! A.d topPartPath
    topPartPath = mkPath $ do
      m   x1          (y3 - r2y)
      l   x1          (y2 + r0)
      aa  r0  r0  0   False  True  (x1 + r0)  y2
      l   (x4 - r0)   y2
      aa  r0  r0  0   False  True  x4  (y2 + r0)
      l   x4          (y3 - r2y)
      S.z
    ----------------------------------------
    midPart =
      S.path
        ! A.stroke "none"
        ! A.strokeWidth "0"
        ! A.d midPartPath
    midPartPath = mkPath $ do
      m   x1           (y3 - r2y - 0.01)
      l   x4           (y3 - r2y - 0.01)
      l   x4           y3
      l   (0.5 + r2x)  y3
      aa  r2x  r2y  0  True  False  (0.5 - r2x)  y3
      l   x1           y3
      l   x1           (y3 - r2y - 0.01)
      m   x1           y4
      l   (0.5 - r2x)  y4
      aa  r2x  r2y  0  True  False  (0.5 + r2x)  y4
      l   x4           y4
      l   x4           (y4 + r2y + 0.01)
      l   x1           (y4 + r2y + 0.01)
      l   x1           y4
    ----------------------------------------
    botPart =
      S.path
        ! A.stroke "none"
        ! A.strokeLinejoin "round"
        ! A.d botPartPath
    botPartPath = mkPath $ do
      m   x1         (y4 + r2y)
      l   x1         (y5 - r0)
      aa  r0         r0   0  False  False  (x1 + r0)  y5
      l   (x4 - r0)  y5
      aa  r0         r0   0  False  False  x4  (y5 - r0)
      l   x4         (y4 + r2y)
      S.z
    ----------------------------------------
    button =
      S.ellipse
        ! (A.cx .: 0.5)
        ! (A.cy .: (y3 + y4)/2 )
        ! (A.rx .: r1x)
        ! (A.ry .: r1y)
        ! A.stroke "none"
    ----------------------------------------



--------------------------------------------------------------------------------
