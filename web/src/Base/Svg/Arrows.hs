{-# LANGUAGE     OverloadedStrings       #-}
{-# OPTIONS_GHC -fno-warn-type-defaults  #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}


module Base.Svg.Arrows where

import Text.Blaze.Svg11 ((!))
import Text.Blaze.Svg11 as S
import Text.Blaze.Svg11.Attributes as A

import Base.Svg.Utils

--------------------------------------------------------------------------------


curvyArrowLeft :: Svg
curvyArrowLeft =
  S.path ! d curvyArrowLeftPath
         ! stroke "none"


curvyArrowRight :: Svg
curvyArrowRight =
  horizontalMirrorSymmetry $
    S.path ! d curvyArrowLeftPath


curvyArrowLeftPath :: AttributeValue
curvyArrowLeftPath =
    mkPath $ do
      m   a1  a2
      aa  r1  r1  0 False False b1 b2
      l   d1  d2
      l   f1  f2
      l   e1  e2
      l   c1  c2
      aa  r2  r2  0 False True a1 a2
  where
    k1 = 0.45
    r1 = 0.43
    r2 = 0.305
    (a1,a2) = ( 0.55 , 1    )
    (b1,b2) = ( k1   , 0.15 )
    (c1,c2) = ( k1   , 0.40 )
    (d1,d2) = ( k1   , 0.00 )
    (e1,e2) = ( k1   , 0.55 )
    (f1,f2) = ( 0.15 , (b2+c2)/2)


--------------------------------------------------------------------------------


triangle :: Svg
triangle =
    S.path
      ! (strokeWidth .: 0)
      ! d directions
  where
    h = 0.5 * sqrt 3   -- height of equilateral triangle of base 1
    y = (1 - h) / 2
    directions = mkPath $ do
      m   0    (1 - y)
      l   1    (1 - y)
      l   0.5  y


triangleUp :: Svg
triangleUp =
  triangle


triangleRight :: Svg
triangleRight =
  triangle ! A.transform (S.rotateAround 90 0.5 0.5)


triangleDown :: Svg
triangleDown =
  triangle ! A.transform (S.rotateAround 180 0.5 0.5)


triangleLeft :: Svg
triangleLeft =
  triangle ! A.transform (S.rotateAround 270 0.5 0.5)


--------------------------------------------------------------------------------


longArrow :: S.Svg
longArrow = do
    arrowBeam
    arrowHead
  where
    s = 0.03
    arrowBeam =
      S.path
        ! (strokeWidth .: 0)
        ! d beamDirs
    beamDirs =
      mkPath $ do
        m   (0.5 - s)  s
        l   (0.5 - s)  (1-s)
        aa  s  s  0  True  False  (0.5 + s)  (1-s)
        l   (0.5 + s)  s
        aa  s  s  0  True  False  (0.5 - s)  s
    arrowHead =
      S.path
        ! (strokeWidth .: 0)
        ! strokeLinejoin "round"
        ! d headDirs
    headDirs =
      mkPath $ do
        m   (0.5 - s)   s
        q   0.42  0.12  0.25  0.25
        l   0.5   0.17
        l   0.75  0.25
        q   0.58  0.12  (0.5 + s)   s



longArrowUp :: Svg
longArrowUp =
  longArrow


longArrowRight :: Svg
longArrowRight =
  longArrow ! A.transform (S.rotateAround 90 0.5 0.5)


longArrowDown :: Svg
longArrowDown =
  longArrow ! A.transform (S.rotateAround 180 0.5 0.5)


longArrowLeft :: Svg
longArrowLeft =
  longArrow ! A.transform (S.rotateAround 270 0.5 0.5)


--------------------------------------------------------------------------------
