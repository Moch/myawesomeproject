{-# LANGUAGE     OverloadedStrings       #-}
{-# OPTIONS_GHC -fno-warn-type-defaults  #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}


module Base.Svg.Religion where

import Text.Blaze.Svg11 ((!))
import Text.Blaze.Svg11 as S
import Text.Blaze.Svg11.Attributes as A

import Base.Svg.Utils


--------------------------------------------------------------------------------


cruzLatina :: Svg
cruzLatina =
    do
      defs ovalGradient
      oval
      horizontalLine
      verticalLine
  where
    s = 0.1
    h = 0.8
    w = 3 * h / 4
    horizontalLine =
      S.path
        ! A.d horizontalDirs
        ! (strokeWidth .: s)
        ! stroke "brown"
    horizontalDirs =
      S.mkPath $ do
        m   (0.5 - w/2)  (0.5 - h/2 + w/2)
        l   (0.5 + w/2)  (0.5 - h/2 + w/2)
    verticalLine =
      S.path
        ! A.d verticalDirs
        ! (strokeWidth .: s)
        ! stroke "brown"
    verticalDirs =
      S.mkPath $ do
        m   0.5  (0.5 - h/2)
        l   0.5  (0.5 + h/2)
    oval =
      S.path
        ! A.strokeWidth "0"
        ! A.stroke      "gold"
        ! A.fill        "url(#svg-mandoria)"
        ! A.d           ovalDirs
    ovalDirs =
      S.mkPath $ do
        m   0.5  0
        aa  0.4  0.5  0  True False  0.5  1
        aa  0.4  0.5  0  True False  0.5  0
        S.z
    ovalGradient =
      radialgradient
        ! A.spreadmethod "reflect"
        ! A.cx "50%"
        ! A.cy "50%"
        ! A.r  "100%"
        ! A.fx "50%"
        ! A.fy "0%"
        ! A.id_ "svg-mandoria"
        $ do
          stop
            ! A.offset "0%"
            ! A.style  "stop-color: rgba(255,255,255,1)"
          stop
            ! A.offset "30%"
            ! A.style  "stop-color: rgba(255,215,  0,1)"
          stop
            ! A.offset "50%"
            ! A.style  "stop-color: rgba(255,255,190,1)"
          stop
            ! A.offset "70%"
            ! A.style  "stop-color: rgba(255,255,  0,1)"
          stop
            ! A.offset "80%"
            ! A.style  "stop-color: rgba(255,255,255,1)"


--------------------------------------------------------------------------------


decoradoVolutas :: Svg
decoradoVolutas =
    do
      volutas
      volutas ! A.transform (S.rotateAround  90 0.5 0.5)
      volutas ! A.transform (S.rotateAround 180 0.5 0.5)
      volutas ! A.transform (S.rotateAround 270 0.5 0.5)
  where
    s = 0.01
    horizontalLine =
      S.path
        ! A.d (mkPath $ m 0.5 0 >> l 0.5 0.5)
        ! (strokeWidth .: 2*s)
        ! stroke "gold"
    volutas =
      g $ do
        horizontalLine
        voluta1
    voluta1 =
      S.path
        ! (strokeWidth .: 2*s)
        ! strokeLinecap "round"
        ! stroke        "gold"
        ! fill          "none"
        ! A.d           voluta1'
    voluta1' =
      mkPath $ do
        m  0.5  0.35
        aa 0.15 0.15  0  True  True  0.45 0.4


--------------------------------------------------------------------------------
