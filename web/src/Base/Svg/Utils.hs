{-# LANGUAGE    OverloadedStrings        #-}
{-# OPTIONS_GHC -fno-warn-type-defaults  #-}


module Base.Svg.Utils where

import Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
import qualified Text.Blaze.Svg11.Attributes as A


--------------------------------------------------------------------------------

infixl 5 .:
(.:) :: (S.AttributeValue -> S.Attribute ) -> Float -> S.Attribute
f .: x = f $ S.toValue x

--------------------------------------------------------------------------------

stdSvg :: S.Svg -> S.Svg
stdSvg = S.svg
  ! A.viewbox "0 0 1 1"
  ! A.preserveaspectratio "xMinYMin meet"


devModeSvg :: S.Svg -> S.Svg
devModeSvg = S.svg
  ! A.viewbox "0 0 1 1"
  ! A.preserveaspectratio "xMinYMin meet"
  ! A.stroke "#000"
  ! A.fill   "#000"

--------------------------------------------------------------------------------

backgroundCircle :: S.AttributeValue -> S.Svg
backgroundCircle col =
  S.circle
    ! (A.cx .: 0.5)
    ! (A.cy .: 0.5)
    ! (A.r  .: 0.5)
    ! A.stroke "none"
    ! A.fill col


squareFrame :: S.Svg   -- For development
squareFrame =
    S.path
      ! A.stroke "#000"
      ! (A.strokeWidth .: 0.005)
      ! A.fill "none"
      ! A.d framePath
  where
    framePath = S.mkPath $ do
      S.m 0 0
      S.l 1 0
      S.l 1 1
      S.l 0 1
      S.z


centeredAxes :: S.Svg
centeredAxes =
    S.path
      ! A.stroke "#000"
      ! (A.strokeWidth .: 0.001)
      ! A.fill "none"
      ! A.d axesPath
  where
    axesPath = S.mkPath $ do
      S.m 0.5 0
      S.l 0.5 1
      S.m 0   0.5
      S.l 1   0.5


{-
circleFrame :: S.Svg
circleFrame =
    S.circle
      ! (A.strokeWidth .: 2*w)
      ! A.fill "none"
      ! (A.cx .: 0.5)
      ! (A.cy .: 0.5)
      ! (A.r  .: 0.5 - w)
  where
    w = 0.005
-}

--------------------------------------------------------------------------------

distance :: (Float,Float) -> (Float,Float) -> Float
distance (x1,y1) (x2,y2) = sqrt $ (x2-x1)^2 + (y2-y1)^2


rotate180 :: S.Svg -> S.Svg
rotate180 s = s ! A.transform (S.rotateAround 180 0.5 0.5)

horizontalMirrorSymmetry :: S.Svg -> S.Svg
horizontalMirrorSymmetry s =
  s ! A.transform (S.matrix (-1) 0 0 1 1 0)

verticalMirrorSymmetry :: S.Svg -> S.Svg
verticalMirrorSymmetry s =
  s ! A.transform (S.matrix 1 0 0 (-1) 0 1)

--------------------------------------------------------------------------------
