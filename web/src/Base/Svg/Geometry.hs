{-# LANGUAGE     OverloadedStrings       #-}
{-# OPTIONS_GHC -fno-warn-type-defaults  #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}


module Base.Svg.Geometry where

import Text.Blaze.Svg11 ((!))
import Text.Blaze.Svg11 as S
import Text.Blaze.Svg11.Attributes as A

import Base.Svg.Utils

--------------------------------------------------------------------------------

infinity :: Svg
infinity = do
    topLeftPart
    bottomLeftPart
    rightPart
  where
    bottomLeftPart =
      verticalMirrorSymmetry topLeftPart
    rightPart =
      horizontalMirrorSymmetry $
        g $ topLeftPart >> bottomLeftPart
    topLeftPart =
      S.path
        ! d topLeftPath
        ! (strokeWidth .: (2*w))
        ! fill "none"
    w = 0.05
    r = 0.2 - w
    topLeftPath = mkPath $ do
      m   w 0.51
      l   w 0.5
      aa  r r 0 False True (r + w) (0.5 - r)
      q   (r + w + 0.1) (0.5 - r) 0.5 0.5


--------------------------------------------------------------------------------

octagon :: Svg
octagon =
    S.path
      ! d octagonPath
      ! fill "#ffffff"
      ! (strokeWidth .: (2*w))
  where
    w = 0.01
    p1 = (1 - 2*w) / (2 + sqrt 2)
    p2 = (1 - 2*w) - p1
    octagonPath = mkPath $ do
      m   p1       w
      l   p2       w
      l   (1 - w)  p1
      l   (1 - w)  p2
      l   p2       (1 - w)
      l   p1       (1 - w)
      l   w        p2
      l   w        p1
      S.z


--------------------------------------------------------------------------------

regularPolygon :: Int -> Float -> Svg
regularPolygon n w =
    S.path
      ! d directions
      ! fill "#ffffff"
      ! (strokeWidth .: (2*w))
  where
    α  = 2 * pi / (fromIntegral n)
    x0 = 0.5 - w
    y0 = 0
    draw k =
      l  (0.5 + x0 * cos (k*α) - y0 * sin (k*α))
         (0.5 + x0 * sin (k*α) + y0 * cos (k*α))
    directions =
      mkPath $ do
        m   (0.5 + x0)  (0.5 + y0)
        mapM_ (draw . fromIntegral) [0..n]
        S.z


--------------------------------------------------------------------------------

geometricalMosaic :: Svg
geometricalMosaic =
    do
      corner
      corner ! A.transform (rotateAround 90  0.5 0.5)
      corner ! A.transform (rotateAround 180 0.5 0.5)
      corner ! A.transform (rotateAround 270 0.5 0.5)
      semicircunference
      semicircunference ! A.transform (rotateAround 180 0.5 0.5)
      semihexagon
      semihexagon ! A.transform (rotateAround 180 0.5 0.5)
      pentagonSide ! A.transform (rotateAround (180 + 0  ) 0.5 0.5)
      pentagonSide ! A.transform (rotateAround (180 + 72 ) 0.5 0.5)
      pentagonSide ! A.transform (rotateAround (180 + 144) 0.5 0.5)
      pentagonSide ! A.transform (rotateAround (180 + 216) 0.5 0.5)
      pentagonSide ! A.transform (rotateAround (180 + 288) 0.5 0.5)
  where
    s = 0.02
    k = 0.12
    --------------------------------------------------
    corner =
      S.path
        ! A.strokeOpacity "0"
        ! A.d cornerDirs
    cornerDirs =
      mkPath $ do
        m   (0.1 - s)  0
        l   (0.1 + s)  0
        l   0          (0.1 + s)
        l   0          (0.1 - s)
        S.z
    --------------------------------------------------
    semicircunference =
      S.path
        ! A.fillOpacity "0"
        ! (A.strokeWidth .: 1.5*s)
        ! A.d semicircunferenceDirs
    semicircunferenceDirs =
      mkPath $ do
        m   0  (0.5 - k)
        aa  k  k  0  True True 0  (0.5 + k)
    --------------------------------------------------
    r1 = k + s
    r2 = k - s
    cos60 = 0.5
    sin60 = 0.5 * sqrt 3
    semihexagon =
      S.path
        ! A.strokeOpacity "0"
        ! A.d semihexagonDirs
    semihexagonDirs =
      mkPath $ do
        m   (0.5 - r1)          0
        l   (0.5 - r1 * cos60)  (r1 * sin60)
        l   (0.5 + r1 * cos60)  (r1 * sin60)
        l   (0.5 + r1)          0
        l   (0.5 + r2)          0
        l   (0.5 + r2 * cos60)  (r2 * sin60)
        l   (0.5 - r2 * cos60)  (r2 * sin60)
        l   (0.5 - r2)          0
        S.z
    --------------------------------------------------
    α = (180 - 90 - 36) * pi / 180
    -- α = 180 - 90 - 36
    pentagonSide =
      S.path
        ! A.strokeOpacity "0"
        ! A.d pentagonSideDirs
    pentagonSideDirs =
      mkPath $ do
        m   (0.5 - r1 * cos α)  (0.5 + r1 * sin α)
        l   (0.5 + r1 * cos α)  (0.5 + r1 * sin α)
        l   (0.5 + r2 * cos α)  (0.5 + r2 * sin α)
        l   (0.5 - r2 * cos α)  (0.5 + r2 * sin α)
        S.z


--------------------------------------------------------------------------------


beeHiveMosaic :: Svg
beeHiveMosaic = do
    svg
      ! A.viewbox      (S.toValue $ "0 0 1 " ++ show (3*rad))
      ! A.preserveaspectratio "xMinYMin meet"
      $ do
        defs honeyGradient
        hexagon
  where
    apt = 0.5                        -- apotema
    rad = (2 / sqrt 3) * apt         -- radio
    aux = sqrt $ rad ^ 2 - apt ^ 2   -- proyeccion en el eje vertical
    hexagon =
      S.path
        ! A.strokeLinecap "round"
        ! A.strokeWidth   "0.05"
        ! A.stroke        "rgb(255,255,155)"
        ! A.fill          "url(#svg-honey-gradient)"
        ! A.d             hexagonDirs
    hexagonDirs =
      mkPath $ do
        m   0.5  (  3 * rad + 0.05)   -- plus epsilon
        l   0.5  (2.5 * rad)
        m   0.5  0
        l   0.5  (0.5 * rad)
        l   0    (0.5 * rad + aux)
        l   0    (1.5 * rad + aux)
        l   0.5  (2.5 * rad)
        l   1    (1.5 * rad + aux)
        l   1    (0.5 * rad + aux)
        l   0.5  (0.5 * rad)
    honeyGradient =
      radialgradient
        -- ! A.spreadmethod "reflect"
        ! A.cx "30%"
        ! A.cy "70%"
        ! A.r  "50%"
        ! A.fx "50%"
        ! A.fy "60%"
        ! A.id_ "svg-honey-gradient"
        $ do
          stop
            ! A.offset "0%"
            ! A.style  "stop-color: rgba(255,255,255,0.3)"
          stop
            ! A.offset "25%"
            ! A.style  "stop-color: rgba(255,255,0,0.4)"
          stop
            ! A.offset "50%"
            ! A.style  "stop-color: rgba(255,215,0,0.4)"
          stop
            ! A.offset "100%"
            ! A.style  "stop-color: rgba(255,140,0,0.4)"



--------------------------------------------------------------------------------


pajaritaNazaríMosaic :: Svg
pajaritaNazaríMosaic =
    svg
      ! A.viewbox      (S.toValue $ "0 0 1 " ++ show (2*h))
      ! A.preserveaspectratio "xMinYMin meet"
      ! A.fill "black"
      $ do
        pajarita
        pajarita          ! A.transform (translate  1       0   )
        pajarita          ! A.transform (translate  (-0.5)  h   )
        pajarita          ! A.transform (translate  0.5     h   )
        pajarita          ! A.transform (translate  (-0.5)  (-h))
        pajaritaInvertida
        pajaritaInvertida ! A.transform (translate  (-1)    0   )
        pajaritaInvertida ! A.transform (translate  (-0.5)  (-h))
        pajaritaInvertida ! A.transform (translate  0.5     (-h))
        pajaritaInvertida ! A.transform (translate  0.5     h   )
  where
    h   = (sqrt 3) / 2
    apt = h / 3
    ax = 0.5
    ay = 0
    bx = 1
    by = h
    cx = 0
    cy = h
    dx = 0.5
    dy = 2*h
    mid x y = (x + y) / 2
    pajarita =
      S.path
        ! A.strokeWidth "0"
        ! A.fill        "rgb(235,235,235)"
        ! A.d           upperDirs
    pajaritaInvertida =
      S.path
        ! A.strokeWidth "0"
        ! A.fill        "transparent"
        ! A.d           lowerDirs
    upperDirs =
      mkPath $ do
        m   ax   ay
        aa  apt  apt  0  False  True    (mid ax bx)  (mid ay by)
        aa  apt  apt  0  False  False   bx           by
        aa  apt  apt  0  False  True    (mid bx cx)  (mid by cy)
        aa  apt  apt  0  False  False   cx           cy
        aa  apt  apt  0  False  True    (mid ax cx)  (mid ay cy)
        aa  apt  apt  0  False  False   ax           ay
    lowerDirs =
      mkPath $ do
        m   bx   by
        aa  apt  apt  0  False  True    (mid bx dx)  (mid by dy)
        aa  apt  apt  0  False  False   dx           dy
        aa  apt  apt  0  False  True    (mid cx dx)  (mid cy dy)
        aa  apt  apt  0  False  False   cx           cy
        aa  apt  apt  0  False  True    (mid bx cx)  (mid by cy)
        aa  apt  apt  0  False  False   bx           by
        S.z



--------------------------------------------------------------------------------
