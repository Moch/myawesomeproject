{-# LANGUAGE     OverloadedStrings      #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}


module Base.Svg.Faces where

import           Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
import qualified Text.Blaze.Svg11.Attributes as A

import           Base.Svg.Utils

--------------------------------------------------------------------------------
-- BORDER

stdFaceBorder :: S.Svg
stdFaceBorder =
  let
    w = 0.05
    radius  = 0.5 - (w / 2)
    (c1,c2) = (,) 0.5 0.5
  in
    S.circle
      ! (A.cx .: c1)
      ! (A.cy .: c2)
      ! (A.r  .: radius)
      ! A.fill   "transparent"
      ! (A.strokeWidth .: w)

--------------------------------------------------------------------------------
-- EYES


happyEyes :: S.Svg
happyEyes =
  do
    leftEye
    rightEye
  where
    leftEye = horizontalMirrorSymmetry rightEye
    rightEye =
      S.path
        ! A.d rightEyePath
        ! A.fill "none"
        ! (A.strokeWidth .: 0.05)
    h = 0.4
    rightEyePath = S.mkPath $ do
      S.m 0.2 h
      S.aa 0.1 0.1 0 False True 0.4 h


sadEyes :: S.Svg
sadEyes =
  do
    leftEye
    rightEye
  where
    leftEye = horizontalMirrorSymmetry rightEye
    rightEye =
      S.path
        ! A.d rightEyePath
        ! A.fill "none"
        ! (A.strokeWidth .: 0.05)
    h = 0.4
    rightEyePath = S.mkPath $ do
      S.m  0.2 h
      S.aa 0.1 0.1 0 False False 0.4 h



openEye :: S.Svg
openEye =
  do
    eyeBorder
    pupil
    glow
  where
    w  = 0.04
    c1 = 0.5
    c2 = 0.43
    cr = 0.24
    k  = 0.15
    eyeBorder =
      S.path
        ! A.d eyePath
        ! A.fill "none"
        ! (A.strokeWidth .: 2*w)
        ! A.strokeLinejoin   "round"
    eyePath = S.mkPath $ do
      S.m   w    0.5
      S.c   0.25  0.1  0.75  0.1   (1-w)  0.5
      S.c   0.75  0.9  0.25  0.9   w      0.5
      S.z
    pupil =
      S.circle
        ! (A.cx .: c1)
        ! (A.cy .: c2)
        ! (A.r  .: cr)
        ! A.stroke "none"
    glow =
      S.path
        ! A.d glowPath
        ! A.fill "none"
        ! A.stroke "white"
        ! (A.strokeWidth .: 1.5*w)
        ! A.strokeLinecap "round"
    glowPath = S.mkPath $ do
      S.m   (c1 - k)   c2
      S.aa  k  k  0  False  True  c1  (c2 - k)



closedEye :: S.Svg
closedEye =
  do
    openEye
    bar
  where
    k   = 0.1
    bar =
      S.path
        ! A.d    barPath
        ! A.fill "none"
        ! A.stroke "rgb(70,70,70)"
        ! A.strokeWidth "0.12"
        ! A.strokeLinecap "round"
    barPath = S.mkPath $ do
      S.m   k     (1-k)
      S.l   (1-k)  k



--------------------------------------------------------------------------------
-- MOUTH

inexpresiveMouth :: S.Svg
inexpresiveMouth =
  let h = 0.7
  in
    S.path
      ! (A.strokeWidth .: 0.05)
      ! A.d (S.mkPath $ S.m 0.3 h >> S.l 0.7 h)


happyMouth :: S.Svg
happyMouth =
    S.path
      ! A.d mouthPath
      ! (A.strokeWidth .: 0.05)
  where
    h = 0.6
    mouthPath = S.mkPath $ do
      S.m 0.3 h
      S.aa 0.2 0.2 0 True False 0.7 h
      S.z

--------------------------------------------------------------------------------
