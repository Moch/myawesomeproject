{-# LANGUAGE     OverloadedStrings            #-}
{-# OPTIONS_GHC -fno-warn-type-defaults       #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing      #-}



module Base.Svg.User where

import Text.Blaze.Svg11 ((!))
import Text.Blaze.Svg11 as S
import Text.Blaze.Svg11.Attributes as A

import Base.Svg.Utils

--------------------------------------------------------------------------------



onOff :: S.Svg
onOff =
  do
    innerCircle
    littleStick
  where
    w1 = 0.05
    k1 = 0.28
    r1 = 0.38
    (a1,a2) = (,) (0.5 - k1) (0.5 - sqrt (r1^2 - k1^2))
    (b1,b2) = (,) (0.5 + k1) (0.5 - sqrt (r1^2 - k1^2))
    innerCircle =
      S.path
        ! fill "none"
        ! (strokeWidth .: 2*w1)
        ! strokeLinecap "round"
        ! d innerCirclePath
    innerCirclePath =
      mkPath $ do
        m   a1 a2
        aa  r1 r1 0 True False b1 b2
    ----------------------------------------
    littleStick =
      S.path
        ! (strokeWidth .: 2*w1)
        ! strokeLinecap "round"
        ! d littleStickPath
    littleStickPath =
      mkPath $ do
        m  0.5  0.5
        l  0.5  0.1



--------------------------------------------------------------------------------


carnet :: S.Svg
carnet =
  do
    cardBorder
    textLines
    photoHead
    photoShoulders
  where
    -- commonColor = "#000"
    ----------------------------------------
    w1 = 0.02
    x1 = 1.618 * y1
    y1 = 0.29
    cardBorder =
      S.path
        ! strokeLinejoin "round"
        ! (strokeWidth .: 2*w1)
        -- ! stroke commonColor
        ! fill   "#fff"
        ! d cardBorderPath
    cardBorderPath =
      mkPath $ do
        m   (0.5 - x1)  (0.5 - y1)
        l   (0.5 + x1)  (0.5 - y1)
        l   (0.5 + x1)  (0.5 + y1)
        l   (0.5 - x1)  (0.5 + y1)
        S.z
    ----------------------------------------
    h1 = 0.38
    h2 = 0.5
    h3 = 0.62
    k1 = 0.58
    k2 = 0.83
    textLines =
      S.path
        ! strokeWidth "0.035"
        -- ! stroke commonColor
        ! strokeLinecap "round"
        ! d textLinesPath
    textLinesPath =
      mkPath $ do
        m k1 h1   >>   l k2 h1
        m k1 h2   >>   l k2 h2
        m k1 h3   >>   l k2 h3
    ----------------------------------------
    q1 = 1 - k2
    q2 = 1 - k1
    qm = (q1+q2)/2
    photoHead =
      circle
        ! (cx .: qm)
        ! (cy .: ((h1+h2)/2))
        ! r "0.07"
        -- ! fill commonColor
        ! stroke "none"
    photoShoulders =
      S.path
        -- ! fill commonColor
        ! d shoulders
        ! stroke "none"
    shoulders =
      mkPath $ do
        m   q1  h3
        aa  (q2 - qm) (q2 - qm) 0 True True q2 h3
        aa  (q2 - qm) 0.025     0 True True q1 h3



--------------------------------------------------------------------------------


lock :: S.Svg
lock =
  do
    arm
    body
    keyhole
  where
    aw  = 0.035
    ax1 = 0.3
    ax2 = 1 - ax1
    axr = (ax2 - ax1) / 2
    ay1 = 0.5
    ay2 = 0.25
    arm =
      S.path
        ! (strokeWidth .: 2*aw)
        ! fill "none"
        ! d armPath
        -- ! stroke "black"
    armPath =
      mkPath $ do
        m   ax1 ay1
        l   ax1 ay2
        aa  axr axr 0 True True ax2 ay2
        l   ax2 ay1
    ----------------------------------------
    bx1 = 0.15
    bx2 = 1 - bx1
    by1 = 0.38
    by2 = 0.95
    body =
      S.path
        ! stroke "none"
        ! d bodyPath
    bodyPath =
      mkPath $ do
        m bx1 by1
        l bx2 by1
        l bx2 by2
        l bx1 by2
        S.z
    ----------------------------------------
    kr  = 0.07
    kw  = 0.038
    ky1 = 0.6
    ky2 = 0.78
    keyhole = do
      S.circle
        ! fill "#fff"
        ! (cx .: 0.5)
        ! (cy .: ky1)
        ! (r  .: kr)
        ! stroke "none"
      S.path
        ! d (mkPath $   m 0.5 ky1  >>  l 0.5 ky2)
        ! stroke "#fff"
        ! (strokeWidth .: 2*kw)
        ! strokeLinecap "round"



--------------------------------------------------------------------------------

personSimple :: S.Svg
personSimple =
  do
    simpleHead
    simpleShoulders
  where
    -- commonColor = "indigo"
    simpleHead =
      circle
        ! cx "0.5"
        ! cy "0.3"
        ! r  "0.18"
        ! stroke "none"
        -- ! fill commonColor
    ----------------------------------------
    kx = 0.2
    ky = 0.75
    kr = (1 - 2*kx) / 2
    simpleShoulders =
      S.path
        -- ! fill commonColor
        ! d    shouldersPath
        ! stroke "none"
    shouldersPath =
      mkPath $ do
        m  kx  ky
        aa kr  kr   0 True True (1 - kx) ky
        aa kr  0.15 0 True True kx       ky



--------------------------------------------------------------------------------

company :: S.Svg
company =
  do
    leftBuilding
    leftWindows
    leftDoor
    rightBuilding
    rightWindows
  where
    w = 0.02
    x1 = 0.12
    x2 = x1 + 0.1
    x3 = x4 - 0.1
    x4 = 0.6
    x5 = 1 - x1
    y1 = 0.10
    y2 = 0.15
    y3 = (y1 + y4) / 2
    y4 = 0.35
    y5 = y4 + 0.05
    y6 = y7 - 0.05
    y7 = 1 - y1
    doorH = 0.12
    ----------------------------------------
    leftBuilding =
      S.path
        ! d leftBuildingPath
        ! (strokeWidth .: 2*w)
        ! fill "none"
    leftBuildingPath =
      mkPath $ do
        m   x1  y7
        l   x1  y2
        l   x2  y2
        l   x2  y1
        l   x3  y1
        l   x3  y2
        l   x4  y2
        l   x4  y7
        S.z
    rightBuilding =
      S.path
        ! d rightBuildingPath
        ! (strokeWidth .: 2*w)
        ! fill "none"
    rightBuildingPath =
      mkPath $ do
        m   x4  y4
        l   x5  y4
        l   x5  y7
        l   x4  y7
    ----------------------------------------
    leftDoor =
      S.path
        ! d (mkPath $   m ((x1+x4)/2) y7 >> l ((x1+x4)/2) (y7 - doorH) )
        ! strokeWidth "0.08"
        ! fill "none"
    ----------------------------------------
    k1 = (x3 - x2) / 3
    leftWindows =
      S.path
        ! d leftWindowsPath
        ! (strokeWidth .: 2*w)
        ! strokeDasharray (S.toValue $ (show $ 3*w) ++ " " ++ (show w))
        ! fill "none"
    leftWindowsPath =
      mkPath $ do
        m   (x2 + 0*k1)  y3
        l   (x2 + 0*k1)  y6
        m   (x2 + 1*k1)  y3
        l   (x2 + 1*k1)  (y7 - doorH)
        m   (x2 + 2*k1)  y3
        l   (x2 + 2*k1)  (y7 - doorH)
        m   (x2 + 3*k1)  y3
        l   (x2 + 3*k1)  y6
    ----------------------------------------
    k2 = ((x5-w) - (x4+w)) / 4
    rightWindows =
      S.path
        ! d rightWindowsPath
        ! (strokeWidth .: 2*w)
        ! (strokeDasharray .: 2*w)
        ! fill "none"
    rightWindowsPath =
      mkPath $ do
        m   (x4 + w + 1*k2)  y5
        l   (x4 + w + 1*k2)  y6
        m   (x4 + w + 2*k2)  y5
        l   (x4 + w + 2*k2)  y6
        m   (x4 + w + 3*k2)  y5
        l   (x4 + w + 3*k2)  y6


--------------------------------------------------------------------------------

bigPencil :: Svg
bigPencil =
    S.path
      ! d pencilPath
      ! (strokeWidth .: 2*s)
      ! strokeLinejoin "round"
      ! fill "none"
  where
    s  = 0.03  -- half-width of the stroke
    w  = 0.3   -- width of the pencil
    x1 = 0.5 - w/2
    x2 = 0.5 + w/2
    y1 = 0.05
    y2 = 0.12
    y3 = 0.20
    y4 = 0.7
    y5 = y6 - 0.09
    y6 = 0.97
    pencilPath = mkPath $ do
      m   0.5  y5
      l   0.5  y6
      l   x1   y4
      l   x2   y4
      l   0.5  y6
      m   x1   y4
      l   x1   y3
      l   x2   y3
      l   x2   y4
      m   0.5  y4
      l   0.5  y3
      m   x1   y3
      l   x1   y2
      l   x2   y2
      l   x2   y3
      m   x1   y2
      l   x1   y1
      m   x2   y1
      l   x2   y2
      m   x1   y1
      aa  (w/2) 0.03 0 True True x2 y1



bigLeanedPencil :: S.Svg
bigLeanedPencil =
  do
    positionedPencil
  where
    positionedPencil =
      bigPencil ! transform (rotateAround 45 0.5 0.5)


--------------------------------------------------------------------------------


magnifyingGlass :: S.Svg
magnifyingGlass =
  do
    glass
    handle
  where
    s1 = 0.04
    r1 = 0.34
    x1 = 0.42
    y1 = 0.42
    glass =
      S.circle
        ! A.fill "none"
        ! (A.strokeWidth .: 2*s1)
        ! (A.r  .: r1)
        ! (A.cx .: x1)
        ! (A.cy .: y1)
    s2 = 0.07
    handle =
      S.path
        ! A.fill "none"
        ! A.strokeLinecap "round"
        ! (A.strokeWidth .: 2*s2)
        ! A.d handlePath
    handlePath = mkPath $ do
      m 0.72 0.72
      l 0.90 0.90



--------------------------------------------------------------------------------


cogwheel :: Int -> Float -> S.Svg
cogwheel n eps =
    S.path
      ! A.stroke "none"
      ! A.d cogPath
  where
    r1 = 0.20 :: Float
    r2 = 0.36 :: Float
    r3 = 0.47 :: Float
    a  = (2 * pi) / (2 * fromIntegral n)
    makeAngles k'  =
      let k = fromIntegral k'
      in [ k*a - eps, k*a + eps ]
    makePoint r α = ( 0.5 + r * cos α , 0.5 + r * sin α)
    outer = map (makePoint r3) $ concatMap makeAngles $ filter even [0 .. 2*n]
    inner = map (makePoint r2) $ concatMap makeAngles $ filter odd  [0 .. 2*n]
    f ((a1,a2):(b1,b2):outs) ((c1,c2):(d1,d2):ins) = do
      l  a1 a2
      l  b1 b2
      l  c1 c2
      aa r2 r2 0 False True d1 d2
      f outs ins
    f _ _ = S.z
    cogPath = mkPath $ do
      m (0.5 + r1) 0.5
      aa r1 r1 0 True False (0.5 - r1) 0.5
      aa r1 r1 0 True False (0.5 + r1) 0.5
      m (fst $ head outer) (snd $ head outer)
      f outer inner



cogwheelSet :: S.Svg
cogwheelSet = do
  cogwheel 21 0.04
    ! A.transform ( translate 0    0    <> S.scale 0.65 0.65 )
  cogwheel 17 0.04
    ! A.transform ( translate 0.5  0.42 <> S.scale 0.5  0.5  )
  cogwheel 13 0.05
    ! A.transform ( translate 0.18 0.62 <> S.scale 0.35 0.35 )


--------------------------------------------------------------------------------

keyWithCircle :: S.Svg
keyWithCircle =
  do
    key
    arc
  where
    w  = 0.05
    x0 = 0.25
    x1 = 0.3
    x2 = 0.65
    y1 = 0.6
    r1 = (x1 - w) / 2
    r2 = 0.5 - w
    y2 = 0.5 - ( sqrt $ r2^2 - (0.5 - x0)^2 )
    key =
      S.path
        ! (A.strokeWidth .: 0.09)
        ! A.fill "none"
        ! A.strokeLinecap "round"
        ! A.d keyPath
    keyPath = mkPath $ do
      m   x1   0.499
      aa  r1   r1   0  True False  x1  0.5
      l   x2   0.5
      l   x2   y1
      m   0.5  0.5
      l   0.5  y1
    arc =
      S.path
        ! (A.strokeWidth .: 2*w)
        ! A.fill "none"
        ! A.strokeLinecap "round"
        ! A.d arcPath
    arcPath = mkPath $ do
      m   x0  y2
      aa  r2  r2   0  True True  x0 (1-y2)



--------------------------------------------------------------------------------
