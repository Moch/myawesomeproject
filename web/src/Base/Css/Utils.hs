{-# LANGUAGE OverloadedStrings #-}


module Base.Css.Utils where

import           Clay
import qualified Clay.Media as Mq
import           Data.Text

---------------------------------- CLASS ---------------------------------------
-- Associate css to class name

associateCss :: [ (Text, Css) ] -> Css
associateCss =
  mapM_ (\ (x,y) -> (byClass x) & y)


---------------------------------  KAOLIN  -------------------------------------
-- COLOR

footerCol :: Color
footerCol = rgb 0 50 112

headerCol :: Color
headerCol = indigo

headerSecondaryCol :: Color
headerSecondaryCol = springgreen

headerContrast :: Color
headerContrast = limegreen

-- FONT

zeroSerif :: Text
zeroSerif = "Didact Gothic"

play :: Text
play = "Play"

smooth :: Text
smooth = "Varela Round"

superCurvy :: Text
superCurvy = "Dancing Script"

ubuntu :: Text
ubuntu = "Ubuntu"

montserrat :: Text
montserrat = "Montserrat"

montserratAlt :: Text
montserratAlt = "Montserrat Alternates"

montserratSub :: Text
montserratSub = "Montserrat Subrayada"

arial :: Text
arial = "Arial"

arabDances :: Text
arabDances = "Arab Dances"

-- GEOMETRY

headerH :: Size LengthUnit
headerH = px 70

-- TRANSITION

halfSecAllTransition :: Css
halfSecAllTransition =
  transition "all" (ms 500) linear (ms 0)

-------------------------------- CERAMIC ---------------------------------------

borderWithRadius ::
  Stroke -> Size LengthUnit -> Color -> Size LengthUnit -> Css
borderWithRadius x y z t = do
  border x y z
  borderRadius t t t t


setFont :: FontWeight -> (Size a) -> [Text] -> Css
setFont wght sz fam = do
  fontWeight wght
  fontSize   sz
  fontFamily fam [sansSerif]

------------------------------- RESPONSIVE -------------------------------------

portrait :: Css -> Css
portrait =
  queryOnly Mq.screen [Mq.minWidth $ px 500]

landscape :: Css -> Css
landscape =
  queryOnly Mq.screen [Mq.minWidth $ px 750]

laptop :: Css -> Css
laptop =
  queryOnly Mq.screen [Mq.minWidth $ px 1000]

desktop :: Css -> Css
desktop =
  queryOnly Mq.screen [Mq.minWidth $ px 1300]

--------------------------------------------------------------------------------
