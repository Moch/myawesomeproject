{-# LANGUAGE OverloadedStrings #-}


module Base.Css.General where

import Clay
-- import Clay.Flexbox as Flex

import Base.Css.Utils

--------------------------------------------------------------------------------

fontCss :: Css
fontCss =
  let
    makeFont (x,y) =
      fontFace $ do
        fontFamily  [x] []
        fontFaceSrc
          [ FontFaceSrcUrl ("/static/fonts/" <> y) (Just TrueType)
          ]
  in
    mapM_ makeFont
      [ (,) "Arab Dances"           "Arab_Dances/ArabDances.ttf"
      , (,) "Dancing Script"        "Dancing_Script/DancingScript-Regular.ttf"
      , (,) "Didact Gothic"         "Didact_Gothic/DidactGothic-Regular.ttf"
      , (,) "Montserrat"            "Montserrat/Montserrat-Regular.ttf"
      , (,) "Montserrat Alternates" "Montserrat_Alternates/MontserratAlternates-Regular.ttf"
      , (,) "Montserrat Subrayada"  "Montserrat_Subrayada/MontserratSubrayada-Regular.ttf"
      , (,) "Play"                  "Play/Play-Regular.ttf"
      , (,) "SourceSansPro"         "Source_Sans_Pro/SourceSansPro-Regular.ttf"
      , (,) "Ubuntu"                "Ubuntu/Ubuntu-Regular.ttf"
      , (,) "Varela Round"          "Varela_Round/VarelaRound-Regular.ttf"
      ]


--------------------------------------------------------------------------------

resetCss :: Css
resetCss = do
  star ? do
    margin  `sym` (px 0)
    padding `sym` (px 0)
    boxSizing borderBox
  star # "::-moz-focus-inner" ? borderStyle none
  star # "::-moz-focus-outer" ? borderStyle none
  input <> textarea ? do
    outlineStyle none
    ":-webkit-autofill" & do
      (-:) "box-shadow" "0px 0px 0px 100px white inset"
    ":webkit-autofill:focus" & do
      (-:) "box-shadow" "0px 0px 0px 100px white inset"


--------------------------------------------------------------------------------

generalCss :: Css
generalCss =
  associateCss
    [ (,) "resp-w"          responsiveWidthForWideElement
    , (,) "flex-center"     flexCenterCss
    , (,) "display-none"    (important $ display none)
    , (,) "rotate-180"      (important $ transform $ rotate (180 :: Angle Deg))
    ]

--------------------------------------------------------------------------------

responsiveWidthForWideElement :: Css
responsiveWidthForWideElement = do
  width     (pct 100)
  maxWidth  (vw  96)
  portrait  (maxWidth $ vw 90)
  desktop   (maxWidth $ px 1300)


flexCenterCss :: Css
flexCenterCss = do
  -- flex parent
  display flex
  justifyContent center
  alignItems     center
  -- geometry
  width (pct 100)

--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
