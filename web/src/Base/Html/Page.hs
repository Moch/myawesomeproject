
module Base.Html.Page where

import qualified Text.Blaze.Html5 as H

import Data.Pool
import Data.Text
import Database.Persist.Sql.Types.Internal

import Base.Html.Head
import Base.Lang
import User.Logic.All

--------------------------------------------------------------------------------

data Page a = Page
  { pageParams :: PageParams
  , htmlHead   :: Head
  , htmlBody   :: PageParams -> a
  }


data PageParams = PageParams
  { ppRoute    :: Text
  , ppLang     :: Lang
  , ppUser     :: Maybe (UserId, UserType)
  , ppDatabase :: Pool  SqlBackend
  , ppErrorMsg :: Maybe Text
  , ppOkMsg    :: Maybe Text
  }

--------------------------------------------------------------------------------

instance H.ToMarkup a => H.ToMarkup (Page a) where
  toMarkup = pageToBlaze


pageToBlaze :: H.ToMarkup a => Page a -> H.Markup
pageToBlaze page =
  let
    p = pageParams page
    h = htmlHead page
    b = htmlBody page
  in
    H.docTypeHtml $ do
      H.toHtml h
      H.body $ H.toHtml $ b p

--------------------------------------------------------------------------------
