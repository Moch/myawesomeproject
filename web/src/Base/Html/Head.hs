{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}


module Base.Html.Head where

import Data.Text (Text)
import           Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

--------------------------------------------------------------------------------

type Head = Html


stdHead :: Text -> Text -> Text -> Html -> Head
stdHead title desc keywords moreHtml =
  H.head $ do
    H.title (H.toHtml title)
    stdMeta desc keywords
    stdCss
    stdJs
    moreHtml


stdMeta :: Text -> Text -> Html
stdMeta desc keywords = do
  H.meta
    ! A.charset "utf-8"
  H.meta
    ! A.name    "viewport"
    ! A.content "width=device-width, initial-scale=1.0, shrink-to-fit=no"
  H.meta
    ! A.name    "description"
    ! A.content (H.toValue desc)
  H.meta
    ! A.name    "keywords"
    ! A.content (H.toValue keywords)


stdCss :: Html
stdCss = mapM_ cssLink
  [ "/static/css/all.css"
  -- , "https://fonts.googleapis.com/css?family=Didact Gothic"
  -- , "https://fonts.googleapis.com/css?family=Play"
  -- , "https://fonts.googleapis.com/css?family=Varela+Round"
  -- , "https://fonts.googleapis.com/css?family=Dancing+Script"
  -- , "https://fonts.googleapis.com/css?family=Ubuntu"
  -- , "https://fonts.googleapis.com/css?family=Montserrat"
  -- , "https://fonts.googleapis.com/css?family=Montserrat+Alternates"
  -- , "https://fonts.googleapis.com/css?family=Montserrat+Subrayada"
  ]


stdJs :: Html
stdJs = mapM_ jsLink
  [ "/static/js/all.js"
  ]


--------------------------------------------------------------------------------


cssLink :: Text -> Html
cssLink url =
  H.link
    ! A.rel   "stylesheet"
    ! A.type_ "text/css"
    ! A.href  (H.toValue url)

jsLink :: Text -> Html
jsLink url =
  H.script ""
    ! A.type_ "text/javascript"
    ! A.src   (H.toValue url)


--------------------------------------------------------------------------------
