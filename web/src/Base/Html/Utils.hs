{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}


module Base.Html.Utils where

import Data.String (fromString)
import Data.Text (Text, unpack)
import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

--------------------------------------------------------------------------------

makeBlazeAttr :: (Text , Text) -> H.Attribute
makeBlazeAttr (x,y) =
   H.customAttribute (fromString $ unpack x) (H.toValue y)

--------------------------------------------------------------------------------

goalInput :: Text -> Html
goalInput v =
  H.input
    ! A.type_ "hidden"
    ! A.name  "goal"
    ! A.value (H.toValue v)

--------------------------------------------------------------------------------
