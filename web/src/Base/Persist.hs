{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies      #-}


module Base.Persist where

import           Conduit
import           Control.Exception
import           Control.Monad.Logger
import           Control.Monad.Reader
import           Data.Pool (Pool)
import           Database.Persist.Postgresql (SqlBackend, runSqlPersistMPool)

--------------------------------------------------------------------------------

runDB
  :: Pool SqlBackend
  -> ReaderT SqlBackend (NoLoggingT (ResourceT IO)) a
  -> IO (Either SomeException a)
runDB pool = fmap Right . flip runSqlPersistMPool pool

--------------------------------------------------------------------------------
