{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Header.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

--------------------------------------------------------------------------------

header :: Html -> Html
header = outerDiv . innerDiv
  where
    outerDiv = H.div ! A.class_ "layout-outer-header"
    innerDiv = H.div ! A.class_ "layout-inner-header"

--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
