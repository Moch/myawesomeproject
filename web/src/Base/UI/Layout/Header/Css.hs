{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Header.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

layoutHeaderCss :: Css
layoutHeaderCss =
  associateCss
    [ (,) "layout-outer-header" outerHeaderCss
    , (,) "layout-inner-header" innerHeaderCss
    ]

--------------------------------------------------------------------------------

outerHeaderCss :: Css
outerHeaderCss = do
  -- box
  -- (-:) "box-shadow" "0px 3px 4px 0px rgba(0,0,0,0.3)"
  -- color
  backgroundColor headerCol
  -- display
  position fixed
  zIndex 2
  -- flex parent
  display flex
  justifyContent center
  alignItems     center
  -- geometry
  height headerH
  width (pct 100)
  let x = px 0
  let y = px 8
  padding y x y x


innerHeaderCss :: Css
innerHeaderCss = do
  -- flex parent
  display flex
  -- geometry
  height (pct 100)
  width (pct 100)
  let x1 = px 10
  let x2 = px 20
  let x3 = px 45
  padding 0 x1 0 x1
  portrait  $ padding 0 x2 0 x2
  landscape $ padding 0 x3 0 x3



--------------------------------------------------------------------------------
