{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Logo.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

layoutLogoCss :: Css
layoutLogoCss =
  associateCss
    [ (,) "layout-logo-wrapper" logoWrapperCss
    , (,) "layout-logo-link"    logoLinkCss
    , (,) "layout-logo-svg"     logoSvgCss
    ]

--------------------------------------------------------------------------------

logoWrapperCss :: Css
logoWrapperCss = do
  -- flex child
  flexGrow 1

--------------------------------------------------------------------------------

logoLinkCss :: Css
logoLinkCss = do
  -- flex parent
  display    flex
  alignItems center
  -- geometry
  height (pct 100)
  -- text
  textDecoration none
  -- SPAN
  Clay.span ? do
    setFont    bold (px 45) ["SourceSansPro"]
    fontColor  white
    marginLeft (Clay.rem 2)

--------------------------------------------------------------------------------

logoSvgCss :: Css
logoSvgCss = do
  -- color
  (-:) "fill" "white"
  -- geometry
  height (Clay.rem 3.5)
  width  (Clay.rem 3.5)

--------------------------------------------------------------------------------
