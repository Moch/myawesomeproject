{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Logo.Svg where

import Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
import qualified Text.Blaze.Svg11.Attributes as A

import Base.Svg.Lambdas
import Base.Svg.Utils

--------------------------------------------------------------------------------

logoSvg :: S.Svg
logoSvg =
  stdSvg curvyLambda
    ! A.class_ "layout-logo-svg"

--------------------------------------------------------------------------------
