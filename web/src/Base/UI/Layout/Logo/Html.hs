{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Logo.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.UI.Layout.Logo.Svg

--------------------------------------------------------------------------------

logo :: Html
logo =
  H.div logoLink
    ! A.class_ "layout-logo-wrapper"

--------------------------------------------------------------------------------

logoLink :: Html
logoLink =
  H.a -- logoSvg
    ! A.href "/"
    ! A.class_ "layout-logo-link"
    $ do
      logoSvg
      H.span "HSjobs"

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
