{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Template.Html where


import Text.Blaze.Html5 (Html)

import Base.Html.Page
import Base.UI.Layout.Body.Html        (outerBody, innerBody)
import Base.UI.Layout.Footer.Html      (footer)
import Base.UI.Layout.Header.Html      (header)
import Base.UI.Layout.Info.Html        (info)
import Base.UI.Layout.LangNav.Html     (langNav)
import Base.UI.Layout.Logo.Html        (logo)
import Base.UI.Layout.Nav.Html         (nav)
import Base.UI.Layout.Payline.Html     (payline)
import Base.UI.Layout.Settings.Html    (settingsBody)

--------------------------------------------------------------------------------

mainTemplate :: (PageParams -> Html) -> PageParams -> Html
mainTemplate innerHtml params =
  outerBody $ do
    header $ do
      logo
      nav params
    innerBody (innerHtml params)
    footer $ do
      info
      langNav params

--------------------------------------------------------------------------------

paylineTemplate :: (PageParams -> Html) -> PageParams -> Html
paylineTemplate innerHtml params =
    mainTemplate body params
  where
    body = \p -> (payline p) >> (innerHtml p)

--------------------------------------------------------------------------------

settingsTemplate :: (PageParams -> Html) -> PageParams -> Html
settingsTemplate innerHtml params =
    mainTemplate body params
  where
    body = \p -> settingsBody p (innerHtml p)

--------------------------------------------------------------------------------
