


module Base.UI.Layout.Template.Css where

import Clay (Css)

import Base.UI.Layout.Body.Css
import Base.UI.Layout.Footer.Css
import Base.UI.Layout.Header.Css
import Base.UI.Layout.Info.Css
import Base.UI.Layout.LangNav.Css
import Base.UI.Layout.Logo.Css
import Base.UI.Layout.Nav.Css
import Base.UI.Layout.Payline.Css
import Base.UI.Layout.Settings.Css

--------------------------------------------------------------------------------

uiLayoutCss :: Css
uiLayoutCss = do
  layoutBodyCss
  layoutFooterCss
  layoutHeaderCss
  layoutInfoCss
  layoutLangNavCss
  layoutLogoCss
  layoutNavCss
  layoutPaylineCss
  layoutSettingsCss

--------------------------------------------------------------------------------
