{-# LANGUAGE    OverloadedStrings       #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}


module Base.UI.Layout.Settings.Svg where

-- import Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
-- import qualified Text.Blaze.Svg11.Attributes as A

import Base.Svg.User
import Base.Svg.Utils

--------------------------------------------------------------------------------

menuIcon :: S.Svg
menuIcon = stdSvg $ cogwheel 9 0.11

--------------------------------------------------------------------------------
