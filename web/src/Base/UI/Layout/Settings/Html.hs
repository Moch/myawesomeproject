{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Settings.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Page
import Base.Lang
import Base.UI.Layout.Settings.Svg
import qualified Base.UI.Layout.Settings.Translate as Translate
import User.API.Routes

--------------------------------------------------------------------------------

settingsBody :: PageParams -> Html -> Html
settingsBody p innerHtml = do
  H.div
    ! A.class_ "layout-settings-body-outer"
    $ H.div
      ! A.class_ "resp-w layout-settings-body-inner"
      $ do
        settingsNav p
        H.div innerHtml
          ! A.class_ "layout-settings-content"
  -- H.script hideMenuOnMobileLoad


--------------------------------------------------------------------------------
{-
hideMenuOnMobileLoad :: Html
hideMenuOnMobileLoad = H.toHtml $ concat
  [ "window.onload = function() {"
  , "  if (window.innerWidth < 500){"
  , "    toggleSettingsMenu('settings_nav','settings_button_text','settings_button_icon');"
  , "  }"
  , "}"
  ]
-}

settingsNav :: PageParams -> Html
settingsNav params =
  let
    lang  = ppLang params
  in
    H.div
      ! A.class_ "layout-settings-nav-wrapper"
      $ do
        dropdownButton lang
        H.div
          ! A.class_ "layout-settings-nav"
          ! A.id     "settings_nav"
          $ settingsList params


settingsList :: PageParams -> Html
settingsList params =
  let
    lang  = ppLang params
    route = ppRoute params
    makeLink (tag,r) =
      if r == route
        then H.div $ do
          H.p (H.toHtml $ tag lang)
          H.span "✎"
        else
          H.a (H.toHtml $ tag lang) ! A.href (H.toValue r)
  in
    mapM_ makeLink
      [ (,) Translate.account       settingsAccountRoute
      , (,) Translate.profile       settingsProfileRoute
      , (,) Translate.employment    settingsEmploymentRoute
      , (,) Translate.notifications settingsNotificationsRoute
      , (,) Translate.billing       settingsBillingRoute
      , (,) Translate.blockedUsers  settingsBlockedUsersRoute
      ]


--------------------------------------------------------------------------------

dropdownButton :: Lang -> Html
dropdownButton lang =
  H.button
    ! A.onclick "toggleSettingsMenu('settings_nav','settings_button_text','settings_button_icon');"
    $ do
      menuIcon
        ! A.id "settings_button_icon"
      H.span (H.toHtml $ Translate.settings lang)
        ! A.id "settings_button_text"

--------------------------------------------------------------------------------
