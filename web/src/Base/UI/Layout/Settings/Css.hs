{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Settings.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

layoutSettingsCss :: Css
layoutSettingsCss =
  associateCss
    [ (,) "layout-settings-hidden"      hiddenCss
    , (,) "layout-settings-body-outer"  outerBodyCss
    , (,) "layout-settings-body-inner"  innerBodyCss
    , (,) "layout-settings-nav-wrapper" navWrapperCss
    , (,) "layout-settings-nav"         navCss
    , (,) "layout-settings-content"     contentCss
    , (,) "layout-settings-inputwrap"   inputwrapCss
    , (,) "layout-settings-input"       inputCss
    , (,) "layout-settings-button"      buttonCss
    , (,) "layout-settings-green"       greenCss
    ]

--------------------------------------------------------------------------------


hiddenCss :: Css
hiddenCss = do
  -- display
  opacity 0
  overflow hidden
  -- font
  fontSize (px 0)
  -- geometry
  important (width  $ px 0)
  important (height $ px 0)
  important (margin `sym` (px 0))



outerBodyCss :: Css
outerBodyCss = do
  -- flex child
  flexGrow 1
  -- flex parent
  display flex
  justifyContent center
  -- geometry
  paddingTop   (Clay.rem 2)
  paddingBottom (Clay.rem 2.5)


innerBodyCss :: Css
innerBodyCss = do
  -- flex child
  flexGrow 1
  -- flex parent
  display flex
  flexDirection column
  landscape $ flexDirection row


navWrapperCss :: Css
navWrapperCss = do
  -- display
  display none
  landscape $ display block
  -- geometry
  minWidth (px 280)
  marginBottom (Clay.rem 1.5)
  -- BUTTON
  button ? do
    outlineStyle none
    backgroundColor lavender
    borderWithRadius solid (px 1) darkgray (px 12)
    setFont bold (px 13) [montserratAlt]
    fontColor $ rgb 90 90 90
    display flex
    alignItems center
    padding `sym` (px 10)
    cursor pointer
    -- SPAN
    Clay.span ? do
      marginLeft (px 15)
      width (px 188)
      halfSecAllTransition
      textAlign $ alignSide sideLeft
    -- SVG
    svg ? do
      width  (px 27)
      height (px 27)
      (-:) "fill" "dimgray"
      halfSecAllTransition
      transform $ rotate (0 :: Angle Deg)



navCss :: Css
navCss = do
  let c = lightgray
  -- border
  borderWidth (px 1)
  borderColor c
  borderStyle4 none solid solid solid
  -- flex parent
  display flex
  flexDirection column
  -- font
  setFont normal (px 13) [montserrat]
  -- geometry
  width  (px 250)
  height (px 300)
  marginTop (Clay.rem 0.5)
  portrait $ marginTop (Clay.rem 1)
  -- transition
  halfSecAllTransition
  -- COMMON
  (a <> Clay.div) ? do
    borderTop   solid (px 1) c
    padding `sym` (px 10)
    height (px 40)
    flexGrow 1
    display flex
    alignItems center
  -- ANCHOR
  a ? do
    outlineStyle none
    fontColor blue
    textDecoration none
    hover & backgroundColor (rgb 240 240 255)
  -- DIV
  Clay.div ? do
    fontColor $ rgb 40 40 40
    fontWeight bold
    cursor cursorDefault
  -- P
  p ? do
    flexGrow 1
    paddingRight (px 10)
  -- SPAN
  Clay.span ? do
    fontSize (Clay.rem 1.8)
    fontWeight normal


--------------------------------------------------------------------------------


contentCss :: Css
contentCss = do
  -- flex child
  flexGrow 1
  -- flex parent
  display flex
  flexDirection column
  -- font
  setFont normal (Clay.rem 0.9) [arial]
  fontColor $ rgb 70 80 90
  -- geometry
  paddingTop (Clay.rem 0.5)
  -- FORM
  form ? do
    marginBottom (Clay.rem 4)
  -- HEADING 1
  h1 ? do
    borderBottom solid (px 1) lightgray
    setFont bold (Clay.rem 1.4) [smooth]
    paddingBottom (Clay.rem 0.5)
    marginBottom (Clay.rem 1.5)
  -- ANCHOR
  a ? do
    outlineStyle none
    display inlineBlock
    padding `sym` (px 10)
    fontColor royalblue
    textDecoration none
    hover & do
      textDecoration underline


inputwrapCss :: Css
inputwrapCss = do
  display flex
  flexDirection column
  fontWeight bold
  maxWidth (px 600)
  marginBottom (Clay.rem 1.8)
  -- cursor pointer
  p ? do
    marginBottom (Clay.rem 0.5)

inputCss :: Css
inputCss = do
  backgroundColor "#fafbfc"
  borderWithRadius solid (px 1) silver (px 3)
  height (Clay.rem 2)
  width  (pct 100)
  padding `sym` (px 8)
  (-:) "box-shadow" "inset 0px 1px 2px 0px rgba(27,31,35,0.075)"
  focus & do
    backgroundColor white
    borderColor dodgerblue
    (-:) "box-shadow" "0px 0px 4px 0px cornflowerblue, 0px 0px 0px 100px white inset"


buttonCss :: Css
buttonCss = do
  (-:) "background" "linear-gradient(white, rgb(235,235,255))"
  borderWithRadius solid (px 1) silver (px 3)
  cursor pointer
  fontColor $ rgb 70 80 90
  fontWeight bold
  fontFamily [arial] [sansSerif]
  width  (px 250)
  height (Clay.rem 2.2)
  marginTop    (Clay.rem 1)
  marginBottom (Clay.rem 0.5)
  hover & do
    background $ rgb 235 230 255
    borderColor darkgray
    (-:) "box-shadow" "0px 1px 2px 0px rgba(27,31,35,0.075)"


greenCss :: Css
greenCss = do
  (-:) "background" "linear-gradient(rgb(50,200,100) , rgb(50,180,120))"
  borderColor green
  fontColor   white
  hover & do
    borderColor forestgreen
    (-:) "background" "linear-gradient(rgb(0,200,120) , rgb(70,150,120))"
    fontColor white


--------------------------------------------------------------------------------
