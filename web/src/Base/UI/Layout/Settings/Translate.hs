{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Settings.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------

account :: Lang -> Text
account lang =
  case lang of
    ES -> "Cuenta"
    _  -> "Account"


profile :: Lang -> Text
profile lang =
  case lang of
    ES -> "Perfil"
    _  -> "Profile"


employment :: Lang -> Text
employment lang =
  case lang of
    ES -> "Empleo"
    _  -> "Employment"


notifications :: Lang -> Text
notifications lang =
  case lang of
    ES -> "Notificaciones"
    _  -> "Notifications"


billing :: Lang -> Text
billing lang =
  case lang of
    ES -> "Pagos"
    _  -> "Billing"


blockedUsers :: Lang -> Text
blockedUsers lang =
  case lang of
    ES -> "Usuarios bloqueados"
    _  -> "Blocked users"


settings :: Lang -> Text
settings lang =
  case lang of
    ES -> "Configuración"
    _  -> "Settings"


--------------------------------------------------------------------------------
