
function toggleSettingsMenu(navId,spanId,iconId) {
  let x = document.getElementById(navId);
  let y = document.getElementById(spanId);
  let z = document.getElementById(iconId);
  if ( x.classList.contains('layout-settings-hidden') ){
    x.classList.remove('layout-settings-hidden');
    y.classList.remove('layout-settings-hidden');
    z.classList.remove('rotate-180');
  }
  else {
    x.classList.add('layout-settings-hidden');
    y.classList.add('layout-settings-hidden');
    z.classList.add('rotate-180');
    }
}

// -----------------------------------------------------------------------------
