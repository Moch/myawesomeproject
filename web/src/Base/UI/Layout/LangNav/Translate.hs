{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.LangNav.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------

language :: Lang -> Text
language lang =
  case lang of
    ES -> "Idioma: "
    _  -> "Language: "

--------------------------------------------------------------------------------
