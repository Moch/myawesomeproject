{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.LangNav.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Lang
import Base.Html.Page
import Base.UI.Layout.LangNav.Translate as Translate
import Root.API.Routes

--------------------------------------------------------------------------------

langNav :: PageParams -> Html
langNav params =
  let
    goTo = ppRoute params
    lang = ppLang  params
  in
    H.div ! A.class_ "layout-lang-nav" $ do
      chooseLanguage lang
      languageChooser $ H.toValue goTo

--------------------------------------------------------------------------------

chooseLanguage :: Lang -> Html
chooseLanguage lang =
  H.span (H.toHtml $ Translate.language lang)
    ! A.class_ "layout-choose-lang"

--------------------------------------------------------------------------------

languageChooser :: H.AttributeValue -> Html
languageChooser goToUrl =
  H.form
    ! A.action  (H.toValue setLangRoute)
    ! A.enctype "multipart/form-data"
    ! A.method  "POST"
    $ do
      hiddenRedirectButton goToUrl
      spanishButton
      englishButton

--------------------------------------------------------------------------------

hiddenRedirectButton :: H.AttributeValue -> Html
hiddenRedirectButton url =
  H.input
    ! A.type_ "hidden"
    ! A.name  "lang-nav-redirect-input"
    ! A.value url


spanishButton :: Html
spanishButton =
  H.input
    ! A.class_ "layout-lang-button"
    ! A.type_ "submit"
    ! A.name  "lang-nav-language-input"
    ! A.value (H.toValue $ show ES)


englishButton :: Html
englishButton =
  H.input
    ! A.class_ "layout-lang-button"
    ! A.type_ "submit"
    ! A.name  "lang-nav-language-input"
    ! A.value (H.toValue $ show EN)

--------------------------------------------------------------------------------
