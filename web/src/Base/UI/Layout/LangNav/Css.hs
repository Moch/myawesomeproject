{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.LangNav.Css where

import Clay
import qualified Clay.Flexbox as Flex

import Base.Css.Utils


--------------------------------------------------------------------------------

layoutLangNavCss :: Css
layoutLangNavCss =
  associateCss
    [ (,) "layout-lang-nav"    langNavCss
    , (,) "layout-choose-lang" chooseLanguageCss
    , (,) "layout-lang-button" buttonCss
    ]

--------------------------------------------------------------------------------

langNavCss :: Css
langNavCss = do
  -- flex parent
  display flex
  justifyContent center
  alignItems     center
  flexWrap       Flex.nowrap
  -- geometry
  paddingTop    (Clay.rem 0.8)
  paddingBottom (Clay.rem 0.8)

--------------------------------------------------------------------------------

chooseLanguageCss :: Css
chooseLanguageCss = do
  -- display
  display none
  landscape (display block)
  -- font
  setFont bold (Clay.rem 1) [play]
  -- geometry
  marginRight (px 15)

--------------------------------------------------------------------------------

buttonCss :: Css
buttonCss = do
  -- border
  borderWithRadius solid (px 3) white (px 50)
  -- color
  fontColor       white
  backgroundColor footerCol
  hover & backgroundColor crimson
  -- display
  cursor pointer
  -- font
  setFont bold (Clay.rem 1.1) [play]
  -- geometry
  marginLeft (px 10)
  let x = Clay.rem 3.2
  height x
  width  x
  -- text
  textTransform uppercase

--------------------------------------------------------------------------------
