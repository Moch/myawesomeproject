{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Info.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A


--------------------------------------------------------------------------------

info :: Html
info =
  H.div ! A.class_ "layout-footer-info"
    $ "Blame Brendan Eich"

--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
