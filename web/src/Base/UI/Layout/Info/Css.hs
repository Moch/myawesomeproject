{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Info.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

layoutInfoCss :: Css
layoutInfoCss =
  associateCss
    [ (,) "layout-footer-info" infoCss
    ]

--------------------------------------------------------------------------------

infoCss :: Css
infoCss = do
  -- flex parent
  display flex
  alignItems     center
  -- flex child
  flexGrow 1

--------------------------------------------------------------------------------
