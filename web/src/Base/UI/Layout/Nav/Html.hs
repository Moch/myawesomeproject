{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Nav.Html where

import           Data.Maybe
import           Data.Text
import           Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Page
import Base.Lang
import Base.UI.Layout.Nav.Svg
import qualified Base.UI.Layout.Nav.Translate as Translate
import Base.UI.Layout.Settings.Html
import Job.API.Routes
import User.API.Cookies
import User.API.Routes
import User.Logic.Base


--------------------------------------------------------------------------------

nav :: PageParams -> Html
nav p =
  H.div
    ! A.class_ "layout-nav"
    $ do
      jobsSection     p
      settingsSection p
      accessSection   p





jobsSection :: PageParams -> Html
jobsSection p =
  do
    jobsButton (ppLang p)
    jobsMenu   p


settingsSection :: PageParams -> Html
settingsSection p =
  case ppUser p of
    Nothing -> return ()
    Just _  -> do
      settingsButton (ppLang p)
      settingsMenu   p


accessSection :: PageParams -> Html
accessSection p =
  let
    lang = ppLang p
    userIsLogged = isJust $ ppUser p
  in
    if userIsLogged
      then do
        sessionButton lang
        sessionMenu   p
      else do
        accessButton lang
        accessMenu   p

--------------------------------------------------------------------------------


navButton :: Text -> Html -> Html -> Html
navButton btnId icon tooltip = do
  H.button
    ! A.id (H.toValue btnId)
    ! A.class_ "layout-nav-button"
    $ do
      icon
      H.span tooltip
        ! A.class_ "layout-nav-tooltip"




accessButton :: Lang -> Html
accessButton lang =
  navButton
    "layout_nav_access_btn"
    accessIcon
    (H.toHtml $ Translate.access lang)


jobsButton :: Lang -> Html
jobsButton lang =
  navButton
    "layout_nav_jobs_btn"
    jobsIcon
    (H.toHtml $ Translate.jobs lang)


sessionButton :: Lang -> Html
sessionButton lang =
  navButton
    "layout_nav_session_btn"
    accessIcon
    (H.toHtml $ Translate.session lang)


settingsButton :: Lang -> Html
settingsButton lang =
  navButton
    "layout_nav_settings_btn"
    settingsIcon
    (H.toHtml $ Translate.settings lang)


--------------------------------------------------------------------------------


navMenuBuildScript :: Text -> Text -> Html
navMenuBuildScript openButtonId menuId =
    H.script (H.toHtml $ "buildNavMenu(" <> args <> ");")
  where
    args =
         "'" <> openButtonId <> "'"
      <> ","
      <> "'" <> menuId <> "'"


accessMenu :: PageParams -> Html
accessMenu p =
  let
    lang = ppLang p
    signIn =
      H.a
        ! A.href (H.toValue signInRoute)
        ! A.onclick (H.toValue $ putCookieLoginRedirect $ ppRoute p)
        $ do
          signInIcon
          H.toHtml $ Translate.signIn lang
    signUp =
      H.a
        ! A.href (H.toValue signUpRoute)
        ! A.onclick (H.toValue $ putCookieSignUpRedirect $ ppRoute p)
        $ do
          signUpIcon
          H.toHtml $ Translate.signUp lang
    recoverPw =
      H.a
        ! A.href (H.toValue recoverPwRoute)
        $ do
          recoverPwIcon
          H.toHtml $ Translate.recoverPassword lang
  in
    H.div
      ! A.class_ "layout-nav-menu display-none"
      ! A.id "layout_nav_access_menu"
      $ do
        signIn
        signUp
        recoverPw
        navMenuBuildScript "layout_nav_access_btn" "layout_nav_access_menu"


jobsMenu :: PageParams -> Html
jobsMenu p =
  let
    lang = ppLang p
    viewJobs =
      H.a
        ! A.href "/"
        $ do
          viewJobsIcon
          H.toHtml $ Translate.viewJobs lang
    postJob =
      H.a
        ! A.href (H.toValue postJobRoute)
        $ do
          postJobIcon
          H.toHtml $ Translate.postJob lang
    pinnedJobs =
      H.a
        ! A.href (H.toValue manageJobsRoute)
        $ do
          pinnedJobsIcon
          H.toHtml $ Translate.pinnedJobs lang
    manageJobs =
      H.a
        ! A.href (H.toValue manageJobsRoute)
        $ do
          manageJobsIcon
          H.toHtml $ Translate.manageJobs lang
  in do
    H.div
      ! A.class_ "layout-nav-menu display-none"
      ! A.id "layout_nav_jobs_menu"
      $ case ppUser p of
        Nothing -> do
          viewJobs
          postJob
        Just x -> case snd x of
          UserCompany -> do
            viewJobs
            postJob
            manageJobs
          UserPerson -> do
            viewJobs
            pinnedJobs
    navMenuBuildScript "layout_nav_jobs_btn" "layout_nav_jobs_menu"


settingsMenu :: PageParams -> Html
settingsMenu p =
  H.div
    ! A.class_ "layout-nav-menu display-none"
    ! A.id "layout_nav_settings_menu"
    $ do
      settingsList p
      navMenuBuildScript "layout_nav_settings_btn" "layout_nav_settings_menu"


sessionMenu :: PageParams -> Html
sessionMenu p =
  let
    lang = ppLang p
    profileIcon = case ppUser p of
      Just x -> case snd x of
        UserCompany -> companyIcon
        UserPerson  -> personIcon
      _  -> personIcon
    signOut =
      H.a
        ! A.href (H.toValue logoutRoute)
        $ do
          signOutIcon
          H.toHtml $ Translate.signOut lang
    profile =
      H.a
        ! A.href (H.toValue profileRoute)
        $ do
          profileIcon
          H.toHtml $ Translate.profile lang
    editProfile =
      H.a
        ! A.href (H.toValue settingsProfileRoute)
        $ do
          editProfileIcon
          H.toHtml $ Translate.editProfile lang
  in
    H.div
      ! A.class_ "layout-nav-menu display-none"
      ! A.id "layout_nav_session_menu"
      $ do
        profile
        editProfile
        signOut
        navMenuBuildScript "layout_nav_session_btn" "layout_nav_session_menu"


--------------------------------------------------------------------------------
