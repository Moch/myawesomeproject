


function buildNavMenu(openButtonId , menuId) {
  let openButton  = document.getElementById(openButtonId);
  let tooltip     = openButton.getElementsByClassName('layout-nav-tooltip')[0];
  let menuElem    = document.getElementById(menuId);
  let keyListener = function(e) {
    switch (e.keyCode) {
      case 27 :
        closeNavMenu();
        break;
    }
  };

  function closeNavMenu() {
    menuElem.classList.add('display-none');
    menuElem.classList.remove('display-flex');
    tooltip.classList.remove('display-none');
    document.removeEventListener("keyup", keyListener);
  };

  function openNavMenu() {
    menuElem.classList.add('display-flex');
    menuElem.classList.remove('display-none');
    tooltip.classList.add('display-none');
    document.addEventListener("keyup", keyListener);
  };

  openButton.onclick = () => {
    if (menuElem.classList.contains('display-none'))
      { openNavMenu(); }
    else { closeNavMenu(); }
  };

  document.addEventListener("click", (event) => {
    if (event.target != openButton) { closeNavMenu(); }
  });

  menuElem.onclick = function(event) {event.stopPropagation();};

}


// -----------------------------------------------------------------------------
