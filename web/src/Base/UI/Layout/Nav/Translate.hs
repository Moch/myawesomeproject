{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Nav.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------


access :: Lang -> Text
access lang =
  case lang of
    ES -> "Acceso"
    _  -> "Access"


jobs :: Lang -> Text
jobs lang =
  case lang of
    ES -> "Empleo"
    _  -> "Jobs"


session :: Lang -> Text
session lang =
  case lang of
    ES -> "Sesión"
    _  -> "Session"


settings :: Lang -> Text
settings lang =
  case lang of
    ES -> "Ajustes"
    _  -> "Settings"


signIn :: Lang -> Text
signIn lang =
  case lang of
    ES -> "Iniciar sesión"
    _  -> "Sign In"


signUp :: Lang -> Text
signUp lang =
  case lang of
    ES -> "Crear cuenta"
    _  -> "Sign Up"


recoverPassword :: Lang -> Text
recoverPassword lang =
  case lang of
    ES -> "Recuperar contraseña"
    _  -> "Recover password"


viewJobs :: Lang -> Text
viewJobs lang =
  case lang of
    ES -> "Ver empleos"
    _  -> "View jobs"


postJob :: Lang -> Text
postJob lang =
  case lang of
    ES -> "Publicar anuncio"
    _  -> "Post job"


pinnedJobs :: Lang -> Text
pinnedJobs lang =
  case lang of
    ES -> "Tus anuncios marcados"
    _  -> "Your pinned jobs"


manageJobs :: Lang -> Text
manageJobs lang =
  case lang of
    ES -> "Mis anuncios"
    _  -> "My jobs"


signOut :: Lang -> Text
signOut lang =
  case lang of
    ES -> "Salir"
    _  -> "Logout"


profile :: Lang -> Text
profile lang =
  case lang of
    ES -> "Perfil público"
    _  -> "Public profile"


editProfile :: Lang -> Text
editProfile lang =
  case lang of
    ES -> "Editar perfil"
    _  -> "Edit profile"


--------------------------------------------------------------------------------
