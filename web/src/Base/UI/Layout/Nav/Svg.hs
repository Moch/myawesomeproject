{-# LANGUAGE    OverloadedStrings       #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}


module Base.UI.Layout.Nav.Svg where

-- import Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
-- import qualified Text.Blaze.Svg11.Attributes as A

import Base.Svg.Business
import Base.Svg.User
import Base.Svg.Utils

--------------------------------------------------------------------------------

accessIcon :: S.Svg
accessIcon = stdSvg keyWithCircle

jobsIcon :: S.Svg
jobsIcon = stdSvg briefcase

settingsIcon :: S.Svg
settingsIcon = stdSvg $ cogwheel 11 0.1 -- cogwheelSet

personIcon :: S.Svg
personIcon = stdSvg personSimple

companyIcon :: S.Svg
companyIcon = stdSvg company

editProfileIcon :: S.Svg
editProfileIcon = stdSvg bigLeanedPencil

--------------------------------------------------------------------------------

signInIcon :: S.Svg
signInIcon = stdSvg onOff

signUpIcon :: S.Svg
signUpIcon = stdSvg carnet

signOutIcon :: S.Svg
signOutIcon = stdSvg onOff

recoverPwIcon :: S.Svg
recoverPwIcon = stdSvg lock

--------------------------------------------------------------------------------

postJobIcon :: S.Svg
postJobIcon = stdSvg documentWithPencil

viewJobsIcon :: S.Svg
viewJobsIcon = stdSvg magnifyingGlass

pinnedJobsIcon :: S.Svg
pinnedJobsIcon = stdSvg pin

manageJobsIcon :: S.Svg
manageJobsIcon = stdSvg analytics


--------------------------------------------------------------------------------
