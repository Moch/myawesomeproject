{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Nav.Css where

import Clay
import Data.String

import Base.Css.Utils


--------------------------------------------------------------------------------


layoutNavCss :: Css
layoutNavCss = do
  navButtonAnimationKeyframes
  associateCss
    [ (,) "layout-nav"          navCss
    , (,) "layout-nav-button"   buttonCss
    , (,) "layout-nav-tooltip"  tooltipCss
    , (,) "layout-nav-menu"     menuCss
    ]


--------------------------------------------------------------------------------


navButtonAnimationKeyframes :: Css
navButtonAnimationKeyframes =
  let
    f x = (,) x $ (-:) "background" $ fromString $
         "radial-gradient(circle, purple "
      ++ (show x)
      ++ "%, indigo "
      ++ (show x) ++ "%)"
  in
    keyframes "navButtonAnimation" $ Prelude.map f [20,40,60,80,100]



navCss :: Css
navCss = do
  -- display
  position relative
  -- flex parent
  display flex
  alignItems center


buttonCss :: Css
buttonCss = do
  -- border
  borderWithRadius none (px 1) indigo (px 25)
  outlineStyle none
  -- color
  (-:) "fill"   "lavender"
  (-:) "stroke" "lavender"
  background indigo
  hover & do
    animation
      "navButtonAnimation"
      (ms 300) linear (ms 0) (iterationCount 1) normal forwards
    ".layout-nav-tooltip" ? do
      opacity 1
      transition "all" (ms 250) linear (ms 500)
  active & do
    (-:) "fill"       "white"
    (-:) "stroke"     "white"
  -- display
  cursor pointer
  position relative
  -- flex parent
  display flex
  alignItems center
  justifyContent center
  -- geometry
  height (px 50)
  width  (px 50)
  let y1 = px 10
  let y2 = px 20
  marginLeft y1
  landscape $ do
    marginLeft y2
  -- SVG
  svg ? do
    height (pct 100)
    padding `sym` (px 7)
    pointerEvents none




tooltipCss :: Css
tooltipCss = do
  -- border
  borderRadius `sym` (px 2)
  -- color
  backgroundColor dimgray
  fontColor white
  opacity 0
  -- display
  position absolute
  top headerH
  zIndex 0
  pointerEvents none
  -- font
  setFont normal (Clay.rem 0.85) [ubuntu]
  -- geometry
  padding `sym` (px 10)
  -- transition
  transition "all" (ms 250) linear (ms 0)






menuCss :: Css
menuCss = do
  -- border
  borderStyle  solid
  borderWidth  (px 1)
  outlineStyle none
  -- box
  (-:)
    "box-shadow"
    "0px 10px 25px 0px rgba(0,0,0,0.3), 0 7px 10px -5px rgba(0,0,0,0.45)"
  -- color
  backgroundColor white
  borderColor silver
  fontColor   indigo
  -- display
  position fixed
  top $ (@+@) headerH (px 15)
  right (px 25)
  zIndex 1
  -- flex parent
  flexDirection column
  -- font
  setFont normal (Clay.rem 0.8) [montserrat]
  -- geometry
  minWidth (px 250)
  -- COMMON
  (a <> Clay.div) ? do
    -- border
    outlineStyle none
    fontColor indigo
    -- flex child
    flexGrow 1
    -- flex parent
    display flex
    alignItems center
    -- geometry
    let x = px 25
    padding 0 x 0 x
    height (px 45)
    width  (pct 100)
  -- ANCHOR
  a ? do
    -- text
    textDecoration none
    -- hover
    hover & do
      backgroundColor $ rgb 242 242 242
  -- DIV
  Clay.div ? do
    fontColor $ rgb 40 40 40
    cursor cursorDefault
  -- P
  p ? do
    flexGrow 1
    paddingRight (px 10)
  -- SPAN
  Clay.span ? do
    fontSize (Clay.rem 1.2)
  -- SVG
  svg ? do
    -- color
    (-:) "fill"   "indigo"
    (-:) "stroke" "indigo"
    -- geometry
    width  (px 25)
    height (px 25)
    marginRight (px 20)


--------------------------------------------------------------------------------
