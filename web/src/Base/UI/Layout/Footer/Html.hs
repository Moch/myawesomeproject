{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Footer.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A


--------------------------------------------------------------------------------

footer :: Html -> Html
footer = outerDiv . innerDiv
  where
    outerDiv = H.div ! A.class_ "layout-outer-footer"
    innerDiv = H.div ! A.class_ "layout-inner-footer resp-w"

--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
