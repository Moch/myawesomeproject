{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Footer.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

layoutFooterCss :: Css
layoutFooterCss =
  associateCss
    [ (,) "layout-outer-footer" outerFooterCss
    , (,) "layout-inner-footer" innerFooterCss
    ]

--------------------------------------------------------------------------------

outerFooterCss :: Css
outerFooterCss = do
  -- color
  fontColor white
  backgroundColor footerCol
  -- flex parent
  display flex
  justifyContent center
  alignItems     center
  -- geometry
  width (pct 100)

innerFooterCss :: Css
innerFooterCss = do
  -- flex parent
  display flex

--------------------------------------------------------------------------------
