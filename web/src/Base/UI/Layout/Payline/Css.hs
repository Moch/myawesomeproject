{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Payline.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

layoutPaylineCss :: Css
layoutPaylineCss =
  associateCss
    [ (,) "layout-outer-payline"  outerWrapper
    , (,) "layout-inner-payline"  innerWrapper
    , (,) "layout-payline-arrow"  arrowWrapper
    , (,) "layout-payline-step"   stepWrapper
    , (,) "layout-payline-active" activeStep
    ]

--------------------------------------------------------------------------------

outerWrapper :: Css
outerWrapper = do
  -- color
  backgroundColor white
  fontColor silver
  -- border
  borderBottom solid (px 1) silver
  -- display
  cursor cursorDefault
  -- flex parent
  display flex
  justifyContent center
  alignItems     center
  -- geometry
  width (pct 100)

innerWrapper :: Css
innerWrapper = do
  -- flex parent
  display flex
  flexDirection row
  justifyContent center
  alignItems center


arrowWrapper :: Css
arrowWrapper = do
  -- flex parent
  display        flex
  justifyContent center
  alignItems     center
  -- geometry
  let x1 = Clay.rem 0.5
  let x2 = Clay.rem 1
  let x3 = Clay.rem 1.5
  let x4 = Clay.rem 3
  let x5 = Clay.rem 5
  padding 0 x1 0 x1
  portrait  $ padding 0 x2 0 x2
  landscape $ padding 0 x3 0 x3
  laptop    $ padding 0 x4 0 x4
  desktop   $ padding 0 x5 0 x5
  -- SVG
  svg ? do
    -- color
    (-:) "fill" "silver"
    -- geometry
    let s1 = Clay.rem 1
    let s2 = Clay.rem 1.8
    width  s1
    height s1
    landscape $ do
      width  s2
      height s2


stepWrapper :: Css
stepWrapper = do
  -- font
  fontVariant smallCaps
  setFont bold (Clay.rem 1) [smooth]
  -- geometry
  let x = Clay.rem 1
  paddingTop x
  paddingBottom x


activeStep :: Css
activeStep = do
  -- color
  fontColor indigo


--------------------------------------------------------------------------------
