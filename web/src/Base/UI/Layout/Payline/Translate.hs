{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Payline.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------

choose :: Lang -> Text
choose lang =
  case lang of
    ES -> "Elige"
    _  -> "Choose"


create :: Lang -> Text
create lang =
  case lang of
    ES -> "Crea"
    _  -> "Create"


purchase :: Lang -> Text
purchase lang =
  case lang of
    ES -> "Compra"
    _  -> "Purchase"


--------------------------------------------------------------------------------
