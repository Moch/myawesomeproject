{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Payline.Html where

import Data.Text (Text, isPrefixOf)
import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Page
import Base.Lang
import Base.UI.Layout.Payline.Svg
import qualified Base.UI.Layout.Payline.Translate as Translate
import Job.API.Routes

--------------------------------------------------------------------------------

payline :: PageParams -> Html
payline p =
  H.div ! A.class_ "layout-outer-payline" $
    H.div ! A.class_ "layout-inner-payline resp-w" $ do
      chooseJobPost   (ppLang p) (ppRoute p)
      arrow
      writeJobPost    (ppLang p) (ppRoute p)
      arrow
      purchaseJobPost (ppLang p) (ppRoute p)


arrow :: Html
arrow =
  H.span rightArrow
    ! A.class_ "layout-payline-arrow"

--------------------------------------------------------------------------------

chooseJobPost :: Lang -> Text -> Html
chooseJobPost lang r =
  let
    mActiveStep =
      if postJobRoute `isPrefixOf` r
        then "layout-payline-active" else ""
  in
    H.span (H.toHtml $ Translate.choose lang)
      ! A.class_ (H.toValue $ "layout-payline-step " ++ mActiveStep)


writeJobPost :: Lang -> Text -> Html
writeJobPost lang r =
  let
    mActiveStep =
      if createJobPostRoute `isPrefixOf` r
        then "layout-payline-active" else ""
  in
    H.span (H.toHtml $ Translate.create lang)
      ! A.class_ (H.toValue $ "layout-payline-step " ++ mActiveStep)


purchaseJobPost :: Lang -> Text -> Html
purchaseJobPost lang r =
  let
    mActiveStep =
      if purchaseJobPostRoute `isPrefixOf` r
        then "layout-payline-active" else ""
  in
    H.span (H.toHtml $ Translate.purchase lang)
      ! A.class_ (H.toValue $ "layout-payline-step " ++ mActiveStep)


--------------------------------------------------------------------------------
