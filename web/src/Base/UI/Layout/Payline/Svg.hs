{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Payline.Svg where

-- import Text.Blaze.Svg11 ((!))
import Text.Blaze.Svg11 as S
-- import Text.Blaze.Svg11.Attributes as A

import Base.Svg.Arrows
import Base.Svg.Utils

--------------------------------------------------------------------------------


rightArrow :: Svg
rightArrow =
  stdSvg longArrowRight


--------------------------------------------------------------------------------
