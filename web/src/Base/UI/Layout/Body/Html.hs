{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Body.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

--------------------------------------------------------------------------------

outerBody :: Html -> Html
outerBody = H.div ! A.class_ "layout-outer-body"

innerBody :: Html -> Html
innerBody = H.div ! A.class_ "layout-inner-body"

--------------------------------------------------------------------------------
