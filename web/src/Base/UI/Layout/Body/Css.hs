{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Layout.Body.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

layoutBodyCss :: Css
layoutBodyCss =
  associateCss
    [ (,) "layout-outer-body" outerBodyCss
    , (,) "layout-inner-body" innerBodyCss
    ]

--------------------------------------------------------------------------------

outerBodyCss :: Css
outerBodyCss = do
  -- flex parent
  display flex
  flexDirection column
  -- geometry
  minHeight (vh 100)

innerBodyCss :: Css
innerBodyCss = do
  -- flex parent
  display flex
  flexDirection column
  -- flex child
  flexGrow 1
  -- geometry
  marginTop headerH

--------------------------------------------------------------------------------
