{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.SelectMulti.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------


add :: Lang -> Text
add lang =
  case lang of
    ES -> "Añadir"
    _  -> "Add"


--------------------------------------------------------------------------------
