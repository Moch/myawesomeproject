{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.SelectMulti.Svg where

-- import Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
-- import qualified Text.Blaze.Svg11.Attributes as A

import Base.Svg.Text
import Base.Svg.Utils

--------------------------------------------------------------------------------

closeIcon :: S.Svg
closeIcon = stdSvg bigX

--------------------------------------------------------------------------------
