{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.SelectMulti.Html where

import Data.Text (Text)
import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Lang
import Base.UI.Elems.SelectEnriched.Html
import Base.UI.Elems.SelectMulti.Svg
import Base.UI.Elems.SelectMulti.Translate as Translate


--------------------------------------------------------------------------------


select2 ::
     Lang -> Text
  -> (Bool, Text, [(Text,Text)])
  -> (Bool, Text, [(Text,Text)])
  -> [(Text,Text)]
  -> [(Text,Text)]
  -> Html
select2 lang identifier (b1, ph1, opt1) (b2, ph2, opt2) memo1 memo2 =
    H.div
      ! A.class_ "select-multi"
      ! A.id     (H.toValue identifier)
      $ do
        H.div
          ! A.class_ "select-multi-controls"
          $ do
            richSelect   lang id1 b1 ph1 opt1
            richSelect   lang id2 b2 ph2 opt2
            acceptButton lang
        H.div
          ! A.class_ "select-multi-options"
          $ do
            acceptedInput identifier ("","") ("","")
              ! A.style "display: none;"
            mapM_ (uncurry $ acceptedInput identifier) $ zip memo1 memo2
        buildScript2 identifier
  where
    id1 = "select_multi_aux1_" <> identifier
    id2 = "select_multi_aux2_" <> identifier


buildScript2 :: Text -> Html
buildScript2 identifier =
    H.script (H.toHtml $ "buildSelect2('" <> identifier <> "');")


acceptButton :: Lang -> Html
acceptButton lang =
  H.button
    ! A.class_ "select-multi-accept-btn"
    ! A.type_  "button"
    $ H.toHtml $ Translate.add lang


acceptedInput :: Text -> (Text , Text) -> (Text , Text) -> Html
acceptedInput identifier (a1,a2) (b1,b2) =
  H.div
    ! A.class_ "select-multi-accepted-wrap"
    $ do
      H.input
        ! A.type_ "hidden"
        ! A.name  (H.toValue identifier)
        ! A.value (H.toValue $ a1 <> "+" <> b1)
      H.span (H.toHtml a2)
        ! A.class_ "select-multi-accepted-fst"
      H.span (H.toHtml $ "(" <> b2 <> ")" )
        ! A.class_ "select-multi-accepted-snd"
      H.button closeIcon
        ! A.class_  "select-multi-accepted-close"
        ! A.onclick "this.parentNode.parentNode.removeChild(this.parentNode);"


--------------------------------------------------------------------------------
