

function buildSelect2(identifier){
  let wrapper = document.getElementById(identifier);
  let opt1    = document.getElementById("select_multi_aux1_"+identifier);
  let opt2    = document.getElementById("select_multi_aux2_"+identifier);
  let input1  = opt1.getElementsByClassName("rich-select-hidden-input")[0];
  let input2  = opt2.getElementsByClassName("rich-select-hidden-input")[0];
  let tag1    = opt1.getElementsByClassName("rich-select-public-value")[0];
  let tag2    = opt2.getElementsByClassName("rich-select-public-value")[0];
  let addBtn  = wrapper.getElementsByClassName("select-multi-accept-btn")[0];
  let tmplt   = wrapper.getElementsByClassName("select-multi-accepted-wrap")[0];
  let tagWrap = tmplt.parentNode;

  let addOpt = function() {
    var newTag = tmplt.cloneNode(true);
    newTag.removeAttribute("style");
    let inpt = newTag.getElementsByTagName("INPUT")[0];
    let col1 = newTag.getElementsByClassName("select-multi-accepted-fst")[0];
    let col2 = newTag.getElementsByClassName("select-multi-accepted-snd")[0];
    let memo = wrapper.getElementsByClassName("select-multi-accepted-wrap");
    let newVal = input1.value + '+' + input2.value;
    var isValid = true;
    var v = '';
    for (i = 0; i < memo.length; i++) {
      v = memo[i].getElementsByTagName("INPUT")[0].value;
      if (v === newVal) {
        isValid = false;
        break;
      }
    }
    isValid =
      isValid &&
      tag1.classList.contains('rich-select-fulfilled') &&
      tag2.classList.contains('rich-select-fulfilled');
    if (isValid) {
      inpt.value = newVal;
      col1.innerHTML = tag1.innerHTML;
      col2.innerHTML = '(' + tag2.innerHTML + ')';
      tagWrap.appendChild(newTag);
    }
  };

  addBtn.addEventListener("click", addOpt);

};

// -----------------------------------------------------------------------------
