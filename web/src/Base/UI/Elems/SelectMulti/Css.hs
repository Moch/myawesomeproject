{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.SelectMulti.Css where


import Clay
import qualified Clay.Flexbox as Flex

import Base.Css.Utils

--------------------------------------------------------------------------------

selectMultiCss :: Css
selectMultiCss =
  associateCss
    [ (,) "select-multi"                outerCss
    , (,) "select-multi-controls"       controlsCss
    , (,) "select-multi-accept-btn"     buttonCss
    , (,) "select-multi-options"        optionsCss
    , (,) "select-multi-accepted-wrap"  acceptedWrapperCss
    , (,) "select-multi-accepted-fst"   acceptedFstCss
    , (,) "select-multi-accepted-snd"   acceptedSndCss
    , (,) "select-multi-accepted-close" acceptedCloseCss
    ]

--------------------------------------------------------------------------------

outerCss :: Css
outerCss = do
  -- flex parent
  display flex
  flexDirection column


controlsCss :: Css
controlsCss = do
  -- flex parent
  display flex
  alignItems center
  flexWrap   Flex.wrap
  -- geometry
  marginBottom (Clay.rem 0.5)


buttonCss :: Css
buttonCss = do
  -- border
  borderWithRadius none (px 0) white (px 3)
  outlineStyle     none
  -- color
  backgroundColor royalblue
  fontColor       white
  hover & do
    backgroundColor dodgerblue
  -- display
  cursor pointer
  -- font
  setFont bold (px 12) [smooth]
  -- geometry
  minHeight (Clay.rem 2)
  marginBottom (px 5)
  let x = px 15
  paddingLeft  x
  paddingRight x
  -- text
  textTransform uppercase
  textAlign     center


--------------------------------------------------------------------------------


optionsCss :: Css
optionsCss = do
  -- flex parent
  display flex
  flexDirection column
  -- geometry
  minWidth (px 250)
  (-:) "width" "-webkit-fit-content"
  (-:) "width" "-moz-fit-content"
  (-:) "width" "fit-content"


acceptedWrapperCss :: Css
acceptedWrapperCss = do
  -- border
  borderWithRadius solid (px 1) silver (px 5)
  -- color
  backgroundColor $ rgb 240 245 250
  -- flex parent
  display    flex
  alignItems center
  -- geometry
  let x = px 12
  let y = px 6
  padding y x y x
  marginBottom (px 8)


acceptedFstCss :: Css
acceptedFstCss = do
  -- font
  fontWeight bold
  -- geometry
  width       (pct 100)
  marginRight (px 8)


acceptedSndCss :: Css
acceptedSndCss = do
  -- font
  fontColor $ rgb 150 150 150
  -- geometry
  width (pct 100)


acceptedCloseCss :: Css
acceptedCloseCss = do
  -- border
  borderWithRadius solid (px 1) tomato (px 50)
  -- color
  backgroundColor transparent
  (-:) "stroke" "tomato"
  hover & do
    backgroundColor tomato
    (-:) "stroke" "white"
  -- display
  cursor pointer
  -- flex parent
  display flex
  justifyContent center
  alignItems center
  -- geometry
  marginLeft (px 12)
  padding `sym` (px 6)
  -- SVG
  svg ? do
    height (px 8)
    width  (px 8)


--------------------------------------------------------------------------------
