{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.ClosableMsg.Html where

import Data.Text (Text)
import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.UI.Elems.ClosableMsg.Svg


--------------------------------------------------------------------------------

neutralMsg :: Maybe Text -> Html
neutralMsg =
  maybe "" (closableMsg . H.toHtml)

errorMsg :: Maybe Text -> Html
errorMsg =
  maybe "" (closableError . H.toHtml)

okMsg :: Maybe Text -> Html
okMsg =
  maybe "" (closableOk . H.toHtml)



closableMsg :: Html -> Html
closableMsg =
  closableMsgCore "closable-msg closable-msg-neutral" ""


closableError :: Html -> Html
closableError =
  closableMsgCore "closable-msg closable-msg-error" errorIcon

closableOk :: Html -> Html
closableOk =
  closableMsgCore "closable-msg closable-msg-ok" okIcon




--------------------------------------------------------------------------------


closableMsgCore :: Text -> Html -> Html -> Html
closableMsgCore cssClass icon content =
  H.div
    ! A.class_ (H.toValue cssClass)
    $ do
      H.div
        ! A.class_ "closable-msg-content"
        $ do
          icon ! A.class_ "closable-msg-icon"
          H.span content
      H.div
        ! A.class_ "closable-msg-bar"
        $ do
          H.button closeIcon
            ! A.class_ "closable-msg-close-btn"
            ! A.type_   "button"
            ! A.onclick "this.parentNode.parentNode.style.display = 'none';"



--------------------------------------------------------------------------------
