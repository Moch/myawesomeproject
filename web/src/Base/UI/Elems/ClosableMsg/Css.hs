{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.ClosableMsg.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

closableMsgCss :: Css
closableMsgCss =
  associateCss
    [ (,) "closable-msg"           coreCss
    , (,) "closable-msg-bar"       barCss
    , (,) "closable-msg-close-btn" closeButtonCss
    , (,) "closable-msg-content"   contentCss
    , (,) "closable-msg-neutral"   neutralCss
    , (,) "closable-msg-error"     errorCss
    , (,) "closable-msg-ok"        okCss
    ]


x1 :: Size LengthUnit
x1 = px 20


--------------------------------------------------------------------------------


coreCss :: Css
coreCss = do
  -- border
  borderStyle  solid
  borderWidth  (px 1)
  borderRadius `sym` (px 3)
  -- flex parent
  display flex
  -- font
  setFont normal (Clay.rem 0.9) [zeroSerif]
  -- geometry
  width (pct 100)
  marginBottom (Clay.rem 1.5)


barCss :: Css
barCss = do
  -- flex parent
  display flex
  flexDirection column
  -- geometry
  width  x1


closeButtonCss :: Css
closeButtonCss = do
  -- border
  borderStyle  none
  outlineStyle none
  -- color
  backgroundColor transparent
  -- display
  cursor pointer
  -- flex parent
  display flex
  justifyContent center
  alignItems center
  -- geometry
  width  x1
  height x1
  -- SVG
  svg ? do
    width  (pct 50)
    height (pct 50)


contentCss :: Css
contentCss = do
  -- flex parent
  display flex
  alignItems center
  -- geometry
  marginTop    x1
  marginLeft   x1
  marginBottom x1
  width (pct 100)
  svg ? ".closable-msg-icon" & do
    height (px 30)
    width  (px 30)
    minHeight (px 20)
    minWidth  (px 20)
    marginRight (px 15)


neutralCss :: Css
neutralCss = return ()


errorCss :: Css
errorCss = do
  backgroundColor $ rgb 255 245 245
  borderColor firebrick
  fontColor   firebrick
  svg ? ".closable-msg-icon" & do
    (-:) "fill"   "red"
    (-:) "stroke" "red"
  button ? ".closable-msg-close-btn" & do
    (-:) "fill"   "firebrick"
    (-:) "stroke" "firebrick"
    hover & do
      (-:) "fill"   "red"
      (-:) "stroke" "red"



okCss :: Css
okCss = do
  backgroundColor $ rgb 245 255 245
  borderColor mediumseagreen
  fontColor   seagreen
  svg ? ".closable-msg-icon" & do
    (-:) "fill"   "limegreen"
    (-:) "stroke" "limegreen"
  button ? ".closable-msg-close-btn" & do
    (-:) "fill"   "gray"
    (-:) "stroke" "gray"
    hover & do
      (-:) "fill"   "darkgray"
      (-:) "stroke" "darkgray"


--------------------------------------------------------------------------------
