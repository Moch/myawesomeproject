{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.PhotoCropper.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Lang
-- import Base.UI.Elems.ModalWindow.Html


--------------------------------------------------------------------------------


photoCropperOpenButton :: Lang -> String -> String -> Html
photoCropperOpenButton lang cssClass identifier =
    H.div
      ! A.id (H.toValue identifier)
      $ do
        openButton
  where
    openButton =
      H.label
        ! A.class_ (H.toValue cssClass)
        $ do
          buttonText lang
          H.input
            ! A.type_ "file"
            ! A.style "display: none;"
            ! A.name  (H.toValue identifier)


buttonText :: Lang -> Html
buttonText lang = case lang of
  ES -> "Nueva imagen"
  _  -> "Upload new picture"




photoCropper :: String -> Html
photoCropper _ =
  H.div "" ! A.class_ "fake"

--------------------------------------------------------------------------------
