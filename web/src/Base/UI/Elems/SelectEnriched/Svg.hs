{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.SelectEnriched.Svg where

-- import Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
-- import qualified Text.Blaze.Svg11.Attributes as A

import Base.Svg.Arrows
import Base.Svg.User
import Base.Svg.Utils

--------------------------------------------------------------------------------

grayTriangleUp :: S.Svg
grayTriangleUp =
  stdSvg triangleUp

grayTriangleDown :: S.Svg
grayTriangleDown =
  stdSvg triangleDown


iconoLupa :: S.Svg
iconoLupa =
  stdSvg magnifyingGlass

--------------------------------------------------------------------------------
