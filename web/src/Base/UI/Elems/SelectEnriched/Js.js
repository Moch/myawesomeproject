


function buildRichSelect(identifier) {
  let richSelect  = document.getElementById(identifier);
  let hiddenInput = richSelect.querySelector('.rich-select-hidden-input');
  let openButton  = richSelect.querySelector('.rich-select-toggler');
  let publicVal   = richSelect.querySelector('.rich-select-public-value');
  let svgUp       = richSelect.querySelector('.rich-select-svg-up');
  let svgDown     = richSelect.querySelector('.rich-select-svg-down');
  let innerWrap   = richSelect.querySelector('.rich-select-wrapper');
  let searchBar   = richSelect.querySelector('.rich-select-search-bar');
  let optionList  = richSelect.querySelectorAll('.rich-select-option');
  let noneFound   = richSelect.querySelector('.rich-select-no-matches');
  let keyListener =
    function(e) {
      switch (e.keyCode) {
        case 27 :
          closeRichSelect();
          break;
      }
    };
  let clickListener = function(event) {
      if (!richSelect.contains(event.target)) {
        closeRichSelect();
      };
    };

  let switchVertically = function() {
    if (innerWrap.classList.contains('rich-select-display-none')) {
      openButton.classList.remove('rich-select-toggler-up');
      openButton.classList.remove('rich-select-toggler-down');
      innerWrap.classList.remove('rich-select-wrapper-up');
      innerWrap.classList.remove('rich-select-wrapper-down');
      svgUp.classList.add('rich-select-display-none');
      svgDown.classList.remove('rich-select-display-none');
    }
    else {
      let h1 = window.innerHeight;
      let h2 = openButton.getBoundingClientRect().top;
      let h3 = openButton.getBoundingClientRect().height;
      let h4 = innerWrap.getBoundingClientRect().height;
      if (h1 > h2 + h3 + h4) {
        openButton.classList.add('rich-select-toggler-down');
        openButton.classList.remove('rich-select-toggler-up');
        innerWrap.classList.add('rich-select-wrapper-down');
        innerWrap.classList.remove('rich-select-wrapper-up');
        svgUp.classList.remove('rich-select-display-none');
        svgDown.classList.add('rich-select-display-none');
      }
      else {
        openButton.classList.remove('rich-select-toggler-down');
        openButton.classList.add('rich-select-toggler-up');
        innerWrap.classList.remove('rich-select-wrapper-down');
        innerWrap.classList.add('rich-select-wrapper-up');
        svgUp.classList.add('rich-select-display-none');
        svgDown.classList.remove('rich-select-display-none');
      };
    };
  };

  let openRichSelect = function() {
    innerWrap.classList.remove('rich-select-display-none');
    switchVertically();
    searchBar.focus();
    document.addEventListener("keyup", keyListener);
    document.addEventListener("click", clickListener);
    document.addEventListener("resize", switchVertically);
    document.addEventListener("scroll", switchVertically);
  };

  let closeRichSelect = function() {
    innerWrap.classList.add('rich-select-display-none');
    switchVertically();
    searchBar.value = '';
    searchBarAction();
    document.removeEventListener("keyup", keyListener);
    document.removeEventListener("click", clickListener);
    document.removeEventListener("resize", switchVertically);
    document.removeEventListener("scroll", switchVertically);
  };


  let searchBarAction = function() {
    let searchString   = searchBar.value.toLowerCase();
    var noMatchesFound = true;
    for (var i = 0, max = optionList.length; i < max; i += 1) {
      var option = optionList[i];
      if (option.innerHTML.toLowerCase().includes(searchString)) {
        option.classList.remove('rich-select-display-none');
        noMatchesFound = false;
      }
      else {
        option.classList.add('rich-select-display-none');
      }
    }
    if (noMatchesFound) {
      noneFound.classList.remove('rich-select-display-none');
    }
    else {
      noneFound.classList.add('rich-select-display-none');
    }
  };

  openButton.onclick = () => {
    if (innerWrap.classList.contains('rich-select-display-none')) {
      openRichSelect();
    }
    else { closeRichSelect(); }
  };

  searchBar.addEventListener("input", searchBarAction);

  for (var i = 0, max = optionList.length; i < max; i += 1) {
    let x = optionList[i].value;
    let y = optionList[i].innerHTML;
    optionList[i].onclick = function() {
      hiddenInput.value = x;
      publicVal.innerHTML = y;
      publicVal.classList.add('rich-select-fulfilled');
      closeRichSelect();
    }
  };

}


// -----------------------------------------------------------------------------
