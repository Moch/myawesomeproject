{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.SelectEnriched.Html where

import Data.Text (Text)
import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Lang
import Base.UI.Elems.SelectEnriched.Svg
import Base.UI.Elems.SelectEnriched.Translate as Translate

--------------------------------------------------------------------------------


richSelect ::
  Lang -> Text -> Bool -> Text -> [(Text,Text)]-> Html
richSelect lang identifier hasSearch ph content =
  H.div
    ! A.class_ "rich-select"
    ! A.id     (H.toValue identifier)
    $ do
      hiddenInput  identifier
      toggleButton ph
      H.div
        ! A.class_ "rich-select-wrapper rich-select-display-none"
        $ do
          searchBar  hasSearch
          listValues lang content
      buildScript identifier


--------------------------------------------------------------------------------


buildScript :: Text -> Html
buildScript identifier =
    H.script (H.toHtml $ "buildRichSelect('" <> identifier <> "');")


hiddenInput :: Text -> Html
hiddenInput identifier =
  H.input
    ! A.type_  "hidden"
    ! A.name   (H.toValue identifier)
    ! A.class_ "rich-select-hidden-input"
    ! A.value  ""


toggleButton :: Text -> Html
toggleButton buttonTag =
  H.button
    ! A.class_ "rich-select-toggler"
    ! A.type_  "button"
    $ do
      H.div
        ! A.class_ "rich-select-public-value"
        $ H.toHtml buttonTag
      grayTriangleUp
        ! A.class_ "rich-select-svg-up rich-select-display-none"
      grayTriangleDown
        ! A.class_ "rich-select-svg-down"


searchBar :: Bool -> Html
searchBar hasSearch =
  let
    displayOrNot = case hasSearch of
      True  -> ""
      False -> " rich-select-display-none"
  in
    H.label
      ! A.class_ (H.toValue $ "rich-select-search-wrapper" ++ displayOrNot)
      $ do
        H.input
          ! A.class_ "rich-select-search-bar"
        iconoLupa


listValues :: Lang -> [(Text,Text)] -> Html
listValues lang xs =
    H.div
      ! A.class_ "rich-select-list"
      $ do
        mapM_ f xs
        H.div
          ! A.class_ "rich-select-no-matches rich-select-display-none"
          $ H.toHtml $ Translate.noMatchesFound lang
  where
    f (x,y) =
      H.button (H.toHtml y)
        ! A.type_  "button"
        ! A.class_ "rich-select-option"
        ! A.value  (H.toValue x)


--------------------------------------------------------------------------------
