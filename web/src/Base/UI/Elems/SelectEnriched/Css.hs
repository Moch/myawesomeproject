{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.SelectEnriched.Css where


import Clay
import qualified Clay.Flexbox as Flex

import Base.Css.Utils

--------------------------------------------------------------------------------

richSelectCss :: Css
richSelectCss =
  associateCss
    [ (,) "rich-select-display-none"   (important $ display none)
    , (,) "rich-select"                selectCss
    , (,) "rich-select-fulfilled"      fulfilledCss
    , (,) "rich-select-toggler"        togglerCss
    , (,) "rich-select-toggler-up"     togglerGoUpCss
    , (,) "rich-select-toggler-down"   togglerGoDownCss
    , (,) "rich-select-wrapper"        wrapperCss
    , (,) "rich-select-wrapper-up"     wrapperGoUpCss
    , (,) "rich-select-wrapper-down"   wrapperGoDownCss
    , (,) "rich-select-search-wrapper" searchWrapperCss
    , (,) "rich-select-list"           listCss
    , (,) "rich-select-option"         optionCss
    , (,) "rich-select-no-matches"     noMatchesFoundCss
    ]

--------------------------------------------------------------------------------



selectCss :: Css
selectCss = do
  -- color
  backgroundColor white
  -- flex parent
  display inlineFlex
  -- geometry
  position     relative
  minWidth     (px 150)
  maxWidth     (px 250)
  minHeight    (Clay.rem 2)
  marginRight  (px 10)
  marginBottom (px 5)


fulfilledCss :: Css
fulfilledCss = do
  important $ fontColor dimgray


togglerCss :: Css
togglerCss = do
  -- border
  borderWithRadius solid (px 1) silver (px 3)
  outlineStyle none
  -- color
  backgroundColor white
  fontColor       darkgray
  -- display
  cursor pointer
  -- flex parent
  display    flex
  alignItems center
  -- geometry
  width (pct 100)
  minHeight (Clay.rem 2)
  padding `sym` (px 10)
  -- text
  textAlign start
  -- DIV
  Clay.div ? do
    flexGrow 1
    paddingRight (px 10)
  -- SVG
  svg ? do
    (-:) "fill" "darkgray"
    let x = px 7
    height x
    width  x


togglerGoUpCss :: Css
togglerGoUpCss = do
  -- border
  borderColor       dodgerblue
  borderTopColor    white
  borderRadius      (px 0) (px 0) (px 5) (px 5)
  (-:) "box-shadow" "0px 0px 4px 0px cornflowerblue"


togglerGoDownCss :: Css
togglerGoDownCss = do
  -- border
  borderColor       dodgerblue
  borderBottomColor white
  borderRadius      (px 5) (px 5) (px 0) (px 0)
  (-:) "box-shadow" "0px 0px 4px 0px cornflowerblue"


wrapperCss :: Css
wrapperCss = do
  -- border
  borderStyle  solid
  borderColor  dodgerblue
  borderWidth  (px 1)
  -- color
  backgroundColor white
  -- display
  zIndex 1
  -- flex parent
  display flex
  flexDirection column
  -- geometry
  position absolute
  width     (pct 100)
  maxHeight (px 200)



wrapperGoUpCss :: Css
wrapperGoUpCss = do
  -- border
  borderBottomStyle none
  borderRadius     (px 5) (px 5) (px 0) (px 0)
  (-:) "box-shadow" "0px -2px 6px -2px cornflowerblue"
  -- display
  bottom (Clay.rem 2)


wrapperGoDownCss :: Css
wrapperGoDownCss = do
  -- border
  borderTopStyle none
  borderRadius   (px 0) (px 0) (px 5) (px 5)
  (-:) "box-shadow" "0px 2px 6px -2px cornflowerblue, 0px 7px 7px -7px dimgray"
  -- display
  top (Clay.rem 2)


searchWrapperCss :: Css
searchWrapperCss = do
  -- border
  borderWithRadius solid (px 1) silver (px 5)
  outlineStyle none
  (-:) "box-shadow" "inset 0px 0px 1px 2px rgba(27,31,35,0.05)"
  -- display
  (-:) "cursor" "text"
  -- flex child
  Flex.flex 0 0 auto
  -- flex parent
  display    flex
  alignItems center
  justifyContent spaceBetween
  -- geometry
  margin  (px 5) (px  5) (px 0) (px  5)
  padding `sym` (px 5)
  -- INPUT
  input ? do
    borderStyle none
    width       (pct 82)
  -- SVG
  svg ? do
    (-:) "stroke" "gray"
    let x = px 16
    height x
    width  x


listCss :: Css
listCss = do
  -- display
  overflowY auto
  -- flex child
  flexGrow 1
  -- flex parent
  display       flex
  flexDirection column
  -- geometry
  margin `sym` (px 5)


optionCss :: Css
optionCss = do
  -- border
  borderStyle  none
  outlineStyle none
  -- color
  backgroundColor white
  hover & do
    backgroundColor $ rgb 50 120 180
    fontColor white
  -- display
  cursor pointer
  -- flex child
  Flex.flex 0 0 (pct 20)  -- This line is the result of a 5-hour research. DO NOT REMOVE
  -- geometry
  margin  `sym` (px 0)
  let x = px 10
  let y = px 5
  padding y x y x
  -- font
  fontFamily [zeroSerif] [sansSerif]
  -- text
  textAlign start


noMatchesFoundCss :: Css
noMatchesFoundCss = do
  -- border
  borderStyle  none
  outlineStyle none
  -- color
  backgroundColor $ rgb 255 245 235
  fontColor sienna
  -- geometry
  margin  `sym` (px 0)
  let x = px 10
  let y = px 2
  padding y x y x
  -- font
  fontFamily [zeroSerif] [sansSerif]



--------------------------------------------------------------------------------
