{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.SelectEnriched.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------

noMatchesFound :: Lang -> Text
noMatchesFound lang =
  case lang of
    ES -> "No hay resultados"
    _  -> "No matches found"


--------------------------------------------------------------------------------
