{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.GoldenLayout.Html where

import qualified Data.List.Safe              as Safe
import           Data.Numbers.Fibonacci
import           Data.Text                   (Text)
import           Text.Blaze.Html5            (Html, (!))
import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A


--------------------------------------------------------------------------------

data Dir = N | E | S | W


-- The following names make sense when drawing from the outside, squares first

horizontalWithSpiralNW :: [Dir]
horizontalWithSpiralNW = cycle [W,N,E,S]

horizontalWithSpiralNE :: [Dir]
horizontalWithSpiralNE = cycle [E,N,W,S]

horizontalWithSpiralSE :: [Dir]
horizontalWithSpiralSE = cycle [E,S,W,N]

horizontalWithSpiralSW :: [Dir]
horizontalWithSpiralSW = cycle [W,S,E,N]


verticalWithSpiralNW :: [Dir]
verticalWithSpiralNW = cycle [N,W,S,E]

verticalWithSpiralNE :: [Dir]
verticalWithSpiralNE = cycle [N,E,S,W]

verticalWithSpiralSE :: [Dir]
verticalWithSpiralSE = cycle [S,E,N,W]

verticalWithSpiralSW :: [Dir]
verticalWithSpiralSW = cycle [S,W,N,E]



dirToCss :: Dir -> Text
dirToCss d =
  case d of
    N -> "column-reverse"
    E -> "row"
    S -> "column"
    W -> "row-reverse"

--------------------------------------------------------------------------------

fibonacciLayout :: Text -> [Dir] -> Int -> [Html] -> Html
fibonacciLayout squareCssCN dirList pxScaleRatio contentList =
  let
    n = length contentList
    fibList = map fib $ reverse [1..n]
    dim k = show (k * pxScaleRatio) <> "px"
    makeRect (dir, k, content) innerThing =
      H.div
        ! A.class_ "fib-rect"
        ! A.style  (H.toValue $ "--fdir:" <> dirToCss dir)
        $ do
          H.div content
            ! A.style  (H.toValue $ "--dim: " <> dim k)
            ! A.class_ (H.toValue $ "fib-square " <> squareCssCN)
          innerThing
  in
    foldr makeRect "" $ zip3 dirList fibList contentList


fibLayoutHNW :: Int -> [Html] -> Html
fibLayoutHNW = fibonacciLayout "fib-with-gaps" horizontalWithSpiralNW


--------------------------------------------------------------------------------

fibonacciLayoutResponsive :: Text -> [([Dir], Maybe Int)] -> [Html] -> Html
fibonacciLayoutResponsive squareCssCN responsiveList contentList =
  let
    n = length contentList
    dim a b = show (a * b) <> "px"
    mobile    = (Safe.!!) responsiveList (4 :: Int)
    portrait  = (Safe.!!) responsiveList (3 :: Int)
    landscape = (Safe.!!) responsiveList (2 :: Int)
    laptop    = (Safe.!!) responsiveList (1 :: Int)
    desktop   = (Safe.!!) responsiveList (0 :: Int)
    calcDim x = maybe "" (dim (fib $ x+1)) . snd
    calcDir x = dirToCss . (!! (n-x-1)) . fst
    squareStyle k =
         "--omfg:hello"
      <> maybe "" (("; --dimMobile:    " <>) . calcDim k) mobile
      <> maybe "" (("; --dimPortrait:  " <>) . calcDim k) portrait
      <> maybe "" (("; --dimLandscape: " <>) . calcDim k) landscape
      <> maybe "" (("; --dimLaptop:    " <>) . calcDim k) laptop
      <> maybe "" (("; --dimDesktop:   " <>) . calcDim k) desktop
    rectStyle k =
         "--dummy:line"
      <> maybe "" (("; --dirMobile:    " <>) . calcDir k) mobile
      <> maybe "" (("; --dirPortrait:  " <>) . calcDir k) portrait
      <> maybe "" (("; --dirLandscape: " <>) . calcDir k) landscape
      <> maybe "" (("; --dirLaptop:    " <>) . calcDir k) laptop
      <> maybe "" (("; --dirDesktop:   " <>) . calcDir k) desktop
    makeRect (k, content) innerThing =
      H.div
        ! A.class_ "fib-rect-resp"
        ! A.style (H.toValue $ rectStyle k)
        $ do
          H.div content
            ! A.class_ (H.toValue $ "fib-square-resp " <> squareCssCN)
            ! A.style  (H.toValue $ squareStyle k)
          innerThing
  in
    foldr makeRect "" $ zip (reverse [0..(n-1)]) contentList


fibResponsiveWithGaps :: [([Dir], Maybe Int)] -> [Html] -> Html
fibResponsiveWithGaps = fibonacciLayoutResponsive "fib-with-gaps"


fibLayoutFullPage7 :: [Html] -> Html
fibLayoutFullPage7 =
  fibResponsiveWithGaps
    [ (,) horizontalWithSpiralNW (Just 59)
    , (,) horizontalWithSpiralNW (Just 49)
    , (,) verticalWithSpiralNW   (Just 57)
    , (,) [N,N,N,N,E,E,E,E,E,E]  Nothing
    , (,) [N,N,N,N,E,E,E,E,E,E]  Nothing
    ]

--------------------------------------------------------------------------------
