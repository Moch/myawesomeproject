{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.GoldenLayout.Css where


import Clay
-- import qualified Clay.Flexbox as Flex

import Base.Css.Utils

--------------------------------------------------------------------------------

goldenLayoutCss :: Css
goldenLayoutCss =
  associateCss
    [ (,) "fib-square"         fibSquare
    , (,) "fib-rect"           fibRect
    , (,) "fib-square-resp"    fibSquareResponsive
    , (,) "fib-rect-resp"      fibRectResponsive
    , (,) "fib-with-gaps"      fibSquareWithGapsCss
    ]

--------------------------------------------------------------------------------

fibSquare :: Css
fibSquare = do
  -- geometry
  "height" -: "var(--dim)"
  "width"  -: "var(--dim)"

fibRect :: Css
fibRect = do
  -- flex parent
  display flex
  "flex-direction" -: "var(--fdir, column-reverse)"



fibSquareResponsive :: Css
fibSquareResponsive = do
  -- geometry
  "height" -: "var(--dimMobile, 100%)"
  "width"  -: "var(--dimMobile, 100%)"
  portrait $ do
    "height" -: "var(--dimPortrait, 100%)"
    "width"  -: "var(--dimPortrait, 100%)"
  landscape $ do
    "height" -: "var(--dimLandscape, 100%)"
    "width"  -: "var(--dimLandscape, 100%)"
  laptop $ do
    "height" -: "var(--dimLaptop, 100%)"
    "width"  -: "var(--dimLaptop, 100%)"
  desktop $ do
    "height" -: "var(--dimDesktop, 100%)"
    "width"  -: "var(--dimDesktop, 100%)"

fibRectResponsive :: Css
fibRectResponsive = do
  -- flex parent
  display flex
  "flex-direction" -: "var(--dirMobile, column-reverse)"
  portrait  $ "flex-direction" -: "var(--dirPortrait, column-reverse)"
  landscape $ "flex-direction" -: "var(--dirLandscape, column-reverse)"
  laptop    $ "flex-direction" -: "var(--dirLaptop, column-reverse)"
  desktop   $ "flex-direction" -: "var(--dirDesktop, column-reverse)"




--------------------------------------------------------------------------------

fibSquareWithGapsCss :: Css
fibSquareWithGapsCss = do
  -- border solid (px 1) black
  -- geometry
  padding `sym` (px 3)
  desktop   $ padding `sym` (px 5)


--------------------------------------------------------------------------------
