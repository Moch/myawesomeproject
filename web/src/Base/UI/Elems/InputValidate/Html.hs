{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}


module Base.UI.Elems.InputValidate.Html where

import Data.Aeson
import Data.ByteString.Lazy.Char8 as BS (unpack)
import Data.Maybe (isNothing)
import Data.Text as T
import GHC.Generics
import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Lang
import Base.UI.Elems.InputValidate.Svg
import Base.UI.Elems.InputValidate.Translate as Translate


--------------------------------------------------------------------------------


data InputParams
  = InputParams
    { inputIdentifier   :: Text
    , inputValue        :: Text
    , inputPlaceholder  :: Text
    , inputCssClass     :: Text
    , inputLanguage     :: Lang
    , inputMinLength    :: Int
    , inputMaxLength    :: Int
    , inputOnlyEmail    :: Bool
    , inputOnlyAlphanum :: Bool
    , inputHasEyeButton :: Bool
    , inputExistRoute   :: Maybe Text
    , inputSubmitId     :: Text
    } deriving (Generic, Show)

instance ToJSON InputParams



-- Returns the input element and the build script element
-- The build script must be placed below the submit button
inputWithValidations :: InputParams -> (Html,Html)
inputWithValidations p =
  (,) (inputWrapper p) (buildScript p)


buildScript :: InputParams -> Html
buildScript p =
  H.script (H.toHtml $ BS.unpack $ "buildInputVal(" <> (encode $ toJSON p) <> ");")


inputWrapper :: InputParams -> Html
inputWrapper p@InputParams{..} =
  H.div
    ! A.id (H.toValue inputIdentifier)
    ! A.class_ "inputval-outerwrap"
    $ do
      H.div
        ! A.class_ "inputval-inputwrapper"
        $ do
          inputElement p
          if inputHasEyeButton then eyeButton else ""
      H.div
        ! A.class_ "inputval-js-tooshorterror inputval-errormsg inputval-hide"
        $ H.toHtml $ Translate.tooShort inputLanguage inputMinLength
      H.div
        ! A.class_ "inputval-js-toolongerror inputval-errormsg inputval-hide"
        $ H.toHtml $ Translate.tooLong inputLanguage inputMaxLength
      if not inputOnlyEmail then "" else
        H.div
          ! A.class_ "inputval-js-emailerror inputval-errormsg inputval-hide"
          $ H.toHtml $ Translate.invalidEmail inputLanguage
      if not inputOnlyAlphanum then "" else
        H.div
          ! A.class_ "inputval-js-notalphanumerror inputval-errormsg inputval-hide"
          $ H.toHtml $ Translate.notAlphanum inputLanguage
      if isNothing inputExistRoute then "" else
        H.div
          ! A.class_ "inputval-js-existerror inputval-errormsg inputval-hide"
          $ H.toHtml $ Translate.notAvailable inputLanguage


inputElement :: InputParams -> Html
inputElement InputParams{..} =
  H.input
    ! A.type_       (if inputHasEyeButton then "password" else "text")
    ! A.name        (H.toValue inputIdentifier)
    ! A.value       (H.toValue inputValue)
    ! A.placeholder (H.toValue inputPlaceholder)
    ! A.class_      ("inputval-js-input " <> H.toValue inputCssClass)


eyeButton :: Html
eyeButton =
  H.button
    ! A.type_   "button"
    ! A.class_  "inputval-js-eyebutton inputval-eyebutton"
    $ do
      closedEyeIcon
        ! A.class_ "inputval-js-closeye inputval-eyeicon"
      openEyeIcon
        ! A.class_ "inputval-js-openeye inputval-eyeicon inputval-hide"


--------------------------------------------------------------------------------
