{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.InputValidate.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

inputValidateCss :: Css
inputValidateCss =
  associateCss
    [ (,) "inputval-outerwrap"    outerCss
    , (,) "inputval-inputwrapper" wrapperCss
    , (,) "inputval-eyebutton"    eyeButtonCss
    , (,) "inputval-eyeicon"      eyeIconCss
    , (,) "inputval-errormsg"     errorMsgCss
    , (,) "inputval-hide"         (important $ display none)
    , (,) "inputval-inputerror"   inputErrorCss
    , (,) "inputval-inputok"      inputOkCss
    , (,) "inputval-disabled"     disabledButtonCss
    ]

--------------------------------------------------------------------------------

outerCss :: Css
outerCss = do
  -- geometry
  width (pct 100)
  marginBottom (Clay.rem 1.2)


wrapperCss :: Css
wrapperCss = do
  -- flex parent
  display flex
  justifyContent center
  alignItems center
  -- geometry
  width (pct 100)


eyeButtonCss :: Css
eyeButtonCss = do
  -- border
  borderStyle  none
  outlineStyle none
  -- color
  backgroundColor transparent
  -- display
  cursor pointer
  -- flex parent
  display flex
  justifyContent center
  alignItems     center
  -- geometry
  marginLeft    (px 10)
  padding `sym` (px 5)


eyeIconCss :: Css
eyeIconCss = do
  "stroke" -: "dimgray"
  "fill"   -: "dimgray"
  height (px 20)
  width  (px 20)


errorMsgCss :: Css
errorMsgCss = do
  -- color
  fontColor red
  -- font
  setFont normal (Clay.rem 0.9) [zeroSerif]
  -- geometry
  marginTop (Clay.rem 1)


inputErrorCss :: Css
inputErrorCss = important $ do
  -- background
  background $ url "/static/svg/redX.svg"
  backgroundPosition $ positioned (pct 95) (pct 50)
  backgroundSize $ (px 12) `by` (px 12)
  backgroundRepeat noRepeat
  -- color
  (-:) "box-shadow" "0px 0px 5px 2px rgba(255,0,0,0.5), 0px 0px 0px 100px white inset"
  borderColor red


inputOkCss :: Css
inputOkCss = important $ do
  -- background
  background $ url "/static/svg/greenTick.svg"
  backgroundPosition $ positioned (pct 95) (pct 50)
  backgroundSize $ (px 12) `by` (px 12)
  backgroundRepeat noRepeat


disabledButtonCss :: Css
disabledButtonCss = important $ do
  -- color
  backgroundColor firebrick
  backgroundImage $ url "/static/svg/whiteX.svg"
  backgroundPosition $ placed sideCenter sideCenter
  backgroundSize $ (px 20) `by` (px 20)
  backgroundRepeat noRepeat
  -- display
  (-:) "cursor" "not-allowed"
  -- font
  fontSize (px 0)
  -- text
  (-:) "text-shadow" "0 0 0 black"


--------------------------------------------------------------------------------
