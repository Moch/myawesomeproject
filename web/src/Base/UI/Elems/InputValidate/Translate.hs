{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.InputValidate.Translate where

import Data.Text as T

import Base.Lang


--------------------------------------------------------------------------------

tooShort :: Lang -> Int -> Text
tooShort lang n =
  pack $ case lang of
    ES -> "Error: longitud insuficiente, el mínimo es " ++ show n
    _  -> "Error: too short, minimum is " ++ show n


tooLong :: Lang -> Int -> Text
tooLong lang n =
  pack $ case lang of
    ES -> "Error: longitud excesiva, el máximo es " ++ show n
    _  -> "Error: too long, maximum is " ++ show n


notAlphanum :: Lang -> Text
notAlphanum lang =
  case lang of
    ES -> "Error: solo puede contener carácteres alfanuméricos o guiones simples, y no puede empezar o terminar en guión"
    _  -> "Error: it may only contain alphanumeric characters or single hyphens, and cannot begin or end with a hyphen"


notAvailable :: Lang -> Text
notAvailable lang =
  case lang of
    ES -> "Error: no está disponible"
    _  -> "Error: not available"


invalidEmail :: Lang -> Text
invalidEmail lang =
  case lang of
    ES -> "Error: dirección de correo electrónico inválida"
    _  -> "Error: invalid email address"


--------------------------------------------------------------------------------
