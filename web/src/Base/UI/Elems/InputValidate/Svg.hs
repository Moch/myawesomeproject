{-# LANGUAGE    OverloadedStrings       #-}


module Base.UI.Elems.InputValidate.Svg where

-- import Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
-- import qualified Text.Blaze.Svg11.Attributes as A

import Base.Svg.Faces
import Base.Svg.Utils

--------------------------------------------------------------------------------

openEyeIcon :: S.Svg
openEyeIcon = stdSvg openEye


closedEyeIcon :: S.Svg
closedEyeIcon = stdSvg closedEye

--------------------------------------------------------------------------------
