

function buildInputVal(inputParams){
  let wrapperElem    = document.getElementById(inputParams.inputIdentifier);
  let inputElem      = wrapperElem.querySelector('.inputval-js-input');
  let eyeButton      = wrapperElem.querySelector('.inputval-js-eyebutton');
  let closedEye      = wrapperElem.querySelector('.inputval-js-closeye');
  let openEye        = wrapperElem.querySelector('.inputval-js-openeye');
  let tooShortError  = wrapperElem.querySelector('.inputval-js-tooshorterror');
  let tooLongError   = wrapperElem.querySelector('.inputval-js-toolongerror');
  let emailError     = wrapperElem.querySelector('.inputval-js-emailerror');
  let alphanumError  = wrapperElem.querySelector('.inputval-js-notalphanumerror');
  let existenceError = wrapperElem.querySelector('.inputval-js-existerror');
  let submitButton   = document.getElementById(inputParams.inputSubmitId);


  function clearErrors(){
    submitButton.disabled = false;
    submitButton.classList.remove('inputval-disabled');
    inputElem.classList.remove('inputval-inputerror');
    inputElem.classList.remove('inputval-inputok');
    tooShortError.classList.add('inputval-hide');
    tooLongError.classList.add('inputval-hide');
    if (null !== inputParams.inputExistRoute){
      existenceError.classList.add('inputval-hide');
    };
    if (inputParams.inputOnlyEmail){
      emailError.classList.add('inputval-hide');
    };
    if (inputParams.inputOnlyAlphanum){
      alphanumError.classList.add('inputval-hide');
    };
  };


  function activateError(errorElem){
    errorElem.classList.remove('inputval-hide');
    inputElem.classList.add('inputval-inputerror');
  };


  function validateInputMinLength(){
    var isValid = inputElem.value.length >= inputParams.inputMinLength;
    if (!isValid) { activateError(tooShortError); };
    return isValid;
  };


  function validateInputMaxLength(){
    var isValid = inputElem.value.length <= inputParams.inputMaxLength;
    if (!isValid) { activateError(tooLongError); };
    return isValid;
  };


  function validateExistence(){
    let url   = inputParams.inputExistRoute + '?p=' + inputElem.value;
    var xhttp = new XMLHttpRequest();
    // xhttp.onreadystatechange = function() {
    //   if (this.readyState == 4 && this.status == 303) {
    //     activateError(existenceError);
    //   }
    // };
    xhttp.open("GET", url, false);
    xhttp.send();
    if (xhttp.status == 303) {
      activateError(existenceError);
      return false;
    }
    else {return true;}
  };


  function validateEmailSyntax(){
    var isValid = /^.+@.+\..+$/.test(inputElem.value);
    if (!isValid){ activateError(emailError) };
    return isValid;
  };


  function validateOnlyAlphanum(){
    let str = inputElem.value;
    let len = str.length;
    var isValid = true;
    var code, i;
    for (i = 0; i < len; i++) {
      code = str.charCodeAt(i);
      if (!(code > 47 && code < 58)   &&    // numeric (0-9)
          !(code > 64 && code < 91)   &&    // upper alpha (A-Z)
          !(code > 96 && code < 123)  &&    // lower alpha (a-z)
          !(code === 45)              ||    // hyphen
          (code === 45 && str.charCodeAt(i-1) === 45)) {
        isValid = false;
      }
    }
    if (str.charCodeAt(0) === 45 || str.charCodeAt(len-1) === 45) {
      isValid = false;
    }
    if (!isValid) { activateError(alphanumError); };
    return isValid;
  };


  function validateInput(){
    var isValid = true;
    isValid = isValid && validateInputMinLength();
    isValid = isValid && validateInputMaxLength();
    if (null !== inputParams.inputExistRoute) {
      isValid = isValid && validateExistence();
    };
    if (inputParams.inputOnlyEmail){
      isValid = isValid && validateEmailSyntax();
    };
    if (inputParams.inputOnlyAlphanum){
      isValid = isValid && validateOnlyAlphanum();
    };
    if (isValid){
      inputElem.classList.add('inputval-inputok');
    }
    return isValid;
  }


  function toggleVisibility(){
    if (inputElem.type == "password") {
      inputElem.type = "text";
      openEye.classList.remove('inputval-hide');
      closedEye.classList.add('inputval-hide');
    }
    else {
      inputElem.type = "password";
      openEye.classList.add('inputval-hide');
      closedEye.classList.remove('inputval-hide');
    }
  };




  if (inputParams.inputHasEyeButton) {
    eyeButton.addEventListener("click", toggleVisibility);
  };

  inputElem.addEventListener("input", clearErrors);
  inputElem.addEventListener("change", validateInput);

  submitButton.form.addEventListener("submit", (event) => {
    event.preventDefault();
    let formulary = submitButton.form;
    validateInput();
    var inputList  = formulary.querySelectorAll('.inputval-js-input');
    var allOk = true;
    for (var i = 0; i < inputList.length; i++) {
      allOk = allOk && inputList[i].classList.contains('inputval-inputok')
    }
    if (allOk) {
      submitButton.form.submit();
    }
    else {
      submitButton.classList.add('inputval-disabled');
    }
  });

};

// -----------------------------------------------------------------------------
