


function buildModalWindow(openButtonId) {
  let openButton  = document.getElementById(openButtonId);
  let modalMask   = document.getElementById(openButtonId + '_modal');
  let innerWindow = modalMask.querySelector('.modal-window-content');
  let closeButton = modalMask.querySelector('.modal-window-close-button');
  let keyListener = function(e) {
    switch (e.keyCode) {
      case 27 :
        closeModalWindow();
        break;
    }
  };

  function closeModalWindow() {
    modalMask.style.display = "none";
    document.removeEventListener("keyup", keyListener);
  }

  function openModalWindow() {
    modalMask.style.display = "flex";
    document.addEventListener("keyup", keyListener);
  }

  openButton.onclick  = openModalWindow;
  modalMask.onclick   = closeModalWindow;
  closeButton.onclick = closeModalWindow;
  innerWindow.onclick = function(event) {event.stopPropagation();};

}

// -----------------------------------------------------------------------------
