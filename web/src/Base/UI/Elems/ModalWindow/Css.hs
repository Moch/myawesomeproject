{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.ModalWindow.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

modalWindowCss :: Css
modalWindowCss =
  associateCss
    [ (,) "modal-window-mask"         maskCss
    , (,) "modal-window-content"      contentCss
    , (,) "modal-window-title"        titleCss
    -- , (,) "modal-window-close-button" buttonCss
    --         THIS STYLE GOES INLINE, BUT THE CLASS IS NEEDED FOR JAVASCRIPT
    ]

--------------------------------------------------------------------------------

maskCss :: Css
maskCss = do
  -- color
  backgroundColor $ rgba 0 0 0 0.4
  -- display
  position fixed
  zIndex 5
  -- flex parent
  display none
  justifyContent center
  alignItems center
  -- geometry
  left (px 0)
  top  (px 0)
  width  (pct 100)
  height (pct 100)


contentCss :: Css
contentCss = do
  -- border
  border solid (px 1) (rgb 159 166 173)
  -- box
  (-:) "box-shadow" "0px 2px 5px 1px rgba(12,13,14,0.3)"
  -- color
  backgroundColor white
  -- font
  setFont normal (Clay.rem 0.9) ["Arial"]
  -- geometry
  width  (px 300)
  portrait $ width (px 450)
  let x = px 25
  padding x x x x


titleCss :: Css
titleCss = do
  -- font
  fontWeight bold
  -- flex parent
  display flex
  justifyContent flexEnd
  alignItems center
  -- geometry
  marginBottom (px 20)
  height (px 40)
  -- DIV
  Clay.div ? do
    flexGrow 1


buttonCss :: Css
buttonCss = do
  -- border
  borderStyle  none
  outlineStyle none
  -- box
  (-:) "box-shadow" "0px 0px 0px 0px black"
  -- color
  background white
  (-:) "stroke" "dimgray"
  -- display
  cursor pointer
  -- flex parent
  display flex
  justifyContent center
  alignItems center
  -- geometry
  width  (px 40)
  height (px 40)
  margin  `sym` (px 0)
  padding `sym` (px 0)


--------------------------------------------------------------------------------
