{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.ModalWindow.Html where

import           Clay.Render (renderWith, htmlInline)
import           Data.Text
import           Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.UI.Elems.ModalWindow.Css (buttonCss)
import Base.UI.Elems.ModalWindow.Svg (closeIcon)


--------------------------------------------------------------------------------


modalWindow :: Text -> Html -> Html -> Html
modalWindow openButtonId heading innerContent =
  H.div
    ! A.class_ "modal-window-mask"
    ! A.id     (H.toValue $ openButtonId <> "_modal")
    $ do
       H.div
         ! A.class_ "modal-window-content"
         $ do
           H.div
             ! A.class_ "modal-window-title"
             $ do
               H.div heading
               modalWindowCloseButton
           innerContent
       modalWindowBuildScript openButtonId


--------------------------------------------------------------------------------

modalWindowCloseButton :: Html
modalWindowCloseButton =
  H.button
    ! A.type_  "button"
    ! A.class_ "modal-window-close-button"
    ! A.style (H.toValue $ renderWith htmlInline [] buttonCss)
    ! A.onmouseover "this.style.stroke = 'crimson'"
    ! A.onmouseout  "this.style.stroke = 'dimgray'"
    $ closeIcon
        ! A.style "width: 13px; height: 13px;"


modalWindowBuildScript :: Text -> Html
modalWindowBuildScript openButtonId =
    H.script (H.toHtml $ "buildModalWindow(" <> args <> ");")
  where
    modalId = openButtonId <> "_modal"
    args =
         "'" <> openButtonId <> "'"
      <> ","
      <> "'" <> modalId <> "'"


--------------------------------------------------------------------------------
