{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.MarkdownViewer.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

markdownViewerCss :: Css
markdownViewerCss =
  ".md-viewer" ? do
    containerCss
    p    ? paragraphCss
    a    ? anchorCss
    img  ? imageCss
    code ? codeCss
    pre  ? codeBlockCss

--------------------------------------------------------------------------------

containerCss :: Css
containerCss = do
  -- font
  fontColor black
  setFont normal (Clay.rem 1) [montserrat]
  -- geometry
  let x = Clay.rem 0.5
  padding x x x x
  marginTop (Clay.rem 2)
  width    (pct 100)
  maxWidth (pct 100)
  -- text
  textAlign justify
  overflowWrap breakWord


paragraphCss :: Css
paragraphCss = do
  -- geometry
  marginBottom (Clay.rem 1.5)


anchorCss :: Css
anchorCss = do
  -- color
  fontColor indigo
  hover & fontColor darkviolet
  -- text
  textDecoration none


imageCss :: Css
imageCss = do
  return ()


codeCss :: Css
codeCss = do
  -- color
  backgroundColor lavender
  fontColor coral
  -- geometry
  let x = px 2
  padding x x x x


codeBlockCss :: Css
codeBlockCss = do
  -- color
  backgroundColor "#282828"
  -- geometry
  padding 0 0 0 (px 0)


--------------------------------------------------------------------------------
