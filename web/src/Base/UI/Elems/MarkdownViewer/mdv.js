

var previewer = new showdown.Converter({extensions: ['youtube']});
previewer.setOption('noHeaderId', true);
previewer.setOption('parseImgDimensions', true);
previewer.setOption('headerLevelStart', 2);
previewer.setOption('simplifiedAutoLink', true);
previewer.setOption('strikethrough', true);
previewer.setOption('smoothLivePreview', true);
// previewer.setOption('simpleLineBreaks', true);
previewer.setOption('openLinksInNewWindow', true);
