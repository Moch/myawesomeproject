{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.MarkdownViewer.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

--------------------------------------------------------------------------------

mdViewer :: Html
mdViewer = do
  H.script "" ! A.src "/static/js/showdown.min.js"
  H.script "" ! A.src "/static/js/showdown-youtube.js"
  H.script "" ! A.src "/static/js/mdv.js"
  H.div ""
    ! A.class_ "md-viewer mde-preview"

--------------------------------------------------------------------------------
