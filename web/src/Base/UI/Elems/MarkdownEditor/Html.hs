{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}


module Base.UI.Elems.MarkdownEditor.Html where

import           Data.Text
import           Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import           Text.Blaze.Svg11 (Svg)

import Base.Lang
import Base.UI.Elems.MarkdownEditor.Svg
import qualified Base.UI.Elems.MarkdownEditor.Translate as Translate
import Base.UI.Elems.MarkdownViewer.Html (mdViewer)

--------------------------------------------------------------------------------

markdownEditor :: Lang -> Text -> Html
markdownEditor lang mdeId =
  do
    H.div
      ! A.id (H.toValue mdeId)
      ! A.class_ "mde-wrapper"
      $ do
        editorBar        lang
        editableTextarea
        linkModal        lang
        imageModal       lang
        videoModal       lang
        mdViewer
    H.script "" ! A.src "/static/js/mde.js"
    H.script (H.toHtml $ initMDE (pack $ show lang) mdeId)



initMDE :: Text -> Text -> Text
initMDE lang mdeId =
  "MarkdownEditor('" <> lang <> "' , '" <> mdeId <> "');"


editableTextarea :: Html
editableTextarea =
  H.textarea ""
    ! A.class_   "mde-textarea"
    ! A.required "required"


--------------------------------------------------------------------------------

editorBar :: Lang -> Html
editorBar lang =
  let
    wrapper =
      H.span ! A.class_ "mde-bar-part"
  in
    H.div ! A.class_ "mde-bar" $ do
      wrapper $ do
        boldButton   lang
        italicButton lang
      wrapper $ do
        linkButton  lang
        imageButton lang
        videoButton lang
      wrapper $ do
        numberListButton     lang
        bulletListButton     lang
        headerButton         lang
        horizontalRuleButton lang
      wrapper $ do
        undoButton lang
        redoButton lang
      helpButton lang



makeButton :: Svg -> Text -> Text -> Html
makeButton svgIcon buttonSelector tooltip =
  H.button svgIcon
    ! A.class_  (H.toValue $ "mde-button" <> " " <> buttonSelector)
    ! A.type_   "button"
    ! A.title   (H.toValue tooltip)



boldButton :: Lang -> Html
boldButton lang =
  makeButton boldIcon "mde-bold-button" $ Translate.bold lang


italicButton :: Lang -> Html
italicButton lang =
  makeButton italicIcon "mde-italic-button" $ Translate.italic lang


linkButton :: Lang -> Html
linkButton lang =
  makeButton linkIcon "mde-link-button" $ Translate.hyperlink lang


imageButton :: Lang -> Html
imageButton lang =
  makeButton imageIcon "mde-image-button" $ Translate.image lang


videoButton :: Lang -> Html
videoButton lang =
  makeButton videoIcon "mde-video-button" $ Translate.video lang


bulletListButton :: Lang -> Html
bulletListButton lang =
  makeButton bulletListIcon "mde-ul-button" $ Translate.bulletList lang


numberListButton :: Lang -> Html
numberListButton lang =
  makeButton numberListIcon "mde-ol-button" $ Translate.numberList lang


headerButton :: Lang -> Html
headerButton lang =
  makeButton headerIcon "mde-header-button" $ Translate.heading lang


horizontalRuleButton :: Lang -> Html
horizontalRuleButton lang =
  makeButton horizontalRuleIcon "mde-hr-button" $ Translate.horizontalRule lang


undoButton :: Lang -> Html
undoButton lang =
  makeButton undoIcon "mde-undo-button mde-disabled" (Translate.undo lang)
    ! A.disabled "disabled"


redoButton :: Lang -> Html
redoButton lang =
  makeButton redoIcon "mde-redo-button mde-disabled" (Translate.redo lang)
    ! A.disabled "disabled"


helpButton :: Lang -> Html
helpButton lang =
  H.a helpIcon
    ! A.class_ "mde-button"
    ! A.title  (H.toValue $ Translate.help lang)
    ! A.target "blank"
    ! A.href   (H.toValue $ Translate.helpUrl lang)


--------------------------------------------------------------------------------


data Modal = Modal
  { modalLang      :: Lang
  , modalFlag      :: Text
  , modalHeading   :: Lang -> Text
  , descInputTitle :: Lang -> Text
  , descInputPH    :: Lang -> Text
  , urlInputTitle  :: Lang -> Text
  , urlInputPH     :: Lang -> Text
  }


linkModal :: Lang -> Html
linkModal lang =
  modalWindow
    Modal
      { modalLang = lang
      , modalFlag = "link"
      , modalHeading   = Translate.insertHyperlink
      , descInputTitle = Translate.description
      , descInputPH    = Translate.textToShow
      , urlInputTitle  = Translate.urlAddress
      , urlInputPH     = Translate.exampleUrl
      }


imageModal :: Lang -> Html
imageModal lang =
  modalWindow
    Modal
      { modalLang = lang
      , modalFlag = "image"
      , modalHeading   = Translate.insertImage
      , descInputTitle = Translate.alternativeText
      , descInputPH    = Translate.willBeShownIfImageUnavailable
      , urlInputTitle  = Translate.imageUrl
      , urlInputPH     = Translate.imageUrlExample
      }


videoModal :: Lang -> Html
videoModal lang =
  modalWindow
    Modal
      { modalLang = lang
      , modalFlag = "video"
      , modalHeading   = Translate.insertVideo
      , descInputTitle = Translate.alternativeText
      , descInputPH    = Translate.willBeShownIfVideoUnavailable
      , urlInputTitle  = Translate.videoUrl
      , urlInputPH     = Translate.videoUrlExample
      }


modalWindow :: Modal -> Html
modalWindow Modal{..} =
  H.div
    ! A.class_ (H.toValue $ "mde-modal-window " <> "mde-modal-" <> modalFlag)
    $ H.div
      ! A.class_ "mde-modal-content"
      $ do
        H.div (H.toHtml $ modalHeading modalLang) ! A.class_ "mde-modal-title"
        H.div ! A.class_ "mde-modal-wrapper" $ do
          H.span (H.toHtml $ descInputTitle modalLang)
          H.input
            ! A.form   ""
            ! A.type_  "text"
            ! A.class_ "mde-modal-input"
            ! A.placeholder (H.toValue $ descInputPH modalLang)
        H.div ! A.class_ "mde-modal-wrapper" $ do
          H.span (H.toHtml $ urlInputTitle modalLang)
          H.input
            ! A.form   ""
            ! A.type_  "url"
            ! A.class_ "mde-modal-input"
            ! A.value  (H.toValue $ urlInputPH modalLang)
        H.div ! A.class_ "flex-center" $ do
          modalButtonOk
          modalButtonCancel modalLang


modalButtonOk :: Html
modalButtonOk =
  H.input
    ! A.class_  "mde-modal-button mde-modal-ok-button"
    ! A.type_   "button"
    ! A.value   "OK"


modalButtonCancel :: Lang -> Html
modalButtonCancel lang =
  H.input
    ! A.class_  "mde-modal-button mde-modal-cancel-button"
    ! A.type_   "button"
    ! A.value   (H.toValue $ Translate.cancel lang)

--------------------------------------------------------------------------------
