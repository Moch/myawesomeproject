{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.MarkdownEditor.Translate where

import Data.Text as T

import Base.Lang


--------------------------------------------------------------------------------


bold :: Lang -> Text
bold lang =
  case lang of
    ES -> "negrita"
    _  -> "bold"


italic :: Lang -> Text
italic lang =
  case lang of
    ES -> "cursiva"
    _  -> "italic"


hyperlink :: Lang -> Text
hyperlink lang =
  case lang of
    ES -> "insertar hipervínculo"
    _  -> "insert hyperlink"


image :: Lang -> Text
image lang =
  case lang of
    ES -> "insertar imagen"
    _  -> "insert image"


video :: Lang -> Text
video lang =
  case lang of
    ES -> "insertar video"
    _  -> "insert video"


bulletList :: Lang -> Text
bulletList lang =
  case lang of
    ES -> "lista punteada"
    _  -> "bullet list"


numberList :: Lang -> Text
numberList lang =
  case lang of
    ES -> "lista numerada"
    _  -> "number list"


heading :: Lang -> Text
heading lang =
  case lang of
    ES -> "título"
    _  -> "heading"


horizontalRule :: Lang -> Text
horizontalRule lang =
  case lang of
    ES -> "línea horizontal"
    _  -> "horizontal rule"


undo :: Lang -> Text
undo lang =
  case lang of
    ES -> "deshacer - Ctrl+Z"
    _  -> "undo - Ctrl+Z"


redo :: Lang -> Text
redo lang =
  case lang of
    ES -> "rehacer - Ctrl+Shift+Z"
    _  -> "redo - Ctrl+Shift+Z"


help :: Lang -> Text
help lang =
  case lang of
    ES -> "ayuda sobre markdown"
    _  -> "help about markdown"


helpUrl :: Lang -> Text
helpUrl lang =
  case lang of
    ES -> "https://es.wikipedia.org/wiki/Markdown"
    _  -> "https://en.wikipedia.org/wiki/Markdown"


--------------------------------------------------------------------------------


insertHyperlink :: Lang -> Text
insertHyperlink lang =
  case lang of
      ES -> "Insertar Hipervínculo"
      _  -> "Insert Hyperlink"


description :: Lang -> Text
description lang =
  case lang of
      ES -> "Descripción:"
      _  -> "Description:"


textToShow :: Lang -> Text
textToShow lang =
  case lang of
      ES -> "texto para mostrar"
      _  -> "text to show"


urlAddress :: Lang -> Text
urlAddress lang =
  case lang of
      ES -> "Dirección URL:"
      _  -> "URL address:"


exampleUrl :: Lang -> Text
exampleUrl lang =
  case lang of
      ES -> "http://ejemplo.com"
      _  -> "http://example.com"


insertImage :: Lang -> Text
insertImage lang =
  case lang of
      ES -> "Insertar Imagen"
      _  -> "Insert Image"


alternativeText :: Lang -> Text
alternativeText lang =
  case lang of
      ES -> "Texto alternativo:"
      _  -> "Alternative text:"


willBeShownIfImageUnavailable :: Lang -> Text
willBeShownIfImageUnavailable lang =
  case lang of
      ES -> "se mostrará si la imagen no está disponible"
      _  -> "will be shown if the image is unavailable"


imageUrl :: Lang -> Text
imageUrl lang =
  case lang of
      ES -> "Dirección URL de tu imagen:"
      _  -> "URL address of your image:"


imageUrlExample :: Lang -> Text
imageUrlExample lang =
  case lang of
      ES -> "https://imagen.svg"
      _  -> "https://image.svg"


insertVideo :: Lang -> Text
insertVideo lang =
  case lang of
      ES -> "Insertar Video"
      _  -> "Insert Video"


willBeShownIfVideoUnavailable :: Lang -> Text
willBeShownIfVideoUnavailable lang =
  case lang of
      ES -> "se mostrará si el video no está disponible"
      _  -> "will be shown if the video is unavailable"


videoUrl :: Lang -> Text
videoUrl lang =
  case lang of
      ES -> "Dirección URL de tu video:"
      _  -> "URL address of your video:"


videoUrlExample :: Lang -> Text
videoUrlExample lang =
  case lang of
      ES -> "https://www.youtube.com/watch?v=mlTO510zO78"
      _  -> "https://www.youtube.com/watch?v=mlTO510zO78"


cancel :: Lang -> Text
cancel lang =
  case lang of
    ES -> "Cancelar"
    _  -> "Cancel"


--------------------------------------------------------------------------------
