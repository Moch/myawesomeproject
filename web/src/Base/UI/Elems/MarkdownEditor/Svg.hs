{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.MarkdownEditor.Svg where

import Text.Blaze.Svg11 ((!), Svg)
import Text.Blaze.Svg11.Attributes (class_)

import Base.Svg.Textbox
import Base.Svg.Utils (stdSvg)

--------------------------------------------------------------------------------

addClass :: Svg -> Svg
addClass x = x ! class_ "mde-svg"

makeIcon :: Svg -> Svg
makeIcon = addClass . stdSvg

--------------------------------------------------------------------------------

boldIcon :: Svg
boldIcon = makeIcon bold

italicIcon :: Svg
italicIcon = makeIcon italic

linkIcon :: Svg
linkIcon = makeIcon link

imageIcon :: Svg
imageIcon = makeIcon image

videoIcon :: Svg
videoIcon = makeIcon video

bulletListIcon :: Svg
bulletListIcon = makeIcon bulletList

numberListIcon :: Svg
numberListIcon = makeIcon numberList

headerIcon :: Svg
headerIcon = makeIcon header

horizontalRuleIcon :: Svg
horizontalRuleIcon = makeIcon horizontalRule

undoIcon :: Svg
undoIcon = makeIcon undo

redoIcon :: Svg
redoIcon = makeIcon redo

helpIcon :: Svg
helpIcon = makeIcon questionMark


--------------------------------------------------------------------------------
