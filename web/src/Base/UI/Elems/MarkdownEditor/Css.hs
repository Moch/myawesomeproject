{-# LANGUAGE OverloadedStrings #-}


module Base.UI.Elems.MarkdownEditor.Css where

import Clay
import qualified Clay.Flexbox as Flex

import Base.Css.Utils

--------------------------------------------------------------------------------

markdownEditorCss :: Css
markdownEditorCss =
  associateCss
    [ (,) "mde-wrapper"  wrapperCss
    , (,) "mde-textarea" textareaCss
    , (,) "mde-bar"      barCss
    , (,) "mde-bar-part" barPartCss
    , (,) "mde-button"   buttonCss
    , (,) "mde-disabled" disabledButtonCss
    , (,) "mde-svg"      svgCss
    ----
    , (,) "mde-modal-window"  modalWindowCss
    , (,) "mde-modal-content" modalContentCss
    , (,) "mde-modal-title"   modalTitleCss
    , (,) "mde-modal-wrapper" modalWrapperCss
    , (,) "mde-modal-input"   modalInputCss
    , (,) "mde-modal-button"  modalButtonCss
    ]

--------------------------------------------------------------------------------

wrapperCss :: Css
wrapperCss = do
  -- geometry
  marginTop (Clay.rem 0.5)


textareaCss :: Css
textareaCss = do
  -- border
  border solid (px 1) silver
  borderTopStyle none
  outlineStyle none
  -- font
  setFont normal (Clay.rem 1) ["Helvetica"]
  -- geometry
  width (pct 100)
  height (Clay.rem 15)
  "resize" -: "vertical"
  let x = Clay.rem 0.5
  padding x x x x


barCss :: Css
barCss = do
  -- color
  backgroundColor cornsilk
  -- border
  border solid (px 1) silver
  -- flex parent
  display flex
  flexWrap Flex.wrap
  -- geometry
  padding (px 3) (px 8) (px 3) (px 8)


barPartCss :: Css
barPartCss = do
  -- flex child
  flexGrow 1
  -- flex parent
  display inlineFlex
  justifyContent flexStart
  alignItems center


buttonCss :: Css
buttonCss = do
  -- border
  borderStyle none
  -- color
  backgroundColor transparent
  hover & (svg ? (-:) "fill" "darkorange")
  -- display
  cursor pointer
  -- flex parent
  display inlineFlex
  justifyContent center
  alignItems center
  -- geometry
  let x = px 3
  padding x x x x
  let y = px 5
  margin y (px 15) y y


disabledButtonCss :: Css
disabledButtonCss =
  let
    svgColor = svg ? (-:) "fill" "rgba(45,45,45,0.2)"
  in do
    svgColor
    hover & do
      svgColor
      (-:) "cursor" "not-allowed"


svgCss :: Css
svgCss = do
  -- color
  (-:) "fill" "rgb(95,95,95)"
  -- geometry
  let x = px 20
  height x
  width  x
  margin 0 0 0 (px 0)

--------------------------------------------------------------------------------

modalWindowCss :: Css
modalWindowCss = do
  -- color
  backgroundColor $ rgba 0 0 0 0.4
  -- display
  position fixed
  zIndex 1
  -- flex parent
  display none
  justifyContent center
  alignItems center
  -- geometry
  left (px 0)
  top  (px 0)
  width  (pct 100)
  height (pct 100)


modalContentCss :: Css
modalContentCss = do
  -- border
  border solid (px 1) (rgb 159 166 173)
  -- box
  (-:) "box-shadow" "0px 2px 5px 1px rgba(12,13,14,0.3)"
  -- color
  backgroundColor white
  -- font
  setFont normal (Clay.rem 0.9) ["Arial"]
  -- geometry
  width  (px 300)
  portrait $ width (px 450)
  let x = px 25
  padding x x x x


modalTitleCss :: Css
modalTitleCss = do
  -- font
  fontWeight bold
  -- geometry
  marginBottom (px 25)


modalWrapperCss :: Css
modalWrapperCss = do
  -- flex parent
  display flex
  flexDirection column


modalInputCss :: Css
modalInputCss = do
  -- border
  border solid (px 1) lightsteelblue
  focus & borderColor royalblue
  -- font
  setFont normal (Clay.rem 0.9) ["Helvetica"]
  -- geometry
  marginTop (px 8)
  marginBottom (px 20)
  let x = px 8
  padding x x x x


modalButtonCss :: Css
modalButtonCss = do
  -- box
  (-:) "box-shadow" "0px 1px 0px 0px rgb(74,179,255) inset"
  -- border
  borderWithRadius solid (px 1) (rgb 0 103 177) (px 3)
  -- color
  backgroundColor (rgb 0 133 227)
  hover & backgroundColor (rgb 0 100 210)
  -- display
  cursor pointer
  -- font
  setFont normal (Clay.rem 0.85) ["Arial"]
  fontColor white
  -- geometry
  width  (px 95)
  height (px 32)
  let x = px 12
  marginLeft x
  marginRight x
  marginTop (px 5)


--------------------------------------------------------------------------------
