

function MarkdownEditor(lang, containerId) {

  const textAreaSelector  = '.mde-textarea'
  const previewSelector   = ".mde-preview"

  const boldButton        = '.mde-bold-button'
  const italicButton      = '.mde-italic-button'

  const linkButton        = ".mde-link-button"
  const linkModal         = ".mde-modal-link"
  const imageButton       = ".mde-image-button"
  const imageModal        = ".mde-modal-image"
  const videoButton       = ".mde-video-button"
  const videoModal        = ".mde-modal-video"

  const modalInnerContent = ".mde-modal-content"
  const modalAcceptButton = ".mde-modal-ok-button"
  const modalCancelButton = ".mde-modal-cancel-button"
  const modalInput        = ".mde-modal-input"

  const olButton     = ".mde-ol-button"
  const ulButton     = ".mde-ul-button"
  const headerButton = ".mde-header-button"
  const hrButton     = ".mde-hr-button"

  const undoButton = ".mde-undo-button"
  const redoButton = ".mde-redo-button"



  // ---------------------------------------------------------------------------

  let containerElem = document.getElementById(containerId);
  let taElem = containerElem.querySelector(textAreaSelector);
  let previewElem = containerElem.querySelector(previewSelector);
  var undoHistory = [];
  var taElemState = getRelevantInfo(taElem);
  var redoHistory = [];


  init();

  function init () {
    renderPreview();
    buildTextareaEvents();
    buildBold();
    buildItalic();
    buildLink();
    buildImage();
    buildVideo();
    buildOl();
    buildUl();
    buildHeader();
    buildHR();
    buildUndo();
    buildRedo();
  };


  function buildTextareaEvents () {
    taElem.oninput = function () {
      renderPreview();
      pushToUndoHistory();
    };
    taElem.onkeydown = function (event) {
      let ctrlZ      = event.keyCode == 90 && event.ctrlKey && !event.shiftKey
      let ctrlShiftZ = event.keyCode == 90 && event.ctrlKey && event.shiftKey
      if (undoHistory.length != 0 && ctrlZ)
        {event.preventDefault(); undo(); return};
      if (redoHistory.length != 0 && ctrlShiftZ)
        {event.preventDefault(); redo(); return};
    };
  }


  function buildBold () {
    let ph = 'strong text';
    switch (lang) {
      case 'ES':
        ph = 'texto en negrita';
        break;
    };
    let e = containerElem.querySelector(boldButton);
    e.onclick = function() {
      taInputProtocol( () => markdownBothSides('**' , ph) )
    };
  };


  function buildItalic () {
    let ph = 'italic text';
    switch (lang) {
      case 'ES':
        ph = 'texto en cursiva';
        break;
    };
    let e = containerElem.querySelector(italicButton);
    e.onclick = function () {
      taInputProtocol( () => markdownBothSides('_' , ph) )
    };
  };


  function buildLink () {
    let openButton   = containerElem.querySelector(linkButton);
    let modalMask    = containerElem.querySelector(linkModal);
    buildModal(openButton , modalMask);
  };


  function buildImage () {
    let openButton   = containerElem.querySelector(imageButton);
    let modalMask    = containerElem.querySelector(imageModal);
    buildModal(openButton , modalMask);
  };


  function buildVideo () {
    let openButton   = containerElem.querySelector(videoButton);
    let modalMask    = containerElem.querySelector(videoModal);
    buildModal(openButton , modalMask);
  };


  function buildOl () {
    let e = containerElem.querySelector(olButton);
    e.onclick = function() {
      taInputProtocol( () => startList("1.") )
    };
  }


  function buildUl () {
    let e = containerElem.querySelector(ulButton);
    e.onclick = function() {
      taInputProtocol( () => startList("-") )
    };
  }


  function buildHeader() {
    let ph = 'Heading';
    switch (lang) {
      case 'ES':
        ph = 'Título';
        break;
    };
    let e = containerElem.querySelector(headerButton);
    e.onclick = function() {
      taInputProtocol( () => makeHeader(ph))
    };
  };


  function buildHR() {
    let e = containerElem.querySelector(hrButton);
    e.onclick = function() {
      taInputProtocol( makeHR )
    };
  };


  function buildUndo() {
    let e = containerElem.querySelector(undoButton);
    e.onclick = function() { undo(); };
  };


  function buildRedo() {
    let e = containerElem.querySelector(redoButton);
    e.onclick = function() { redo(); };
  };



  // ---------------------------------------------------------------------------



  function taInputProtocol(action) {
    // pushToUndoHistory();    This was originally here, and it's the main reason for this function having an 'action' argument
    action();
    pushToUndoHistory();
    renderPreview();
    taElem.focus();
  }


  function renderPreview() {
    var preview = previewer.makeHtml(taElem.value);
    previewElem.innerHTML = preview;
  }






  function insert(text, x1, x2) {
    // insert text (or replace), and update selection ends (relatively)
    let v    = taElem.value
    let sel1 = taElem.selectionStart
    let sel2 = taElem.selectionEnd
    let init = v.substring( 0    , sel1     )
    let tail = v.substring( sel2 , v.length )
    taElem.value = init + text + tail
    taElem.selectionStart = sel1 + x1
    taElem.selectionEnd   = sel2 + x2
  }






  function alreadyMarkedBothSides(v, sel1, sel2, mark) {
    let lSide = v.substring(sel1 - mark.length, sel1)
    let rSide = v.substring(sel2 , sel2 + mark.length)
    return lSide === mark && rSide === mark
  }


  function markdownBothSides(mark, ph) {
    let v    = taElem.value
    let sel1 = taElem.selectionStart
    let sel2 = taElem.selectionEnd
    if (sel1 === sel2)
      insert(mark + ph + mark, mark.length, ph.length + mark.length);
      else {
        let text = v.substring(sel1,sel2);
        if (!alreadyMarkedBothSides(v, sel1, sel2, mark))
          insert(mark + text + mark, mark.length, mark.length);
          else {
            taElem.selectionStart = sel1 - mark.length;
            taElem.selectionEnd   = sel2 + mark.length;
            insert(text, 0, (-2)*mark.length)
          }
      }
  }






  function buildModal(openButton, modalMask) {
    let modalContent = modalMask.querySelector(modalInnerContent);
    let acceptButton = modalMask.querySelector(modalAcceptButton);
    let closeButton  = modalMask.querySelector(modalCancelButton);
    let keyListener  =
      function(event) {modalKeyboard(event, modalMask, keyListener);};
    openButton.onclick   = function() {openModal(modalMask, keyListener);};
    modalMask.onclick    = function() {closeModal(modalMask, keyListener);};
    closeButton.onclick  = function() {closeModal(modalMask, keyListener);};
    acceptButton.onclick = function() {
      taInputProtocol( () => acceptModal(modalMask, keyListener) );
    };
    modalContent.onclick = function(event) {event.stopPropagation();};
  };


  function openModal(modal, evListener) {
    modal.style.display = "flex";
    document.addEventListener("keyup", evListener);
    let firstInput = modal.querySelector(modalInput)
    firstInput.value =
      taElem.value.substring(taElem.selectionStart, taElem.selectionEnd);
    firstInput.focus()
  }


  function closeModal(modal, evListener) {
    modal.style.display = "none";
    document.removeEventListener("keyup", evListener);
    taElem.focus();
  }


  function modalKeyboard(e, modal, evListener) {
    switch (e.keyCode) {
      case 27 :
        closeModal(modal, evListener);
        break;
      case 13 :
        acceptModal(modal, evListener);
        break;
    };
  }


  function acceptModal(modal, evListener) {
    let inputList = modal.querySelectorAll(modalInput);
    var desc = inputList[0].value
    var url  = inputList[1].value
    var prefix = "!"
    var suffix = "=300x300"
    if (modal.className.includes("link")) {prefix = ''; suffix = '';}
    let t = ' ' + prefix + '[' + desc + '](' + url + suffix + ') '
    insert("",0,0)
    taElem.selectionEnd = taElem.selectionStart
    insert(t, t.length, t.length);
    closeModal(modal, evListener);
  }






  function startList(mark) {
    let v = taElem.value
    let selection = v.substring(taElem.selectionStart, taElem.selectionEnd);
    if (selection === "") {selection = "List item"};
    let prefix = "\n\n" + mark + " "
    let suffix = "\n\n"
    let text = prefix + selection + suffix
    insert(text, prefix.length , prefix.length);
    taElem.selectionEnd = taElem.selectionStart + selection.length
  }


  function makeHeader(placeholder) {
    let v = taElem.value
    let selection = v.substring(taElem.selectionStart, taElem.selectionEnd);
    if (selection === "") {selection = placeholder};
    let prefix = "\n\n" + "#" + " "
    let suffix = "\n\n"
    let text = prefix + selection + suffix
    insert(text, prefix.length , prefix.length);
    taElem.selectionEnd = taElem.selectionStart + selection.length
  }


  function makeHR() {
    let v = taElem.value
    let prefix = "\n\n" + "-----"
    let suffix = "\n\n"
    let text = prefix + suffix
    insert(text, text.length , text.length);
    taElem.selectionEnd = taElem.selectionStart
  }






  function disableButton(btn) {
    btn.disabled = true
    let c = btn.className
    if (c.indexOf(" mde-disabled") === -1) {
      btn.className = c + " mde-disabled"
    };
  }

  function enableButton(btn) {
    btn.disabled = false
    let c = btn.className
    btn.className = c.replace(/ mde-disabled/g, "")
  }

  function getRelevantInfo(ta) {
    return [ta.value, ta.selectionStart, ta.selectionEnd]
  }

  function setRelevantInfo(ta, info) {
    ta.value = info[0];
    ta.selectionStart = info[1];
    ta.selectionEnd = info[2];
  }

  function pushToUndoHistory() {
    redoHistory = []
    disableButton(containerElem.querySelector(redoButton))
    undoHistory.push(taElemState)
    enableButton(containerElem.querySelector(undoButton))
    let x = getRelevantInfo(taElem)
    taElemState = x
    }

  function undo() {
    redoHistory.push(taElemState)
    enableButton(containerElem.querySelector(redoButton))
    let info = undoHistory.pop()
    if (undoHistory.length === 0) {
      disableButton(containerElem.querySelector(undoButton))
    };
    setRelevantInfo(taElem, info)
    taElemState = info
    renderPreview();
    taElem.focus();
  }

  function redo() {
    undoHistory.push(taElemState)
    enableButton(containerElem.querySelector(undoButton))
    let info = redoHistory.pop()
    if (redoHistory.length === 0) {
      disableButton(containerElem.querySelector(redoButton))
    }
    setRelevantInfo(taElem, info)
    taElemState = info
    renderPreview();
    taElem.focus();
  }

}
