{-# LANGUAGE RecordWildCards #-}


module Base.Validate where

import Data.Text as T (Text, null, length, filter)


--------------------------------------------------------------------------------


data FileRestrictions = FileRestrictions
  { fileMinSize :: Integer
  , fileMaxSize :: Integer
  }

--------------------------------------------------------------------------------


data TextRestrictions = TextRestrictions
  { textMinLength  :: Int
  , textMaxLength  :: Int
  , textValidChars :: (Char -> Bool)
  }


validateText :: TextRestrictions -> Text -> Maybe Text
validateText TextRestrictions{..} t
    | textMinLength > l      = Nothing
    | textMaxLength < l      = Nothing
    | not $ T.null $ badList = Nothing
    | otherwise  = Just t
  where
    l = T.length t
    badList = T.filter (not . textValidChars) t


--------------------------------------------------------------------------------
