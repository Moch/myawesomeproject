{-# LANGUAGE OverloadedStrings #-}


module Root.UI.Views.Home.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

homeCss :: Css
homeCss =
  associateCss
    [ (,) "home-div" homeBodyCss
    ]

--------------------------------------------------------------------------------

homeBodyCss :: Css
homeBodyCss = do
  backgroundImage $ url "/static/pictures/wip.jpg"
  backgroundPosition $ placed sideCenter sideCenter
  backgroundRepeat noRepeat
  backgroundSize $ (px 500) `by` (px 500)
  -- flex parent
  display flex
  flexDirection  column
  justifyContent center
  -- flex child
  flexGrow 1
  -- font
  setFont normal (Clay.rem 1) [montserratAlt]
  -- geometry
  let x = Clay.rem 2
  paddingTop    x
  paddingBottom x
  let y = Clay.rem 10
  paddingLeft  y
  paddingRight y

--------------------------------------------------------------------------------
