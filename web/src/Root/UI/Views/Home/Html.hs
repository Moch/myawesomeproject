{-# LANGUAGE OverloadedStrings #-}


module Root.UI.Views.Home.Html where

import Text.Blaze.Html5 (Html, (!), toHtml)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Head
import Base.Html.Page
import Base.UI.Layout.Template.Html

--------------------------------------------------------------------------------

homePageView :: PageParams -> Html
homePageView params =
    toHtml homePage
  where
    homePage = Page
      { pageParams = params
      , htmlHead   = homeHead
      , htmlBody   = homeBody
      }


homeHead :: Head
homeHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

--------------------------------------------------------------------------------

homeBody :: PageParams -> Html
homeBody = mainTemplate home


home :: PageParams -> Html
home _ =
  H.div ! A.class_ "home-div" $ do
    H.h1 ""

--------------------------------------------------------------------------------
