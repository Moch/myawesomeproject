{-# LANGUAGE OverloadedStrings #-}


module Root.UI.Views.Found.Svg where

-- import Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
-- import qualified Text.Blaze.Svg11.Attributes as A

import Base.Svg.Religion
import Base.Svg.Utils

--------------------------------------------------------------------------------

foundImage :: S.Svg
foundImage = stdSvg cruzLatina

--------------------------------------------------------------------------------
