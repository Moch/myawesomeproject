{-# LANGUAGE OverloadedStrings #-}


module Root.UI.Views.Found.Html where

import Text.Blaze.Html5 (Html, (!), toHtml)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import           Base.Html.Head
import           Base.Html.Page
import           Base.UI.Layout.Template.Html
import           Root.UI.Views.Found.Svg

--------------------------------------------------------------------------------

foundPageView :: PageParams -> Html
foundPageView params =
    toHtml foundPage
  where
    foundPage = Page
      { pageParams = params
      , htmlHead   = foundHead
      , htmlBody   = mainTemplate foundBody
      }


foundHead :: Head
foundHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

--------------------------------------------------------------------------------


foundBody :: PageParams -> Html
foundBody _ =
  H.div
    ! A.class_ "found-body"
    $ do
      H.div
        ! A.class_ "found-background"
        $ foundImage


--------------------------------------------------------------------------------
