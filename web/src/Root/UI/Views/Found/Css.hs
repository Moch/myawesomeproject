{-# LANGUAGE OverloadedStrings #-}


module Root.UI.Views.Found.Css where

import Clay
-- import Data.Text (Text)

import Base.Css.Utils

--------------------------------------------------------------------------------

foundCss :: Css
foundCss =
  associateCss
    [ (,) "found-body"       bodyCss
    , (,) "found-background" backgroundCss
    ]

--------------------------------------------------------------------------------

bodyCss :: Css
bodyCss = do
  -- background
  (-:)
    "background-image"
    "radial-gradient(white, palegoldenrod, lightyellow, gold)"
  -- flex child
  flexGrow 1
  -- geometry
  let x = Clay.rem 0
  paddingTop    x
  paddingBottom x



backgroundCss :: Css
backgroundCss = do
  -- background
  backgroundImage    $ url "/static/svg/volutas.svg"
  backgroundPosition $ placed sideCenter sideCenter
  backgroundSize     $ (px 90) `by` (px 90)
  backgroundRepeat   Clay.repeat
  -- flex parent
  display flex
  flexDirection  column
  justifyContent center
  alignItems     center
  -- geometry
  height (pct 100)
  width  (pct 100)
  -- SVG
  svg ? do
    let y = px 300
    height y
    width  y


--------------------------------------------------------------------------------
