{-# LANGUAGE OverloadedStrings #-}


module Root.UI.Views.NotFound.Html where

import Text.Blaze.Html5 (Html, (!), toHtml)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import           Base.Html.Head
import           Base.Html.Page
import           Base.UI.Layout.Template.Html
import           Root.API.Routes
import qualified Root.UI.Views.NotFound.Translate as Translate

--------------------------------------------------------------------------------

notFoundPageView :: PageParams -> Html
notFoundPageView params =
    toHtml notFoundPage
  where
    notFoundPage = Page
      { pageParams = params
      , htmlHead   = notFoundHead
      , htmlBody   = mainTemplate notFoundBody
      }


notFoundHead :: Head
notFoundHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

--------------------------------------------------------------------------------


notFoundBody :: PageParams -> Html
notFoundBody p =
  let
    lang = ppLang p
    z2   = "ℤ" >> H.sub "2"
    z4   = "ℤ" >> H.sub "4"
    z101 = "ℤ" >> H.sub "101"
  in
    H.div ! A.class_ "not-found-outer-body" $
      H.div ! A.class_ "not-found-inner-body" $ do
        H.a
          ! A.href (H.toValue foundRoute)
          ! A.class_ "not-found-header" $ do
            H.h1  "404"
            H.h2  (H.toHtml $ Translate.notFound lang)
        H.div
          ! A.class_ "not-found-math"
          $ do
            H.toHtml $ Translate.anyGroup lang
            H.br
            H.span $ do
               z2
               " ⊕ "
               z2
               " ⊕ "
               z101
            H.toHtml $ Translate.or lang
            H.span $ do
              z4
              " ⊕ "
              z101


--------------------------------------------------------------------------------
