{-# LANGUAGE OverloadedStrings #-}


module Root.UI.Views.NotFound.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

notFoundCss :: Css
notFoundCss =
  associateCss
    [ (,) "not-found-outer-body" outerCss
    , (,) "not-found-inner-body" innerCss
    , (,) "not-found-header"     contentCss
    , (,) "not-found-math"       mathCss
    ]


--------------------------------------------------------------------------------

outerCss :: Css
outerCss = do
  -- background
  backgroundImage $ url "/static/svg/mosaicGeo.svg"
  backgroundSize $ (px 120) `by` (px 120)
  -- flex parent
  display flex
  flexDirection  column
  alignItems     center
  justifyContent center
  -- flex child
  flexGrow 1
  -- font
  fontFamily [smooth] [sansSerif]


innerCss :: Css
innerCss = do
  -- background
  (-:) "background-image" "linear-gradient(to right, rgba(0,0,0,0.3), white, rgba(0,0,0,0.3))"
  -- flex parent
  display flex
  flexDirection  column
  alignItems     center
  justifyContent center
  -- flex child
  flexGrow 1
  -- geometry
  width (pct 100)
  padding `sym` (Clay.rem 5)


contentCss :: Css
contentCss = do
  -- color
  background whitesmoke
  -- border
  outlineStyle     none
  borderWithRadius solid (px 2) dimgray (px 300)
  (-:) "box-shadow" "0 0 35px 15px white"
  -- flex parent
  display flex
  flexDirection  column
  alignItems     center
  justifyContent center
  -- geometry
  let x = px 300
  width  x
  height x
  marginBottom (px 40)
  -- text
  textDecoration none
  -- H1
  h1 ? do
    fontColor     whitesmoke
    fontSize      (px 70)
    letterSpacing (px 4)
    (-:) "text-shadow" "2px 0 0 black, -2px 0 0 black, 0 2px 0 black, 0 -2px 0 black"
  h2 ? do
    fontColor (rgb 50 50 50)
    fontSize  (px 25)
    marginTop (px 10)


mathCss :: Css
mathCss = do
  -- border
  borderWithRadius solid nil black (px 100)
  (-:) "box-shadow" "0 0 25px 10px white"
  -- color
  backgroundColor white
  -- geometry
  padding `sym` (px 10)
  -- text
  textAlign center
  -- SPAN
  Clay.span ? do
    let x = px 10
    marginLeft  x
    marginRight x


--------------------------------------------------------------------------------
