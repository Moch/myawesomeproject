{-# LANGUAGE OverloadedStrings #-}


module Root.UI.Views.NotFound.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------

notFound :: Lang -> Text
notFound lang =
  case lang of
    ES -> "No Encontrado"
    _  -> "Not Found"

anyGroup :: Lang -> Text
anyGroup lang =
  case lang of
    ES -> "Cualquier grupo abeliano de orden 404 es isomorfo a "
    _  -> "Any abelian group of order 404 is isomorphic to "

or :: Lang -> Text
or lang =
  case lang of
    ES -> " ó isomorfo a "
    _  -> " or isomorphic to"


--------------------------------------------------------------------------------
