{-# LANGUAGE OverloadedStrings #-}


module Root.API.Handler where

import Control.Monad (msum)
import Happstack.Server

import Base.API
import Base.Html.Page
import Root.API.Cookies
import Root.API.Routes
import Root.UI.Views.Found.Html
import Root.UI.Views.Home.Html
import Root.UI.Views.NotFound.Html

--------------------------------------------------------------------------------

rootHandler :: PageParams -> ServerPart Response
rootHandler p =
  msum
    [ (^-^) setLangRoute  putCookieLang
    , (^-^) foundRoute    (foundHandler p)
    , homeHandler p
    , notFoundHandler p
    ]

--------------------------------------------------------------------------------

foundHandler :: PageParams -> ServerPart Response
foundHandler p =
  do
    method GET
    ok $ toResponse (foundPageView p{ppRoute=foundRoute})


homeHandler :: PageParams -> ServerPart Response
homeHandler p =
  do
    nullDir
    method GET
    ok $ toResponse (homePageView p{ppRoute="/"})


notFoundHandler :: PageParams -> ServerPart Response
notFoundHandler p =
  do
    method GET
    notFound $ toResponse (notFoundPageView p{ppRoute=notFoundRoute})

--------------------------------------------------------------------------------
