{-# LANGUAGE OverloadedStrings #-}


module Root.API.Cookies where

import           Control.Applicative  (optional)
import           Data.Maybe           (maybe)
import qualified Data.Time            as Time
import           Data.Text.Lazy       (unpack)
import           Happstack.Server
import           Text.Read            (readMaybe)

import Base.Lang

--------------------------------------------------------------------------------

getCookieLang :: ServerPart Lang
getCookieLang = do
  mMemory <- optional $ lookCookieValue "language"
  let lang = maybe EN id $ mMemory >>= readMaybe
  return lang

--------------------------------------------------------------------------------

putCookieLang :: ServerPart Response
putCookieLang =
  let
    cookieEndDay = Time.fromGregorian 3000 1 1
    cookieEndSec = Time.secondsToDiffTime 0
    cookieLife   = Time.UTCTime cookieEndDay cookieEndSec
    mkLangCookie s = (Expires cookieLife , mkCookie "language" s)
  in do
    method POST
    langInput <- lookText "lang-nav-language-input"
    goToUrl   <- lookText "lang-nav-redirect-input"
    addCookies [ mkLangCookie $ unpack langInput ]
    seeOther goToUrl (toResponse ())

--------------------------------------------------------------------------------
