{-# LANGUAGE OverloadedStrings #-}


module Root.API.Routes where

import Data.Text

--------------------------------------------------------------------------------

setLangRoute :: Text
setLangRoute = "/set-lang"

notFoundRoute :: Text
notFoundRoute = "/404"

foundRoute :: Text
foundRoute = "/303"

--------------------------------------------------------------------------------
