{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SettingsNotifications.Html
  ( settingsNotificationsPageView
  ) where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Head
import Base.Html.Page
import Base.Lang
import Base.UI.Layout.Template.Html


--------------------------------------------------------------------------------

settingsNotificationsHead :: Head
settingsNotificationsHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

settingsNotificationsPageView :: PageParams -> Html
settingsNotificationsPageView params =
    H.toHtml page
  where
    page = Page
      { pageParams = params
      , htmlHead   = settingsNotificationsHead
      , htmlBody   = settingsTemplate settingsNotificationsBody
      }

settingsNotificationsBody :: PageParams -> Html
settingsNotificationsBody p =
  let
    lang = ppLang p
  in
    H.div ! A.class_ "" $ case lang of
      ES -> "Hola"
      _  -> "Hello"


--------------------------------------------------------------------------------
