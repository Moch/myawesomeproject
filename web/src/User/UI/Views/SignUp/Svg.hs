{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SignUp.Svg where

-- import Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
-- import qualified Text.Blaze.Svg11.Attributes as A

import Base.Svg.User
import Base.Svg.Utils

--------------------------------------------------------------------------------

personIcon :: S.Svg
personIcon = stdSvg personSimple

companyIcon :: S.Svg
companyIcon = stdSvg company

--------------------------------------------------------------------------------
