{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SignUp.Html
  ( signUpPageView
  ) where

import           Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import           Base.Html.Head
import           Base.Html.Page
import           Base.Lang
import           Base.Validate
import           Base.UI.Elems.InputValidate.Html
import           Base.UI.Layout.Template.Html
import           User.API.Routes
import           User.Logic.All
import           User.UI.Views.SignUp.Svg
import qualified User.UI.Views.SignUp.Translate as Translate

--------------------------------------------------------------------------------


signUpPageView :: UserType -> PageParams -> Html
signUpPageView utype params =
    H.toHtml signUpPage
  where
    signUpPage = Page
      { pageParams = params
      , htmlHead   = signUpHead utype
      , htmlBody   = mainTemplate $ signUpBody utype
      }


signUpHead :: UserType -> Head
signUpHead _ =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

--------------------------------------------------------------------------------

signUpBody :: UserType -> PageParams -> Html
signUpBody utype params =
  let
    lang = ppLang params
  in
    H.div ! A.class_ "sign-up-body" $ do
      H.h1
        ! A.class_ "sign-up-heading"
        $ H.toHtml $ Translate.joinAs lang
      H.div
        ! A.class_ "sign-up-form-wrapper"
        $ do
          switcher  utype lang
          formulary utype params
      backgroundImagePreloaderTrick


--------------------------------------------------------------------------------


switcher :: UserType -> Lang -> Html
switcher utype lang =
  let
    person = do
      personIcon
      H.toHtml $ Translate.person lang
    company = do
      companyIcon
      H.toHtml $ Translate.company lang
    wrapperDiv =
      H.div ! A.class_ "sign-up-switcher"
  in case utype of
    UserPerson ->
      wrapperDiv $ do
        H.div person
        H.a   company  ! A.href (H.toValue $ signUpRouteByType UserCompany)
    UserCompany ->
      wrapperDiv $ do
        H.a   person   ! A.href (H.toValue $ signUpRouteByType UserPerson)
        H.div company


formulary :: UserType -> PageParams -> Html
formulary utype params =
    H.form inputs
      ! A.method "post"
      ! A.action (H.toValue $ ppRoute params)
      ! A.class_ "sign-up-form"
  where
    inputs =
      mapM_ ($ (ppLang params)) $
        case utype of
          UserPerson  -> personForm
          UserCompany -> companyForm
    buildScriptForAllInputs l = do
      snd $ nameInput  l
      snd $ emailInput l
      snd $ pwInput    l
    personForm =
      [ fst . nameInput
      , fst . emailInput
      , fst . pwInput
      , signUpButton
      , buildScriptForAllInputs
      ]
    companyForm =
      personForm


nameInput :: Lang -> (Html,Html)
nameInput lang =
  inputWithValidations
    InputParams
      { inputIdentifier   = "name-input"
      , inputValue        = ""
      , inputPlaceholder  = Translate.username lang
      , inputCssClass     = "sign-up-input"
      , inputLanguage     = lang
      , inputMinLength    = textMinLength userNameRestrictions
      , inputMaxLength    = textMaxLength userNameRestrictions
      , inputOnlyEmail    = False
      , inputOnlyAlphanum = True
      , inputHasEyeButton = False
      , inputExistRoute   = Just userNameExistsRoute
      , inputSubmitId     = "submit-button"
      }


emailInput :: Lang -> (Html,Html)
emailInput lang =
  inputWithValidations
    InputParams
      { inputIdentifier   = "email-input"
      , inputValue        = ""
      , inputPlaceholder  = Translate.email lang
      , inputCssClass     = "sign-up-input"
      , inputLanguage     = lang
      , inputMinLength    = textMinLength userEmailRestrictions
      , inputMaxLength    = textMaxLength userEmailRestrictions
      , inputOnlyEmail    = True
      , inputOnlyAlphanum = False
      , inputHasEyeButton = False
      , inputExistRoute   = Just userEmailExistsRoute
      , inputSubmitId     = "submit-button"
      }


pwInput :: Lang -> (Html,Html)
pwInput lang =
  inputWithValidations
    InputParams
      { inputIdentifier   = "pw-input"
      , inputValue        = ""
      , inputPlaceholder  = Translate.password lang
      , inputCssClass     = "sign-up-input"
      , inputLanguage     = lang
      , inputMinLength    = textMinLength userPwRestrictions
      , inputMaxLength    = textMaxLength userPwRestrictions
      , inputOnlyEmail    = False
      , inputOnlyAlphanum = False
      , inputHasEyeButton = True
      , inputExistRoute   = Nothing
      , inputSubmitId     = "submit-button"
      }


--------------------------------------------------------------------------------


signUpButton :: Lang -> Html
signUpButton lang =
    H.input
      ! A.class_ "sign-up-submit-button"
      ! A.type_  "submit"
      ! A.value  (H.toValue $ Translate.createAccount lang)
      ! A.id     "submit-button"


--------------------------------------------------------------------------------

backgroundImagePreloaderTrick :: Html
backgroundImagePreloaderTrick = do
  H.img
    ! A.style "display: none"
    ! A.src "/static/pictures/palmtrees.png"


--------------------------------------------------------------------------------
