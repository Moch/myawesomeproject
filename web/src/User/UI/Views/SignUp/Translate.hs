{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SignUp.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------

joinAs :: Lang -> Text
joinAs lang =
  case lang of
    ES -> "Únete como"
    _  -> "Join as"


person :: Lang -> Text
person lang =
  case lang of
    ES -> "Persona"
    _  -> "Person"


company :: Lang -> Text
company lang =
  case lang of
    ES -> "Empresa"
    _  -> "Company"


username :: Lang -> Text
username lang =
  case lang of
    ES -> "Nombre de usuario (alias)"
    _  -> "Username (alias)"


email :: Lang -> Text
email lang =
  case lang of
    ES -> "Correo electrónico"
    _  -> "Email address"


password :: Lang -> Text
password lang =
  case lang of
    ES -> "Contraseña"
    _  -> "Password"


createAccount :: Lang -> Text
createAccount lang =
  case lang of
    ES -> "Crear cuenta"
    _  -> "Create account"

--------------------------------------------------------------------------------
