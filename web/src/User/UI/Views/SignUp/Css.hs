{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SignUp.Css where

import Clay
-- import qualified Clay.Flexbox as Flex
import Data.Text (Text)

import Base.Css.Utils

--------------------------------------------------------------------------------

signUpCss :: Css
signUpCss =
  associateCss
    [ (,) "sign-up-body"          bodyCss
    , (,) "sign-up-heading"       headingCss
    , (,) "sign-up-form-wrapper"  wrapperCss
    , (,) "sign-up-switcher"      switcherCss
    , (,) "sign-up-form"          formCss
    , (,) "sign-up-input"         inputCss
    , (,) "sign-up-submit-button" signUpButtonCss
    ]

--------------------------------------------------------------------------------

sunsetGradient :: Text
sunsetGradient
  =  "url(/static/pictures/palmtrees.png) no-repeat 85% 48% / 50px 50px"
  <> ","
  <> "linear-gradient"
  <> "( rgba(0,0,255,0.15)"
  <> ", rgba(190,80,190,0.15)"
  <> ", rgba(255,0,0,0.15)"
  <> ", rgba(255,165,0,0.3)"
  <> ", rgba(255,255,0,0.3) 50%"
  <> ", rgba(0,0,255,0.4) 50%"
  <> ")"
  <> ","
  <> "linear-gradient"
  <> "( to right"
  <> ", rgba(255,160,0,0.3)"
  <> ", rgba(255,105,180,0.3)"
  <> ", rgba(255,255,0,0.3) 20%"
  <> ", rgba(255,0,0,0.3)"
  <> ", rgba(220,20,60,0.3)"
  <> ", rgba(128,0,128,0.3)"
  <> ", rgba(50,50,255,0.3)"
  <> ", rgba(65,105,225,0.3)"
  <> ", rgba(0,0,128,0.3)"
  <> ")"
  <> ","
  <> "radial-gradient"
  <> "( circle farthest-corner at 20% 55%"
  <> ", rgba(195,195,195,0.15)"
  <> ", rgba(0,0,255,0.15)"
  <> ")"


bodyCss :: Css
bodyCss = do
  -- background
  (-:) "background" sunsetGradient
  -- flex parent
  display flex
  flexDirection  column
  justifyContent center
  alignItems     center
  -- flex child
  flexGrow 1
  -- geometry
  let x = Clay.rem 3
  paddingTop    x
  paddingBottom x


--------------------------------------------------------------------------------


headingCss :: Css
headingCss = do
  -- color
  fontColor indigo -- $ rgb 225 20 125
  -- font
  setFont normal (Clay.rem 2.5) [zeroSerif]
  -- geometry
  marginBottom (Clay.rem 2)
  paddingRight (Clay.rem 0.5)
  paddingLeft  (Clay.rem 0.5)
  -- text
  textAlign  center
  -- textShadow (px 1) (px 1) 0 greenyellow


--------------------------------------------------------------------------------

wrapperCss :: Css
wrapperCss = do
  -- color
  backgroundColor white
  -- border
  borderWithRadius solid (px 1) gainsboro (px 5)
  (-:) "box-shadow" "0px 0px 9px 0px rgba(0,0,0,0.1)"
  -- geometry
  width     (pct 100)
  maxWidth  (vw  96)
  landscape (maxWidth $ px 600)


switcherCss :: Css
switcherCss = do
  -- font
  setFont normal (Clay.rem 0.9) [smooth]
  fontColor dimgray
  (-:) "fill" "dimgray"
  (-:) "stroke" "dimgray"
  -- flex parent
  display flex
  -- geometry
  width (pct 100)
  height (Clay.rem 4)
  -- COMMON TO ANCHOR AND DIV
  let
    shared = do
      -- box
      boxSizing paddingBox
      -- flex parent
      display flex
      justifyContent center
      alignItems center
      -- geometry
      width (pct 100)
      height (pct 100)
      marginLeft (px (-1))
      marginRight (px (-1))
      -- text
      textAlign center
      textDecoration none
  -- ANCHOR
  a ? do
    shared
    -- border
    borderWithRadius solid (px 1) gainsboro (px 5)
    outlineStyle none
    -- color
    backgroundColor $ rgb 240 240 240
    (-:) "fill" "darkgray"
    (-:) "stroke" "darkgray"
    fontColor darkgray
    hover & do
      fontColor darkcyan
      (-:) "fill" "darkcyan"
      (-:) "stroke" "darkcyan"
      textDecoration underline
  -- DIV
  Clay.div ? do
    shared
    -- border
    borderStyle4 solid solid none solid
    borderWidth $ px 1
    borderColor gainsboro
    borderRadius `sym` (px 5)
    -- color
    backgroundColor white
    -- display
    cursor cursorDefault
  -- SVG
  svg ? do
    let x = Clay.rem 2.5
    height x
    width x
    marginRight (px 5)



formCss :: Css
formCss = do
  -- flex parent
  display flex
  flexDirection column
  justifyContent center
  alignItems center
  -- geometry
  let x1 = px 15
  let x2 = px 35
  let x3 = px 50
  let y = px 35
  padding y x1 y x1
  portrait $ padding y x2 y x2
  landscape $ padding y x3 y x3
  width (pct 100)


inputCss :: Css
inputCss = do
  -- border
  borderWithRadius solid (px 1) gainsboro (px 5)
  focus & do
    borderColor $ rgba 30 144 255 0.8
    (-:) "box-shadow" "0px 0px 5px 0px rgba(30, 144, 255, 0.5), 0px 0px 0px 100px white inset"
  -- font
  setFont normal (Clay.rem 1.2) [zeroSerif]
  -- geometry
  width        (pct 100)
  height       (Clay.rem 3)
  paddingLeft  (px 10)
  paddingRight (pct 10)


--------------------------------------------------------------------------------

signUpButtonCss :: Css
signUpButtonCss = do
  -- border
  borderStyle none
  borderRadius `sym` (px 5)
  outlineStyle none
  -- color
  backgroundColor $ rgb 0 205 100
  hover & backgroundColor (rgb 0 255 110)
  -- display
  cursor pointer
  -- font
  setFont   normal (Clay.rem 1.8) [smooth]
  fontColor white
  -- geometry
  height       (Clay.rem 3.5)
  width        (pct 100)
  minWidth     (px 170)
  marginTop    (Clay.rem 0.5)
  -- text
  (-:) "text-shadow" "1px 0 0 teal, -1px 0 0 teal, 0 1px 0 teal, 0 -1px 0 teal"


--------------------------------------------------------------------------------
