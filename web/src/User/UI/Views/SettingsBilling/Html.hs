{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SettingsBilling.Html
  ( settingsBillingPageView
  ) where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Head
import Base.Html.Page
import Base.Lang
import Base.UI.Layout.Template.Html


--------------------------------------------------------------------------------

settingsBillingHead :: Head
settingsBillingHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

settingsBillingPageView :: PageParams -> Html
settingsBillingPageView params =
    H.toHtml page
  where
    page = Page
      { pageParams = params
      , htmlHead   = settingsBillingHead
      , htmlBody   = settingsTemplate settingsBillingBody
      }

settingsBillingBody :: PageParams -> Html
settingsBillingBody p =
  let
    lang = ppLang p
  in
    H.div ! A.class_ "" $ case lang of
      ES -> "Hola"
      _  -> "Hello"


--------------------------------------------------------------------------------
