{-# LANGUAGE    OverloadedStrings       #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}


module User.UI.Views.SettingsProfile.Svg where

-- import Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
-- import qualified Text.Blaze.Svg11.Attributes as A

import Base.Svg.Social
import Base.Svg.User
import Base.Svg.Utils

--------------------------------------------------------------------------------

personIcon :: S.Svg
personIcon = stdSvg personSimple

companyIcon :: S.Svg
companyIcon = stdSvg company

--------------------------------------------------------------------------------

githubIcon :: S.Svg
githubIcon = github

linkedinIcon :: S.Svg
linkedinIcon = linkedin

mediumIcon :: S.Svg
mediumIcon = medium

stackoverflowIcon :: S.Svg
stackoverflowIcon = stackoverflow

twitterIcon :: S.Svg
twitterIcon = twitter



--------------------------------------------------------------------------------
