{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SettingsProfile.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

settingsProfileCss :: Css
settingsProfileCss =
  associateCss
    [ (,) "settings-profile-corewrap"    wrapperCss
    , (,) "settings-profile-picture"     pictureCss
    , (,) "settings-profile-languages"   languagesSectionCss
    , (,) "settings-profile-social"      socialCss
    ]

--------------------------------------------------------------------------------


wrapperCss :: Css
wrapperCss = do
  -- flex parent
  display       flex
  flexDirection column
  desktop $ do
    flexDirection rowReverse
  -- geometry
  width (pct 100)


pictureCss :: Css
pictureCss = do
  -- border
  borderWithRadius solid (px 1) silver (px 5)
  -- color
  (-:) "fill"   "darkgray"
  (-:) "stroke" "darkgray"
  -- geometry
  let x = px 250
  width  x
  height x


languagesSectionCss :: Css
languagesSectionCss = do
  display block
  marginBottom (Clay.rem 1.8)
  p ? do
    fontWeight bold
    marginBottom (Clay.rem 0.5)


socialCss :: Css
socialCss = do
  -- flex parent
  display flex
  alignItems     center
  -- SVG
  svg ? do
    (-:) "stroke"       "dimgray"
    (-:) "fill"         "dimgray"
    (-:) "stroke-width" "0"
    width  (Clay.rem 1.8)
    height (Clay.rem 1.8)
    marginRight (Clay.rem 0.8)
    overflow visible



--------------------------------------------------------------------------------
