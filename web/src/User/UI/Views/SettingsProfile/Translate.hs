{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SettingsProfile.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------

updatedProfileOkMsg :: Lang -> Text
updatedProfileOkMsg lang = case lang of
  ES -> "Tu perfil ha sido actualizado"
  _  -> "Your profile has been updated"


publicProfile :: Lang -> Text
publicProfile lang =
  case lang of
    ES -> "Perfil público"
    _  -> "Public profile"


companyLogo :: Lang -> Text
companyLogo lang =
  case lang of
   ES -> "Logo de empresa"
   _  -> "Company logo"


profilePicture :: Lang -> Text
profilePicture lang =
  case lang of
    ES -> "Foto de perfil"
    _  -> "Profile picture"


name :: Lang -> Text
name lang =
  case lang of
    ES -> "Nombre"
    _  -> "Name"


publicEmail :: Lang -> Text
publicEmail lang =
  case lang of
    ES -> "Email público"
    _  -> "Public email"


location :: Lang -> Text
location lang =
  case lang of
    ES -> "Ubicación"
    _  -> "Location"


humanLanguages :: Lang -> Text
humanLanguages lang =
  case lang of
    ES -> "Lenguajes humanos"
    _  -> "Human languages"


chooseLanguage :: Lang -> Text
chooseLanguage lang =
  case lang of
    ES -> "Selecciona un idioma"
    _  -> "Select a language"


level :: Lang -> Text
level lang =
  case lang of
    ES -> "Nivel"
    _  -> "Level"


summary :: Lang -> Text
summary lang =
  case lang of
    ES -> "Descripción escueta"
    _  -> "Summary"


updateProfile :: Lang -> Text
updateProfile lang =
  case lang of
    ES -> "Actualizar perfil"
    _  -> "Update profile"


yourSocial :: Lang -> Text
yourSocial lang =
  case lang of
    ES -> "Tus caras en Internet"
    _  -> "Your faces on the Internet"


saveChanges :: Lang -> Text
saveChanges lang =
  case lang of
    ES -> "Guardar cambios"
    _  -> "Save changes"    


--------------------------------------------------------------------------------
