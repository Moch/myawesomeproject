{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SettingsProfile.Html
  ( settingsProfilePageView
  , updatedProfileOkMsg
  ) where

import           Data.Text        (Text, pack)
import           Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import           Base.Html.Head
import           Base.Html.Page
import           Base.Html.Utils
import           Base.Lang
import           Base.UI.Elems.ClosableMsg.Html
import           Base.UI.Elems.PhotoCropper.Html
import           Base.UI.Elems.SelectMulti.Html
import           Base.UI.Layout.Template.Html
import           User.Logic.All
import           User.UI.Views.SettingsProfile.Svg
import qualified User.UI.Views.SettingsProfile.Translate as Translate


--------------------------------------------------------------------------------

settingsProfileHead :: Head
settingsProfileHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

settingsProfilePageView :: PageParams -> FullUser -> Html
settingsProfilePageView params u =
    H.toHtml page
  where
    page = Page
      { pageParams = params
      , htmlHead   = settingsProfileHead
      , htmlBody   = settingsTemplate (settingsProfileBody u)
      }

settingsProfileBody :: FullUser -> PageParams -> Html
settingsProfileBody u p =
  let
    lang  = ppLang  p
    route = ppRoute p
    uType = maybe UserPerson snd $ ppUser p
  in do
    errorMsg $ ppErrorMsg p
    okMsg    $ ppOkMsg    p
    coreProfile route lang uType (uProfile u) (uLangs  u)
    socialLinks route lang uType (uSocial u)


updatedProfileOkMsg :: Lang -> Text
updatedProfileOkMsg = Translate.updatedProfileOkMsg

--------------------------------------------------------------------------------

coreProfile :: Text -> Lang -> UserType -> Profile -> [ProfileLang] -> Html
coreProfile route lang uType info langs = do
    H.h1 $ H.toHtml $ Translate.publicProfile lang
    H.div
      ! A.class_ "settings-profile-corewrap"
      $ do
        photoForm
        coreForm
  where
    photoForm =
      H.form
        ! A.method "post"
        ! A.action (H.toValue route)
        ! A.enctype "multipart/form-data"
        $ do
          goalInput "update-profile-photo"
          photoSection lang uType Nothing
    coreForm =
      H.form
        ! A.method "post"
        ! A.action (H.toValue route)
        ! A.enctype "multipart/form-data"
        ! A.style "flex-grow: 1;"
        $ do
          goalInput "update-profile"
          nameInput         lang $ profileName        info
          emailInput        lang $ profileEmail       info
          locationInput     lang $ profileLocation    info
          languagesSection  lang langs
          description       lang $ profileDescription info
          updateProfileBtn  lang


--------------------------------------------------------------------------------


photoSection :: Lang -> UserType -> Maybe Text -> Html
photoSection lang uType mRoute =
    H.div
      ! A.class_ "layout-settings-inputwrap"
      $ do
        H.p heading
        photo
        photoCropperOpenButton
          lang
          "layout-settings-button flex-center"
          "photo-input"
  where
    heading =
      H.toHtml $ case uType of
        UserCompany -> Translate.companyLogo    lang
        UserPerson  -> Translate.profilePicture lang
    photo = case mRoute of
      Nothing ->
        photoPlaceholder
          ! A.class_ "settings-profile-picture"
      Just r  ->
        H.img
          ! A.class_ "settings-profile-picture"
          ! A.src    (H.toValue r)
    photoPlaceholder = case uType of
      UserCompany -> companyIcon
      UserPerson  -> personIcon


--------------------------------------------------------------------------------


nameInput :: Lang -> Text -> Html
nameInput lang pName =
  H.label
    ! A.class_ "layout-settings-inputwrap"
    $ do
      H.p $ H.toHtml $ Translate.name lang
      H.input
        ! A.type_  "text"
        ! A.name   "name-input"
        ! A.class_ "layout-settings-input"
        ! A.value  (H.toValue pName)


emailInput :: Lang -> Text -> Html
emailInput lang pEmail =
  H.label
    ! A.class_ "layout-settings-inputwrap"
    $ do
      H.p $ H.toHtml $ Translate.publicEmail lang
      H.input
        ! A.type_  "email"
        ! A.name   "email-input"
        ! A.class_ "layout-settings-input"
        ! A.value  (H.toValue pEmail)


locationInput :: Lang -> Text -> Html
locationInput lang pLocation =
  H.label
    ! A.class_ "layout-settings-inputwrap"
    $ do
      H.p $ H.toHtml $ Translate.location lang
      H.input
        ! A.type_  "text"
        ! A.name   "location-input"
        ! A.class_ "layout-settings-input"
        ! A.value  (H.toValue pLocation)


languagesSection :: Lang -> [ProfileLang] -> Html
languagesSection lang langList =
    H.div
      ! A.class_ "settings-profile-languages"
      $ do
        H.p $ H.toHtml $ Translate.humanLanguages lang
        select2 lang "profile_langs"
          (True,  ph1, opt1)
          (False, ph2, opt2)
          (map (f . profileLangCode)  langList)
          (map (f . profileLangLevel) langList)
  where
    f x = (,) (pack $ show x) (translate lang x)
    ph1 = Translate.chooseLanguage lang
    ph2 = Translate.level lang
    opt2 =
      map f
        [ LevelBasic
        , LevelAdvanced
        , LevelFluent
        ]
    opt1 =
      map f [AA .. ZU]



description :: Lang -> Text -> Html
description lang pDescription =
  H.label
    ! A.class_ "layout-settings-inputwrap"
    $ do
      H.p $ H.toHtml $ Translate.summary lang
      H.textarea
        ! A.name   "description-input"
        ! A.class_ "layout-settings-input"
        ! A.style  "height: 8rem; "
        $ H.toHtml pDescription


updateProfileBtn :: Lang -> Html
updateProfileBtn lang =
  H.button
    ! A.type_  "submit"
    ! A.class_ "layout-settings-button layout-settings-green"
    $ H.toHtml $ Translate.updateProfile lang


--------------------------------------------------------------------------------


socialLinks :: Text -> Lang -> UserType -> [ProfileLink] -> Html
socialLinks route lang uType linkList =
  H.form
    ! A.method "post"
    ! A.action (H.toValue route)
    $ do
      H.h1 $ H.toHtml $ Translate.yourSocial lang
      goalInput "update-social"
      socialInputs lang uType linkList
      saveChangesButton lang


socialInputs :: Lang -> UserType -> [ProfileLink] -> Html
socialInputs lang uType linkList = do
    mapM_ (g . f)
      [ Selflink
      , Github
      , Linkedin
      , Medium
      , Stackoverflow
      , Twitter
      ]
  where
    g (x,y) =
      socialInputTemplate
        (associateIcon x)
        (translateSocialPlatform uType lang x)
        y
    f x = (,) x $ maybe "" id $ lookup x pairL
    pairL = map (\x -> (,) (profileLinkPlatform x) (profileLinkUrl x)) linkList
    associateIcon p = case p of
      Selflink      -> portfolioIcon
      Github        -> githubIcon
      Linkedin      -> linkedinIcon
      Medium        -> mediumIcon
      Stackoverflow -> stackoverflowIcon
      Twitter       -> twitterIcon
    portfolioIcon = case uType of
      UserCompany -> companyIcon
      UserPerson  -> personIcon


socialInputTemplate :: Html -> Text -> Text -> Html
socialInputTemplate svgIcon x y =
  H.label
    ! A.class_ "layout-settings-inputwrap"
    $ do
      H.p (H.toHtml x)
      H.div
        ! A.class_ "settings-profile-social"
        $ do
          svgIcon
          H.input
            ! A.type_  "text"
            ! A.name   "social-link-input"
            ! A.class_ "layout-settings-input"
            ! A.value  (H.toValue y)


saveChangesButton :: Lang -> Html
saveChangesButton lang =
  H.button
    ! A.type_  "submit"
    ! A.class_ "layout-settings-button"
    $ H.toHtml $ Translate.saveChanges lang

--------------------------------------------------------------------------------
