{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SignUpEval.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

signUpEvalCss :: Css
signUpEvalCss =
  associateCss
    [ (,) "sign-up-eval-body"        bodyCss
    , (,) "sign-up-eval-error"       errorCss
    , (,) "sign-up-eval-ok"          okCss
    , (,) "sign-up-eval-right"       goRightCss
    , (,) "sign-up-eval-block"       blockCss
    , (,) "sign-up-eval-welcome"     welcomeCss
    , (,) "sign-up-eval-jobslink"    jobsCss
    , (,) "sign-up-eval-gobacklink"  goBackCss
    , (,) "sign-up-eval-profilelink" profileCss
    ]

--------------------------------------------------------------------------------

bodyCss :: Css
bodyCss = do
  -- font
  setFont normal (Clay.rem 0.9) [smooth]
  fontColor black
  -- flex parent
  display flex
  flexDirection  column
  justifyContent center
  alignItems     center
  -- flex child
  flexGrow 1
  -- geometry
  let x = Clay.rem 3
  paddingTop    x
  paddingBottom x
  -- ELEMENTS
  h1 ? do
    -- font
    fontSize (Clay.rem 1.2)
    -- geometry
    marginTop    (Clay.rem 1)
    marginBottom (Clay.rem 1)
    paddingRight (Clay.rem 0.5)
    paddingLeft  (Clay.rem 0.5)
    -- text
    textAlign  center




errorCss :: Css
errorCss = do
  -- color
  (-:) "background" "radial-gradient(circle farthest-corner at 50% 50%, white 50%, rgb(255,220,220))"
  h1 ? fontColor chocolate
  svg ? do
    (-:) "fill"   "orangered"
    (-:) "stroke" "orangered"
    width  (px 90)
    height (px 90)
    margin `sym` (Clay.rem 1.5)
  Clay.span ? do
    -- font
    fontColor maroon
    fontSize (Clay.rem 0.8)
    -- geometry
    width (vw 90)
    maxWidth (px 800)
    -- text
    textAlign center
  a ? do
    -- border
    borderWithRadius solid (px 2) darkorchid (px 5)
    -- color
    backgroundColor gold
    fontColor darkorchid
    hover & do
      borderColor darkmagenta
      backgroundColor yellow
      fontColor darkmagenta
    -- font
    setFont   bold (Clay.rem 1.1) [smooth]
    -- geometry
    marginTop    (Clay.rem 2)
    marginBottom (Clay.rem 2)
    padding (px 12) (px 35) (px 12) (px 35)
    -- text
    textAlign      center
    textDecoration none


goRightCss :: Css
goRightCss = do
  -- flex parent
  display flex
  flexDirection column
  alignItems flexEnd
  justifyContent center


okCss :: Css
okCss = do
  -- background
  background $ url "/static/pictures/StarryNight.jpg"
  backgroundRepeat noRepeat
  backgroundPosition $ placed sideCenter sideCenter
  backgroundSize cover



blockCss :: Css
blockCss = do
  -- geometry
  width  (pct 100)
  height (pct 100)
  svg ? do
    width  (pct 100)
    height (pct 100)
  -- ANCHOR
  a ? do
    backgroundColor white
    borderStyle solid
    borderWidth (px 2)
    borderRadius `sym` (px 8)
    width  (pct 100)
    height (pct 100)
    padding `sym` (px 5)
    display flex
    justifyContent center
    alignItems center
    textDecoration none
    textAlign center
    halfSecAllTransition

welcomeCss :: Css
welcomeCss = do
  backgroundColor white
  borderWithRadius solid (px 2) navy (px 10)
  fontColor navy
  cursor cursorDefault
  -- flex parent
  display flex
  flexDirection  column
  justifyContent center
  alignItems     center
  -- geometry
  height (pct 100)
  width  (pct 100)
  padding `sym` (px 30)
  -- SVG
  svg ? do
    (-:) "fill"   "navy"
    (-:) "stroke" "navy"
    width  (px 80)
    height (px 80)
    marginBottom (px 10)
  -- HOVER
  hover & do
    background $ url "/static/pictures/fireworks.gif"
    backgroundPosition $ placed sideCenter sideCenter
    backgroundRepeat noRepeat
    backgroundSize cover
    borderColor   black
    fontColor     white
    (-:) "cursor" "none"
    svg ? do
      (-:) "fill"   "transparent"
      (-:) "stroke" "transparent"



jobsCss :: Css
jobsCss = do
  flexDirection column
  padding `sym` (px 20)
  borderColor   (rgb 102 51 153)
  fontColor     (rgb 102 51 153)
  (-:) "fill"   "rgb(102,51,153)"
  (-:) "stroke" "rgb(102,51,153)"
  hover & do
    borderColor   mediumorchid
    fontColor     mediumorchid
    (-:) "fill"   "mediumorchid"
    (-:) "stroke" "mediumorchid"
  svg ? do
    height (px 60)
    width  (px 60)
    marginBottom (px 20)


goBackCss :: Css
goBackCss = do
  flexDirection column
  borderColor dimgray
  fontColor   dimgray
  (-:) "fill"   "dimgray"
  (-:) "stroke" "dimgray"
  hover & do
    borderColor darkgray
    fontColor   dimgray
    (-:) "fill"   "darkgray"
    (-:) "stroke" "darkgray"
  svg ? do
    height (px 30)
    width  (px 30)
    marginBottom (px 5)


profileCss :: Css
profileCss = do
  flexDirection column
  borderColor teal
  fontColor   teal
  (-:) "fill"   "teal"
  (-:) "stroke" "teal"
  hover & do
    borderColor lightseagreen
    fontColor   lightseagreen
    (-:) "fill"   "lightseagreen"
    (-:) "stroke" "lightseagreen"
  svg ? do
    height (px 30)
    width  (px 30)
    marginBottom (px 5)



--------------------------------------------------------------------------------
