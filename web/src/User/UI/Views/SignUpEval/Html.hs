{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SignUpEval.Html
  ( signUpErrorPageView
  , signUpOkPageView
  ) where

import Data.Text (Text)
import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Head
import Base.Html.Page
import Base.Lang
import Base.UI.Elems.GoldenLayout.Html
import Base.UI.Layout.Nav.Svg
import Base.UI.Layout.Template.Html
import Job.API.Routes
import User.API.Routes
import User.Logic.All
import User.UI.Views.SignUpEval.Svg
import User.UI.Views.SignUpEval.Translate as Translate


--------------------------------------------------------------------------------

signUpErrorHead :: Head
signUpErrorHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""


signUpErrorPageView :: UserError -> PageParams -> Html
signUpErrorPageView err params =
    H.toHtml page
  where
    page = Page
      { pageParams = params
      , htmlHead   = signUpErrorHead
      , htmlBody   = mainTemplate (errorBody err)
      }


errorBody :: UserError -> PageParams -> Html
errorBody err params =
  let
    lang = ppLang params
  in
    H.div
      ! A.class_ "sign-up-eval-body sign-up-eval-error"
      $ do
        signUpErrorSvg
        H.h1   (H.toHtml $ somethingWentWrong lang)
        H.span (H.toHtml $ translate lang err)
        goBackButton params


goBackButton :: PageParams -> Html
goBackButton p =
  H.a (H.toHtml $ Translate.goBack $ ppLang p)
    ! A.href   (H.toValue $ ppRoute p)


--------------------------------------------------------------------------------

signUpOkHead :: Head
signUpOkHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""


signUpOkPageView :: Text -> PageParams -> Html
signUpOkPageView url params =
    H.toHtml page
  where
    page = Page
      { pageParams = params
      , htmlHead   = signUpOkHead
      , htmlBody   = mainTemplate (okBody url)
      }


okBody :: Text -> PageParams -> Html
okBody url params =
  let
    lang  = ppLang params
    uType = case ppUser params of
      Nothing -> UserPerson
      Just x  -> snd x
    jobsRoute = case uType of
      UserCompany -> postJobRoute
      UserPerson  -> "/"
    welcomeDiv =
      H.div
        ! A.class_ "sign-up-eval-welcome"
        $ do
          signUpOkSvg
          H.h1 $ H.toHtml $ case uType of
            UserCompany -> Translate.pluralWelcome   lang
            UserPerson  -> Translate.singularWelcome lang
    jobsLink =
      H.a
        ! A.href (H.toValue jobsRoute)
        ! A.class_ "sign-up-eval-jobslink"
        $ do
          case uType of
            UserCompany -> postJobIcon
            UserPerson  -> viewJobsIcon
          H.p $ H.toHtml $ case uType of
            UserCompany -> Translate.postJob  lang
            UserPerson  -> Translate.viewJobs lang
    goBackLink =
      H.a
        ! A.href   (H.toValue url)
        ! A.class_ "sign-up-eval-gobacklink"
        $ do
          goBackIcon
          H.p (H.toHtml $ Translate.goBack lang)
    profileLink =
      H.a
        ! A.href   (H.toValue profileRoute)
        ! A.class_ "sign-up-eval-profilelink"
        $ do
          case uType of
            UserCompany -> companyIcon
            UserPerson  -> personIcon
          H.p (H.toHtml $ Translate.profile lang)
  in do
    H.div ! A.class_ "sign-up-eval-body sign-up-eval-ok" $
      H.div
        ! A.class_ "sign-up-eval-right resp-w"
        $ fibonacciLayout
            "fib-with-gaps"
            verticalWithSpiralSW 85
            $ map (\x -> H.div x ! A.class_ "sign-up-eval-block")
              [ welcomeDiv
              , jobsLink
              , goBackLink
              , profileLink
              ]
    backgroundImagePreloaderTrick




backgroundImagePreloaderTrick :: Html
backgroundImagePreloaderTrick = do
  H.img
    ! A.style "display: none"
    ! A.src "/static/pictures/StarryNight.jpg"
  H.img
    ! A.style "display: none"
    ! A.src "/static/pictures/fireworks.gif"


--------------------------------------------------------------------------------
