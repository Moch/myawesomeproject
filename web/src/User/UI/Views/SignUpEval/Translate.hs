{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SignUpEval.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------

somethingWentWrong :: Lang -> Text
somethingWentWrong lang =
  case lang of
    ES -> "Algo salió mal"
    _  -> "Something went wrong"


goBack :: Lang -> Text
goBack lang =
  case lang of
    ES -> "Volver"
    _  -> "Go back"


singularWelcome :: Lang -> Text
singularWelcome lang =
  case lang of
    ES -> "Bienvenido/a"
    _  -> "Welcome"


pluralWelcome :: Lang -> Text
pluralWelcome lang =
  case lang of
    ES -> "Bienvenidos"
    _  -> "Welcome"


profile :: Lang -> Text
profile lang =
  case lang of
    ES -> "Perfil"
    _  -> "Profile"


postJob :: Lang -> Text
postJob lang =
  case lang of
    ES -> "Publicar anuncio"
    _  -> "Post job"


viewJobs :: Lang -> Text
viewJobs lang =
  case lang of
    ES -> "Ver empleos"
    _  -> "View jobs"

--------------------------------------------------------------------------------
