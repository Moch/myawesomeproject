{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SignUpEval.Svg where

import Text.Blaze.Svg11 (Svg)

import Base.Svg.Arrows
import Base.Svg.Faces
import Base.Svg.Utils


--------------------------------------------------------------------------------

signUpErrorSvg :: Svg
signUpErrorSvg =
  stdSvg $ do
    stdFaceBorder
    sadEyes
    inexpresiveMouth


signUpOkSvg :: Svg
signUpOkSvg =
  stdSvg $ do
    stdFaceBorder
    happyEyes
    happyMouth

--------------------------------------------------------------------------------

goBackIcon :: Svg
goBackIcon = stdSvg curvyArrowLeft


--------------------------------------------------------------------------------
