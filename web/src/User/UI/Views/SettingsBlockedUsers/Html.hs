{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SettingsBlockedUsers.Html
  ( settingsBlockedUsersPageView
  ) where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Head
import Base.Html.Page
import Base.Lang
import Base.UI.Layout.Template.Html


--------------------------------------------------------------------------------

settingsBlockedUsersHead :: Head
settingsBlockedUsersHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

settingsBlockedUsersPageView :: PageParams -> Html
settingsBlockedUsersPageView params =
    H.toHtml page
  where
    page = Page
      { pageParams = params
      , htmlHead   = settingsBlockedUsersHead
      , htmlBody   = settingsTemplate settingsBlockedUsersBody
      }

settingsBlockedUsersBody :: PageParams -> Html
settingsBlockedUsersBody p =
  let
    lang = ppLang p
  in
    H.div ! A.class_ "" $ case lang of
      ES -> "Hola"
      _  -> "Hello"


--------------------------------------------------------------------------------
