{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SettingsEmployment.Html
  ( settingsEmploymentPageView
  ) where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Head
import Base.Html.Page
import Base.Lang
import Base.UI.Layout.Template.Html


--------------------------------------------------------------------------------

settingsEmploymentHead :: Head
settingsEmploymentHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

settingsEmploymentPageView :: PageParams -> Html
settingsEmploymentPageView params =
    H.toHtml page
  where
    page = Page
      { pageParams = params
      , htmlHead   = settingsEmploymentHead
      , htmlBody   = settingsTemplate settingsEmploymentBody
      }

settingsEmploymentBody :: PageParams -> Html
settingsEmploymentBody p =
  let
    lang = ppLang p
  in
    H.div ! A.class_ "" $ case lang of
      ES -> "Hola"
      _  -> "Hello"


--------------------------------------------------------------------------------
