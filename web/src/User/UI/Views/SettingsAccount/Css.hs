{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SettingsAccount.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

settingsAccountCss :: Css
settingsAccountCss =
  associateCss
    [ (,) "settings-account-modal-content" modalContentCss
    , (,) "settings-account-redstyle"      redStyleCss
    ]

--------------------------------------------------------------------------------


modalContentCss :: Css
modalContentCss = do
  -- list
  listStylePosition inside
  -- UL
  ul ? do
    marginBottom (Clay.rem 2)
  -- INPUT
  input ? do
    marginTop (Clay.rem 1)
  -- BUTTON
  button ? do
    width (pct 100)
    height (Clay.rem 3)




redStyleCss :: Css
redStyleCss = do
  let redC = rgb 200 50 60
  h1 ?
    fontColor redC
  button ? do
    fontColor redC
    hover & do
      (-:) "background" "linear-gradient(rgb(235,80,80) , rgb(200,40,70))"
      borderColor gray
      fontColor white




--------------------------------------------------------------------------------
