{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SettingsAccount.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------


pwChangeOkMsg :: Lang -> Text
pwChangeOkMsg lang = case lang of
  ES -> "Tu contraseña ha sido actualizada"
  _  -> "Your password has been updated"


usernameChangeOkMsg :: Lang -> Text
usernameChangeOkMsg lang = case lang of
  ES -> "Tu nombre de usuario ha sido actualizado"
  _  -> "Your username has been updated"


changePassword :: Lang -> Text
changePassword lang =
  case lang of
    ES -> "Modificar contraseña"
    _  -> "Change password"


oldPassword :: Lang -> Text
oldPassword lang =
  case lang of
    ES -> "Contraseña actual"
    _  -> "Old password"

newPassword :: Lang -> Text
newPassword lang =
  case lang of
    ES -> "Nueva contraseña"
    _  -> "New password"


updatePassword :: Lang -> Text
updatePassword lang =
  case lang of
    ES -> "Cambiar contraseña"
    _  -> "Update password"


forgotPassword :: Lang -> Text
forgotPassword lang =
  case lang of
    ES -> "No recuerdo mi contraseña"
    _  -> "I forgot my password"


changeUsername :: Lang -> Text
changeUsername lang =
  case lang of
    ES -> "Cambiar nombre de usuario"
    _  -> "Change username"


notRecommended :: Lang -> Text
notRecommended lang = case lang of
  ES -> "Cambiar tu nombre de usuario puede tener efectos no deseados."
  _  -> "Changing your username can have unintended side effects."


reallyChangeUsername :: Lang -> Text
reallyChangeUsername lang =
  case lang of
    ES -> "¿Cambiar nombre de usuario?"
    _  -> "Really change your username?"


freeOldName :: Lang -> Text
freeOldName lang = case lang of
  ES -> "Tu antiguo nombre de usuario será liberado."
  _  -> "Your old username will be available for anyone to use."


noRedirect :: Lang -> Text
noRedirect lang =
  case lang of
    ES -> "Las visitas a tu perfil anterior no serán redireccionadas."
    _  -> "We will not set up redirects for your old profile page."


persistActivity :: Lang -> Text
persistActivity lang =
  case lang of
    ES -> "Tus actividades anteriores reflejarán adecuadamente el cambio."
    _  -> "Your past activities will correctly display the change."


deleteAccount :: Lang -> Text
deleteAccount lang =
  case lang of
    ES -> "Cancelar cuenta"
    _  -> "Delete your account"


noGoingBack :: Lang -> Text
noGoingBack lang =
  case lang of
    ES -> "Precaución, no hay vuelta atrás."
    _  -> "There is no going back. Please be certain."


reallyDeleteAccount :: Lang -> Text
reallyDeleteAccount lang =
  case lang of
    ES -> "¿Borrar la cuenta?"
    _  -> "Really delete your account?"


freeEmail :: Lang -> Text
freeEmail lang =
  case lang of
    ES -> "Tu email será liberado."
    _  -> "Your email will be freed too."


yourPassword :: Lang -> Text
yourPassword lang =
  case lang of
    ES -> "Tu contraseña:"
    _  -> "Your password:"


deleteThisAccount :: Lang -> Text
deleteThisAccount lang =
  case lang of
    ES -> "Borrar esta cuenta"
    _  -> "Delete this account"


--------------------------------------------------------------------------------
