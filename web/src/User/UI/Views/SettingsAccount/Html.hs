{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SettingsAccount.Html
  ( settingsAccountPageView
  , pwChangeOkMsg
  , usernameChangeOkMsg
  ) where

import           Data.Text        (Text)
import           Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import           Base.Html.Head
import           Base.Html.Page
import           Base.Html.Utils
import           Base.Lang
import           Base.UI.Elems.ClosableMsg.Html
import           Base.UI.Elems.InputValidate.Html
import           Base.UI.Elems.ModalWindow.Html
import           Base.UI.Layout.Template.Html
import           Base.Validate
import           User.API.Routes
import           User.Logic.All
import qualified User.UI.Views.SettingsAccount.Translate as Translate


--------------------------------------------------------------------------------

settingsAccountHead :: Head
settingsAccountHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

settingsAccountPageView ::
  PageParams -> FullUser -> Html
settingsAccountPageView params u =
    H.toHtml page
  where
    page = Page
      { pageParams = params
      , htmlHead   = settingsAccountHead
      , htmlBody   = settingsTemplate (settingsAccountBody u)
      }


settingsAccountBody :: FullUser -> PageParams -> Html
settingsAccountBody u p =
  let
    lang   = ppLang p
    route  = ppRoute p
    uName  = userName $ uCore u
  in do
    errorMsg $ ppErrorMsg p
    okMsg    $ ppOkMsg    p
    changePasswordSection route lang
    changeUsernameSection route lang uName
    deleteAccountSection  route lang



pwChangeOkMsg :: Lang -> Text
pwChangeOkMsg = Translate.pwChangeOkMsg


usernameChangeOkMsg :: Lang -> Text
usernameChangeOkMsg = Translate.usernameChangeOkMsg

--------------------------------------------------------------------------------


changePasswordSection :: Text -> Lang -> Html
changePasswordSection route lang =
    H.form
      ! A.method "post"
      ! A.action (H.toValue route)
      $ do
        H.h1 $ H.toHtml $ Translate.changePassword lang
        H.label
          ! A.class_ "layout-settings-inputwrap"
          $ do
            H.p $ H.toHtml $ Translate.oldPassword lang
            oldPasswordInput
        H.label
          ! A.class_ "layout-settings-inputwrap"
          $ do
            H.p $ H.toHtml $ Translate.newPassword lang
            fst $ newPasswordInput lang
        goalInput "change-password"
        H.div $ do
          changePasswordButton   lang
          snd $ newPasswordInput lang
          forgotPasswordLink     lang


oldPasswordInput :: Html
oldPasswordInput =
  H.input
    ! A.type_       "password"
    ! A.required    "required"
    ! A.name        "old-pw"
    ! A.class_      "layout-settings-input"


newPasswordInput :: Lang -> (Html,Html)
newPasswordInput lang =
  inputWithValidations
    InputParams
      { inputIdentifier   = "pw-input"
      , inputValue        = ""
      , inputPlaceholder  = ""
      , inputCssClass     = "layout-settings-input"
      , inputLanguage     = lang
      , inputMinLength    = textMinLength userPwRestrictions
      , inputMaxLength    = textMaxLength userPwRestrictions
      , inputOnlyEmail    = False
      , inputOnlyAlphanum = False
      , inputHasEyeButton = True
      , inputExistRoute   = Nothing
      , inputSubmitId     = "submit-pw-change"
      }



changePasswordButton :: Lang -> Html
changePasswordButton lang =
  H.button
    ! A.type_  "submit"
    ! A.id     "submit-pw-change"
    ! A.class_ "layout-settings-button"
    $ H.toHtml $ Translate.updatePassword lang


forgotPasswordLink :: Lang -> Html
forgotPasswordLink lang =
  H.a
    ! A.href (H.toValue recoverPwRoute)
    $ H.toHtml $ Translate.forgotPassword lang


--------------------------------------------------------------------------------

changeUsernameSection :: Text -> Lang -> Text -> Html
changeUsernameSection route lang uName =
  H.form
    ! A.method "post"
    ! A.action (H.toValue route)
    $ do
      H.h1 $ H.toHtml $ Translate.changeUsername lang
      H.p  $ H.toHtml $ Translate.notRecommended lang
      changeUsernameOpenModalButton lang
      changeUsernameModalWindow lang uName


changeUsernameOpenModalButton :: Lang -> Html
changeUsernameOpenModalButton lang =
  H.button
    ! A.type_  "button"
    ! A.id     "change_username"
    ! A.class_ "layout-settings-button"
    $ H.toHtml $ Translate.changeUsername lang


changeUsernameModalWindow :: Lang -> Text -> Html
changeUsernameModalWindow lang uName =
    modalWindow
      "change_username"
      (H.toHtml $ Translate.reallyChangeUsername lang)
      innerContent
  where
    innerContent =
      H.div
        ! A.class_ "settings-account-modal-content settings-account-redstyle"
        $ do
          H.ul $ do
            H.li $ H.toHtml $ Translate.freeOldName     lang
            H.li $ H.toHtml $ Translate.noRedirect      lang
            H.li $ H.toHtml $ Translate.persistActivity lang
          fst $ nameInput lang uName
          goalInput "change-username"
          H.button
            ! A.type_ "submit"
            ! A.id "submit-name-change"
            ! A.class_ "layout-settings-button"
            $ H.toHtml $ Translate.changeUsername lang
          snd $ nameInput lang uName


nameInput :: Lang -> Text -> (Html,Html)
nameInput lang uName =
  inputWithValidations
    InputParams
      { inputIdentifier   = "name-input"
      , inputValue        = uName
      , inputPlaceholder  = ""
      , inputCssClass     = "layout-settings-input"
      , inputLanguage     = lang
      , inputMinLength    = textMinLength userNameRestrictions
      , inputMaxLength    = textMaxLength userNameRestrictions
      , inputOnlyEmail    = False
      , inputOnlyAlphanum = True
      , inputHasEyeButton = False
      , inputExistRoute   = Just userNameExistsRoute
      , inputSubmitId     = "submit-name-change"
      }


--------------------------------------------------------------------------------


deleteAccountSection :: Text -> Lang -> Html
deleteAccountSection route lang =
    H.form
      ! A.method "post"
      ! A.action (H.toValue route)
      ! A.class_ "settings-account-redstyle"
      $ do
        H.h1 $ H.toHtml $ Translate.deleteAccount lang
        H.p  $ H.toHtml $ Translate.noGoingBack   lang
        deleteAccountOpenModalButton lang
        deleteAccountModalWindow     lang


deleteAccountOpenModalButton :: Lang -> Html
deleteAccountOpenModalButton lang =
  H.button
    ! A.type_  "button"
    ! A.id     "delete_account"
    ! A.class_ "layout-settings-button"
    $ H.toHtml $ Translate.deleteAccount lang


deleteAccountModalWindow :: Lang -> Html
deleteAccountModalWindow lang =
    modalWindow
      "delete_account"
      (H.toHtml $ Translate.reallyDeleteAccount lang)
      innerContent
  where
    innerContent =
      H.div
        ! A.class_ "settings-account-modal-content"
        $ do
          H.ul $ do
            H.li $ H.toHtml $ Translate.freeOldName lang
            H.li $ H.toHtml $ Translate.freeEmail   lang
          H.p    $ H.toHtml $ Translate.yourPassword lang
          H.input
            ! A.type_    "password"
            ! A.required "required"
            ! A.name     "pw-input"
            ! A.class_   "layout-settings-input"
          goalInput "delete-account"
          H.button
            ! A.type_  "submit"
            ! A.id     "submit-delete-account"
            ! A.class_ "layout-settings-button"
            $ H.toHtml $ Translate.deleteThisAccount lang


--------------------------------------------------------------------------------
