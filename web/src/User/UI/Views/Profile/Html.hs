{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.Profile.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Head
import Base.Html.Page
-- import Base.Lang
import Base.UI.Elems.GoldenLayout.Html
import Base.UI.Layout.Template.Html
-- import User.API.Routes
import User.Logic.All

--------------------------------------------------------------------------------

profileHead :: Head
profileHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

profilePageView :: PageParams -> FullUser -> Html
profilePageView params u =
    H.toHtml page
  where
    page = Page
      { pageParams = params
      , htmlHead   = profileHead
      , htmlBody   = mainTemplate (profileBody u)
      }

profileBody :: FullUser -> PageParams -> Html
profileBody _ _ =
  let
    fibLayout = fibLayoutFullPage7
    whiteBox =
      H.div ! A.class_ "flex-center profile-white-box"
  in do
    H.div
      ! A.class_ "profile-body"
      $ fibLayout $ map whiteBox
        [ "13"
        , "8"
        , "5"
        , "3"
        , "2"
        , "1"
        , "1"
        ]



--------------------------------------------------------------------------------



--------------------------------------------------------------------------------


--------------------------------------------------------------------------------



--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
