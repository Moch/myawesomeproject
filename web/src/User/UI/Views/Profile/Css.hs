{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.Profile.Css where

import Clay
-- import qualified Clay.Flexbox as Flex

import Base.Css.Utils

--------------------------------------------------------------------------------

profileCss :: Css
profileCss =
  associateCss
    [ (,) "profile-body"        bodyCss
    , (,) "profile-white-box"   whiteBoxCss
    ]

--------------------------------------------------------------------------------





bodyCss :: Css
bodyCss = do
  -- background
  backgroundColor $ rgba 216 191 216 0.4
  -- flex parent
  display flex
  flexDirection  column
  justifyContent center
  alignItems center
  -- flex child
  flexGrow 1
  -- geometry
  let x = Clay.rem 2.5
  paddingTop    x
  paddingBottom x


whiteBoxCss :: Css
whiteBoxCss = do
  -- border
  borderWithRadius solid (px 1) silver (px 8)
  (-:) "box-shadow" "0px 0px 5px 2px rgba(0,0,0,0.1)"
  -- color
  backgroundColor white
  -- geometry
  height (pct 100)
  width  (pct 100)
  maxWidth (px 500)
  landscape $ maxWidth (pct 100)



--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
