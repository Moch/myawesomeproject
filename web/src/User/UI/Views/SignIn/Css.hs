{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SignIn.Css where

import Clay
-- import qualified Clay.Flexbox as Flex
import Data.Text (Text)

import Base.Css.Utils

--------------------------------------------------------------------------------

signInCss :: Css
signInCss =
  associateCss
    [ (,) "sign-in-body"            bodyCss
    , (,) "sign-in-heading"         headingCss
    , (,) "sign-in-form"            formCss
    , (,) "sign-in-input"           inputCss
    , (,) "sign-in-links-wrapper"   linksWrapperCss
    , (,) "sign-in-link"            linkCss
    , (,) "sign-in-recover-pw"      recoverPwCss
    , (,) "sign-in-submit-button"   signInButtonCss
    ]

--------------------------------------------------------------------------------

elBuenGradiente :: Text
elBuenGradiente
  =  "linear-gradient"
  <> "( rgba(200,225,255,0.5) 25%"
  <> ", rgba(255,255,255,1) 40%"
  <> ", rgba(0,120,220,0.5) 40%"
  <> ")"
  <> ","
  <> "radial-gradient"
  <> "( circle at 0% 30%"
  <> ", rgba(255,255,255,0.9)"
  <> ", rgba(0,0,255,0.5)"
  <> ")"
  <> ","
  <> "radial-gradient"
  <> "( ellipse 100% 50% at 100% 0%"
  <> ", rgba(0,185,225,0.9)"
  <> ", rgba(135,206,250,0.7)"
  <> ", transparent"
  <> ")"
  <> ","
  <> "radial-gradient(ellipse 100% 50% at 100% 100%, black, navy)"
  -- <> ","
  -- <> "radial-gradient(circle at 0% 100%, black, navy)"
  -- <> ","
  -- <> "radial-gradient(circle at 60% 60%, white, navy)"


bodyCss :: Css
bodyCss = do
  -- background
  (-:) "background" elBuenGradiente
  -- flex parent
  display flex
  flexDirection  column
  justifyContent center
  alignItems center
  -- flex child
  flexGrow 1
  -- geometry
  let x = Clay.rem 3.5
  paddingTop    x
  paddingBottom x

--------------------------------------------------------------------------------

headingCss :: Css
headingCss = do
  -- color
  fontColor $ rgb 0 0 90
  -- font
  setFont normal (Clay.rem 2.5) [zeroSerif]
  -- geometry
  marginBottom (Clay.rem 2)
  paddingRight (Clay.rem 0.5)
  paddingLeft  (Clay.rem 0.5)
  -- text
  textAlign  center

--------------------------------------------------------------------------------

formCss :: Css
formCss = do
  -- border
  -- borderWithRadius solid (px 1) silver (px 3)
  borderRadius `sym` (px 4)
  (-:) "box-shadow" "0px 0px 15px 0px rgba(0,0,0,0.2)"
  -- color
  backgroundColor white
  -- flex parent
  display flex
  flexDirection column
  alignItems center
  -- geometry
  let x = px 45
  let y = px 35
  padding y x y x
  width     (pct 100)
  maxWidth  (vw  96)
  landscape (maxWidth $ px 600)


inputCss :: Css
inputCss = do
  -- geometry
  width (pct 100)
  -- INPUT
  input ? do
    -- border
    backgroundColor  white
    borderWithRadius solid (px 1) gainsboro (px 5)
    focus & do
      borderColor $ rgba 30 144 255 1
      (-:) "box-shadow" "0px 0px 4px 0px rgba(30, 144, 255, 1), 0px 0px 0px 100px white inset"
    -- font
    setFont normal (Clay.rem 1.2) [zeroSerif]
    -- geometry
    width        (pct 100)
    height       (Clay.rem 3)
    -- marginTop    (Clay.rem 1)
    marginBottom (Clay.rem 1)
    paddingLeft  (px 10)
    paddingRight (pct 10)


linksWrapperCss :: Css
linksWrapperCss = do
  -- font
  setFont normal (Clay.rem 1.2) [zeroSerif]
  fontColor gray
  -- flex parent
  display flex
  justifyContent spaceBetween
  alignItems center
  -- geometry
  marginTop (Clay.rem 2)
  marginBottom (Clay.rem 4)
  width     (pct 100)
  maxWidth  (vw  96)
  landscape (maxWidth $ px 600)
  -- text
  textAlign center
  -- SVG
  svg ? do
    -- geometry
    let x = px 32
    width x
    height x


linkCss :: Css
linkCss = do
  -- border
  border solid (px 1) silver
  borderRadius `sym` (px 5)
  (-:) "box-shadow" "0px 0px 3px 1px rgba(0,0,0,0.2)"
  -- color
  backgroundColor whitesmoke
  fontColor crimson
  (-:) "fill"   "crimson"
  (-:) "stroke" "crimson"
  hover & do
    (-:) "fill"   "deeppink"
    (-:) "stroke" "deeppink"
    fontColor deeppink
  -- font
  fontSize (Clay.rem 1)
  -- flex parent
  display flex
  alignItems center
  justifyContent center
  flexDirection column
  portrait $ flexDirection row
  -- geometry
  width (pct 45)
  let x = px 15
  let y = px 10
  padding y x y x
  height (px 120)
  portrait $ height auto
  -- text
  textDecoration none
  hover & textDecoration underline
  -- SPAN
  Clay.span ? do
    paddingRight $ px 15
    paddingLeft $ px 15


recoverPwCss :: Css
recoverPwCss = do
  let c1 = "rgb(30,75,160)"
  let c2 = "rgb(35,75,245)"
  -- color
  (-:) "fill"   c1
  (-:) "stroke" c1
  (-:) "color"  c1
  hover & do
    (-:) "fill"   c2
    (-:) "stroke" c2
    (-:) "color"  c2


signInButtonCss :: Css
signInButtonCss = do
  -- border
  borderStyle none
  borderRadius `sym` (px 5)
  outlineStyle none
  -- color
  backgroundColor limegreen
  hover & backgroundColor (rgb 50 255 50)
  -- display
  cursor pointer
  -- font
  setFont   normal (Clay.rem 1.8) [smooth]
  fontColor white
  -- geometry
  height       (Clay.rem 3.5)
  width        (pct 100)
  minWidth     (px 170)
  marginTop    (Clay.rem 1.5)
  -- text
  (-:) "text-shadow" "1px 0 0 green, -1px 0 0 green, 0 1px 0 green, 0 -1px 0 green"


--------------------------------------------------------------------------------
