{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SignIn.Html
  ( signInPageView
  ) where

import Data.Text (Text)
import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Head
import Base.Html.Page
import Base.Lang
import Base.UI.Elems.ClosableMsg.Html
import Base.UI.Layout.Template.Html
import User.API.Routes
import User.UI.Views.SignIn.Svg
import User.UI.Views.SignIn.Translate as Translate

--------------------------------------------------------------------------------

signInPageView :: PageParams -> Html
signInPageView params =
    H.toHtml signInPage
  where
    signInPage = Page
      { pageParams = params
      , htmlHead   = signInHead
      , htmlBody   = mainTemplate signInBody
      }


signInHead :: Head
signInHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

--------------------------------------------------------------------------------


signInBody :: PageParams -> Html
signInBody params =
  let
    lang = ppLang     params
    r    = ppRoute    params
    mErr = ppErrorMsg params
  in
    H.div ! A.class_ "sign-in-body" $ do
      heading    lang
      signInForm lang r mErr
      someLinks  lang


--------------------------------------------------------------------------------

heading :: Lang -> Html
heading lang =
  H.h1
    ! A.class_ "sign-in-heading"
    $ H.toHtml $ Translate.signIn lang


signInForm :: Lang -> Text -> (Maybe Text) -> Html
signInForm lang r mErr =
  H.form
    ! A.method "post"
    ! A.action (H.toValue r)
    ! A.class_ "sign-in-form resp-w"
    $ do
      errorMsg mErr
      mapM_ ($ lang)
        [ nameInput
        , pwInput
        , signInButton
        ]


nameInput :: Lang -> Html
nameInput lang =
    H.div ! A.class_ "sign-in-input" $ do
      H.input
        ! A.type_       "text"
        ! A.name        "name-input"
        ! A.placeholder (H.toValue $ Translate.email lang)


pwInput :: Lang -> Html
pwInput lang =
  H.div ! A.class_ "sign-in-input" $ do
    H.input
      ! A.type_       "password"
      ! A.name        "pw-input"
      ! A.placeholder (H.toValue $ Translate.password lang)


signInButton :: Lang -> Html
signInButton lang =
    H.input
      ! A.class_ "sign-in-submit-button"
      ! A.type_  "submit"
      ! A.value  (H.toValue $ Translate.signIn lang)

--------------------------------------------------------------------------------


someLinks :: Lang -> Html
someLinks lang =
    H.div ! A.class_ "sign-in-links-wrapper" $ do
      recoverPw
      signUpLink
  where
    recoverPw =
      H.a
        ! A.href (H.toValue recoverPwRoute)
        ! A.class_ "sign-in-link sign-in-recover-pw"
        $ do
          recoverPwIcon
          H.span $ H.toHtml $ Translate.recoverPassword lang
    signUpLink =
      H.a
        ! A.href (H.toValue signUpRoute)
        ! A.class_ "sign-in-link"
        $ do
          signUpIcon
          H.span $ H.toHtml $ Translate.signUp lang


--------------------------------------------------------------------------------
