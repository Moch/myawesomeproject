{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SignIn.Svg where

-- import Text.Blaze.Svg11 ((!))
import qualified Text.Blaze.Svg11 as S
-- import qualified Text.Blaze.Svg11.Attributes as A

import Base.Svg.User
import Base.Svg.Utils

--------------------------------------------------------------------------------

signUpIcon :: S.Svg
signUpIcon = stdSvg carnet

recoverPwIcon :: S.Svg
recoverPwIcon = stdSvg lock

--------------------------------------------------------------------------------
