{-# LANGUAGE OverloadedStrings #-}


module User.UI.Views.SignIn.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------


signIn :: Lang -> Text
signIn lang =
  case lang of
    ES -> "Iniciar Sesión"
    _  -> "Sign In"


email :: Lang -> Text
email lang =
  case lang of
    ES -> "Email o nombre de usuario"
    _  -> "Email or username"


password :: Lang -> Text
password lang =
  case lang of
    ES -> "Contraseña"
    _  -> "Password"


recoverPassword :: Lang -> Text
recoverPassword lang =
  case lang of
    ES -> "Recuperar contraseña"
    _  -> "Recover password"


signUp :: Lang -> Text
signUp lang =
  case lang of
    ES -> "Crear cuenta"
    _  -> "Sign up"


--------------------------------------------------------------------------------
