{-# LANGUAGE OverloadedStrings #-}


module User.API.Handler where

import Control.Monad          (msum)
import Control.Monad.IO.Class (liftIO)
import Data.Text.Lazy         (toStrict)
import qualified Data.Text    as Text
import Data.Time              (getCurrentTime)
import Happstack.Server

import Base.API
import Base.Lang
import Base.Html.Page
import User.API.Cookies
import User.API.Routes
import User.DB.All
import User.Logic.All
import User.UI.Views.Profile.Html
import User.UI.Views.SettingsAccount.Html
import User.UI.Views.SettingsBilling.Html
import User.UI.Views.SettingsBlockedUsers.Html
import User.UI.Views.SettingsEmployment.Html
import User.UI.Views.SettingsNotifications.Html
import User.UI.Views.SettingsProfile.Html
import User.UI.Views.SignIn.Html
import User.UI.Views.SignUp.Html
import User.UI.Views.SignUpEval.Html

--------------------------------------------------------------------------------

userHandler :: PageParams -> ServerPart Response
userHandler p =
  msum
    [ (^-^) logoutRoute            logout
    , (^-^) userNameExistsRoute  (userNameExstEndpoint  p)
    , (^-^) userEmailExistsRoute (userEmailExstEndpoint p)
    , (^-^) signInRoute $ msum
        [ signInPOST p
        , signInGET  p
        ]
    , (^-^) (signUpRouteByType UserPerson) $ msum
        [ signUpGET  UserPerson p
        , signUpPOST UserPerson p
        ]
    , (^-^) (signUpRouteByType UserCompany) $ msum
        [ signUpGET  UserCompany p
        , signUpPOST UserCompany p
        ]
    , (^-^) profileRoute (redirectToSelfProfile p)
    , (^-^) userRoute    (userProfile p)
    , dir "settings" $ settingsHandler p
    ]


settingsHandler :: PageParams -> ServerPart Response
settingsHandler p =
  msum
    [ dir "account"       $ settingsAccount       p
    , dir "billing"       $ settingsBilling       p
    , dir "blocked-users" $ settingsBlockedUsers  p
    , dir "employment"    $ settingsEmployment    p
    , dir "notifications" $ settingsNotifications p
    , dir "profile"       $ settingsProfile       p
    ]

--------------------------------------------------------------------------------

logout :: ServerPart Response
logout = do
  method GET
  removeCookieUser
  redirectToRoot


userNameExstEndpoint :: PageParams -> ServerPart Response
userNameExstEndpoint p =
  do
    method GET
    uName <- safeLookText "p"
    case validateUserName uName of
      Nothing -> nullResponse 404
      Just  x -> do
        alreadyExists <- liftIO $ userNameAlreadyExists (ppDatabase p) x
        case alreadyExists of
          Left  _     -> nullResponse 404
          Right True  -> nullResponse 303
          Right False -> nullResponse 404


userEmailExstEndpoint :: PageParams -> ServerPart Response
userEmailExstEndpoint p =
  do
    method GET
    uEmail <- safeLookText "p"
    case validateUserEmail uEmail of
      Nothing -> nullResponse 404
      Just  x -> do
        alreadyExists <- liftIO $ userEmailAlreadyExists (ppDatabase p) x
        case alreadyExists of
          Left  _     -> nullResponse 404
          Right True  -> nullResponse 303
          Right False -> nullResponse 404


--------------------------------------------------------------------------------


signInGET :: PageParams -> ServerPart Response
signInGET p =
  let params = p{ppRoute= signInRoute}
  in do
    method GET
    case ppUser p of
      Nothing -> ok $ toResponse (signInPageView params)
      Just _  -> redirectToRoot


signInPOST :: PageParams -> ServerPart Response
signInPOST params =
  let
    p = params {ppRoute = signInRoute}
    errorResponse = saveErrorMsg (ppRoute p) . translate (ppLang p)
    validate eUser mPw =
      case eUser of
        Left err1      -> errorResponse err1
        Right Nothing  -> errorResponse UserDoesNotExistError
        Right (Just u) -> case validateCredentials mPw (uCore u) of
          Left err2 -> errorResponse err2
          Right _   -> do
            _   <- putCookieUser (uID u) (userType $ uCore u)
            url <- getCookieLoginRedirect
            seeOther url (toResponse ())
  in do
    method POST
    mName   <- safeLookText "name-input"
    mPw     <- safeLookText "pw-input"
    case mName of
      Nothing -> errorResponse UserDoesNotExistError
      Just nameOrEmail -> do
        case validateLogin nameOrEmail of
          Left email ->
            case validateUserEmail (Just email) of
              Nothing   -> errorResponse UserEmailError
              Just mail -> do
                eUser <- liftIO $ getUserByEmail (ppDatabase p) mail
                validate eUser mPw
          Right name ->
            case validateUserName (Just name) of
              Nothing    -> errorResponse UserNameError
              Just uname -> do
                eUser <- liftIO $ getUserByName (ppDatabase p) uname
                validate eUser mPw


--------------------------------------------------------------------------------


signUpGET :: UserType -> PageParams -> ServerPart Response
signUpGET utype params =
  let p = params {ppRoute = signUpRouteByType utype}
  in do
    method GET
    case ppUser p of
      Nothing -> ok $ toResponse (signUpPageView utype p)
      Just _  -> redirectToRoot



signUpPOST :: UserType -> PageParams -> ServerPart Response
signUpPOST uType params =
  let
    p = params {ppRoute = signUpRouteByType uType}
  in do
    method POST
    uName     <- safeLookText "name-input"
    uEmail    <- safeLookText "email-input"
    uPw       <- safeLookText "pw-input"
    regTime   <- liftIO getCurrentTime
    let e1 = validateNewUser uType uName uEmail uPw regTime
    case e1 of
      Left err -> ok $ toResponse (signUpErrorPageView err p)
      Right x -> do
        e2 <- liftIO $ persistNewUser (ppDatabase p) x
        case e2 of
          Left err -> ok $ toResponse (signUpErrorPageView err p)
          Right u -> do
            let uId = fst u
            uCk <- putCookieUser uId uType
            url <- getCookieSignUpRedirect
            ok $ toResponse (signUpOkPageView url p{ppUser = Just uCk})


--------------------------------------------------------------------------------


kickUserOrRespond ::
  PageParams -> (FullUser -> ServerPart Response) -> ServerPart Response
kickUserOrRespond p response =
  let
    kickUser = seeOther logoutRoute (toResponse ())
  in case ppUser p of
    Nothing -> kickUser
    Just (uId, _) -> do
      e <- liftIO $ getUserById (ppDatabase p) uId
      case e of
        Left  _        -> kickUser
        Right Nothing  -> kickUser
        Right (Just u) -> response u


--------------------------------------------------------------------------------

redirectToSelfProfile :: PageParams -> ServerPart Response
redirectToSelfProfile p =
  kickUserOrRespond
    p
    (\u -> seeOther (userRoute <> "/" <> (userName $ uCore u)) (toResponse ()))


userProfile :: PageParams -> ServerPart Response
userProfile p =
  path $ \uName -> do
    method GET
    case validateUserName (Just uName) of
      Nothing -> redirectToRoot
      Just  _ -> do
        eUser <- liftIO $ getUserByName (ppDatabase p) uName
        case eUser of
          Left  _        -> redirectToRoot
          Right Nothing  -> redirectToRoot
          Right (Just u) -> do
            let url    = userRoute <> "/" <> uName
            let params = p {ppRoute = url}
            ok $ toResponse $ profilePageView params u


--------------------------------------------------------------------------------

settingsAccount :: PageParams -> ServerPart Response
settingsAccount params =
    msum
      [ method POST
        >> kickUserOrRespond p processForm
      , method GET
        >> kickUserOrRespond p (ok . toResponse . settingsAccountPageView p)
      ]
  where
    p = params {ppRoute = settingsAccountRoute}
    lang = ppLang p
    errorResponse = saveErrorMsg (ppRoute p) . translate (ppLang p)
    processForm = \u -> do
      oldPw    <- safeLookText "old-pw"
      newPw    <- safeLookText "pw-input"
      newName  <- safeLookText "name-input"
      goal     <- safeLookText "goal"
      currentT <- liftIO getCurrentTime
      case fmap Text.unpack goal of
        Just "change-password" ->
          processPasswordChange u oldPw newPw
        Just "change-username" ->
          processNameChange u newName
        Just "delete-account"  ->
          processAccountDeletion u newPw currentT
        _ ->
          seeOther logoutRoute (toResponse ())
    processPasswordChange u oldPw newPw =
      case validatePwChange (userPw $ uCore u) oldPw newPw of
        Left err -> errorResponse err
        Right pw -> do
          e <- liftIO $ changePassword (ppDatabase p) (uID u) pw
          case e of
            Left  err' -> errorResponse err'
            Right _    ->
              saveOkMsg settingsAccountRoute $ pwChangeOkMsg lang
    processNameChange u newName =
      case validateUsernameChange (userName $ uCore u) newName of
        Left  err   -> errorResponse err
        Right uName -> do
          e <- liftIO $ changeUsername (ppDatabase p) (uID u) uName
          case e of
            Left err' -> errorResponse err'
            Right _   ->
              saveOkMsg settingsAccountRoute $ usernameChangeOkMsg lang
    processAccountDeletion u pw t =
      case validateAccountDeletion (uCore u) pw t of
        Left err -> errorResponse err
        Right _  -> do
          e <- liftIO $ deleteUser (ppDatabase p) (uID u)
          case e of
            Left err' -> errorResponse err'
            Right   _ -> seeOther logoutRoute (toResponse ())



settingsBilling :: PageParams -> ServerPart Response
settingsBilling params =
  let
    p = params {ppRoute = settingsBillingRoute}
  in do
    method GET
    ok $ toResponse (settingsBillingPageView p)

settingsBlockedUsers :: PageParams -> ServerPart Response
settingsBlockedUsers params =
  let
    p = params {ppRoute = settingsBlockedUsersRoute}
  in do
    method GET
    ok $ toResponse (settingsBlockedUsersPageView p)

settingsEmployment :: PageParams -> ServerPart Response
settingsEmployment params =
  let
    p = params {ppRoute = settingsEmploymentRoute}
  in do
    method GET
    ok $ toResponse (settingsEmploymentPageView p)

settingsNotifications :: PageParams -> ServerPart Response
settingsNotifications params =
  let
    p = params {ppRoute = settingsNotificationsRoute}
  in do
    method GET
    ok $ toResponse (settingsNotificationsPageView p)


settingsProfile :: PageParams -> ServerPart Response
settingsProfile params =
    msum
      [ changeProfileSettings
      , viewProfileSettings
      ]
  where
    p = params {ppRoute = settingsProfileRoute}
    errorResponse = saveErrorMsg (ppRoute p) . translate (ppLang p)
    okResponse    = saveOkMsg    (ppRoute p) $ updatedProfileOkMsg (ppLang p)
    viewProfileSettings = do
      method GET
      kickUserOrRespond p (ok . toResponse . settingsProfilePageView p)
    changeProfileSettings = do
      method POST
      kickUserOrRespond p processForm
    processForm = \u -> do
      name      <- safeLookText "name-input"
      email     <- safeLookText "email-input"
      location  <- safeLookText "location-input"
      langs     <- fmap (validateSpeakerList . map toStrict) $ lookTexts "profile_langs"
      desc      <- safeLookText "description-input"
      links     <- fmap (validateSocialLinks . map toStrict) $ lookTexts "social-link-input"
      goal      <- safeLookText "goal"
      case fmap Text.unpack goal of
        Just "update-profile-photo" ->
          processPhotoChange
        Just "update-profile" ->
          processProfileChange u name email location langs desc
        Just "update-social" ->
          processSocialChange u links
        _ ->
          seeOther logoutRoute (toResponse ())
    processPhotoChange = do
      -- (fPath, fName, fType) <- lookFile "photo-input"
      okResponse
    processProfileChange u name email loc langs desc =
      case validateProfileChange (uProfile u) name email loc desc of
        Left err   -> errorResponse err
        Right prof -> do
          e <- liftIO $ updateProfile (ppDatabase p) (uID u) langs prof
          case e of
            Left err' -> errorResponse err'
            Right _   -> okResponse
    processSocialChange u links = do
      e <- liftIO $ updateSocialLinks (ppDatabase p) (uID u) links
      case e of
        Left err -> errorResponse err
        Right _  -> okResponse





--------------------------------------------------------------------------------
