{-# LANGUAGE OverloadedStrings #-}


module User.API.Routes where

import Data.Text

import User.Logic.All

--------------------------------------------------------------------------------

signUpRoute :: Text
signUpRoute = signUpRouteByType UserPerson

signInRoute :: Text
signInRoute = "/sign-in"


logoutRoute :: Text
logoutRoute = "/logout"


signUpRouteByType :: UserType -> Text
signUpRouteByType UserPerson  = "/sign-up"
signUpRouteByType UserCompany = "/sign-up-as-company"


recoverPwRoute :: Text
recoverPwRoute = "/recover-password"

userNameExistsRoute :: Text
userNameExistsRoute = "/check-user-name"

userEmailExistsRoute :: Text
userEmailExistsRoute = "/check-user-email"


profileRoute :: Text
profileRoute = "/profile"


userRoute :: Text
userRoute = "/user"


manageJobsRoute :: Text
manageJobsRoute = "/manage-jobs"

--------------------------------------------------------------------------------

settingsAccountRoute :: Text
settingsAccountRoute = "/settings/account"


settingsProfileRoute :: Text
settingsProfileRoute = "/settings/profile"


settingsEmploymentRoute :: Text
settingsEmploymentRoute = "/settings/employment"


settingsNotificationsRoute :: Text
settingsNotificationsRoute = "/settings/notifications"


settingsBillingRoute :: Text
settingsBillingRoute = "/settings/billing"


settingsBlockedUsersRoute :: Text
settingsBlockedUsersRoute = "/settings/blocked-users"


--------------------------------------------------------------------------------
