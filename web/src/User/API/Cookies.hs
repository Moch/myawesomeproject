{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}


module User.API.Cookies where

import           Control.Applicative  (optional)
import           Control.Monad        (liftM2)
import           Data.Text
import           Data.Time
import           Happstack.Server
import           Text.Read            (readMaybe)

import User.Logic.All


--------------------------------------------------------------------------------

getCookieUser :: ServerPart (Maybe (UserId, UserType))
getCookieUser = do
  m1' <- optional $ lookCookieValue "userId"
  m2' <- optional $ lookCookieValue "userType"
  let m1 =  m1' >>= readMaybe
  let m2 =  m2' >>= readMaybe
  return $ liftM2 ((,)) m1 m2


putCookieUser :: UserId -> UserType -> ServerPart (UserId,UserType)
putCookieUser uId uType =
  let
    cookieLife =
      Expires $ UTCTime (fromGregorian 3000 1 1) (secondsToDiffTime 0)
  in do
    addCookies $ fmap ( (,) cookieLife )
      [ mkCookie "userId"   (show uId)
      , mkCookie "userType" (show uType)
      ]
    return (uId,uType)


removeCookieUser :: ServerPart ()
removeCookieUser = do
  expireCookie "userId"
  expireCookie "userType"

--------------------------------------------------------------------------------

getCookieLoginRedirect :: ServerPart Text
getCookieLoginRedirect = do
  m1' <- optional $ lookCookieValue "loginRedirect"
  return $ maybe "/" pack m1'


putCookieLoginRedirect :: Text -> Text
putCookieLoginRedirect url =
  "(function(){document.cookie = 'loginRedirect=" <> url <> "; path=/'; })()"


getCookieSignUpRedirect :: ServerPart Text
getCookieSignUpRedirect = do
  m1' <- optional $ lookCookieValue "signupRedirect"
  return $ maybe "/" pack m1'


putCookieSignUpRedirect :: Text -> Text
putCookieSignUpRedirect url =
  "(function(){document.cookie = 'signupRedirect=" <> url <> "; path=/'; })()"



--------------------------------------------------------------------------------
