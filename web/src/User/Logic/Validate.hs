{-# LANGUAGE OverloadedStrings #-}




module User.Logic.Validate where

import           Data.Either.Combinators (maybeToRight)
import           Data.Maybe              (isJust)
import           Data.Text as Text       (Text, find, take)
import           Data.Time

import User.Logic.Base
import User.Logic.Exception
import User.Logic.Models

--------------------------------------------------------------------------------

validateNewUser ::
     UserType
  -> Maybe Text
  -> Maybe Text
  -> Maybe Text
  -> UTCTime
  -> Either UserError User
validateNewUser uType mName mEmail mPw time = do
  name <- maybeToRight UserNameError  (validateUserName mName)
  mail <- maybeToRight UserEmailError (validateUserEmail mEmail)
  pw   <- maybeToRight UserPwError    (validateUserPw mPw)
  return $ User
    { userType    = uType
    , userName    = name
    , userEmail   = mail
    , userPw      = pw
    , userBanDate = Nothing
    , userDelDate = Nothing
    , userRegDate = Just time
    , userValDate = Nothing
    }


validateLogin :: Text -> Either Text Text
validateLogin t =
  case find (== '@') $ Text.take 500 t of
    Nothing -> Right t
    Just _  -> Left  t



validateCredentials :: Maybe Text ->  User -> Either UserError ()
validateCredentials mPw u =
  if isJust $ userBanDate u
    then Left UserIsBannedError
    else if isJust $ userDelDate u
      then  Left UserIsDeletedError
      else case mPw of
        Nothing -> Left UserPwError
        Just pw -> if pw == userPw u
          then Right ()
          else Left InvalidPwError


validatePwChange ::
  Text -> Maybe Text -> Maybe Text -> Either UserError Text
validatePwChange currentPw inputPw newPw =
  case inputPw of
    Nothing -> Left InvalidPwError
    Just x  -> if currentPw /= x
      then Left InvalidPwError
      else maybeToRight UserPwError $ validateUserPw newPw


validateUsernameChange :: Text -> Maybe Text -> Either UserError Text
validateUsernameChange oldName newName =
  case validateUserName newName of
    Nothing -> Left UserNameError
    Just x  -> if x == oldName
      then Left RedundantUsernameChangeError
      else Right x


validateAccountDeletion ::
  User -> Maybe Text -> UTCTime -> Either UserError User
validateAccountDeletion u inputPw t =
  case inputPw of
    Nothing -> Left InvalidPwError
    Just x  -> if (userPw u) /= x
      then Left InvalidPwError
      else Right u{userDelDate = Just t}


--------------------------------------------------------------------------------


validateProfileChange ::
     Profile
  -> Maybe Text
  -> Maybe Text
  -> Maybe Text
  -> Maybe Text
  -> Either UserError Profile
validateProfileChange oldProfile mName mEmail mLoc mDesc =
  do
    name  <- maybeToRight ProfileNameError        $ validateProfileName mName
    email <- maybeToRight ProfileEmailError       $ validateProfileEmail mEmail
    loc   <- maybeToRight ProfileLocationError    $ validateProfileLocation mLoc
    desc  <- maybeToRight ProfileDescriptionError $ validateProfileDescription mDesc
    return
      oldProfile
        { profileName = name
        , profileEmail = email
        , profileLocation = loc
        , profileDescription = desc
        }



--------------------------------------------------------------------------------
