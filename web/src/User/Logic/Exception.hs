{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}


module User.Logic.Exception where

import Control.Exception
import Data.Text

import Base.Lang
import User.Logic.Translate as Translate


--------------------------------------------------------------------------------


data UserError
  = UserDatabaseError SomeException
  ---------------------------------------------------------
  | UserNameError
  | UserEmailError
  | UserPwError
  | UserDoesNotExistError
  | UserAlreadyExists
  | RedundantUsernameChangeError
  -- | PwMismatchError
  | InvalidPwError
  | UserIsBannedError
  | UserIsDeletedError
  ---------------------------------------------------------
  | ProfileNameError
  | ProfileEmailError
  | ProfileDescriptionError
  | ProfileLocationError



instance Translate UserError where
  translate = translateUserError


translateUserError :: Lang -> UserError -> Text
translateUserError lang err =
  case err of
    UserDatabaseError _ -> Translate.userDatabaseError lang
    ----------------------------------------------------------------------------
    UserNameError                -> Translate.userNameError lang
    UserEmailError               -> Translate.userEmailError lang
    UserPwError                  -> Translate.userPwError lang
    UserDoesNotExistError        -> Translate.userDoesNotExistError lang
    UserAlreadyExists            -> Translate.userAlreadyExists lang
    RedundantUsernameChangeError -> Translate.redundantUsernameChangeError lang
    InvalidPwError               -> Translate.invalidPwError lang
    UserIsBannedError            -> Translate.userIsBannedError lang
    UserIsDeletedError           -> Translate.userIsDeletedError lang
    ----------------------------------------------------------------------------
    ProfileNameError        -> Translate.profileNameError lang
    ProfileEmailError       -> Translate.profileEmailError lang
    ProfileDescriptionError -> Translate.profileDescriptionError lang
    ProfileLocationError    -> Translate.profileLocationError lang


--------------------------------------------------------------------------------
