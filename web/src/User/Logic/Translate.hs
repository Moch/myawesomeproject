{-# LANGUAGE OverloadedStrings #-}


module User.Logic.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------

companySite :: Lang -> Text
companySite lang =
  case lang of
    ES -> "Página de empresa"
    _  -> "Company site"


basic :: Lang -> Text
basic lang =
  case lang of
    ES -> "Básico"
    _  -> "Basic"


advanced :: Lang -> Text
advanced lang =
  case lang of
    ES -> "Avanzado"
    _  -> "Advanced"


fluent :: Lang -> Text
fluent lang =
  case lang of
    ES -> "Fluido"
    _  -> "Fluent"


--------------------------------------------------------------------------------


userDatabaseError :: Lang -> Text
userDatabaseError lang =
  case lang of
    ES -> "Vaya, algo salió mal con la base de datos"
    _  -> "Oops, something went wrong with the database"


userNameError :: Lang -> Text
userNameError lang =
  case lang of
    ES -> "Error en el nombre de usuario "
    _  -> "Error in user name input "


userEmailError :: Lang -> Text
userEmailError lang =
  case lang of
    ES -> "Error en la dirección email "
    _  -> "Error in email input "


userPwError :: Lang -> Text
userPwError lang =
  case lang of
    ES -> "Error en la contraseña "
    _  -> "Error in password input "


userDoesNotExistError :: Lang -> Text
userDoesNotExistError lang =
  case lang of
    ES -> "El usuario no existe"
    _  -> "User does not exist"


userAlreadyExists :: Lang -> Text
userAlreadyExists lang =
  case lang of
    ES -> "El usuario ya existe"
    _  -> "User already exists"


redundantUsernameChangeError :: Lang -> Text
redundantUsernameChangeError lang =
  case lang of
    ES -> "¿Quieres cambiar tu nombre de usuario o marear la perdiz?"
    _  -> "Do you want to change your username or not?"


invalidPwError :: Lang -> Text
invalidPwError lang =
  case lang of
    ES -> "Error: la contraseña es incorrecta"
    _  -> "Error: incorrect password"


userIsBannedError :: Lang -> Text
userIsBannedError lang =
  case lang of
    ES -> "Esta cuenta ha sido bloqueada"
    _  -> "This account has been blocked"


userIsDeletedError :: Lang -> Text
userIsDeletedError lang =
  case lang of
    ES -> "Esta cuenta ha sido borrada"
    _  -> "This account has been deleted"


profileNameError :: Lang -> Text
profileNameError lang =
  case lang of
    ES -> "Error en el nombre introducido "
    _  -> "Error in user name input "


profileEmailError :: Lang -> Text
profileEmailError lang =
  case lang of
    ES -> "Error en la dirección email "
    _  -> "Error in email input: "


profileDescriptionError :: Lang -> Text
profileDescriptionError lang =
  case lang of
    ES -> "Error en la descripción "
    _  -> "Error in description input "


profileLocationError :: Lang -> Text
profileLocationError lang =
  case lang of
    ES -> "Error en la localización "
    _  -> "Error in location input "

--------------------------------------------------------------------------------
