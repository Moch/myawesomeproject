-- {-# LANGUAGE EmptyDataDecls             #-}
-- {-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}




module User.Logic.Models where

import           Control.Monad
-- import           Control.Monad.IO.Class  (liftIO)
import           Data.Text (Text)
import           Data.Time
import           Database.Persist
-- import           Database.Persist.Postgresql
import           Database.Persist.TH

import           User.Logic.Base

--------------------------------------------------------------------------------

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
  User
    type    UserType
    name    Text
    email   Text
    pw      Text
    banDate UTCTime Maybe
    delDate UTCTime Maybe
    regDate UTCTime Maybe
    valDate UTCTime Maybe
    UniqueUserName  name
    UniqueUserEmail email
    deriving Show
  Profile
    userId      UserId
    name        Text
    email       Text
    location    Text
    description Text
    UniqueProfileUserId userId
    deriving Show
  ProfileLang
    userId   UserId
    code     SpeakerLang
    level    SpeakerLevel
    deriving Show Eq
  ProfileLink
    userId   UserId
    platform SocialPlatform
    url      Text
    deriving Show
  |]


newProfile :: UserId -> Profile
newProfile uId = Profile uId "" "" "" ""

--------------------------------------------------------------------------------

data FullUser = FullUser
  { uID      :: UserId
  , uCore    :: User
  , uProfile :: Profile
  , uLangs   :: [ ProfileLang ]
  , uSocial  :: [ ProfileLink ]
  }


mkFullUser
  :: UserId
  -> Maybe User
  -> Maybe (Entity Profile)
  -> [Entity ProfileLang]
  -> [Entity ProfileLink]
  -> Maybe FullUser
mkFullUser uId mUser mProfile langList linkList =
    liftM2 f mUser mProfile
  where
    g (Entity _ x) = x
    f u p =
      FullUser
        { uID      = uId
        , uCore    = u
        , uProfile = g p
        , uLangs   = map g langList
        , uSocial  = map g linkList
        }


--------------------------------------------------------------------------------
