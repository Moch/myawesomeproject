{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}


module User.Logic.Base where

import           Data.Char           (isDigit)
import           Data.List           (groupBy, sort, nubBy)
import           Data.Maybe          (fromJust, isJust)
import           Data.Text           (Text, pack)
import qualified Data.Text           as T
import           Database.Persist.TH
import           Text.Read           (readMaybe)

import Base.Lang
import Base.Validate
import User.Logic.Translate as Translate


--------------------------------------------------------------------------------

data UserType = UserPerson | UserCompany
  deriving (Show, Read, Eq)
derivePersistField "UserType"

--------------------------------------------------------------------------------

data SocialPlatform
  = Selflink
  | Github
  | Linkedin
  | Medium
  | Stackoverflow
  | Twitter
  deriving (Show, Read, Eq, Ord)
derivePersistField "SocialPlatform"

parseSocialLink :: Text -> (SocialPlatform, Text)
parseSocialLink t
    | check "https://github.com/"              = res Github
    | check "https://www.linkedin.com/in/"     = res Linkedin
    | check "https://medium.com/@"             = res Medium
    | check "https://stackoverflow.com/users/" = res Stackoverflow
    | check "https://twitter.com/"             = res Twitter
    | otherwise                                = res Selflink
  where
    check = flip T.isPrefixOf t
    res x = (x,t)


translateSocialPlatform :: UserType -> Lang -> SocialPlatform -> Text
translateSocialPlatform uType lang platform =
  case platform of
    Selflink -> case uType of
      UserCompany -> Translate.companySite lang
      UserPerson  -> "Portfolio"
    other -> pack $ show other



--------------------------------------------------------------------------------

newtype SpeakerLang = SpeakerLang Lang
  deriving (Eq, Ord)
derivePersistField "SpeakerLang"

instance Show SpeakerLang where
  show (SpeakerLang x) = show x

instance Read SpeakerLang where
  readsPrec n s = map (\(a,b) -> (SpeakerLang a, b)) $ readsPrec n s

instance Translate SpeakerLang where
  translate lang (SpeakerLang x) = translate lang x


data SpeakerLevel
  = LevelFluent
  | LevelAdvanced
  | LevelBasic
  deriving (Eq, Ord, Show, Read)
derivePersistField "SpeakerLevel"


instance Translate SpeakerLevel where
  translate lang lvl = case lvl of
    LevelBasic    -> Translate.basic    lang
    LevelAdvanced -> Translate.advanced lang
    LevelFluent   -> Translate.fluent   lang



data Speaker = Speaker
  { speakerLang  :: SpeakerLang
  , speakerLevel :: SpeakerLevel
  } deriving (Eq, Ord)

instance Show Speaker where
  show (Speaker a b) = (show a) ++ "+" ++ (show b)

instance Read Speaker where
  readsPrec _ s = case s of
    a:b:'+':rest -> [ (Speaker (read [a,b]) (read rest) , "") ]
    _            -> []

--------------------------------------------------------------------------------

validateUserName :: Maybe Text -> Maybe Text
validateUserName mName =
  let
    noLeadingHyphens t =
      if T.head t == '-' then Nothing else Just t
    noEndingHyphens t =
      if T.last t == '-' then Nothing else Just t
    onlySingleHyphens t =
      if "--" `T.isInfixOf` t then Nothing else Just t
  in
    mName
    >>= validateText userNameRestrictions
    >>= noLeadingHyphens
    >>= noEndingHyphens
    >>= onlySingleHyphens


userNameRestrictions :: TextRestrictions
userNameRestrictions =
    TextRestrictions 3 254 f
  where
    f c =
         isDigit c
      || c `elem` ['a'..'z']
      || c `elem` ['A'..'Z']
      || c == '-'



validateUserEmail :: Maybe Text -> Maybe Text
validateUserEmail =
  (>>= validateText userEmailRestrictions)

userEmailRestrictions :: TextRestrictions
userEmailRestrictions = TextRestrictions 3 254 (const True)



validateUserPw :: Maybe Text -> Maybe Text
validateUserPw =
  (>>= validateText userPwRestrictions)

userPwRestrictions :: TextRestrictions
userPwRestrictions = TextRestrictions 3 254 (const True)


--------------------------------------------------------------------------------


validateProfileName :: Maybe Text -> Maybe Text
validateProfileName =
  (>>= validateText profileNameRestrictions)

profileNameRestrictions :: TextRestrictions
profileNameRestrictions = TextRestrictions 0 254 (const True)



validateProfileEmail :: Maybe Text -> Maybe Text
validateProfileEmail =
  (>>= validateText profileEmailRestrictions)

profileEmailRestrictions :: TextRestrictions
profileEmailRestrictions = TextRestrictions 0 254 (const True)



validateProfileLocation :: Maybe Text -> Maybe Text
validateProfileLocation =
  (>>= validateText profileLocationRestrictions)

profileLocationRestrictions :: TextRestrictions
profileLocationRestrictions = TextRestrictions 0 254 (const True)



validateProfileDescription :: Maybe Text -> Maybe Text
validateProfileDescription =
  (>>= validateText profileDescriptionRestrictions)

profileDescriptionRestrictions :: TextRestrictions
profileDescriptionRestrictions = TextRestrictions 0 500 (const True)




validateSpeakerList :: [Text] -> [Speaker]
validateSpeakerList =
    map head
  . groupBy (\ s1 s2 -> speakerLang s1 == speakerLang s2)
  . sort
  . map fromJust
  . filter isJust
  . map (readMaybe . T.unpack)



validateSocialLinks :: [Text] -> [ (SocialPlatform , Text) ]
validateSocialLinks =
    nubBy (\ (a,_) (b,_) -> a == b)
  . map (parseSocialLink . fromJust)
  . filter isJust
  . map (validateText socialLinkRestrictions)

socialLinkRestrictions :: TextRestrictions
socialLinkRestrictions = TextRestrictions 0 254 (const True)

--------------------------------------------------------------------------------
