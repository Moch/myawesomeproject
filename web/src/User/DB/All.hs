-- {-# LANGUAGE EmptyDataDecls             #-}
-- {-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}


module User.DB.All where

import           Data.Either.Combinators (mapLeft)
import           Data.Maybe              (isJust)
import           Data.Pool               (Pool)
import           Data.Text               (Text)
import           Database.Persist
import           Database.Persist.Sql    (SqlBackend)

import Base.Persist
import User.Logic.All


--------------------------------------------------------------------------------

userNameAlreadyExists :: Pool SqlBackend -> Text -> IO (Either UserError Bool)
userNameAlreadyExists pool t =
  fmap (mapLeft UserDatabaseError) $ runDB pool $ do
    mUser <- getBy $ UniqueUserName t
    return $ isJust mUser


userEmailAlreadyExists :: Pool SqlBackend -> Text -> IO (Either UserError Bool)
userEmailAlreadyExists pool t =
  fmap (mapLeft UserDatabaseError) $ runDB pool $ do
    mUser <- getBy $ UniqueUserEmail t
    return $ isJust mUser


persistNewUser
  :: Pool SqlBackend
  -> User
  -> IO ( Either UserError (Key User, User) )
persistNewUser pool u = do
  e1 <- runDB pool $ do
    m1 <- getBy $ UniqueUserName  (userName u)
    m2 <- getBy $ UniqueUserEmail (userEmail u)
    pure (m1,m2)
  case e1 of
    Left err -> return $ Left $ UserDatabaseError err
    Right (Just _, _) -> return $ Left UserAlreadyExists
    Right (_, Just _) -> return $ Left UserAlreadyExists
    _ -> do
      e2 <- runDB pool $ do
        uId <- insert u
        _   <- insert $ newProfile uId
        pure (uId, u)
      return $ mapLeft UserDatabaseError e2


getUserById
  :: Pool SqlBackend
  -> Key User
  -> IO (Either UserError (Maybe FullUser))
getUserById pool uId =
  fmap (mapLeft UserDatabaseError) $ runDB pool $ do
    core     <- get uId
    prof     <- getBy $ UniqueProfileUserId uId
    langList <- selectList [ProfileLangUserId ==. uId] []
    linkList <- selectList [ProfileLinkUserId ==. uId] []
    return $
      mkFullUser uId core prof langList linkList


getUserByName
  :: Pool SqlBackend
  -> Text
  -> IO (Either UserError (Maybe FullUser))
getUserByName pool t =
  fmap (mapLeft UserDatabaseError) $ runDB pool $ do
    core <- getBy $ UniqueUserName t
    case core of
      Nothing -> return Nothing
      Just (Entity uId u) -> do
        prof     <- getBy $ UniqueProfileUserId uId
        langList <- selectList [ProfileLangUserId ==. uId] []
        linkList <- selectList [ProfileLinkUserId ==. uId] []
        return $
          mkFullUser uId (Just u) prof langList linkList


getUserByEmail
 :: Pool SqlBackend
 -> Text
 -> IO (Either UserError (Maybe FullUser))
getUserByEmail pool t =
  fmap (mapLeft UserDatabaseError) $ runDB pool $ do
    core <- getBy $ UniqueUserEmail t
    case core of
      Nothing -> return Nothing
      Just (Entity uId u) -> do
        prof     <- getBy $ UniqueProfileUserId uId
        langList <- selectList [ProfileLangUserId ==. uId] []
        linkList <- selectList [ProfileLinkUserId ==. uId] []
        return $
          mkFullUser uId (Just u) prof langList linkList


deleteUser
  :: Pool SqlBackend
  -> Key User
  -> IO (Either UserError ())
deleteUser pool uId =
  fmap (mapLeft UserDatabaseError) $ runDB pool $ do
    delete uId
    deleteBy $ UniqueProfileUserId uId
    deleteWhere [ProfileLangUserId ==. uId]
    deleteWhere [ProfileLinkUserId ==. uId]


changePassword
  :: Pool SqlBackend
  -> Key User
  -> Text
  -> IO (Either UserError ())
changePassword pool uId newPw =
  fmap (mapLeft UserDatabaseError) $ runDB pool $ do
    update uId [UserPw =. newPw]


changeUsername
  :: Pool SqlBackend
  -> Key User
  -> Text
  -> IO (Either UserError ())
changeUsername pool uId newName =
  fmap (mapLeft UserDatabaseError) $ runDB pool $ do
    update uId [UserName =. newName]


--------------------------------------------------------------------------------

updateProfile
 :: (PersistEntity record, PersistEntityBackend record ~ SqlBackend)
 => Pool SqlBackend
 -> Key User
 -> [Speaker]
 -> record
 -> IO (Either UserError ())
updateProfile pool uId langs prof =
    fmap (mapLeft UserDatabaseError) $ runDB pool $ do
      deleteBy $ UniqueProfileUserId uId
      deleteWhere [ProfileLangUserId ==. uId]
      _ <- insert prof
      _ <- insertMany $ map f langs
      return ()
  where
    f (Speaker lang lvl) =
      ProfileLang
        { profileLangUserId = uId
        , profileLangCode   = lang
        , profileLangLevel  = lvl
        }


updateSocialLinks
  :: Pool SqlBackend
  -> Key User
  -> [(SocialPlatform, Text)]
  -> IO (Either UserError ())
updateSocialLinks pool uId links =
    fmap (mapLeft UserDatabaseError) $ runDB pool $ do
      deleteWhere [ProfileLinkUserId ==. uId]
      _ <- insertMany $ map f links
      return ()
  where
    f (x,y) =
      ProfileLink
        { profileLinkUserId   = uId
        , profileLinkPlatform = x
        , profileLinkUrl      = y
        }


--------------------------------------------------------------------------------
