{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Elems.Inputs.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

jobInputsCss :: Css
jobInputsCss =
  associateCss
    [ (,) "job-input-wrapper"          inputWrapperCss
    , (,) "job-input-title"            inputTitleCss
    , (,) "job-input"                  inputCss
    , (,) "job-input-radio-wrapper"    radioWrapperCss
    , (,) "job-input-radio"            radioInputCss
    , (,) "job-input-checkbox-wrapper" checkboxWrapperCss
    , (,) "job-input-checkbox"         checkboxCss
    , (,) "job-input-apply-method"     howToApplyCss
    ]

--------------------------------------------------------------------------------

inputWrapperCss :: Css
inputWrapperCss = do
  -- flex parent
  display flex
  flexDirection column
  -- geometry
  paddingBottom (Clay.rem 2.5)


inputTitleCss :: Css
inputTitleCss = do
  -- font
  fontColor $ rgb 0 80 80
  setFont normal (Clay.rem 1) [ubuntu]
  -- geometry
  marginBottom (Clay.rem 0.5)


inputCss :: Css
inputCss = do
  -- border
  borderWithRadius solid (px 1) darkgray (px 8)
  -- font
  setFont normal (Clay.rem 1) [smooth]
  -- geometry
  width (pct 100)
  maxWidth (Clay.rem 40)
  height (Clay.rem 2)
  paddingLeft (Clay.rem 0.5)


radioWrapperCss :: Css
radioWrapperCss = do
  -- display
  cursor pointer
  -- flex parent
  display flex
  alignItems center
  -- geometry
  maxWidth (Clay.rem 15)
  paddingTop (Clay.rem 0.5)


radioInputCss :: Css
radioInputCss = do
  -- border
  outlineStyle none
  -- display
  cursor pointer
  -- geometry
  marginRight (Clay.rem 0.5)


checkboxWrapperCss :: Css
checkboxWrapperCss = do
  -- display
  cursor pointer
  -- flex parent
  display flex
  alignItems center
  -- geometry
  paddingTop (Clay.rem 0.5)
  maxWidth (Clay.rem 20)


checkboxCss :: Css
checkboxCss = do
  -- border
  outlineStyle none
  -- display
  cursor pointer
  -- geometry
  marginRight (Clay.rem 0.5)


howToApplyCss :: Css
howToApplyCss = do
  -- border
  border solid (px 1) silver
  -- font
  setFont normal (Clay.rem 1) [smooth]
  -- geometry
  width (pct 100)
  height (Clay.rem 5)
  "resize" -: "vertical"
  let x = Clay.rem 0.5
  padding x x x x

--------------------------------------------------------------------------------
