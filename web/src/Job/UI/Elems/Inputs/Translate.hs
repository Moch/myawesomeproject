{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Elems.Inputs.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------

skillLevel :: Lang -> Text
skillLevel lang =
  case lang of
    ES -> "Nivel de experiencia: "
    _  -> "Skill level: "


jobTitle :: Lang -> Text
jobTitle lang =
  case lang of
    ES -> "Título del anuncio: "
    _  -> "Job title: "


jobTitlePlaceholder :: Lang -> Text
jobTitlePlaceholder lang =
  case lang of
    ES -> "ej: Ingeniero Haskell"
    _  -> "e.g. Haskell Engineer"


location :: Lang -> Text
location lang =
  case lang of
    ES -> "Ubicación: "
    _  -> "Location: "


locationPlaceholder :: Lang -> Text
locationPlaceholder lang =
  case lang of
    ES -> "ej: Madrid, España"
    _  -> "e.g. Madrid, Spain"


allowRemote :: Lang -> Text
allowRemote lang =
  case lang of
    ES -> "Permitir trabajo a distancia"
    _  -> "Allow remote work"


keywords :: Lang -> Text
keywords lang =
  case lang of
    ES -> "Palabras clave: "
    _  -> "Key words: "


keywordsPlaceholder :: Lang -> Text
keywordsPlaceholder lang =
  case lang of
    ES -> "ej: Haskell, Blockchain"
    _  -> "e.g. Haskell, product management, Blockchain"


salary :: Lang -> Text
salary lang =
  case lang of
    ES -> "Sueldo: "
    _  -> "Salary: "


salaryPlaceholder :: Lang -> Text
salaryPlaceholder lang =
  case lang of
    ES -> "ej: 80k-100k € , competitivo"
    _  -> "e.g. €80k-100k, competitive"


employmentType :: Lang -> Text
employmentType lang =
  case lang of
    ES -> "Tipo de contrato: "
    _  -> "Employment type: "


fullTime :: Lang -> Text
fullTime lang =
  case lang of
    ES -> "Jornada completa"
    _  -> "Full-time"


partTime :: Lang -> Text
partTime lang =
  case lang of
    ES -> "Media jornada"
    _  -> "Part-time"


contract :: Lang -> Text
contract lang =
  case lang of
    ES -> "Contrato por obra"
    _  -> "Contract"


temporary :: Lang -> Text
temporary lang =
  case lang of
    ES -> "Temporal"
    _  -> "Temporary"


internship :: Lang -> Text
internship lang =
  case lang of
    ES -> "Prácticas"
    _  -> "Internship"


howToApply :: Lang -> Text
howToApply lang =
  case lang of
    ES -> "Método de contacto: "
    _  -> "How to apply: "


howtoapplyPlaceholder :: Lang -> Text
howtoapplyPlaceholder lang =
  case lang of
    ES -> "ej: enviar currículum a email@empresa.com"
    _  -> "e.g. send resume to email@company.com"


jobDescription :: Lang -> Text
jobDescription lang =
  case lang of
    ES -> "Descripción del empleo: "
    _  -> "Job description: "

--------------------------------------------------------------------------------
