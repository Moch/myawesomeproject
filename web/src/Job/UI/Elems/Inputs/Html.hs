{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Elems.Inputs.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Lang
import Base.UI.Elems.MarkdownEditor.Html
import Job.Logic.All
import Job.UI.Elems.Inputs.Translate as Translate
import Job.UI.Elems.JobType.Css

--------------------------------------------------------------------------------

jobSkillDiv :: JobType -> Lang -> Html
jobSkillDiv p lang =
    H.div ! A.class_ "job-input-wrapper" $ do
      H.span (H.toHtml $ Translate.skillLevel lang)
        ! A.class_ "job-input-title"
      H.span (H.toHtml $ translate lang p)
        ! A.class_ (H.toValue $ jobTypeClass p)


jobTitleDiv :: Lang -> Html
jobTitleDiv lang =
    H.div ! A.class_ "job-input-wrapper" $ do
      H.div (H.toHtml $ Translate.jobTitle lang)
        ! A.class_ "job-input-title"
      H.input
        ! A.class_   "job-input"
        ! A.type_    "text"
        ! A.required "required"
        ! A.name     "job-title"
        ! A.placeholder (H.toValue $ Translate.jobTitlePlaceholder lang)


jobLocationDiv :: Lang -> Html
jobLocationDiv lang =
    H.div ! A.class_ "job-input-wrapper" $ do
      H.div (H.toHtml $ Translate.location lang)
        ! A.class_ "job-input-title"
      H.input
        ! A.class_   "job-input"
        ! A.type_    "text"
        ! A.required "required"
        ! A.name     "job-location"
        ! A.placeholder (H.toValue $ Translate.locationPlaceholder lang)
      allowRemoteLabel
  where
    allowRemoteLabel =
      H.label ! A.class_ "job-input-checkbox-wrapper" $ do
        allowRemoteInput
        H.toHtml $ Translate.allowRemote lang
    allowRemoteInput =
      H.input
        ! A.class_ "job-input-checkbox"
        ! A.type_ "checkbox"
        ! A.name  "job-allows-remote"


jobKeywordsDiv :: Lang -> Html
jobKeywordsDiv lang =
    H.div ! A.class_ "job-input-wrapper" $ do
      H.div (H.toHtml $ Translate.keywords lang)
        ! A.class_ "job-input-title"
      H.input
        ! A.class_   "job-input"
        ! A.type_    "text"
        ! A.required "required"
        ! A.name     "job-keywords"
        ! A.placeholder (H.toValue $ Translate.keywordsPlaceholder lang)


jobSalaryDiv :: Lang -> Html
jobSalaryDiv lang =
    H.div ! A.class_ "job-input-wrapper" $ do
      H.div (H.toHtml $ Translate.salary lang)
        ! A.class_ "job-input-title"
      H.input
        ! A.class_   "job-input"
        ! A.type_    "text"
        ! A.required "required"
        ! A.name     "job-salary"
        ! A.placeholder (H.toValue $ Translate.salaryPlaceholder lang)


jobTypeDiv :: Lang -> Html
jobTypeDiv lang =
    H.div ! A.class_ "job-input-wrapper" $ do
      H.div (H.toHtml $ Translate.employmentType lang)
        ! A.class_ "job-input-title"
      H.label ! A.class_ "job-input-radio-wrapper" $ do
        radioInput "full-time"
        H.toHtml $ Translate.fullTime lang
      H.label ! A.class_ "job-input-radio-wrapper" $ do
        radioInput "part-time"
        H.toHtml $ Translate.partTime lang
      H.label ! A.class_ "job-input-radio-wrapper" $ do
        radioInput "contract"
        H.toHtml $ Translate.contract lang
      H.label ! A.class_ "job-input-radio-wrapper" $ do
        radioInput "temporary"
        H.toHtml $ Translate.temporary lang
      H.label ! A.class_ "job-input-radio-wrapper" $ do
        radioInput "internship"
        H.toHtml $ Translate.internship lang
  where
    radioInput x =
      H.input
        ! A.class_   "job-input-radio"
        ! A.type_    "radio"
        ! A.required "required"
        ! A.name     "job-type"
        ! A.value    x


jobApplyMethodDiv :: Lang -> Html
jobApplyMethodDiv lang =
    H.div ! A.class_ "job-input-wrapper" $ do
      H.div (H.toHtml $ Translate.howToApply lang)
        ! A.class_ "job-input-title"
      H.textarea (H.toHtml $ Translate.howtoapplyPlaceholder lang)
        ! A.class_   "job-input-apply-method"
        ! A.required "required"
        ! A.name     "job-apply-method"


jobDescriptionDiv :: Lang -> Html
jobDescriptionDiv lang =
    H.div ! A.class_ "job-input-wrapper" $ do
      H.div (H.toHtml $ Translate.jobDescription lang)
        ! A.class_ "job-input-title"
      markdownEditor lang "job_description_mde_textarea"

--------------------------------------------------------------------------------
