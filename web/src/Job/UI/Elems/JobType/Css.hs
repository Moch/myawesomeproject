{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Elems.JobType.Css where

import Clay
import Data.Text

import Base.Css.Utils
import Job.Logic.All

--------------------------------------------------------------------------------

jobTypeCss :: Css
jobTypeCss =
  associateCss
    [ (,) (jobTypeClass Novice)       noviceCss
    , (,) (jobTypeClass Expert)       expertCss
    , (,) (jobTypeClass Master)       masterCss
    , (,) (jobTypeButtonClass Novice) noviceButtonCss
    , (,) (jobTypeButtonClass Expert) expertButtonCss
    , (,) (jobTypeButtonClass Master) masterButtonCss
    ]

--------------------------------------------------------------------------------


jobTypeClass :: JobType -> Text
jobTypeClass lvl = case lvl of
  Novice -> "job-type-novice"
  Expert -> "job-type-expert"
  Master -> "job-type-master"


noviceCss :: Css
noviceCss = do
  -- color
  (-:) "fill" "forestgreen"
  borderColor forestgreen
  fontColor forestgreen


expertCss :: Css
expertCss = do
  -- color
  (-:) "fill" "darkorange"
  borderColor darkorange
  fontColor darkorange


masterCss :: Css
masterCss = do
  -- color
  (-:) "fill" "red"
  borderColor red
  fontColor red


jobTypeButtonClass :: JobType -> Text
jobTypeButtonClass lvl = case lvl of
  Novice -> "job-type-button-novice"
  Expert -> "job-type-button-expert"
  Master -> "job-type-button-master"


noviceButtonCss :: Css
noviceButtonCss = do
  -- color
  backgroundColor (rgb 200 255 200)
  hover & do
    h3 ? fontColor white
    fontColor white
    backgroundColor forestgreen


expertButtonCss :: Css
expertButtonCss = do
  -- color
  backgroundColor (rgb 255 255 155)
  hover & do
    h3 ? fontColor white
    fontColor white
    backgroundColor darkorange


masterButtonCss :: Css
masterButtonCss = do
  -- color
  backgroundColor (rgb 255 200 200)
  hover & do
    h3 ? fontColor white
    fontColor white
    backgroundColor red



--------------------------------------------------------------------------------
