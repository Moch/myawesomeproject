{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Views.CreateJobPost.Html where

import Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Head
import Base.Html.Page
import Base.Lang
import Base.UI.Layout.Template.Html
import Job.API.Routes
import Job.Logic.All
import Job.UI.Elems.Inputs.Html
import Job.UI.Views.CreateJobPost.Translate as Translate

--------------------------------------------------------------------------------

createJobPostPageView :: JobType -> PageParams -> Html
createJobPostPageView lvl params =
    H.toHtml createJobPostPage
  where
    createJobPostPage = Page
      { pageParams = params
      , htmlHead   = createJobPostHead
      , htmlBody   = paylineTemplate (createJobPostBody lvl)
      }


createJobPostHead :: Head
createJobPostHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""

--------------------------------------------------------------------------------

createJobPostBody :: JobType -> PageParams -> Html
createJobPostBody lvl params =
  let
    lang = ppLang params
  in
    H.div ! A.class_ "create-job-post-body" $
      H.div ! A.class_ "resp-w" $ do
        heading lang
        H.div ! A.class_ "create-job-post-wrapper" $ do
          H.div ! A.class_ "create-job-post-thin-column" $ do
            importantNote lang
            instructions  lang
          H.div ! A.class_ "create-job-post-wide-column" $
            createJobPostForm lvl lang

--------------------------------------------------------------------------------

heading :: Lang -> Html
heading lang =
  H.h1 (H.toHtml $ Translate.createYourJobPost lang)
    ! A.class_ "create-job-post-heading"


instructions :: Lang -> Html
instructions lang =
  H.div
    ! A.class_ "create-job-post-instructions"
    $ H.toHtml $ Translate.englishPref lang


importantNote :: Lang -> Html
importantNote lang =
  H.div (H.toHtml $ Translate.noteText lang)
    ! A.class_ "create-job-post-note"


--------------------------------------------------------------------------------


createJobPostForm :: JobType -> Lang -> Html
createJobPostForm lvl lang =
  H.form
    ! A.class_  "create-job-post-form"
    ! A.action  (H.toValue purchaseJobPostRoute)
    ! A.enctype "multipart/form-data"
    ! A.method  "POST"
    $ do
      jobSkillDiv lvl lang
      jobTitleDiv lang
      jobLocationDiv lang
      jobKeywordsDiv lang
      jobSalaryDiv lang
      jobTypeDiv lang
      jobApplyMethodDiv lang
      jobDescriptionDiv lang
      hiddenInput lvl
      submitButton lang


hiddenInput :: JobType -> Html
hiddenInput lvl =
    H.input
      ! A.type_ "hidden"
      ! A.name  "job-package"
      ! A.value (H.toValue $ show lvl)



submitButton :: Lang -> Html
submitButton lang =
  H.div ! A.class_ "create-job-post-button-wrap" $
    H.input
      ! A.class_ "create-job-post-button"
      ! A.type_  "submit"
      ! A.value  (H.toValue $ Translate.done lang)

--------------------------------------------------------------------------------
