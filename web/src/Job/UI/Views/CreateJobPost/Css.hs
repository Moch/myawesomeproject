{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Views.CreateJobPost.Css where

import Clay

import Base.Css.Utils

--------------------------------------------------------------------------------

createJobPostCss :: Css
createJobPostCss =
  associateCss
    [ (,) "create-job-post-body"         outerBodyCss
    , (,) "create-job-post-heading"      headingCss
    , (,) "create-job-post-wrapper"      wrapperCss
    , (,) "create-job-post-thin-column"  thinColumnCss
    , (,) "create-job-post-wide-column"  wideColumnCss
    , (,) "create-job-post-instructions" instructionsCss
    , (,) "create-job-post-note"         noteCss
    , (,) "create-job-post-note-a"       noteLinkCss
    , (,) "create-job-post-form"         formWrapperCss
    , (,) "create-job-post-button-wrap"  buttonWrapperCss
    , (,) "create-job-post-button"       buttonCss
    ]

--------------------------------------------------------------------------------


outerBodyCss :: Css
outerBodyCss = do
  -- background
  backgroundColor $ rgb 230 230 255
  -- flex child
  flexGrow 1
  -- flex parent
  display flex
  justifyContent center


headingCss :: Css
headingCss = do
  -- font
  setFont normal (Clay.rem 2.2) [smooth]
  fontColor indigo
  -- flex parent
  display flex
  justifyContent center
  alignItems center
  -- geometry
  padding (Clay.rem 2) (Clay.rem 2) (px 0) (Clay.rem 2)
  laptop $ paddingTop (Clay.rem 3.5)
  -- text
  textAlign center

--------------------------------------------------------------------------------

wrapperCss :: Css
wrapperCss = do
  -- color
  fontColor $ rgb 0 0 50
  -- flex parent
  display flex
  flexDirection column
  laptop $
    flexDirection row
  -- geometry
  let x = Clay.rem 2
  padding 0 x x x


thinColumnCss :: Css
thinColumnCss = do
  -- flex child
  order 0
  laptop $ order 1
  -- geometry
  paddingTop (Clay.rem 2)
  width (pct 100)
  laptop $ do
    paddingTop (Clay.rem 3)
    width (pct 25)
  -- text
  textAlign justify


wideColumnCss :: Css
wideColumnCss = do
  -- geometry
  paddingTop (Clay.rem 2)
  width (pct 100)
  laptop $ do
    maxWidth (pct 75)
    paddingTop (Clay.rem 3)
    paddingRight (Clay.rem 2)


--------------------------------------------------------------------------------

noteCss :: Css
noteCss = do
  -- font
  setFont normal (Clay.rem 1) [zeroSerif]

noteLinkCss :: Css
noteLinkCss = do
  -- color
  fontColor indigo
  hover & fontColor purple
  -- display
  display block
  -- font
  setFont normal (Clay.rem 1) [play]
  -- geometry
  marginTop (px 10)
  -- text
  textDecoration none
  textAlign center


instructionsCss :: Css
instructionsCss = do
  -- font
  setFont normal (Clay.rem 1) [ubuntu]
  -- geometry
  marginTop (Clay.rem 2.5)
  marginBottom (Clay.rem 1)

--------------------------------------------------------------------------------

formWrapperCss :: Css
formWrapperCss = do
  -- font
  setFont normal (Clay.rem 1) [smooth]


buttonWrapperCss :: Css
buttonWrapperCss = do
  -- flex parent
  display flex
  justifyContent center
  alignItems center
  -- geometry
  paddingTop (Clay.rem 1)
  paddingBottom (Clay.rem 4)


buttonCss :: Css
buttonCss = do
  -- border
  borderWithRadius solid (px 1) teal (px 12)
  (-:) "box-shadow" "0px 0px 4px 1px rgba(0,0,0,0.3)"
  -- color
  backgroundColor limegreen
  fontColor white
  hover & backgroundColor lime
  -- display
  cursor pointer
  -- flex parent
  display flex
  justifyContent center
  alignItems center
  -- font
  fontVariant smallCaps
  setFont bold (Clay.rem 1.5) [smooth]
  -- geometry
  let x = Clay.rem 0.8
  padding x x x x
  width (px 280)
  -- text
  (-:)
    "text-shadow"
    "1px 0 0 indigo, -1px 0 0 indigo, 0 -1px 0 indigo, 0 1px 0 indigo"

--------------------------------------------------------------------------------
