{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Views.CreateJobPost.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------


createYourJobPost :: Lang -> Text
createYourJobPost lang =
  case lang of
    ES -> "Crea tu Anuncio"
    _  -> "Create Your Job Listing"


englishPref :: Lang -> Text
englishPref lang =
  case lang of
    ES -> Data.Text.concat
      [ "Recomendamos escribir el anuncio en inglés, "
      , "podrás crear diferentes versiones en otros idiomas más tarde."
      ]
    _  -> Data.Text.concat
      [ "We recommend that you write the job post in english, "
      , "you will be able to create more versions in other languages later."
      ]


noteText :: Lang -> Text
noteText lang =
  case lang of
    ES -> Data.Text.concat
      [ "Por motivos de comodidad y consistencia, "
      , "tu información de empresa (descripción y logo) "
      , "se rellena desde tu perfil "
      , "y se comparte entre todos tus anuncios."
      ]
    _  -> Data.Text.concat
      [ "For convenience and consistency reasons, "
      , "your company info (description and logo) "
      , "is filled in your profile "
      , "and shared between all your job listings."
      ]


done :: Lang -> Text
done lang =
  case lang of
    ES -> "Hecho"
    _  -> "Done"


--------------------------------------------------------------------------------
