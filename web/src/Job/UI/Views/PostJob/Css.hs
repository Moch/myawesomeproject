{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Views.PostJob.Css where

import Clay
-- import qualified Clay.Flexbox as Flex
import qualified Data.Text as T

import Base.Css.Utils

--------------------------------------------------------------------------------

postJobCss :: Css
postJobCss =
  associateCss
    [ (,) "post-job-body"                   bodyCss
    , (,) "post-job-info-background-colors" infoBackgroundColorCss
    , (,) "post-job-info-background"        infoBackgroundCss
    , (,) "post-job-info-section"           infoSectionCss
    , (,) "post-job-info"                   infoCss
    , (,) "post-job-info-column"            infoColumnCss
    , (,) "post-job-advice"                 adviceCss
    , (,) "post-job-advice-icon"            adviceIconCss
    , (,) "post-job-advice-text"            adviceTextCss
    , (,) "post-job-perk"                   perkCss
    , (,) "post-job-perk-title"             perkTitleCss
    , (,) "post-job-perk-cell"              perkCellCss
    , (,) "post-job-perk-icon"              perkIconCss
    , (,) "post-job-art-comment"            artCommentCss
    , (,) "post-job-offers-header"          offersHeaderCss
    , (,) "post-job-offers-background"      offersBackgroundCss
    , (,) "post-job-offers-section"         offersSectionCss
    , (,) "post-job-offers"                 offersCss
    , (,) "post-job-offer-wrapper"          offerWrapperCss
    , (,) "post-job-offer-price-number"     offerPriceCss
    , (,) "post-job-offer-price-frequency"  offerFrequencyCss
    , (,) "post-job-offer-button"           offerButtonCss
    ]


--------------------------------------------------------------------------------

bodyCss :: Css
bodyCss = do
  -- flex child
  flexGrow 1
  -- flex parent
  display flex
  flexDirection column
  justifyContent center
  alignItems center


--------------------------------------------------------------------------------


infoBackgroundColorCss :: Css
infoBackgroundColorCss = do
  -- background
  (-:)
    "background"
    -- "linear-gradient(60deg, dodgerblue, teal, rgb(0,220,100), steelblue, mediumseagreen, skyblue, deepskyblue, plum)"
    -- "linear-gradient(60deg, rgba(0,0,0,0), rgba(0,0,0,0.7)), linear-gradient(to right, mediumblue, midnightblue, deepskyblue)"
    "linear-gradient(120deg, rgba(0,0,0,0), rgba(0,0,0,0.6)), linear-gradient(to right, steelblue, royalblue, aqua)"
  -- flex child
  flexGrow 1
  -- flex parent
  display flex
  -- geometry
  width (pct 100)



infoBackgroundCss :: Css
infoBackgroundCss =
  let
    w :: Double
    w = 60
    h :: Double
    h = fromIntegral (floor (w * sqrt 3) :: Integer)
  in do
    -- background
    backgroundImage $ url "/static/svg/mosaicPajarita.svg"
    backgroundRepeat Clay.repeat
    backgroundSize $ (px w) `by` (px h)
    -- flex child
    flexGrow 1
    -- flex parent
    display        flex
    flexDirection  column
    alignItems     center
    justifyContent center
    -- geometry
    paddingTop (Clay.rem 7)
    width      (pct 100)


infoSectionCss :: Css
infoSectionCss = do
  -- border
  borderWithRadius solid (px 5) indigo (px 15)
  (-:) "box-shadow" "0px 0px 13px 2px rgba(0,0,0,0.4) inset"
  -- color
  backgroundColor white
  -- geometry
  marginBottom (Clay.rem 5)
  -- H1
  h1 ? do
    -- font
    fontColor purple
    setFont (weight 650) (Clay.rem 2) [smooth]
    fontVariant smallCaps
    -- geometry
    margin `sym` (Clay.rem 2)
    -- text
    textAlign center
  -- H2
  h2 ? do
    -- font
    fontColor indigo
    setFont bold (Clay.rem 1.5) [smooth]
    fontVariant smallCaps
    -- geometry
    margin `sym` (Clay.rem 1)
    -- text
    textAlign center


infoCss :: Css
infoCss = do
  -- flex parent
  display flex
  justifyContent center
  alignItems     center
  flexDirection  column
  landscape $ do
    flexDirection row
    alignItems    flexStart


infoColumnCss :: Css
infoColumnCss = do
  -- geometry
  padding `sym` (px 25)
  width (px 270)
  portrait $ width (px 350)
  laptop   $ width (px 450)


adviceCss :: Css
adviceCss = do
  -- flex parent
  display    flex
  alignItems center
  -- font
  setFont normal (px 16) [ubuntu]
  -- geometry
  height (Clay.rem 7)


adviceIconCss :: Css
adviceIconCss = do
  -- background
  backgroundImage $ url "/static/svg/dodecagon.svg"
  -- flex child
  flexShrink 0
  -- flex parent
  display flex
  alignItems     center
  justifyContent center
  -- font
  fontColor mediumvioletred
  setFont bold (px 30) [superCurvy]
  -- geometry
  let x = px 55
  width  x
  height x
  marginRight (px 20)


adviceTextCss :: Css
adviceTextCss = do
  -- color
  fontColor $ rgb 40 0 90
  -- flex parent
  display flex
  alignItems center


perkCss :: Css
perkCss = do
  -- flex parent
  display flex
  justifyContent center
  alignItems     center
  -- geometry
  width (pct 100)


perkTitleCss :: Css
perkTitleCss = do
  -- font
  setFont normal (px 14) [montserrat]
  fontColor indigo
  -- flex child
  flexGrow 1
  -- flex parent
  display flex
  justifyContent flexEnd
  -- geometry
  paddingRight (px 20)
  -- text
  textAlign end


perkCellCss :: Css
perkCellCss = do
  -- flex child
  flexShrink 0
  -- geometry
  padding `sym` (px 13)
  let x = Clay.px 65
  height x
  width  x


perkIconCss :: Css
perkIconCss = do
  -- background
  backgroundImage $ url "/static/svg/octagon.svg"
  -- SVG
  svg ? do
    (-:) "stroke" "indigo"
    height (pct 100)
    width  (pct 100)



artCommentCss :: Css
artCommentCss = do
  -- background
  backgroundColor $ rgba 255 255 255 0.95
  -- border
  (-:) "box-shadow" "0px 3px 5px 2px rgba(0,0,0,0.5)"
  -- font
  fontColor  (rgb 30 30 30)
  fontSize   (px 23)
  fontFamily [superCurvy] [sansSerif]
  -- geometry
  minWidth (px 280)
  sym2  padding (px 30) (px 20)
  marginBottom (Clay.rem 12)
  -- ANCHOR
  Clay.a ? do
    outlineStyle   none
    fontColor      inherit
    textDecoration underline
    hover & fontColor darkblue
  -- PARAGRAPH
  p ? do
    display block
    paddingTop (px 25)
    fontFamily [arabDances, superCurvy] [sansSerif]


--------------------------------------------------------------------------------


offersHeaderCss :: Css
offersHeaderCss = do
  -- background
  (-:)
    "background"
    "linear-gradient(to bottom right, gainsboro, silver 30%, gainsboro 40%, white, silver, gainsboro, silver)"
  -- border
  borderWithRadius solid (px 5) indigo (px 100)
  -- display
  zIndex 1
  (-:) "cursor" "default"
  -- flex parent
  display flex
  alignItems center
  -- font
  fontColor   darkcyan
  setFont     bold (px 30) [superCurvy]
  fontVariant smallCaps
  -- geometry
  height        (px 100)
  marginTop     (px (-51))
  marginBottom  (px (-49))
  sym2 padding  (px 20) (px 75)
  -- text
  textAlign center
  (-:) "text-shadow" "1px 0 0 pink, -1px 0 0 pink, 0 1px 0 pink, 0 -1px 0 pink"


offersBackgroundCss :: Css
offersBackgroundCss = do
  let
    transcribe colorList =
      T.intercalate "," $ colorList ++ Prelude.reverse colorList
    beehiveGradient = ""
      <> "linear-gradient(60deg, "
      <> transcribe colors1
      <> "), "
      <> "linear-gradient(-60deg, "
      <> transcribe colors2
      <> ")"
    colors1 =
      [ "rgba(178, 34, 34, 0.7)"      -- "firebrick"
      , "rgba(255,140,  0, 0.8)"      -- "darkorange"
      , "rgba(255,165,  0, 0.7)"      -- "orange"
      , "rgba(255,215,  0, 0.6)"      -- "gold"
      , "rgba(255,255,  0, 0.5)"      -- "yellow"
      ]
    colors2 =
      [ "rgba(255,140,  0, 0.5)"      -- "darkorange"
      , "rgba(255,255,100, 0.6)"      -- "yellow with modifications"
      , "rgba(255,215,  0, 0.7)"      -- "gold"
      , "rgba(255,140,  0, 0.8)"      -- "darkorange"
      ]
  -- background
  (-:) "background" beehiveGradient
  -- flex child
  flexGrow 1
  -- flex parent
  display flex
  -- geometry
  width (pct 100)


offersSectionCss :: Css
offersSectionCss =
  let
    w :: Double
    w = 80
    h :: Double
    h = fromIntegral (floor (w * sqrt 3) :: Integer)
  in do
    -- background
    backgroundImage $ url "/static/svg/mosaicBee.svg"
    backgroundRepeat Clay.repeat
    backgroundSize $ (px w) `by` (px h)
    -- border
    borderTop solid (px 5) indigo
    -- flex child
    flexGrow 1
    -- flex parent
    display flex
    justifyContent center
    alignItems     center
    -- geometry
    width (pct 100)


offersCss :: Css
offersCss = do
  -- flex parent
  display flex
  justifyContent center
  alignItems     center
  -- flexWrap Flex.wrap
  flexDirection column
  laptop $ flexDirection row
  -- geometry
  marginTop    (Clay.rem 7)
  marginBottom (Clay.rem 5)



offerWrapperCss :: Css
offerWrapperCss = do
  ul ? offerListCss
  h4 ? do
    setFont (bold) (Clay.rem 1.5) [montserratAlt]
    fontVariant smallCaps
  -- border
  borderStyle solid
  borderWidth (px 10)
  -- color
  backgroundColor white
  -- flex parent
  display flex
  flexDirection column
  justifyContent spaceBetween
  alignItems center
  -- geometry
  paddingTop    (Clay.rem 3)
  paddingBottom (Clay.rem 3)
  let w = 250
  width  (px w)
  height (px (1.618 * w))
  marginBottom (px 50)
  let m1 = px 30
  marginRight m1
  marginLeft  m1
  portrait $ do
    let w1 = 300
    width (px w1)
    height (px (1.618 * w1))
  laptop $ do
    let m2 = px 15
    marginRight m2
    marginLeft  m2
  desktop $ do
    let m3 = px 80
    marginRight m3
    marginLeft  m3


offerListCss :: Css
offerListCss = do
  -- flex parent
  display flex
  flexDirection column
  justifyContent spaceBetween
  -- font
  fontColor (rgb 95 95 95)
  setFont normal (Clay.rem 0.9) [smooth]
  -- geometry
  height (pct 25)
  width (pct 70)
  -- list
  listStyleType none
  -- SVG
  svg ? do
    let x = px 13
    width x
    height x
    marginRight (px 5)


offerPriceCss :: Css
offerPriceCss = do
  -- font
  fontColor (rgb 80 80 80)
  setFont bolder (Clay.rem 2.1) [montserrat]


offerFrequencyCss :: Css
offerFrequencyCss = do
  -- flex parent
  display inlineFlex
  alignItems flexEnd
  -- font
  fontColor (rgb 120 120 120)
  setFont normal (Clay.rem 0.9) [montserratAlt]
  -- geometry
  height (pct 100)
  paddingBottom (px 5)


offerButtonCss :: Css
offerButtonCss = do
  -- border
  borderStyle solid
  borderWidth (px 2)
  -- flex parent
  display flex
  justifyContent center
  alignItems center
  -- font
  setFont normal (Clay.rem 0.9) [montserrat]
  -- geometry
  sym2 padding (Clay.rem 0.8) (Clay.rem 1.5)
  -- text
  textDecoration none
  -- transition
  transition "all" (ms 300) linear (ms 0)
  -- HOVER
  hover & do
    (-:) "box-shadow" "0px 5px 4px 2px rgba(0,0,0,0.4)"
    marginTop    (px (-5))
    marginBottom (px 5)



--------------------------------------------------------------------------------
