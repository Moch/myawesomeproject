{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Views.PostJob.Html where

import           Text.Blaze.Html5 (Html, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Base.Html.Head
import Base.Html.Page
import Base.Lang
import Base.UI.Layout.Template.Html
import Job.API.Routes
import Job.Logic.All
import Job.UI.Elems.JobType.Css
import Job.UI.Views.PostJob.Svg
import qualified Job.UI.Views.PostJob.Translate as Translate

--------------------------------------------------------------------------------

postJobPageView :: PageParams -> Html
postJobPageView params =
  H.toHtml
    Page
      { pageParams = params
      , htmlHead   = postJobHead
      , htmlBody   = paylineTemplate postJobBody
      }


postJobHead :: Head
postJobHead =
  stdHead "Funjobs" "Funjobs" "Funjobs" ""


postJobBody :: PageParams -> Html
postJobBody p =
  H.div ! A.class_ "post-job-body" $ do
    postJobInfo (ppLang p)
    ourPackages (ppLang p)


--------------------------------------------------------------------------------


postJobInfo :: Lang -> Html
postJobInfo lang =
  H.div
    ! A.class_ "post-job-info-background-colors"
    $ H.div
      ! A.class_ "post-job-info-background"
      $ do
        H.div
          ! A.class_ "post-job-info-section"
          $ do
            H.h1 $ H.toHtml $ Translate.needSoftwareDevs lang
            H.h2 $ H.toHtml $ Translate.hireWithUs lang
            H.div
              ! A.class_ "post-job-info"
              $ do
                postJobLeft   lang
                postJobRight  lang
        artComment lang


postJobLeft :: Lang -> Html
postJobLeft lang =
  let
    adviceTemplate n advice =
      H.div
        ! A.class_ "post-job-advice"
        $ do
          H.div n
            ! A.class_ "post-job-advice-icon"
          H.div
            ! A.class_ "post-job-advice-text"
            $ H.toHtml $ advice lang
  in
    H.div
      ! A.class_ "post-job-info-column"
      $ do
        adviceTemplate "1" Translate.notHeadhunting
        adviceTemplate "2" Translate.monthlyPlans
        adviceTemplate "3" Translate.chooseAccordingToExperience



postJobRight :: Lang -> Html
postJobRight lang =
  let
    perkTemplate title e =
      H.div
        ! A.class_ "post-job-perk"
        $ do
          H.div (H.toHtml $ title lang)
            ! A.class_ "post-job-perk-title"
          case e of
            Left  icon -> do
              H.div icon
                ! A.class_ "post-job-perk-cell post-job-perk-icon"
              H.div ""
                ! A.class_ "post-job-perk-cell"
            Right icon -> do
              H.div ""
                ! A.class_ "post-job-perk-cell"
              H.div icon
                ! A.class_ "post-job-perk-cell post-job-perk-icon"
  in
    H.div
      ! A.class_ "post-job-info-column"
      $ do
          perkTemplate Translate.unlimitedHiring $ Left  unlimitedHiringIcon
          perkTemplate Translate.analytics       $ Right analyticsIcon
          perkTemplate Translate.emailAlerts     $ Left  emailAlertsIcon
          perkTemplate Translate.editAnytime     $ Right editAnytimeIcon
          perkTemplate Translate.qualified       $ Left  qualifiedCandidatesIcon
          perkTemplate Translate.socialMedia     $ Right socialMediaIcon



artComment :: Lang -> Html
artComment lang =
  H.div
    ! A.class_ "post-job-art-comment"
    $ do
      H.em     $ H.toHtml $ Translate.backgroundArt lang
      H.br
      H.strong $ do
        H.toHtml $ Translate.mosaic lang
        H.a
          ! A.href "https://es.wikipedia.org/wiki/Arte_nazar%C3%AD"
          ! A.target "blank"
          $ H.toHtml $ Translate.pajaritaNazari lang
      H.p $ H.toHtml $ Translate.granadasAlhambra lang
      H.p $ do
         H.toHtml $ Translate.nazariKingdom    lang
         H.br
         "1238-1492 A.D."


--------------------------------------------------------------------------------


ourPackages :: Lang -> Html
ourPackages lang = do
  H.h1
    ! A.class_ "post-job-offers-header"
    $ H.toHtml $ Translate.ourPlans lang
  H.div
    ! A.class_ "post-job-offers-background"
    $ H.div
        ! A.class_ "post-job-offers-section"
        $ do
          H.div
            ! A.class_ "resp-w post-job-offers"
            $ do
              packageTemplate lang Novice
              packageTemplate lang Expert
              packageTemplate lang Master



packageTemplate :: Lang -> JobType -> Html
packageTemplate lang jobLevel =
  let
    x = jobTypeClass jobLevel
    y = " " <> jobTypeButtonClass jobLevel
    flexCenter = H.div ! A.class_ "flex-center"
    enlist point =
      H.li $ do
        lambdaIcon ! A.class_ (H.toValue x)
        point
    levelReq =
      H.span
        ! A.class_ (H.toValue x)
        $ H.toHtml $ case jobLevel of
          Novice -> Translate.low  lang
          Expert -> Translate.mid  lang
          Master -> Translate.high lang
    knowledge =
      H.toHtml $ case jobLevel of
        Novice -> Translate.needsTraining  lang
        Expert -> Translate.canTeach       lang
        Master -> Translate.knowsTheTricks lang
  in
    H.div
      ! A.class_ (H.toValue $ "post-job-offer-wrapper " <> x)
      $ do
        flexCenter
          $ H.h4 $ H.toHtml (translate lang jobLevel)
        H.ul
          $ do
            enlist $ (H.toHtml $ Translate.levelRequired lang) >> levelReq
            enlist knowledge
        flexCenter
          $ do
            H.span (H.toHtml $ (\p -> show p ++ "€") $ packagePrice jobLevel)
              ! A.class_ "post-job-offer-price-number"
            H.span (H.toHtml $ Translate.perMonth lang)
              ! A.class_ "post-job-offer-price-frequency"
        flexCenter
          $ H.a (H.toHtml $ Translate.choose lang)
            ! A.href (H.toValue $ createJobPostRouteByType jobLevel)
            ! A.class_ (H.toValue $ "post-job-offer-button " <> x <> y)


--------------------------------------------------------------------------------
