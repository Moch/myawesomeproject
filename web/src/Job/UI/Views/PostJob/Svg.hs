{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Views.PostJob.Svg where

import Text.Blaze.Svg11 as S

import Base.Svg.Business
import Base.Svg.Geometry
import Base.Svg.Lambdas
import Base.Svg.Utils

--------------------------------------------------------------------------------

lambdaIcon :: Svg
lambdaIcon = stdSvg curvyLambda

--------------------------------------------------------------------------------

unlimitedHiringIcon :: Svg
unlimitedHiringIcon = stdSvg infinity

analyticsIcon :: Svg
analyticsIcon = stdSvg analytics

emailAlertsIcon :: Svg
emailAlertsIcon = stdSvg envelope

editAnytimeIcon :: Svg
editAnytimeIcon = stdSvg documentWithPencil

qualifiedCandidatesIcon :: Svg
qualifiedCandidatesIcon =
  stdSvg $ horizontalMirrorSymmetry $ g bullseye

socialMediaIcon :: Svg
socialMediaIcon =
  stdSvg circlesWithConnections

--------------------------------------------------------------------------------
