{-# LANGUAGE OverloadedStrings #-}


module Job.UI.Views.PostJob.Translate where

import Data.Text

import Base.Lang

--------------------------------------------------------------------------------


needSoftwareDevs :: Lang -> Text
needSoftwareDevs lang =
  case lang of
    ES -> "¿Buscas Ingenieros de Software?"
    _  -> "Need Software Engineers?"


hireWithUs :: Lang -> Text
hireWithUs lang =
  case lang of
    ES -> "Contrata con nosotros"
    _  -> "Hire with us"


notHeadhunting :: Lang -> Text
notHeadhunting lang =
  case lang of
    ES -> "Esto no es un servicio de reclutamiento, es un tablón de anuncios."
    _  -> "This is not a headhunting service, it's a job board."


monthlyPlans :: Lang -> Text
monthlyPlans lang =
  case lang of
    ES -> "Planes mensuales"
    _  -> "Montly plans"


chooseAccordingToExperience :: Lang -> Text
chooseAccordingToExperience lang =
  case lang of
    ES -> "Elige en función del nivel de experiencia que busques para el puesto"
    _  -> "Choose according to the level of experience you seek for the job"



unlimitedHiring :: Lang -> Text
unlimitedHiring lang =
  case lang of
    ES -> "Contratos Ilimitados"
    _  -> "Unlimited Hiring"


analytics :: Lang -> Text
analytics lang =
  case lang of
    ES -> "Estadísticas"
    _  -> "Analytics"


emailAlerts :: Lang -> Text
emailAlerts lang =
  case lang of
    ES -> "Alertas por Email"
    _  -> "Email Alerts"


editAnytime :: Lang -> Text
editAnytime lang =
  case lang of
    ES -> "Edita o Congela"
    _  -> "Edit or Freeze"


qualified :: Lang -> Text
qualified lang =
  case lang of
    ES -> "Candidatos Cualificados"
    _  -> "Qualified Candidates"


socialMedia :: Lang -> Text
socialMedia lang =
  case lang of
    ES -> "Redes Sociales"
    _  -> "Social Media"


backgroundArt :: Lang -> Text
backgroundArt lang =
  case lang of
    ES -> "Arte del fondo:"
    _  -> "Background art:"


mosaic :: Lang -> Text
mosaic lang =
  case lang of
    ES -> "Réplica computerizada de mosaico con tesela básica "
    _  -> "Computerized replica of mosaic with base tile "


pajaritaNazari :: Lang -> Text
pajaritaNazari lang =
  case lang of
    ES -> "pajarita nazarí"
    _  -> "nasrid birdie"


granadasAlhambra :: Lang -> Text
granadasAlhambra lang =
  case lang of
    ES -> "Alhambra de Granada, Andalucia, España"
    _  -> "Granada's Alhambra, Andalucia, Spain"


nazariKingdom :: Lang -> Text
nazariKingdom lang =
  case lang of
    ES -> "Reino Nazari de Granada"
    _  -> "Nasrid Kingdom of Granada"


--------------------------------------------------------------------------------


ourPlans :: Lang -> Text
ourPlans lang =
  case lang of
    ES -> "Las Ofertas"
    _  -> "Our Plans"


perMonth :: Lang -> Text
perMonth lang =
  case lang of
    ES -> "/mes"
    _  -> "/month"


choose :: Lang -> Text
choose lang =
  case lang of
    ES -> "Elegir"
    _  -> "Choose"


levelRequired :: Lang -> Text
levelRequired lang =
  case lang of
    ES -> "Nivel de experiencia requerido: "
    _  -> "Targeted level of experience: "


low :: Lang -> Text
low lang =
  case lang of
    ES -> "bajo"
    _  -> "low"


mid :: Lang -> Text
mid lang =
  case lang of
    ES -> "medio"
    _  -> "average"


high :: Lang -> Text
high lang =
  case lang of
    ES -> "alto"
    _  -> "high"


needsTraining :: Lang -> Text
needsTraining lang =
  case lang of
    ES -> "Necesita entrenamiento, pero quiere aprender"
    _  -> "Needs training, but wants to learn"


canTeach :: Lang -> Text
canTeach lang =
  case lang of
    ES -> "Puede instruir a los talentos más jovenes"
    _  -> "Can train the younger talents"


knowsTheTricks :: Lang -> Text
knowsTheTricks lang =
  case lang of
    ES -> "Conoce los trucos del oficio"
    _  -> "Knows the tricks of the trade"


--------------------------------------------------------------------------------
