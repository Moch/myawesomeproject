-- {-# LANGUAGE EmptyDataDecls             #-}
-- {-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}




module Job.Logic.Models where

import           Data.Text (Text)
-- import           Database.Persist
-- import           Database.Persist.Postgresql
import           Database.Persist.TH

import           Job.Logic.Base
import           User.Logic.All

--------------------------------------------------------------------------------

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
  Job
    userId      UserId
    type        JobType
    title       Text
    location    Text
    remote      Bool
    keywords    Text
    salary      Text
    employment  Text
    howToApply  Text
    description Text
    deriving Show
  |]


--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
