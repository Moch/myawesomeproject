{-# LANGUAGE OverloadedStrings #-}


module Job.Logic.Translate where

import Data.Text

import Base.Lang


--------------------------------------------------------------------------------


novice :: Lang -> Text
novice lang =
  case lang of
    ES -> "Novato"
    EN -> "Novice"
    _  -> "Novulo"


expert :: Lang -> Text
expert lang =
  case lang of
    ES -> "Experto"
    EN -> "Expert"
    _  -> "Sperto"


master :: Lang -> Text
master lang =
  case lang of
    ES -> "Maestro"
    EN -> "Master"
    _  -> "Majstro"


--------------------------------------------------------------------------------
