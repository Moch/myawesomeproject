{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}


module Job.Logic.Base where

import           Data.Text
import           Database.Persist.TH

import           Base.Lang
import           Job.Logic.Translate as Translate

--------------------------------------------------------------------------------

data JobType =
    Novice
  | Expert
  | Master
  deriving (Eq, Show, Read)
derivePersistField "JobType"

instance Translate JobType where
  translate = translateJobType


translateJobType :: Lang -> JobType -> Text
translateJobType lang lvl =
  case lvl of
    Novice -> Translate.novice lang
    Expert -> Translate.expert lang
    Master -> Translate.master lang


packagePrice :: JobType -> Int
packagePrice lvl = case lvl of
  Novice -> 350
  Expert -> 500
  Master -> 900


--------------------------------------------------------------------------------
