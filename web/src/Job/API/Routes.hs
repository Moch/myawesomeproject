{-# LANGUAGE OverloadedStrings #-}


module Job.API.Routes where

import Data.Text

import Job.Logic.All

--------------------------------------------------------------------------------


postJobRoute :: Text
postJobRoute = "/post-job"


createJobPostRoute :: Text
createJobPostRoute = "/create-job-post"


purchaseJobPostRoute :: Text
purchaseJobPostRoute = "/purchase-job-post"

--------------------------------------------------------------------------------

createJobPostRouteByType :: JobType -> Text
createJobPostRouteByType p =
  createJobPostRoute <> "?package=" <> (pack $ show p)

--------------------------------------------------------------------------------
