{-# LANGUAGE OverloadedStrings #-}


module Job.API.Handler where

import Control.Monad  (msum)
import Data.Text      as Text
import Happstack.Server
import Text.Read (readMaybe)

import Base.API
import Base.Html.Page
import Job.API.Routes
import Job.Logic.All
import Job.UI.Views.CreateJobPost.Html
import Job.UI.Views.PostJob.Html

--------------------------------------------------------------------------------

jobHandler :: PageParams -> ServerPart Response
jobHandler p =
  msum
    [ (^-^) createJobPostRoute (createJobPostHandler p)
    , (^-^) postJobRoute       (postJobHandler p)
    ]

--------------------------------------------------------------------------------

postJobHandler :: PageParams -> ServerPart Response
postJobHandler p =
  let params = p {ppRoute = postJobRoute}
  in do
    method GET
    ok $ toResponse (postJobPageView params)


createJobPostHandler :: PageParams -> ServerPart Response
createJobPostHandler p =
  do
    method GET
    mPackage <- safeLookText "package"
    case mPackage of
      Nothing -> seeOther postJobRoute $ toResponse ()
      Just  x -> do
        let package = maybe Novice id $ readMaybe $ unpack x
        let params = p {ppRoute = createJobPostRouteByType package}
        ok $ toResponse $ createJobPostPageView package params


--------------------------------------------------------------------------------
