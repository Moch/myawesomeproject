#!/bin/sh


echo ""
echo "Changes Detected!"

echo ""
echo "Killing Server"
pkill -f Fun-exe
echo "Server Killed ☠"

echo ""
echo "Rebuilding Server"
stack build --pedantic --ghc-options -threaded
echo "Server Built"

echo ""
echo "Restarting Server"
stack exec Fun-exe +RTS -N2 &
