#!/bin/sh
set -e

stack build --pedantic --ghc-options -threaded

stack exec Fun-exe +RTS -N2 &


find ./src ./app ./test | grep .hs | entr ./_restart.sh
