{-# LANGUAGE OverloadedStrings #-}


module Main where

import           Happstack.Server
import           Control.Monad          (msum)
import           Control.Monad.IO.Class (liftIO)
import           Control.Monad.Logger   (runStderrLoggingT)
import           Data.Pool              (Pool)
import           Database.Persist.Postgresql
-- import System.IO

import Base.API
import Base.Html.Page
import Base.Persist
import Job.API.Handler       (jobHandler)
import Root.API.Cookies      (getCookieLang)
import Root.API.Handler      (rootHandler)
import Static.API.Handler    (staticHandler)
import Static.Compiler.All   (makeStaticFiles)
import User.API.Cookies
import User.API.Handler      (userHandler)
import User.Logic.All        (migrateAll)


--------------------------------------------------------------------------------


dbConnStr :: ConnectionString
dbConnStr =
  "host=db port=5432 dbname=postgres user=postgres password=postgres"

main :: IO ()
main = do
  makeStaticFiles
  runStderrLoggingT $ withPostgresqlPool dbConnStr 10 $ \pool -> liftIO $ do
    _ <- runDB pool (runMigration migrateAll)
    liftIO $ serve (funJobsApp pool)

--------------------------------------------------------------------------------

funJobsApp :: Pool SqlBackend -> ServerPart Response
funJobsApp pool = do
  lang   <- getCookieLang
  u      <- getCookieUser
  errMsg <- getCookieErrorMsg
  okMsg  <- getCookieOkMsg
  let params = PageParams
        { ppRoute    = "This is a placeholder"
        , ppLang     = lang
        , ppUser     = u
        , ppDatabase = pool
        , ppErrorMsg = errMsg
        , ppOkMsg    = okMsg
        }
  funJobsHandler params


funJobsHandler :: PageParams -> ServerPart Response
funJobsHandler p =
  msum
    [ staticHandler
    , jobHandler  p
    , userHandler p
    , rootHandler p
    ]

--------------------------------------------------------------------------------
